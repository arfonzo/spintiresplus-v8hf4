/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <Common/Base/hkBase.h> //for hkArray


#include "typedefs.h"
#include "Vector.h"

struct Shape;

struct CollisionGroup //size 0x10
{
	uint ID;			//0x64 for truck wheels
	byte Unknown[0x4];
	hkUlong UserData;	//points to a WheelParams when ID==0x64

};

class RigidBody //probably a hkpRigidBody or hkpRigidBodyCinfo
{
public:
	byte Unknown0[0x10];
	Shape* m_shape; //or hkpShape* ?
	byte Unknown1[0x68];
	//offset 7c
	hkArray<CollisionGroup> CollisionGroups;	//array
	byte Unknown2[0x4];

	//offset 8C
	float m_friction;
	byte Unknown05[0x50];
	//offset 0xE0	//here starts an hkpMotion
	void* vftable2;	//array of function pointers

	byte Unknown10[0xC];

	//offset 0xF0
	Vector4 XVector;	//the coordinate of the model's own x vector (what you saw in 3dsmax
	Vector4 YVector;	//when you modelled the wheel).
	Vector4 ZVector;	//4th component is always 0

	Vector4 Position2;	//those 3 vectors seem identical	
	Vector4 Position1;	//in doubt, use the 3rd one
	
	//offset 0x140
	Vector4 Position;	//4th component is always 60 to 61. 
	byte Unknown20[0x50];

	//offset 1a0
	Vector3 invInertia; //in 1/kg.m^2

	//offset 0x1ac
	float invMass;	
	//offset 1b0
	Vector4 m_linearVelocity;
	//offset 1c0
	Vector4 m_angularVelocity;
	//offset 1d0
	Vector4 SomePosition1; //very close to BarycenterPosition, last known position?
	Vector3 SomePosition2;

public:
	void Activate();
	//impulse= force or torque * time
	void ApplyAngularImpulse(const Vector4& Torque);
	void SetAngularVelocity(const Vector4& newVel);
	void ApplyPointImpulse(const Vector4& Force,const Vector4& Point);
	const Vector4& getLinearVelocity() const;
	const Vector4& getAngularVelocity() const;

	float GetInvInertiaX() const {return invInertia.X*2;} //'coz havok or Pavel stores half inv inertias
	float GetInvInertiaY() const {return invInertia.Y*2;} //at least on wheels
	float GetInvInertiaZ() const {return invInertia.Z*2;} //

	//returns true if this object collides with object in
	//collision group GroupID.
	bool CollidesWithGroup(uint GroupID) const;

	//Retrives the userdata associated with the specified collision group
	//and stores it at OutPut if found.
	void GetCollisionUserData(uint GroupID,hkUlong* OutPut) const;

};