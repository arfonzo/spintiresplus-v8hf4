/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPDialog.h"
#include "STPMenuManager.h"
#include "AntTweakBar.h"

STPDialog::STPDialog(void)
:mMenu(NULL)
{
	STPMenuManager::Get()->RegisterDialog(this);
}

STPDialog::~STPDialog(void)
{
	STPMenuManager::Get()->UnregisterDialog(this);
	if (mMenu)
	{
		TwDeleteBar(mMenu);
		mMenu=NULL;
	}
}

void STPDialog::OnDeviceLost()
{
	//save position and size
	TwGetParam(mMenu,NULL,"position",TW_PARAM_INT32, 2, &mPos);
	TwGetParam(mMenu,NULL,"size",TW_PARAM_INT32, 2, &mSize);
	TwGetParam(mMenu,NULL,"visible",TW_PARAM_INT32, 1, &mVisible);

	if (mMenu)
	{
		TwDeleteBar(mMenu);
		mMenu=NULL;
	}
}

void STPDialog::Restore()
{
	SetPosition(mPos[0],mPos[1]);
	SetSize(mSize[0],mSize[1]);
	SetVisible(mVisible);

	//standard Spintires configuration
	int Color[4] = { 255, 175, 175, 175 };//ARGB
	TwSetParam(mMenu,NULL,"alpha",TW_PARAM_INT32, 1, Color);
	TwSetParam(mMenu,NULL,"color",TW_PARAM_INT32, 3, Color+1);
	const char TextColor[]="light";
	TwSetParam(mMenu,NULL,"text",TW_PARAM_CSTRING, 1, TextColor);
}

void STPDialog::SetVisible(bool Visible)
{
	mVisible=Visible;
	if (mMenu)
	{
		//converts the bool to an int, because anttweakbar wants an int
		int VisibleInt= mVisible ? 1 : 0;
		TwSetParam(mMenu,NULL,"visible",TW_PARAM_INT32, 1, &VisibleInt);

	}
}

void STPDialog::DoModal()
{
	STPMenuManager::Get()->RenderingLoop();
}

void STPDialog::Render(IDirect3DDevice9 *Device)
{
}

void STPDialog::SetPosition(int TopLeftX,int TopLeftY)
{
	mPos[0]=TopLeftX;
	mPos[1]=TopLeftY;
	if (mMenu)
	{
		TwSetParam(mMenu,NULL,"position",TW_PARAM_INT32, 2, mPos);
	}
}

void STPDialog::SetSize(int Width, int Height)
{
	mSize[0]=Width;
	mSize[1]=Height;
	if (mMenu)
	{
		TwSetParam(mMenu,NULL,"size",TW_PARAM_INT32, 2, mSize);
	}
}