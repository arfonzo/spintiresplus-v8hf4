/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPMapSelectionDialog.h"
#include "PHYSFS_minimal.h"
#include "Settings.h"
#include "AntTweakBar.h"
#include "STPStartupAddonManager.h"
#include "STPMenuManager.h"
#include "STPStartupAddonManager.h"
#include "Common.h"

//defined in hooker.cpp
extern void** (* GetMapMenu_Real)(void);
extern void (__stdcall* SaveMapSelection)(void*, const char*);

int __cdecl SortMapNames( const void *Map1, const void *Map2 )
{
   return _stricmp( * ( char** ) Map1, * ( char** ) Map2 );
}

void* GetMapManager()
{
	void* ReturnValue=*GetMapMenu_Real();
	if (ReturnValue)
	{
		ReturnValue=reinterpret_cast<void*>(reinterpret_cast<Address>(ReturnValue)-0x28);
	}
	return ReturnValue;
}

/////////////////////////////////
// Callbacks
/////////////////////////////////

void TW_CALL STPMapSelectionDialog_OnClick(void *clientData)
{
	STPListDialogWithPreview::EntryData* Param=static_cast<STPListDialogWithPreview::EntryData*>(clientData);
	if (Param)
	{
		STPMapSelectionDialog* Dialog=static_cast<STPMapSelectionDialog*>(Param->mDialog);
		Dialog->QuitCallback(Param);
	}
}
/////////////////////////////////
// STPMapSelectionDialog
/////////////////////////////////
STPMapSelectionDialog::STPMapSelectionDialog(void)
:mPreviewedMapHasSave(false)
{
	STPMenuManager* Manager=STPMenuManager::Get();
	uint WindowWidth=Manager->GetScreenWidth();
	uint WindowHeight=Manager->GetScreenHeight();
	const int PreviewSize=__min(WindowHeight,WindowWidth/2);
	const int FloppyIconSize=PreviewSize/15;

	SetPosition(0,0);
	SetSize(WindowWidth/4,WindowHeight);
	

	mPreviewWindow.SetPosSize(
		WindowWidth-PreviewSize,
		(WindowHeight-PreviewSize)/2,
		PreviewSize,
		PreviewSize,
		false);

	mFloppyIcon.SetDevice(Manager->GetD3DDevice());
	mFloppyIcon.SetPosSize(
		WindowWidth-FloppyIconSize,
		(WindowHeight+PreviewSize)/2-FloppyIconSize,
		FloppyIconSize,
		FloppyIconSize,
		false);
	mFloppyIcon.SetTextureFromFile("gui_diskette__d_a.dds");


	//populate the list of maps
	char **Files = PHYSFS_enumerateFiles("levels");
	uint MapCount=0;
	//count the number of files in the levels directory
	//to preallocate the mEntryData vector;
	for (char **i = Files; *i != NULL; ++i)
	{
		if (IsMapFile(*i) && !IsExcludedMap(*i) )
		{
			++MapCount;
		}
	}
	mEntryData.resize(MapCount);

	//alphabetically sort map names
	qsort(Files,MapCount,sizeof(*Files),SortMapNames);

	//now that has the good size, add maps in it
	{
		uint LastIndex=0;
		for (char **i = Files; *i != NULL; ++i)
		{
			 if (IsMapFile(*i) && !IsExcludedMap(*i))
			 {
				 STPListDialogWithPreview::EntryData& Item=mEntryData[LastIndex];
				 ++LastIndex;
				 Item.mDialog=this;

				 //mReturnValue=filename with extension trimmed
				 Item.mReturnValue=*i;
				 Item.mReturnValue.resize(Item.mReturnValue.rfind('.'));

				 //display name is the filename withut extension or the "level_" prefix
				 Item.mDisplayName=Item.mReturnValue.substr(strlen(MAPFILE_PREFIX));

				 Item.mPreviewFile="levels/"+Item.mReturnValue+".dds";
			 }
		}
	}
	PHYSFS_freeList(Files);

	mSortingCategories=&GlobalSettings::MapCategories;
}


STPMapSelectionDialog::~STPMapSelectionDialog(void)
{
}

void STPMapSelectionDialog::Restore()
{
	STPListDialogWithPreview::Restore(); //restore from parent class
	
	TwSetParam(mMenu,NULL,"label",TW_PARAM_CSTRING,1,"Select Map");
	mFloppyIcon.OnDeviceReset();

}

void STPMapSelectionDialog::OnDeviceLost()
{
	mFloppyIcon.OnDeviceLost();
	STPListDialogWithPreview::OnDeviceLost();
}

void STPMapSelectionDialog::Render(IDirect3DDevice9* Device)
{
	STPListDialogWithPreview::Render(Device);
	if (mPreviewedMapHasSave)
	{
		//the floppy has transparent areas
		Device->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		Device->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
		Device->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
		mFloppyIcon.Render();
	}
}

void STPMapSelectionDialog::QuitCallback(const STPListDialogWithPreview::EntryData* Referer)
{
	if (Referer && !Referer->mReturnValue.empty())
	{
		SaveMapSelection(GetMapManager(),Referer->mReturnValue.c_str());
	}

	//remove all known startup addon since whe changed the map->reset truck selection
	STPStartupAddonManager::Get()->Clear();

	//stop overlay
	STPMenuManager::Get()->StopRenderingLoop();
}

bool STPMapSelectionDialog::IsMapFile(const char *Filename) const
{
	//return true the last letters of Filename are MAPFILE_EXTENSION
	uint Length=strlen(Filename);
	if ( !stricmp(MAPFILE_EXTENSION,Filename+Length-strlen(MAPFILE_EXTENSION)) )
	{	//if the extension is .stg

		if ( !strnicmp(MAPFILE_PREFIX,Filename,strlen(MAPFILE_EXTENSION)) )
		{	//and the map name starts with "level_"
			return true;
		}
	}

	return false;
}

bool STPMapSelectionDialog::IsExcludedMap(const char *Filename) const
{
	if (!stricmp(Filename,MAP_PROVING_FILE) ||
		!stricmp(Filename,MAP_MENU_FILE))
	{
		return true;
	}

	return false;
}

void STPMapSelectionDialog::AddEntryToMenu(const char* VarName,const STPListDialogWithPreview::EntryData& Data,std::string& Label)
{
	TwAddButton(
		mMenu,
		VarName,
		STPMapSelectionDialog_OnClick,
		const_cast<STPListDialogWithPreview::EntryData*>(&Data),
		Label.c_str());
}

void STPMapSelectionDialog::AddControlsOnTopOfList()
{
	TwAddButton(mMenu,"Cancel", STPMapSelectionDialog_OnClick, &mDefaultCallbackParam, NULL);
}

void STPMapSelectionDialog::OnHighlightChange(STPListDialogWithPreview::EntryData *Entry)
{
	STPListDialogWithPreview::OnHighlightChange(Entry);

	std::string SaveFile="UserSaves/"+Entry->mReturnValue+SAVE_EXTENSION;
	if (PHYSFS_exists(SaveFile.c_str()))
	{
		mPreviewedMapHasSave=true;
	}
	else
	{
		mPreviewedMapHasSave=false;
	}
}


int __stdcall DoMapSelection(void* Unknown, const char* OriginalMap)
{
	STPMapSelectionDialog Dialog;

	Dialog.SetVisible(true);
	Dialog.DoModal();

	delete Unknown; //not sure when this thing should be deallocated actually
	return 0;
}

