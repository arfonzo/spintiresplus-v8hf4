/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <D3DX9.h>
#include <d3d9.h>
#pragma comment(lib,"d3dx9.lib")

#include "common.h"
#include "PHYSFS_minimal.h"


#define TEXTURED_VERTEX_FORMAT D3DFVF_XYZRHW|D3DFVF_TEX1
#define SOLIDFILL_VERTEX_FORMAT D3DFVF_XYZRHW|D3DFVF_DIFFUSE

struct TexturedVertex
{
	TexturedVertex() {z=1.0f; w=1.0f;}
    float x, y, z, w;	// The transformed(screen space) position for the vertex.
    float tu,tv;	// Texture coordinates
};

struct SolidFillVertex
{
	SolidFillVertex() {z=0.0f; w=1.0f;}
    float x, y, z, w;
    DWORD Color;
};

/*
loads a texture given its filepath through physfs
the resulting texture is:
-NOT lockable (default pool)
-a render target if RenderTarget is set
-has only one mip level

the function returns NULL on failure
*/
static IDirect3DTexture9* LoadTextureFromPHYSFS(
	const char* FileName,
	IDirect3DDevice9* Device,
	bool RenderTarget,
	_D3DFORMAT ConvertToFormat=D3DFMT_UNKNOWN)
{
	IDirect3DTexture9* OutputTexture;
	PHYSFS_File* TextureFile=PHYSFS_openRead(FileName);
	if (!TextureFile)
	{
		return NULL;
	}
	//read texture file
	unsigned int FileSize=static_cast<unsigned int>(PHYSFS_fileLength(TextureFile)); //this file can't get bigger than 2Gb anyway, or the game would run out of memory just with one texture
	void* TextureBuffer=malloc(FileSize);
	PHYSFS_read(TextureFile,TextureBuffer,FileSize,1);
	PHYSFS_close(TextureFile);
	//create texture from PHYSFS file
	HRESULT hr=D3DXCreateTextureFromFileInMemoryEx(
		Device,
		TextureBuffer,
		FileSize,
		D3DX_DEFAULT,	//width
		D3DX_DEFAULT,	//height
		1,				//mip levels
		RenderTarget ? D3DUSAGE_RENDERTARGET : 0 , //usage
		ConvertToFormat,	//format
		D3DPOOL_DEFAULT,
		D3DX_FILTER_NONE,//filter
		D3DX_FILTER_NONE,//mip filter
		0,				//color key
		NULL,			//pSrcInfo
		NULL,			//palette
		&OutputTexture);
	if (FAILED(hr))
	{
		LOG("D3DXCreateTextureFromFile failed for texture %s\n",FileName);
	}
	//free texture "file"
	free(TextureBuffer);

	return OutputTexture;
}