/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPConstraintNamePiggyBack.h"

STPConstraintNamePiggyBack::STPConstraintNamePiggyBack(void)
{
	mValid=false;
}

STPConstraintNamePiggyBack::~STPConstraintNamePiggyBack(void)
{
}

//sadly for this string we cannot use the PiggyBack engine because
void STPConstraintNamePiggyBack::InitFromChars(const char* Buffer)
{
	int Temp=0;
	if (5==sscanf(Buffer,"|%d|%f|%f|%f|%d|",&mController,&mSpeed,&mResetSpeed,&mResetPosition,&Temp))
	{
		mValid=true;
		mAutoReset= Temp!=0;
	}
	else
	{
		mValid=false;
	}
}

void STPConstraintNamePiggyBack::SaveInString(std::string& String)
{
	std::ostringstream StringStream;
	StringStream<<"|"<<mController<<"|"<<mSpeed<<"|"<<mResetSpeed<<"|"<<mResetPosition<<"|"<<int(mAutoReset)<<"|";
	String+=StringStream.str();
}