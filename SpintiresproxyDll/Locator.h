/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>

#include "typedefs.h"

//key models are garages, cloaks, objectives
//and lumber dispensers
class Locator //total size: 0x848 bytes
{
public:
	enum MarkerType : int
	{
		Garage = 0,
		Fuel = 1,
		AutoLumber = 2,
		Objective = 3,
		Cloak = 4,
		SpanwPoint = 5,
		LogKiosk = 6,
		
	};
	MarkerType Type;
	float X;
	float Y;

	byte Unknown999[0x848-0xC];

public:
	bool IsHidden() const;
};

std::vector<Locator*>* GetLocatorList();