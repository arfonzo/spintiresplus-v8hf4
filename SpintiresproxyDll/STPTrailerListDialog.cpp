/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPTrailerListDialog.h"
#include "AntTweakBar.h"
#include "AddonDefinitionFile.h"

STPTrailerListDialog::STPTrailerListDialog(void)
:mInstalledTrailer(NULL)
{
	mUniqueIdentifier="Select trailer";
}

STPTrailerListDialog::~STPTrailerListDialog(void)
{
}


bool STPTrailerListDialog::IsAddonInstalled(const std::string& AddonFilename) const
{
	return (*mInstalledTrailer)==AddonFilename;
}

void STPTrailerListDialog::Init(std::string& InstalledTrailer)
{
	mInstalledTrailer=&InstalledTrailer;
	PopulateList();
	
	
	//build the GUI
	Restore();
	SetVisible(false);
}

//////////// Quit callback
void STPTrailerListDialog::OnQuit()
{
	{	//loop over all known addons
		uint j=0;
		std::list<StringPair>::const_iterator End=mAddonNames.end();
		for ( std::list<StringPair>::const_iterator i=mAddonNames.begin();	i!=End; ++i,++j)
		{
			if (mCheckedAddons[j])
			{	//if the addon is checked...
				*mInstalledTrailer=(*i).second;
				break;
			}
		}
	}

	//then hide our window
	SetVisible(false);
}

bool STPTrailerListDialog::BelongsToList(const AddonDefinitionFile* Addon) const 
{
	if (Addon)
	{
		return Addon->IsTrailer();
	}
	return false;
}