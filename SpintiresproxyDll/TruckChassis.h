/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>

#include "typedefs.h"
#include "GameStructures.h"
#include "Vector.h"

struct TruckChassis
{
	byte Unknown0[0x8];
	Addon* TruckBase;
	//offset +c
	TruckControls* Constrols;
	byte Unknown10[0x8];
	//offset18
	std::vector<WheelSet> WheelSets;
	uint CurrentWheelset; //index within the WheelSets array, 0 for default wheels
	byte Unknown12[0x30];
	//offset 64
	uint DiffType; //0 std, 1 none, 2 always
	byte Unknown15[0x4];
	DamageInfo HealthInfo;	//size 0x80

	byte Unknown2[0x4C];
	
	//offset 0x138
	std::vector<MountedWheel*> Wheels;
	//offset 144-c
	//std::vector<float> WheelAngVels; <-just No.

	byte Unknown30[0x26C];	//that's a lot of data...
	//offset 3BC
	float SteerSpeed;
	float SteerResponsiveness;
	float BackSteerSpeed;
	float Unknown40[3];	

	//offset 3d4
	float ExhaustStartTime;
	float Unknown41;
	float EngineStartDelay;
	byte Unknown50[0x1C];	//sound-related stuff here

	//offset 3FC
	enum EngineState:byte
	{
		ES_Running = 1,
		ES_Unknown = 2,
	};
	byte EngineStateFlags;	//EngineStateFlags & 1 = engine running
	byte Unknown60[0x5b];
	
	//offset 458
	TruckChassis* Trailer;
	byte Unknown65[0x24];

	
	//offset 480
	union
	{	//those only have a meaning when this TruckChassis
		//is a trailer.
		bool TrailerCanDetach;
		float TrailerDetachingForce;
	};

	byte Unknown70[0x50];
	//offset 4D4
	void* NonZeroWhenWinched1;
	void* NonZeroWhenWinched2;

	//////////////
	///METHODS
	//////////////
public:
	bool HasTrailer() const;

	//returns true if this truck is winched, or true if
	//this truck's trailer is winched (or the trailer's trailer, etc...)
	bool IsWinched() const;

	bool IsTrailer() const;
};