#include "BinaryStream.h"

BinaryStream::BinaryStream(void)
:basic_iostream<char>(&mBuffer)
,mSize(0)
{
}

BinaryStream::~BinaryStream(void)
{
}

uint BinaryStream::GetSize() const
{
	return mSize;
}

const char* BinaryStream::GetRawData() const
{
	return mBuffer.str().data();
}

void BinaryStream::Empty()
{
	//clear stream
	seekp(0, std::ios_base::beg);
	seekg(0, std::ios_base::beg);
	mSize=0;
}

void BinaryStream::operator>>(std::string& Output)
{
	uint StringSize;
	*this>>StringSize;

	void* Buffer=malloc(StringSize);
	this->read((char*)Buffer,StringSize);

	Output.clear();
	Output.append((char*)Buffer,StringSize);

	//cleanup
	free(Buffer);

}
void BinaryStream::operator<<(const std::string& Input)
{
	*this<<(uint)(Input.length());
	this->write(Input.data(),Input.length());
}
