/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once

#include "typedefs.h"
struct StartupTruck //total size: 160 bytes
{
	std::string szType;					//size: 0x1C
	uint nWheelsSet;
	byte Unknown0[0x40];
	std::vector<std::string> Addons;	//size: 0x18
	std::string Trailer;
	byte Unknown1[0xC];
};

struct  StartupTruckOverride //size 0x20 bytes
{
	short Index;
	std::string TruckType;
};

struct MapOverrideList
{
	std::string MapName;
	byte Unknown0[0x4];
	std::vector<StartupTruckOverride> TruckOverrides;
};