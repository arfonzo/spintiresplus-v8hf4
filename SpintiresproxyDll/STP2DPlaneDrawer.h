/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>

struct IDirect3DDevice9;
struct IDirect3DTexture9;
struct IDirect3DVertexBuffer9;

class STP2DPlaneDrawer
{
public:
	STP2DPlaneDrawer(void);
	~STP2DPlaneDrawer(void);

	//Tells the STP2DPlaneDrawer whan device it should use
	//YOu must call this before using SetPosSize;
	void SetDevice(IDirect3DDevice9* Device);

	//defines where this plane should be, in pixels.
	//if ApplyNow is true:
	//-You must call SetDevice before calling this.
	//-Calls to this function are expensive, since they
	//reallocate data to the GPU
	//if ApplyNow is false:
	//-Calls are fast
	//-The change is only applied at the next OnDeviceReset() or UpdateVertexBuffer()
	//X,Y are the coordinates of the top left corner
	void SetPosSize(int X,int Y,int Width, int Height,bool ApplyNow);

	//You should call those members when the appropriate events happen.
	//Always call from the thread who owns the D3D device used.
	void OnDeviceLost();
	void OnDeviceReset();

	//should be called between BeginScene and EndScene
	void Render();
	

	//Loads the specified filename through PHYSFS, and sets applies it to this
	//plane. The STP2DPlaneDrawer takes ownership of the texture and will handle
	//its reloading/releasing on device lost/reset.
	//If a texture with the same filename is already loaded it is not reloaded
	void SetTextureFromFile(const std::string& Filename);

	//Sets the texture displayed on this plane.
	//The STP2DPlaneDrawer will NOT take ownership of the texture and you
	//should handle texture deallocation, reloading and resetting when
	//device is lost then reset.
	void SetTexture(const IDirect3DTexture9* Texture);

	//Allocates a vertex buffer if needed and actually places the plane
	//at the coordinates defined by SetPosSize
	void UpdateVertexBuffer();
private:
	//deallocates the texture if it is owned
	void FreeTextureIfNeeded();

	
private:
	IDirect3DDevice9* mDevice;

	IDirect3DTexture9* mTexture;
	bool mTextureOwned;	//true if whe shold handle texture deallocation
	std::string mTextureFileName;

	IDirect3DVertexBuffer9* mVertexBuffer;
	int mX,mY;
	int mWidth,mHeight;
};
