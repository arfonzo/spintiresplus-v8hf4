/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <d3d9.h>
#include <math.h>
#include <vector>

#include "Common.h"
#include "typedefs.h"
#include "UncloakingHooks.h"

#define FOW_CLOAKED 0x0
#define FOW_UNEXPLORED 0xB0
#define FOW_EXPLORED 0xFF
//radius in meters
#define CLOAK_RADIUS 228


//will paint a (filled) circle centered on CircleX/Y with the given color
//if the pixel at this point isn't already explored.
void SetFogOfWarCicleInBox(D3DLOCKED_RECT* FogOfWar,const RECT& Box,int CircleX, int CircleY, int CircleRadius, byte Color)
{
	const int MinY=__max(Box.top,CircleY-CircleRadius);
	const int MaxY=__min(Box.bottom,CircleY+CircleRadius);
	byte* FOWBytes=static_cast<byte*>(FogOfWar->pBits);

	for (int Y=MinY; Y<=MaxY; ++Y)
	{
		int LineHalfWidth=static_cast<int>(sqrt(float(CircleRadius*CircleRadius - (Y - CircleY)*(Y - CircleY) )));

		const int MinX=__max(Box.left,CircleX-LineHalfWidth);
		const int MaxX=__min(Box.right,CircleX+LineHalfWidth);
		for (int X=MinX; X<MaxX; ++X)
		{
			if (FOWBytes[Y*FogOfWar->Pitch + X]!=FOW_EXPLORED)
			{
				FOWBytes[Y*FogOfWar->Pitch + X]=Color;
			}
		}
	}
}

void __stdcall RemoveClockFromMap_Internal(const Locator* CurrentCoak,IDirect3DTexture9* FogOfWarTexture,const MapData* Map)
{
	D3DLOCKED_RECT LockedFogOfWar;
	//yes I know it could be optimized by only locking CloakBoundingBox.
	FogOfWarTexture->LockRect(0,&LockedFogOfWar,0,0);

	if (LockedFogOfWar.pBits)
	{
		D3DSURFACE_DESC TextureDescriptor;
		FogOfWarTexture->GetLevelDesc(0,&TextureDescriptor);
		const int MapWidth=static_cast<int>(Map->Width);
		const int MapHeight=static_cast<int>(Map->Height);

		//minimap texture size varies with map size, with a conversion ration roughtly
		//equal to 449 minimap pixels for 1024 meters.
		const int MiniMapHeight=static_cast<int>(TextureDescriptor.Height);
		const int MiniMapWidth=static_cast<int>(TextureDescriptor.Width);

		const int CloakRange=CLOAK_RADIUS *MiniMapWidth/MapWidth;
		const int CloakRangeSquared=CLOAK_RADIUS*CLOAK_RADIUS;

		const int CloakX=static_cast<int>(CurrentCoak->X) * MiniMapWidth/MapWidth + MiniMapWidth/2;
		const int CloakY=static_cast<int>(CurrentCoak->Y) * MiniMapHeight/MapHeight + MiniMapHeight/2;

		RECT CloakBoundingBox;
		CloakBoundingBox.top   =__max(CloakY-CloakRange,0);
		CloakBoundingBox.bottom=__min(CloakY+CloakRange,MiniMapHeight-1);

		CloakBoundingBox.left =__max(CloakX - CloakRange,0);
		CloakBoundingBox.right=__min(CloakX + CloakRange,MiniMapWidth-1);

		SetFogOfWarCicleInBox(&LockedFogOfWar,CloakBoundingBox,
			CloakX,CloakY,
			CloakRange,
			FOW_UNEXPLORED);

		//redraw the black circle around the other cloaks that are within range
		std::vector<Locator*>* Cloaks=GetLocatorList();
		if (!Cloaks)
		{
			return;	//should never happen
		}
		const int CloakCount=Cloaks->size();
		for (int i=0;i<CloakCount; i++)
		{	//for every cloak/garage/objective/lumber/etc...

			const Locator* Item=(*Cloaks)[i];
			if (Item->Type==Locator::Cloak && Item!=CurrentCoak && !Item->IsHidden())
			{	//if it's a cloak but not the one we just removed

				const int ItemX=static_cast<int>(Item->X) * MiniMapWidth/MapWidth +TextureDescriptor.Width/2;
				const int ItemY=static_cast<int>(Item->Y) * MiniMapHeight/MapHeight +TextureDescriptor.Height/2;

				const int CloakDistanceSquared=static_cast<int>( (Item->X-CurrentCoak->X)*(Item->X-CurrentCoak->X) + (Item->Y-CurrentCoak->Y)*(Item->Y-CurrentCoak->Y) );
				if (CloakDistanceSquared<=CloakRangeSquared*4)
				{	//and it it's close enough

					//redraw the black cicle, but just the part within the bounding
					//box
					SetFogOfWarCicleInBox(&LockedFogOfWar,CloakBoundingBox,
						ItemX,
						ItemY,
						CloakRange,
						FOW_CLOAKED
					);
				}
			}
		}		
	}

	//FogOfWarTexture->UnlockRect(0); will be done by the game shortly after

}

__declspec(naked) void RemoveClockFromMap()
{
	__asm
	{
		push esi	//MapData
		push eax	//minimap texture
		push ebp	//cloak to remove
		call RemoveClockFromMap_Internal
		cmp esp,0 //the goal is to make the next jg jump
		ret
	}
}