/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>
#include "BinaryStream.h"
#include "typedefs.h"

class PiggyBack
{
public:
	PiggyBack();
	~PiggyBack(void);
	bool IsValid() const {return mValid;}

	//saves all changed data into the associated string
	void SaveInString(std::string& String);
	void InitFromChars(const char* Buffer);

	//Fetches a piggyback from an std::string, if present returns true
	//or false if there is no piggy
	bool InitFromStdString(const std::string& Buffer);

	//called by InitFromChars and InitFromString
	//to fetch data from stream. You should overload this function in subclasses
	//to fetch you own data from mDataStream here
	virtual void Init() = 0;

protected:
	void ClearStream();
	
	BinaryStream mDataStream;
	bool mValid;
};

struct PiggyHeader
{
	uint Magic; //always = ID_CODE
	uint Size;	//size of the piggyback, EXCLUDING the header
};