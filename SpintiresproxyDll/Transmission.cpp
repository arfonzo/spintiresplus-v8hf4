/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include  <math.h>
#define NOMINMAX


#include "Common.h"
#include "RigidBody.h"
#include "GameStructures.h"
#include "STPVehicleTrack.h"
#include "TruckControls.h"
#include "TruckChassis.h"

#include "ExternalMinimap.h"

#ifdef DEBUG_DRIVETRAIN
	#define WIN32_LEAN_AND_MEAN
	#include <Windows.h> //for SetConsoleCursorPosition
#endif


//defined in hooker.cpp
extern void (_stdcall *ApplyTorqueOnWheels_Real) (TruckControls* Truck,float AccelRatio,float FrameTime);

//detects if wheels belonging to a track should brake, reverse, or go forward
//and applies torque on them
//all wheels of a side are locked to the same speed (somewhet)
//and all wheels are powered
void HandleSkidSteering(TruckControls* Truck,std::vector<WheelParams*>& SkidSteeringWheels, float FrameTime)
{
#ifdef DEBUG_DRIVETRAIN
		COORD ConsoleOrigin;
		HANDLE Console=GetStdHandle(STD_OUTPUT_HANDLE);
		ConsoleOrigin.X=0;
		ConsoleOrigin.Y=0;
		SetConsoleCursorPosition(Console,ConsoleOrigin);
#endif
	//no skid steering wheels, no problem
	const uint WheelCount=SkidSteeringWheels.size();
	if (!WheelCount)
	{
		return;
	}

	//convenience
	if (Truck->DiffLocked)
	{
		Truck->SteeringRatio=0.0f;
	}

	bool TruckIsCoughing=false;
	if (Truck->StallRatio)
	{
		if (Truck->ShakeTimer<0.1f)
		{
			Truck->ShakeTimer+=FrameTime;
			if (Truck->ShakeTimer>0.1f)
			{
				TruckIsCoughing=true;
				Truck->ShakeTruck(true);
			}
			
		}
		else if (Truck->ShakeTimer>0.2f)
		{
			Truck->ShakeTimer=0.0f;
			TruckIsCoughing=true;
			Truck->ShakeTruck(false);
		}
		else
		{
			Truck->ShakeTimer+=FrameTime;
		}
	}

	//some sort of safety against vsynch killers
	if (FrameTime<1e-4)
	{
		return;
	}

	//no point in calculating torques if the engine isn't connected
	//actually there is: locking all wheels of a track to the same speed
	if (Truck->IsInNeutral() || Truck->ThrottleRatio<=0.0f || Truck->HandbrakeOn || !Truck->IsAccelerating())
	{	//the last condition mean:
		//-exit if accel ratio is negative (forward key) and we are reversing and autogearbox is engaged
		//-exit if accel ratio is positive (backward key) and we are going forward
	//	ResetWheelAccumulators(SkidSteeringWheels);
		return;
	}

	/////////////////
	///////setup tracks
	/////////////////
	STPVehicleTrack RightTrack(SkidSteeringWheels,true,Truck,FrameTime);
	STPVehicleTrack LeftTrack(SkidSteeringWheels,false,Truck,FrameTime);

	///////calculate distance between tracks
	//needed to calculate turn rate
	Vector4 InterTrack=RightTrack.mPosition;
	InterTrack-=LeftTrack.mPosition;
	float TrackDistance=InterTrack.Length3D();
	
	/////////////////
	///////setup in what direction we want tracks to move
	/////////////////
	float RAngVel=RightTrack.mAngvel;
	float LAngVel=LeftTrack.mAngvel;

	//do not mess up average speed calculation due to reverse steering
	bool InLowGear=Truck->SelectedGear<=1;
	if (RightTrack.IsReverseSteering(Truck->SteeringRatio,InLowGear))
	{
		RAngVel=-RAngVel;
	}
	if (LeftTrack.IsReverseSteering(Truck->SteeringRatio,InLowGear))
	{
		LAngVel=-LAngVel;
	}

	float AverageAngvel=(RAngVel + LAngVel) *0.5f;


	/////////////////
	//////get torque outside the gearbox from current angvel
	/////////////////
	float MaxAngvel;
	float TorqueAfterGearbox= Truck->GetTorqueForAngvel(AverageAngvel,MaxAngvel);
	Truck->GearHeadroom=__max(0.0f,1.0f-abs(AverageAngvel/Truck->GetMaxSpeed()));
	//HACK: game uses max torque for fuel consumption calculation
	//but it expects max WHEEL torque, we give it max ENGINE torque.
	//Since this torque is then multiplied by GearHeadroom in fuel calc
	//and fuel calcualtion forumal is FUBAR (not even homogenous) we just shove an user-defined factor in there.
	Truck->GearHeadroom*=Truck->GetFuelConsumptionFactor();

	TorqueAfterGearbox*=Truck->ThrottleRatio;
	TorqueAfterGearbox*=abs(Truck->AccelRatio); //accel ratio=-1 means full throttle, +1 means full brake!!
												//except when we are reversing in autogearbox, so let's take the abs()
												//since braking causes an early function exit

	
	
	float TargetAngVel=MaxAngvel*Truck->ThrottleRatio;
	float RightTargetVel=RightTrack.AngVel2TrackSpeed(TargetAngVel);
	float LeftTargetVel=LeftTrack.AngVel2TrackSpeed(TargetAngVel);

	//if (Truck->IsInNeutral() || Truck->ThrottleRatio<=0.0f || Truck->HandbrakeOn || !Truck->IsAccelerating())
	//{	
	//	TorqueAfterGearbox=0;
	//	TargetAngVel=AverageAngvel;
	//}


	LOG_DT("m%f res %f opt %f ||\nRPM %.0f avg %f\tsr %f ||\n",Truck->MaxTorque,TorqueAfterGearbox,MaxAngvel,Truck->GetEngineRPM(AverageAngvel),AverageAngvel,Truck->SteeringRatio)

	//basic (proportional) turn rate servoing
	const float MaxTurnRate=DEG2RAD(90.0f);
	const float Radius=TrackDistance;


	const Vector4& TruckAngularSpeed=Truck->Chassis->TruckBase->TruckBody->getAngularVelocity();
	//positive truck angvel means turn to the right
	const float CurentTurnRate=-Truck->Chassis->TruckBase->TruckBody->YVector.Dot(TruckAngularSpeed);
	const float DesiredTurnRate=MaxTurnRate * Truck->SteeringRatio;
	float Error=CurentTurnRate-DesiredTurnRate;

	if (Truck->DiffLocked)	//difflocking simply means go full forward
	{
		Error=0.0f;
	}

	
	if ( RightTrack.IsReverseSteering(Truck->SteeringRatio,InLowGear))
	{	//if steering to the max, in 1st gear, and using forward reverse mode
		RightTargetVel=-RightTargetVel;	
	}
	else
	{	//any other steering mode
		//RightTargetVel=RightTargetVel * (1.0f- Error );
		RightTargetVel-=RightTargetVel * Error;	//same as above
		if (Truck->IsReversing())
		{
			RightTargetVel=__clamp(RightTrack.AngVel2TrackSpeed(MaxAngvel),RightTargetVel,0);
		}
		else
		{
			RightTargetVel=__clamp(0.0f,RightTargetVel,RightTrack.AngVel2TrackSpeed(MaxAngvel));
		}
	}

	if ( LeftTrack.IsReverseSteering(Truck->SteeringRatio,InLowGear))
	{	//if steering to the max, in 1st gear, and using forward reverse mode
		LeftTargetVel=-LeftTargetVel;	
	}
	else
	{	//any other steering mode
		//LeftTargetVel=LeftTargetVel * (1.0f + Error );
		LeftTargetVel+=LeftTargetVel * Error ;	//same as above
		if (Truck->IsReversing())
		{
			LeftTargetVel=__clamp(LeftTrack.AngVel2TrackSpeed(MaxAngvel),LeftTargetVel,0);
		}
		else
		{
			LeftTargetVel=__clamp(0.0f,LeftTargetVel,LeftTrack.AngVel2TrackSpeed(MaxAngvel));
			
		}
	}			

	if (!LeftTargetVel)	
	{	//if we are fully braking on one side, give all torque to the other because this side's clutch is disengaged
		float UsedRTorque=RightTrack.ApplyTorque(TorqueAfterGearbox,RightTargetVel,FrameTime);
		LeftTrack.ApplyTorque(0.0f,LeftTargetVel,FrameTime);

		//do the stalling
		Truck->UpdateStallRatio( (UsedRTorque==TorqueAfterGearbox) ,AverageAngvel,FrameTime);
	}
	else if (!RightTargetVel)
	{
		RightTrack.ApplyTorque(0.0f,RightTargetVel,FrameTime);
		float UsedLTorque=LeftTrack.ApplyTorque(TorqueAfterGearbox,LeftTargetVel,FrameTime);

		//do the stalling
		Truck->UpdateStallRatio( (UsedLTorque==TorqueAfterGearbox) ,AverageAngvel,FrameTime);
	}
	else
	{	//both sides are powered: give half engien power to both
		float RTorque=TorqueAfterGearbox/2.0f;
		float LTorque=TorqueAfterGearbox/2.0f;

		//on tracked vehicles having one side on the air does no reduce the torque on the other side:
		//you just hit the brakes on the flying side
		//float RTorque=RightTrack.CalculateTorqueRequirement(TorqueAfterGearbox/2.0f ,RightTargetVel,FrameTime);
		//float LTorque=LeftTrack.CalculateTorqueRequirement(TorqueAfterGearbox/2.0f ,LeftTargetVel,FrameTime);
		//float DiffTorque;
		//if(Truck->IsReversing())
		//{
		//	DiffTorque=__max(RTorque,LTorque);
		//}
		//else
		//{	//going forward: diff allows only the smallest torque
		//	DiffTorque=__min(RTorque,LTorque);
		//}

		//Finally! Apply! That! TORQUE!!
		LOG_DT("lv %.3frv %.3f lt %.3f rt %.3f||\n",LeftTargetVel,RightTargetVel,LTorque,RTorque);
		float UsedRTorque=RightTrack.ApplyTorque(RTorque,RightTargetVel,FrameTime);
		float UsedLTorque=LeftTrack.ApplyTorque(LTorque,LeftTargetVel,FrameTime);

		//do the stalling
		Truck->UpdateStallRatio( (RTorque && (UsedRTorque==RTorque)) || (LTorque && (UsedLTorque==LTorque)) ,AverageAngvel,FrameTime);
	}


	//auto gearbox fix:
	if (Truck->IsRPMLow(AverageAngvel) && Truck->AutoGearboxEngaged && Truck->SelectedGear>1)
	{
		Truck->SelectedGear--;
		Truck->ShiftTimer=1.5f;
		Truck->EngineSilenceTimer=0.2f;
	}


#ifdef DEBUG_DRIVETRAIN
	LOG_DT("--------------\n");
#endif

}

//game function that calculates the torque to apply on the wheels
//and also applies it.
void _stdcall ApplyTorqueOnWheels(TruckControls* Truck,float AccelRatio,float FrameTime)
{
	if (GlobalSettings::SupportSkidSteering)
	{
		const uint WheelCount=Truck->Chassis->Wheels.size();
		std::vector<WheelParams*> SkidSteeringWheels;
		SkidSteeringWheels.reserve(WheelCount);
		for (uint i=0;i<WheelCount;++i)
		{
			WheelParams* Wheel=Truck->Chassis->Wheels[i]->Wheel;
			if (Wheel->GetSkidSteerMode() != WheelParams::SkidSteer_None)
			{
				SkidSteeringWheels.push_back(Wheel);
			}
		}

		HandleSkidSteering(Truck,SkidSteeringWheels,FrameTime);
		if (SkidSteeringWheels.empty())
		{
			ApplyTorqueOnWheels_Real(Truck,AccelRatio,FrameTime);
		}
	}
	else
	{	//no skid steering -> use the default function
		ApplyTorqueOnWheels_Real(Truck,AccelRatio,FrameTime);
	}
	

	if (GlobalSettings::MinimapEnabled)
	{
		//don't put trailers on the minimap (vehicles with no engine torque
		//or no gearbox
		if (Truck->MaxTorque>0 || Truck->GearBox.size())
		{
			const Vector4& CurrentPosition=Truck->Chassis->TruckBase->TruckBody->Position;
			const Vector4& ForwardVector=Truck->Chassis->TruckBase->TruckBody->XVector;
			STPMinimapManager::Get()->AddTruckMarker(CurrentPosition.X,CurrentPosition.Z,-atan2(ForwardVector.Z,ForwardVector.X));
		}
	}

}