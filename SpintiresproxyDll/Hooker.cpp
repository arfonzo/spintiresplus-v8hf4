/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Psapi.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <time.h>	//for the timebombs second->date


#include "Common.h"
#include "Hooker.h"
#include "Settings.h"
#include "Hooks.h"
#include "LODHooks.h"
#include "FOVHooks.h"
#include "StartupAddonHooks.h"
#include "GameStructures.h"
#include "GameFunctions.h"
#include "AddonDefinitionFile.h"	//we find its global pointer
#include "CameraHooks.h"
#include "ConstraintHooks.h"
#include "WinchHooks.h"
#include "TruckParserHooks.h"
#include "hkpLinearParametricCurve2.h"
#include "PHYSFS_Init.h"
#include "BugFixes.h"
#include "UncloakingHooks.h"
#include "STPControlledConstraints.h"
#include "STPRootTruckSelectionDialog.h"
#include "PHYSFS_minimal.h"	//for PHYSFS_permitSymLinks
#include "STPMapSelectionDialog.h"

#define NOP_OPERATOR		0x90
#define JMP_IMM32_OPERATOR	0xE9
#define CALL_IMM32_OPERATOR	0xE8
#define JCC8_START			0x70
#define JCC8_END			0x7F
#define JCC32_PREFIX		0x0F
#define JCC32_START			0x80
#define JCC32_END			0x8F
#define SIZEOF_CALL			5

const static Address INVALID_ADDRESS=-1;

enum UniqueSignatures
{
	//special
	GlobalVarsLocator,
	ShaderLoader,

	//bugfixes
	CloneMudHeightmap,

	//wheelmass bug
	MudSinkingCheck,
	WheelLoading,
	SuspensionLoading,

	//external minimap
	LoadCloakMesh,	//happens at map startup, also used to locate LoadMesh

	//XML additions
	GetXmlStringFunc,
	MeshLoader,
	MPCustomMaps,
	ClampedFloatReader,
	ClampedIntReader,
	ClampedManualLoadsReader,
	CreateStdStringForWheelType,
	OpenWheelXML,
	IsAddonChassisFullOcclusion,
	AreWeInstallingOrRemovingAddon,
	CalculateTorqueToApplyOnWheel,
	CreateStdStringForWheelset,
	ListWheelsetInGarage,
	GlobalDesiredWheelsetLocator,

	//trailer breakoff threshold
	WriteTrailerCanDetach,
	ReadTrailerCanDetach,

	//driveshafts
	LoadShaftMesh,
	CreateShaft,
	TransformShaft,

	//silent linked steering
	CheckEngineStateForLinkedSteeringSound,
	DoneReadingIsLinkedSteering,


	//new constraints
	HavocAllocator,
	hkpPointToPlaneConstraintData_Constructor,
	hkpPointToPlaneConstraintData_SetInBodySpace,
	BuildContraintFromXML,
	hkpPointToPathConstraintData_Constructor,
	hkpPointToPathConstraintData_SetInBodySpace,
	hkpLinearParametricCurve_Constructor,
	hkpLinearParametricCurve_AddPoint,
	hkpBallSocketChainData_Constructor,
	hkpBallSocketChainData_AddConstraintInfoInBodySpace,
	hkpConstraintChainInstance_Constructor,
	hkpConstraintChainInstance_AddEntity,

	//constrolled constraint
	StoreContraintName,
	InverseMatrixInHUDFunction,


	//general annoyances
	DifflockAutoEngageFix,
	ShifterReleaseFix,
	TrailerLoadFix,
	TrailerLoadFix2,
	SuspensionDamage1,
	SuspensionDamage2,
	GetHavokMemoryManagerCall,

	//cosmetics
	DisableDOF,
	CalculateDistanceForLOD,
	RunTerrainShaderOnMediumDistanceTiles,
	GetTileLODLocator,
	FOVChange,
	NoVisualDamage,
	Hide3DUI,
	HideSteamPicName,

	//Camera
	AutoZoom,
	AdvancedAutoZoom,
	StationAutoZoom,
	ZoomWriter,
	X360ZoomWriter,
	ResetCabinCamera,
	CabinCameraMaxTurnAngle,
	WriteCameraYAngle,
	AdjustCameraToSteering,

	//new game
	DisableCloaks,
	CheckDistanceForStartupTrucks,

	//new menus
	ParseSpawnStartupTrucks,
	GetFriendlyNameLocator,
	GetSourceWorkshopItemLocator,
	MessagPump,
	GlobalAddonArray,
	SaveTruckOverrideCall,
	TruckMenuConstructor,
	SaveMapSelectionCall,
	MapMenuConstructor,

	
	//gameplay tweaks
	HardcoreAutoLogs,
	NoDamage,
	UnlimitedFuel,
	
	//number od loadpoints per objective
	DeliveringLogs1,
	DeliveringLogs2,
	CheckIfObjectiveFull,
	GameItemDeliveredTextColor,
	ObjectiveNTextColor,	//change text color for objective 1,2,3...
	DrawObjectiveProgressBar,
	WriteObjectivePoints,
	
	//time of day
	CalcTimeOfDayIncrement,
	WriteTimeOfDay,
	
	//difflock
	CalcDifflockStress,
	CalcDifflockDamage,
	CheckDiffStress,

	//proving grounds
	ShouldWeDisplayDevMenu,
	ShouldWeDisplaySpawnLocators,

	//winch
	BatteryPoweredWinch1,
	BatteryPoweredWinch2,
	MouseOnWinchCrossTest,
	WinchingRange,

	//legacy cloaks
	GameManagerAccessorLocator,
	LockMinimapFogOfWarTexture,
	CheckIfCloakIsActive,

	UNIQUE_SIGNATURE_COUNT,
};

enum TimebombSignatures
{
	//for a bomb, those would be the timer
	Timebomb_time64,
	Timebomb_SystemTime, //GetSystemTime and FileTimeToSystemTime

	//...and those would be the explosive bundle
	Crasher,		//call register variant
	Crasher_Long,	//call [&msvcrt.rand] variant
	TIMEBOMB_SIGNATURE_COUNT,
	
};



////////////pointer to game functions (has to match what's in GameFunctions.h)
//comments also in in GameFunctions.h
char* (__cdecl* GetXmlString_Real)(char*);
UnknownStructure1** (*GetUnknownStructure1)();
void (__cdecl* GetFriendlyName_Real)(std::wstring&,const char*,const char*,int);
bool (__cdecl* IsAddonInstalled)(TruckChassis* Base,const std::string& AddonName,void* Arg3,uint Arg4);
WorkshopMod* (__cdecl *GetWorkshopItem_Real)(const char* );
void (__cdecl* GameMainRenderFunc)();
void (__stdcall* ApplyTorqueOnWheels_Real) (TruckControls* Truck,float AccelRatio,float FrameTime);
Address (*GetHavokMemoryManager_Real)(); //Not sure it's really the memory manager but havok's stack is 0x88+4 bytes later
void* (__stdcall* ShaftConstructor)(void* DestBuffer,LoadedMesh* Mesh);
LoadedMesh* (__stdcall* LoadMesh_Real)(void* MeshLoaderParam,const char* MeshName);
void (__stdcall* AddWheelsetstonGarageMenu)(void* Menu,const std::wstring& SetName,uint Index);
GameManager*& (__stdcall* GetGameManager)();
void* (_stdcall* GetUnknownObject_LegacyCloaks)(); //Don't know what this is, but IsModelHidden wants *{its return} in eax
bool (__stdcall* IsModelHidden)(const Locator*);
void (__stdcall* OriginalCreateAShaft)(void* Pram1); //takes an additional parameter in edi
ModMenuData* (* GetModMenu)(void);
void (__stdcall* SaveTruckOverride_Real)(const char*); //also takes a ModMenuData* in esi
void** (* GetMapMenu_Real)(void);
void (__stdcall* SaveMapSelection)(void* MapMenuData, const char* MapFilename);
////////////pointer to game global objects
std::vector<AddonDefinitionFile>* AddonDatabase;
std::vector<AssetFile>* TruckDatabase;	//immediately follows AddonDatabase in game
uint* DesiredWheelset;

//////////// Hooker class

Hooker::Hooker()
{
	HooksInstalled=false;
	UniqueSignatures=new Signature[UNIQUE_SIGNATURE_COUNT];
	TimebombSignatures=new Signature[TIMEBOMB_SIGNATURE_COUNT];

	//read our config file
	int ConfigFile=_open("SpintiresPlus_config.xml",_O_RDONLY|_O_BINARY);
	if (ConfigFile)
	{
		struct _stat FileInfo;
		_fstat(ConfigFile,&FileInfo);
		char* FileBuffer=new char[FileInfo.st_size+1];
		_read(ConfigFile,FileBuffer,FileInfo.st_size);
		FileBuffer[FileInfo.st_size]=0;	//null terminate the file
		_close(ConfigFile);


		
		xml_document<> doc;
		doc.parse<0>(FileBuffer);
		xml_node<> *node = doc.first_node("Config");
		for (xml_attribute<> *attr = node->first_attribute();
			attr; attr = attr->next_attribute())
		{
			const char* TokenName=attr->name();
			const char* TokenValue=attr->value();
			if (!strcmp(TokenName,"DebugMode"))
			{
				if (!_stricmp (TokenValue,"true"))
				{
					//debug mode enabled
					EnableConsole();
					GlobalSettings::DebugMode=true;
					printf(STP_VERSION_STRING"\nDebug mode on\n");
					
				}
			}
			else if (!strcmp(TokenName,"DumpShaders"))
			{
				if (!_stricmp (TokenValue,"true"))
				{
					GlobalSettings::DumpShaders=true;
				}
			}
			else if (!strcmp(TokenName,"DefuseTimebombs"))
			{
				if (!_stricmp (TokenValue,"true"))
				{
					GlobalSettings::DefuseTimebombs=true;
				}
			}
			else if (!strcmp(TokenName,"WireframeMode"))
			{
				if (!_stricmp (TokenValue,"true"))
				{
					GlobalSettings::WireframeMode=true;
				}
			}
			//else if (!strcmp(TokenName,"HightlightTest1"))
			//{
			//	if (!_stricmp (TokenValue,"true"))
			//	{
			//		GlobalSettings::HightlightTest1=true;
			//	}
			//}
			//else if (!strcmp(TokenName,"HightlightTest2"))
			//{
			//	if (!_stricmp (TokenValue,"true"))
			//	{
			//		GlobalSettings::HightlightTest2=true;
			//	}
			//}
			//else if (!strcmp(TokenName,"HightlightTest3"))
			//{
			//	if (!_stricmp (TokenValue,"true"))
			//	{
			//		GlobalSettings::HightlightTest3=true;
			//	}
			//}
			//else if (!strcmp(TokenName,"HightlightTest4"))
			//{
			//	if (!_stricmp (TokenValue,"true"))
			//	{
			//		GlobalSettings::HightlightTest4=true;
			//	}
			//}
		}


		ParseConfig_BugFixes(node);
		ParseConfig_Winch(node);
		ParseConfig_Modding(node);
		ParseConfig_Misc(node);
		ParseConfig_Graphics(node);
		ParseConfig_HUD(node);
		ParseConfig_Camera(node);
		ParseConfig_NewGame(node);
		ParseConfig_Gameplay(node);
		ParseConfig_TimeCycle(node);
		ParseConfig_3rdPartyCompatibility(node);
		ParseConfig_ExternalMinimap(node);
		delete [] FileBuffer;
	}

	//hooks that do not require signatures or modification of the game's code
	//ie: typically import hooks
	InstallEarlyHooks();

	{
		//after calls to _time64(), where the result is compared against 0x5637C032=number
		//of seconds elapsed between 02 Nov 2015 19:57:38 GMT and 1st jan 1970.
		byte Bytes[]=	{0x2d,0x32,0xC0,0x37,0x56}; //force the next jump if signed
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF};
		TimebombSignatures[Timebomb_time64].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		//after calls to GetSystemTime, where Pavel does Now=(year*12+month)*35+day
		byte Bytes[]=	{0x6B,0xC0,0x23}; //force the next JLE
		byte Mask[]=	{0xFF,0xFF,0xFF};
		TimebombSignatures[Timebomb_SystemTime].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		 //force the final jnz
		//0xF8 in mask: corresponding opcode is 0xB8+register code (mov r32,imm32)
		//where register code is a 3 bit int.
		byte Bytes[]=	{0xff,0000,0x99,0xb8,0000,0000,0000,0000,0xf7,0000,0x83,0xFA};
		byte Mask[]=	{0xFF,0000,0xFF,0xF8,0000,0000,0000,0000,0xFF,0000,0xFF,0xFF};
		TimebombSignatures[Crasher].Set(Bytes,Mask,sizeof(Bytes));
		/*
		signature begins here
		00A65065  |. FFD3           CALL EBX                     ; ebx= ptr to rand() function
		00A65067  |. 99             CDQ                                      ;  crash causer
		00A65068  |. B9 04020000    MOV ECX,204
		00A6506D  |. F7F9           IDIV ECX
		00A6506F  |. 83FA 01        CMP EDX,1
		00A65072  |. 75 22          JNZ SHORT SpinTire.00A65096		if( rand()%0x204 == 1) then corrupt data
		00A65074  |. FFD3           CALL EBX					 ; ebx= ptr to rand() function
		00A65076  |. 99             CDQ
		00A65077  |. B9 A4030000    MOV ECX,3A4
		00A6507C  |. F7F9           IDIV ECX
		00A6507E  |. 52             PUSH EDX
		00A6507F  |. 6A 00          PUSH 0
		00A65081  |. FFD3           CALL EBX					  ; ebx= ptr to rand() function
		00A65083  |. 99             CDQ
		00A65084  |. B9 09030000    MOV ECX,309
		00A65089  |. F7F9           IDIV ECX
		00A6508B  |. 03D6           ADD EDX,ESI
		00A6508D  |. 52             PUSH EDX					  ; corrupt data by doing:
		00A6508E  |. E8 955AE5FF    CALL <JMP.&MSVCR90.memset>    ; memset(ESI+rand() % 0x309, 0 , rand()% 0x3a4)
		00A65093  |. 83C4 0C        ADD ESP,0C
		*/
	}

	{
		 //force the final jnz
		//0xF8 in mask: corresponding opcode is 0xB8+register code (mov r32,imm32)
		//where register code is a 3 bit int.
		//same pattern as above, but with call [&ptr to rand()] instead of call register.
		byte Bytes[]=	{0xff,0x15,0000,0000,0000,0000,0x99,0xb8,0000,0000,0000,0000,0xf7,0000,0x83,0xFA};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0xF8,0000,0000,0000,0000,0xFF,0000,0xFF,0xFF};
		TimebombSignatures[Crasher_Long].Set(Bytes,Mask,sizeof(Bytes));
	}

	
	
	{
		//in the function that references g_fExternMudMode,g_txExternWaterMap,g_fExternWaterMapMode
		/* look for
		00AD0DF2   . 51             PUSH ECX                                 ; |Arg3
		00AD0DF3   . 52             PUSH EDX                                 ; |Arg2
		00AD0DF4   . 50             PUSH EAX                                 ; |Arg1
		00AD0DF5   . E8 C66AFFFF    CALL Spintires.00AC78C0                  ; \Spintires.00AC78C0
		00AD0DFA   > E8 21B097FF    CALL Spintires.0044BE20	<-we want to call this
		00AD0DFF   . 8BF8           MOV EDI,EAX	
		00AD0E01   . 833F 00        CMP DWORD PTR [EDI],0
		00AD0E04   . 0F84 1A010000  JE Spintires.00AD0F24
		*/
		byte Bytes[]=	{0x52,0x50,0xe8,0000,0000,0000,0000,0xe8,0000,0000,0000,0000,0x8b,0xf8,0x83,0x3f,0x00};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[GlobalVarsLocator].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		//find the function that gives the error message:
		//"Effects| Cant read chunk cacheEntry.pEffectBuffer->GetBufferPointer()."
		//find the jump to its reference, the next memcpy reads the shader code.
		byte Bytes[]=	{0xe8,0000,0000,0000,0000,0x83,0xC4,0x0C,0x85,0xED,0x75};
		byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		
		UniqueSignatures[ShaderLoader].Set(Bytes,Mask,sizeof(Bytes));
	}
	

	{
		byte Bytes[]=	{0xD8,0x4C,0x24,0x10,0xD9,0x5C,0x24,0x10,0xD9,0x44,0x24,0x10,0xdc,0x1d}; //write 8 bytes later
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[MudSinkingCheck].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		byte Bytes[]=	{0x83,0xC4,0x0C,0x80,0x7B,0x70,0x00};	//write 3 bytes later
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[WheelLoading].Set(Bytes,Mask,sizeof(Bytes));
		/*	00AAA11B   . 8B45 0C             MOV EAX,DWORD PTR [EBP+0xC]
			00AAA11E   . 68 FC35C300         PUSH test04.00C335FC                     ;  ASCII "HardpointOffsetZ"
			00AAA123   . D95C24 14           FSTP DWORD PTR [ESP+0x14]
			00AAA127   . E8 F4F8FDFF         CALL <test04.GetXMLString>
			00AAA12C   . 83C4 04             ADD ESP,0x4
			00AAA12F   . 85C0                TEST EAX,EAX
			00AAA131   . 74 10               JE SHORT test04.00AAA143
			00AAA133   . 8D4C24 10           LEA ECX,DWORD PTR [ESP+0x10]
			00AAA137   . 51                  PUSH ECX
			00AAA138   . 68 C81EC300         PUSH test04.00C31EC8                     ;  ASCII "%f"
			00AAA13D   . 50                  PUSH EAX
			00AAA13E   . FFD7                CALL EDI
			00AAA140   . 83C4 0C             ADD ESP,0xC					<-signature
			00AAA143   > 807B 70 00          CMP BYTE PTR [EBX+0x70],0x0	<-Overwritten
			00AAA147   . D94424 10           FLD DWORD PTR [ESP+0x10]
			00AAA14B   . D84424 24           FADD DWORD PTR [ESP+0x24]
			00AAA14F   . D95C24 24           FSTP DWORD PTR [ESP+0x24]
			00AAA153   . 74 0A               JE SHORT test04.00AAA15F
			00AAA155   . D94424 24           FLD DWORD PTR [ESP+0x24]
			00AAA159   . D9E0                FCHS
			00AAA15B   . D95C24 24           FSTP DWORD PTR [ESP+0x24]
			00AAA15F   > D98424 B0010000     FLD DWORD PTR [ESP+0x1B0]                ;  load mass of current wheels
			00AAA166   . 8B8424 24010000     MOV EAX,DWORD PTR [ESP+0x124]
			00AAA16D   . 8D9424 20010000     LEA EDX,DWORD PTR [ESP+0x120]
			00AAA174   . 52                  PUSH EDX
			00AAA175   . 51                  PUSH ECX
			00AAA176   . D91C24              FSTP DWORD PTR [ESP]
			00AAA179   . 50                  PUSH EAX
			00AAA17A   . E8 715ED1FF         CALL <test04.CreatePhysxBody>*/
	}

	{	//the beginjning of GetXMLString()
		//locate the function that parses the wheel's xml, find the function that returns light/superheavy/etc..
		//there are many reference to this function, so we're just locating one
		byte Bytes[]=	{0x83,0xC4,0x0C,0xD9,0x44,0x24,0000,0x57,0x8B,0xCE}; //call's relative offset 0x1C before
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[GetXmlStringFunc].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the only reference to "env/shaft"
		//warning: MeshLoader takes 3 paramters in the editor and 2 in the game. Last parameter
		//in the editor is 0 or 1.
		/*
		00ACAF54  |. 51             |PUSH ECX                                ; /Arg1
		00ACAF55  |. 8DBB A8010000  |LEA EDI,DWORD PTR [EBX+1A8]             ; |
		00ACAF5B  |. E8 20649EFF    |CALL SpinTire.004B1380                  ; \SpinTire.004B1380
		00ACAF60  |. 33FF           |XOR EDI,EDI
		00ACAF62  |> 8B8424 C000000>|MOV EAX,DWORD PTR [ESP+C0]
		00ACAF69  |. 2B8424 BC00000>|SUB EAX,DWORD PTR [ESP+BC]
		00ACAF70  |. 45             |INC EBP
		00ACAF71  |. C1F8 02        |SAR EAX,2					<-signature
		00ACAF74  |. 3BE8           |CMP EBP,EAX
		00ACAF76  |.^0F82 47FEFFFF  \JB SpinTire.00ACADC3
		00ACAF7C  |> E8 5FB993FF    CALL SpinTire.004068E0
		00ACAF81  |. 894424 78      MOV DWORD PTR [ESP+78],EAX
		00ACAF85  |. 8B00           MOV EAX,DWORD PTR [EAX]
		00ACAF87  |. 68 64EEC100    PUSH SpinTire.00C1EE64                   ; /Arg2 = 00C1EE64 ASCII "env/shaft"
		00ACAF8C  |. 50             PUSH EAX                                 ; |Arg1
		00ACAF8D  |. E8 AE40FAFF    CALL <SpinTire.MeshLoader> <-grab this   ; \MeshLoader
		*/	
		byte Bytes[]=	{0xc1,0xf8,0x02,0x3b,0xe8,0x0f,0x82,0000,0xfe,0xff,0xff,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[MeshLoader].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//after the reference to "SuspensionDamping" and before the next call register
		byte Bytes[]=	{0x8B,0x4B,0x10,0x8B,0x11,0x8B,0x52,0x24};	//no offset
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[SuspensionLoading].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	
		/*
		00AFBE70  /$ 83EC 0C             SUB ESP,0C
		00AFBE73  |. 53                  PUSH EBX
		00AFBE74  |. 55                  PUSH EBP
		00AFBE75  |. 56                  PUSH ESI
		00AFBE76  |. 57                  PUSH EDI
		00AFBE77  |. E8 B42894FF         CALL SpinTire.0043E730
		00AFBE7C  |. 8B38                MOV EDI,DWORD PTR [EAX]
		00AFBE7E  |. C687 89000000 01    MOV BYTE PTR [EDI+89],1    <-want this (it says "game CRC ok")
		00AFBE85  |. 8B0D CCE5CC00       MOV ECX,DWORD PTR [CCE5CC]
		00AFBE8B  |. 2B0D C8E5CC00       SUB ECX,DWORD PTR [CCE5C8]
		00AFBE91  |. B8 93244992         MOV EAX,92492493
		00AFBE96  |. F7E9                IMUL ECX
		00AFBE98  |. 03D1                ADD EDX,ECX
		*/
		//version 2015/feb/23 and older
		//byte Bytes[]=	{0x57,0xe8,0000,0000,0000,0000,0x8b,0x38,0xc6,0x87,0000,0000,0x00,0x00,0x01};
		//byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};

		/*
		new version
		00B12AF5  |. 0F84 81000000        JE SpinTire.00B12B7C
		00B12AFB  |. 69FF 90010000        IMUL EDI,EDI,190
		00B12B01  |. 81C7 60DDC800        ADD EDI,SpinTire.00C8DD60
		00B12B07  |. 80BF 88010000 00     CMP BYTE PTR [EDI+188],0
		00B12B0E  |. 75 6C                JNZ SHORT SpinTire.00B12B7C
		00B12B10  |. E8 2BBB92FF          CALL SpinTire.0043E640
		00B12B15  |. 8B00                 MOV EAX,DWORD PTR [EAX]
		00B12B17  |. C680 89000000 01     MOV BYTE PTR [EAX+89],1   <-set the "game files validated" flag, sig starts here
		00B12B1E  |. E8 EDDEFFFF          CALL SpinTire.00B10A10
		00B12B23  |. 84C0                 TEST AL,AL
		00B12B25     74 55                JE SHORT SpinTire.00B12B7C
		00B12B27  |. 8B47 04              MOV EAX,DWORD PTR [EDI+4]
		00B12B2A  |. C687 88010000 01     MOV BYTE PTR [EDI+188],1
		*/	

		byte Bytes[]=	{0x00,0x01,0xe8,0000,0000,0000,0000,0x84,0xc0,0x74};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[MPCustomMaps].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that references "Clamping node %s value %.2f..." and
		//force the first jump.
		byte Bytes[]=	{0x83,0xC4,0x0C,0x85,0xC0,0x0f,0x84,0000,0000,0x00,0x00,0xd9};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[ClampedFloatReader].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that references "Clamping node %s value %d..." and
		//force the first jump.
		byte Bytes[]=	{0x83,0xC4,0x04,0x85,0xC0,0x74,0000,0x3B,0xDD};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[ClampedIntReader].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that references "ManualLoads" several times and 
		//"Clamping node %s value %d..." Then force the jump over
		//all clamping.
		byte Bytes[]=	{0x83,0xC4,0x04,0x85,0xC0,0x74,0000,0x83,0000,0x01};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0000,0xFF};
		UniqueSignatures[ClampedManualLoadsReader].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that references "Type","Type","| Cant read string %s", then "Wheels","Wheels"
		//Since it's the WheelsSet parser appropriate XML token names should be there too.
		//Anyway, overwrite the push r32+call to std::string::operator= before "Wheels".
		byte Bytes[]=	{0x83,0xC4,0x04,0x3B,0xC5,0x74,0000,0x8B,0xF0,0xEB};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[CreateStdStringForWheelType].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//beginning of the parent of the function that parses the wheel's xml (and references "superheavy").
		byte Bytes[]=	{0x8B,0xF1,0x57,0x85,0xF6,0x75};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[OpenWheelXML].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Hook the call immediately after push "IsChassisFullOcclusion"
		//This function is called once when creating an addon (including the truck itself)
		//it also references "TruckData/FuelCapacity", "PhysicsModel" and "Mesh"x2.
		byte Bytes[]=	{0x68,0x00,0xFA,0x00,0x00,0x6A,0x01};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[IsAddonChassisFullOcclusion].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find the function that references "game_uninstall_load" (the warning message about losing your load when
		//removing addons). This function is called when adding or removing addons, find where it branches between
		//adding or removing addons, this is on a call to IsAddonInstalled, hook that call.
		/* looks like this
		0048A09C  |> 56             PUSH ESI
		0048A09D  |. 8D4C24 14      LEA ECX,DWORD PTR [ESP+14]
		0048A0A1  |. 51             PUSH ECX
		0048A0A2  |. 8D5424 74      LEA EDX,DWORD PTR [ESP+74]
		0048A0A6  |. 52             PUSH EDX                                                            ;  addon name (std::string)
		0048A0A7  |. 53             PUSH EBX                                                            ;  TruckChassis ptr
		0048A0A8  |. E8 037B6500    CALL <SpinTire.IsAddonInstalled>
		0048A0AD  |. 83C4 10        ADD ESP,10
		0048A0B0  |. 84C0           TEST AL,AL
		0048A0B2  |. 0F85 9F000000  JNZ SpinTire.0048A157                                               ;  jump if unistalling addon
		0048A0B8  |. 8B8424 5C03000>MOV EAX,DWORD PTR [ESP+35C]
		0048A0BF  |. 8B40 28        MOV EAX,DWORD PTR [EAX+28]
		0048A0C2  |. 83C0 68        ADD EAX,68
		*/
		byte Bytes[]=	{0x83,0xC4,0x10,0x84,0xC0,0x0f,0x85,0000,0000,0x00,0x00,0x8b};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[AreWeInstallingOrRemovingAddon].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Locate the TruckControls structure, find what reads AccelRatio.
		//or just find a sequence of 4+ FMUL, then locate what calls this function.
		//You should end up at 00A97176
		/*
		00A97149   . D95C24 20      FSTP DWORD PTR [ESP+20]
		00A9714D   > D946 08        FLD DWORD PTR [ESI+8]
		00A97150   . 83EC 08        SUB ESP,8
		00A97153   . D95C24 04      FSTP DWORD PTR [ESP+4]                   ; |
		00A97157   . D94424 28      FLD DWORD PTR [ESP+28]                   ; |
		00A9715B   . D91C24         FSTP DWORD PTR [ESP]                     ; |will update gearbox
		00A9715E   . 53             PUSH EBX                 <-signature     ; |Arg1
		00A9715F   . E8 5CE1FFFF    CALL SpinTire.00A952C0                   ; \SpinTire.00A952C0
		00A97164   . D946 08        FLD DWORD PTR [ESI+8]
		00A97167   . 83EC 08        SUB ESP,8
		00A9716A   . D95C24 04      FSTP DWORD PTR [ESP+4]
		00A9716E   . D94424 24      FLD DWORD PTR [ESP+24]                   ;  read accel pedal ratio
		00A97172   . D91C24         FSTP DWORD PTR [ESP]
		00A97175   . 53             PUSH EBX                                 ;  ebx=&TruckRoot
		00A97176   . E8 F5E5FFFF    CALL SpinTire.00A95770
		00A9717B   . D94424 20      FLD DWORD PTR [ESP+20]
		00A9717F   . 51             PUSH ECX                                 ; /Arg2
		00A97180   . D91C24         FSTP DWORD PTR [ESP]                     ; |
		00A97183   . 53             PUSH EBX                                 ; |Arg1
		00A97184   . E8 E7E3FFFF    CALL SpinTire.00A95570                   ; \SpinTire.00A95570
		00A97189   . 8B43 18        MOV EAX,DWORD PTR [EBX+18]
		00A9718C   . 8B88 48010000  MOV ECX,DWORD PTR [EAX+148]
		00A97192   . 2B88 44010000  SUB ECX,DWORD PTR [EAX+144]
		00A97198   . 05 38010000    ADD EAX,138
		*/

		byte Bytes[]=	{0x50,0xe8,0000,0000,0000,0000,0xd9,0000,0000,0x83,0xec,0x08};
		byte Mask[]=	{0xF0,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[CalculateTorqueToApplyOnWheel].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Between "SocketPointB" and "shaft frame"
		/*
		00ACB0CA  |. E8 C1EDABFF    |CALL <JMP.&MSVCR90.operator new>
		00ACB0CF  |. 83C4 04        |ADD ESP,4
		00ACB0D2  |. 894424 58      |MOV DWORD PTR [ESP+58],EAX
		00ACB0D6  |. C68424 2802000>|MOV BYTE PTR [ESP+228],15
		00ACB0DE  |. 85C0           |TEST EAX,EAX
		00ACB0E0  |. 74 0F          |JE SHORT SpinTire.00ACB0F1
		00ACB0E2  |. 8B4C24 18      |MOV ECX,DWORD PTR [ESP+18]              ;  read shaft mesh ptr
		00ACB0E6  |. 51             |PUSH ECX                                ; /Arg2
		00ACB0E7  |. 50             |PUSH EAX                                ; |Arg1
		00ACB0E8  |. E8 63624973    |CALL d3d9.LoadShaftModel       <-hook point 
		00ACB0ED  |. 8BF0           |MOV ESI,EAX					<-signature
		00ACB0EF  |. EB 02          |JMP SHORT SpinTire.00ACB0F3
		00ACB0F1  |> 33F6           |XOR ESI,ESI
		00ACB0F3  |> C68424 2802000>|MOV BYTE PTR [ESP+228],14
		00ACB0FB  |. 6A 5C          |PUSH 5C
		00ACB0FD  |. 89B424 5801000>|MOV DWORD PTR [ESP+158],ESI
		00ACB104  |. E8 87EDABFF    |CALL <JMP.&MSVCR90.operator new>
		*/

		byte Bytes[]=	{0x8B,0xF0,0xEB,0x02,0x33,0xF6,0xC6,0x84,0x24,0000,0000,0x00,0x00,0x14};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[LoadShaftMesh].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Same place as LoadShaftMesh, but at the end of the loop
		/*
		00ACB266  |. D95C24 6C        |FSTP DWORD PTR [ESP+6C]
		00ACB26A  |. 8B4C24 6C        |MOV ECX,DWORD PTR [ESP+6C]
		00ACB26E  |. 52               |PUSH EDX                                  ; /Arg1
		00ACB26F  |. 898424 7C010000  |MOV DWORD PTR [ESP+17C],EAX               ; |esp+174 to 180 are shaft end position
		00ACB276  |. 898C24 80010000  |MOV DWORD PTR [ESP+180],ECX               ; |
		00ACB27D  |. E8 6E629EFF      |CALL spintire.004B14F0     <-hook this & signature begin
		00ACB282  |. EB 0A            |JMP SHORT spintire.00ACB28E
		00ACB284  |> 8B06             |MOV EAX,DWORD PTR [ESI]
		00ACB286  |. 8B10             |MOV EDX,DWORD PTR [EAX]
		00ACB288  |. 6A 01            |PUSH 1
		00ACB28A  |. 8BCE             |MOV ECX,ESI
		00ACB28C  |. FFD2             |CALL EDX
		*/

		byte Bytes[]=	{0xe8,0000,0000,0000,0000,0xeb,0000,0x8B,0x06,0x8B,0x10,0x6A,0x01};
		byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CreateShaft].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//no string refs this time. Found by tracing what read copies of copies of the transforms handled
		//at UniqueSignatures[CreateShaft]
		/*
		00AC70A3   . D81D E039C300  FCOMP DWORD PTR [C339E0]
		00AC70A9   . DFE0           FSTSW AX
		00AC70AB   . F6C4 05        TEST AH,5
		00AC70AE   . 7A 13          JPE SHORT SpinTire.00AC70C3
		00AC70B0   . D905 7083C300  FLD DWORD PTR [C38370]
		00AC70B6   . 8D8C24 3402000>LEA ECX,DWORD PTR [ESP+234]
		00AC70BD   . D91C24         FSTP DWORD PTR [ESP]
		00AC70C0   . 51             PUSH ECX
		00AC70C1   . EB 11          JMP SHORT SpinTire.00AC70D4
		00AC70C3     D905 9083C300  FLD DWORD PTR [C38390]
		00AC70C9   . 8D9424 3402000>LEA EDX,DWORD PTR [ESP+234]
		00AC70D0   . D91C24         FSTP DWORD PTR [ESP]	<-signature				;  arg2=float angle
		00AC70D3   . 52             PUSH EDX										;  D3DXMATRIX in out
		00AC70D4   > E8 9F3CACFF    CALL <JMP.&d3dx9_43.D3DXMatrixRotationY>		;  changes shaft base orientation
		00AC70D9     DB4424 18      FILD DWORD PTR [ESP+18]			^hook that call
		00AC70DD   . 8B4424 18      MOV EAX,DWORD PTR [ESP+18]
		00AC70E1   . 85C0           TEST EAX,EAX
		00AC70E3   . 7D 06          JGE SHORT SpinTire.00AC70EB
		00AC70E5   . D805 707FC300  FADD DWORD PTR [C37F70]							;4.29 billion
		00AC70EB   > DC0D 507FC300  FMUL QWORD PTR [C37F50]							;0.5
		00AC70F1   . 51             PUSH ECX
		00AC70F2   . 8D8C24 F401000>LEA ECX,DWORD PTR [ESP+1F4]
		00AC70F9   . D883 DC010000  FADD DWORD PTR [EBX+1DC]
		00AC70FF   . D95C24 10      FSTP DWORD PTR [ESP+10]
		00AC7103     D94424 10      FLD DWORD PTR [ESP+10]
		00AC7107   . D91C24         FSTP DWORD PTR [ESP]
		00AC710A   . 51             PUSH ECX
		00AC710B   . E8 923CACFF    CALL <JMP.&d3dx9_43.D3DXMatrixRotationZ>         ;  do shaft spinning
		*/

		byte Bytes[]=	{0xd9,0x1c,0x24,0x50,0xe8,0000,0000,0000,0000,0xdb,0x44,0x24};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xF0,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[TransformShaft].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find this function. Multiple matches, any is good
		//you want to call this cuntion later
		/*
		0040122F     CC             INT3
		00401230   . A1 34A3C900    MOV EAX,DWORD PTR [C9A334]
		00401235   . 50             PUSH EAX                                 ; /TlsIndex => 0
		00401236   . FF15 DCD1BC00  CALL DWORD PTR [<&KERNEL32.TlsGetValue>] ; \TlsGetValue
		0040123C   . 8B48 2C        MOV ECX,DWORD PTR [EAX+2C]
		0040123F   . 8B11           MOV EDX,DWORD PTR [ECX]
		00401241   . 8B42 04        MOV EAX,DWORD PTR [EDX+4]
		00401244   . 56             PUSH ESI
		00401245   . FFD0           CALL EAX				<-signature
		00401247   . 66:8970 04     MOV WORD PTR [EAX+4],SI
		0040124B   . C3             RETN
		0040124C     CC             INT3
		*/
		byte Bytes[]=	{0xFF,0xD0,0x66,0x89,0x70,0x04,0xC3};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[HavocAllocator].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		//hkpPointToPlaneConstraintData constructor, compare and match vs havok demos
		/*
		00796BB0 >/$ 8BC1           MOV EAX,ECX                              ;  hkpPointToPlaneConstraintData::ctor
		00796BB2  |. D9EE           FLDZ
		00796BB4  |. C700 444FBE00  MOV DWORD PTR [EAX],SpinTire.00BE4F44
		00796BBA  |. B9 01000000    MOV ECX,1
		00796BBF  |. 66:8948 06     MOV WORD PTR [EAX+6],CX
		00796BC3  |. 33C9           XOR ECX,ECX
		00796BC5  |. 8948 08        MOV DWORD PTR [EAX+8],ECX
		00796BC8  |. BA 02000000    MOV EDX,2
		00796BCD  |. 66:8950 10     MOV WORD PTR [EAX+10],DX
		00796BD1  |. BA 07000000    MOV EDX,7					<-signature
		00796BD6  |. 66:8990 A00000>MOV WORD PTR [EAX+A0],DX
		00796BDD  |. 8B15 A0F0BC00  MOV EDX,DWORD PTR [BCF0A0]
		*/
		//hardcoding the size of havok classes is fine since they won't change.
		byte Bytes[]=	{0xBA,0x07,0x00,0x00,0x00,0x66,0x89,0x90,0xA0,0x00,0x00,0x00};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpPointToPlaneConstraintData_Constructor].Set(Bytes,Mask,sizeof(Bytes));
	}
	{	//second function after hkpPointToPlaneConstraintData's constructor
		byte Bytes[]=	{0x8B,0x44,0x24,0x18,0x8B,0x10,0x89,0x51,0x50};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpPointToPlaneConstraintData_SetInBodySpace].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		//hkpPointToPathConstraintData constructor, compare and match vs havok demos
		/*
		00795E40 >/$ D9EE           FLDZ                                     ;  hkpPointToPathConstraintData::ctor
		00795E42  |. 56             PUSH ESI
		00795E43  |. 8BF1           MOV ESI,ECX
		00795E45  |. C706 F44EBE00  MOV DWORD PTR [ESI],SpinTire.00BE4EF4
		00795E4B  |. B8 01000000    MOV EAX,1
		00795E50  |. 66:8946 06     MOV WORD PTR [ESI+6],AX
		00795E54  |. 33C0           XOR EAX,EAX
		00795E56  |. 8946 08        MOV DWORD PTR [ESI+8],EAX
		00795E59  |. BA 01000000    MOV EDX,1					<-signature
		00795E5E  |. 66:8956 0C     MOV WORD PTR [ESI+C],DX
		00795E62  |. D956 1C        FST DWORD PTR [ESI+1C]
		*/
		//hardcoding the size of havok classes is fine since they won't change.
		byte Bytes[]=	{0xBA,0x01,0x00,0x00,0x00,0x66,0x89,0x56,0x0C,0xD9,0x56,0x1C};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpPointToPathConstraintData_Constructor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//second function after hkpPointToPathConstraintData's constructor
		byte Bytes[]=	{0x89,0x7E,0x18,0xD9,0xEE};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpPointToPathConstraintData_SetInBodySpace].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		//hkpLinearParametricCurve constructor, compare and match vs havok demos
		/*
		0079ACFF     CC             INT3
		0079AD00 >/$ D905 2C2CBD00  FLD DWORD PTR [BD2C2C]                   ;  hkpLinearParametricCurve::ctor
		0079AD06  |. 8BC1           MOV EAX,ECX
		0079AD08  |. D958 08        FSTP DWORD PTR [EAX+8]
		0079AD0B  |. C700 7C52BE00  MOV DWORD PTR [EAX],SpinTire.00BE527C
		0079AD11  |. D9EE           FLDZ
		0079AD13  |. B9 01000000    MOV ECX,1
		0079AD18  |. 66:8948 06     MOV WORD PTR [EAX+6],CX
		0079AD1C  |. 33C9           XOR ECX,ECX
		0079AD1E  |. 8848 0C        MOV BYTE PTR [EAX+C],CL
		0079AD21  |. 8948 20        MOV DWORD PTR [EAX+20],ECX
		0079AD24  |. 8948 24        MOV DWORD PTR [EAX+24],ECX	<-signature
		0079AD27  |. BA 00000080    MOV EDX,80000000
		0079AD2C  |. 8950 28        MOV DWORD PTR [EAX+28],EDX
		0079AD2F  |. 8948 2C        MOV DWORD PTR [EAX+2C],ECX
		0079AD32  |. 8948 30        MOV DWORD PTR [EAX+30],ECX
		0079AD35  |. 8950 34        MOV DWORD PTR [EAX+34],EDX
		0079AD38  |. D950 10        FST DWORD PTR [EAX+10]
		0079AD3B  |. D9E8           FLD1
		0079AD3D  |. D958 14        FSTP DWORD PTR [EAX+14]
		0079AD40  |. D950 18        FST DWORD PTR [EAX+18]
		0079AD43  |. D958 1C        FSTP DWORD PTR [EAX+1C]
		0079AD46  \. C3             RETN
		0079AD47     CC             INT3
		*/
		//hardcoding the size of havok classes is fine since they won't change.
		byte Bytes[]=	{0x89,0x48,0x24,0xBA,0x00,0x00,0x00,0x80};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpLinearParametricCurve_Constructor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the only reference to hkpLinearParametricCurve's consructor (it's the copy constructor)
		//find the next call (it calls addpoint)
		byte Bytes[]=	{0x83,0xEC,0x28,0x56,0x57,0x8B,0xF9,0x8B,0x47,0x28,0x8D,0x77,0x20};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpLinearParametricCurve_AddPoint].Set(Bytes,Mask,sizeof(Bytes));
	}
	{	//hkpBallSocketChainData's constructor
		/*
		00792600 >/$ D905 2873BE00  FLD DWORD PTR [BE7328]
		00792606  |. 56             PUSH ESI
		00792607  |. 8BF1           MOV ESI,ECX
		00792609  |. B8 01000000    MOV EAX,1
		0079260E  |. 66:8946 06     MOV WORD PTR [ESI+6],AX
		00792612  |. 33C0           XOR EAX,EAX
		00792614  |. 8946 08        MOV DWORD PTR [ESI+8],EAX
		00792617  |. C706 CC78BE00  MOV DWORD PTR [ESI],SpinTire.00BE78CC
		0079261D  |. 8D4E 0C        LEA ECX,DWORD PTR [ESI+C]
		00792620  |. BA 01000000    MOV EDX,1
		00792625  |. 66:8911        MOV WORD PTR [ECX],DX
		00792628  |. 8946 18        MOV DWORD PTR [ESI+18],EAX
		0079262B  |. 8946 1C        MOV DWORD PTR [ESI+1C],EAX
		0079262E  |. C746 20 000000>MOV DWORD PTR [ESI+20],80000000
		00792635  |. D95E 24        FSTP DWORD PTR [ESI+24]
		00792638  |. D9E8           FLD1
		0079263A  |. 56             PUSH ESI
		0079263B  |. D95E 28        FSTP DWORD PTR [ESI+28]
		0079263E  |. D905 C078BE00  FLD DWORD PTR [BE78C0]
		00792644  |. D95E 2C        FSTP DWORD PTR [ESI+2C]		<-signature
		00792647  |. D905 1075BD00  FLD DWORD PTR [BD7510]
		0079264D  |. D95E 30        FSTP DWORD PTR [ESI+30]
		00792650  |. E8 6B73FEFF    CALL <SpinTire.hkpBridgeConstraintAtom::init>
		00792655  |. 8BC6           MOV EAX,ESI
		00792657  |. 5E             POP ESI
		00792658  \. C3             RETN
		*/
		byte Bytes[]=	{0xD9,0x5E,0x2C,0xD9,0x05,0000,0000,0000,0000,0xD9,0x5E,0x30};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpBallSocketChainData_Constructor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//hkpBallSocketChainData::addConstraintInfoInBodySpace
		//just above its constructor
		
		byte Bytes[]=	{0x8D,0x71,0x18,0x25,0xFF,0xFF,0xFF,0x3F,0x39,0x46,0x04,0x75,0x10,0x6A,0x20};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpBallSocketChainData_AddConstraintInfoInBodySpace].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//constructor for hkpConstraintChainInstance
		
		byte Bytes[]=	{0x33,0xDB,0x89,0x5E,0x38,0x89,0x5E,0x3C};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpConstraintChainInstance_Constructor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//hkpConstraintChainInstance::addEntity
		//just above its constructor
		
		byte Bytes[]=	{0x56,0x8B,0xF1,0x8B,0x46,0x3C,0x83,0xF8,0x02};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[hkpConstraintChainInstance_AddEntity].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that parses constraints (it referenced "Fixed" "Ragdoll" "UnlimitedHinge" "Motor",...
		//the game checks constraint type against various strings, the last being "Fixed".
		//If fixed doesn't match the game will jump to default case, hook that jump.
		/*
		00B8615C  |. 57             PUSH EDI
		00B8615D  |. E8 8EDBFFFF    CALL <SpinTire.HandleMalleableAndBreakab>
		00B86162  |. 83C4 0C        ADD ESP,0C
		00B86165  |. 8BCE           MOV ECX,ESI
		00B86167  |. 894424 1C      MOV DWORD PTR [ESP+1C],EAX
		00B8616B  |. E8 6051A0FF    CALL <SpinTire.hkReferencedObject::remov>
		00B86170  |. 8B4C24 5C      MOV ECX,DWORD PTR [ESP+5C]
		00B86174  |. 85C9           TEST ECX,ECX
		00B86176  |. 74 05          JE SHORT SpinTire.00B8617D		<-signature
		00B86178  |> E8 5351A0FF    CALL <SpinTire.hkReferencedObject::remov>;  constraint construction finished
		00B8617D  |> 837C24 1C 00   CMP DWORD PTR [ESP+1C],0
		00B86182  |. 75 27          JNZ SHORT SpinTire.00B861AB
		00B86184  |> 68 142FC100    PUSH SpinTire.00C12F14   <-want this push;  ASCII "Havok| Cant load constraint: unrecognized type."
		00B86189  |. 53             PUSH EBX
		00B8618A  |. E8 3157EFFF    CALL SpinTire.00A7B8C0
		00B8618F  |. 8B4424 24      MOV EAX,DWORD PTR [ESP+24]
		00B86193  |. 83C4 08        ADD ESP,8

		beware that the push ErrorMsg isn't exactly at the same place in the game and in the editor
		so you'll have to scan for it
		*/
		byte Bytes[]=	{0x74,0x05,0xe8,0000,0000,0000,0000,0x83,0x7c,0x24,0000,0x00,0x75};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[BuildContraintFromXML].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the 2nd functions that reference "truck/crackle_heavy", scroll down a page or 2 and if you see a
		//MOV EAX,2AAAAAAB you're at the right place.
		//there should be a large onditional jump that lands past "game_engine_stalled" and before "game_engine_broken"
		/*
		00B7069D   . DDD8               FSTP ST(0)
		00B7069F   > D905 58F8C200      FLD DWORD PTR [C2F858]
		00B706A5   . D85E 78            FCOMP DWORD PTR [ESI+78]
		00B706A8   . DFE0               FSTSW AX
		00B706AA   . F6C4 41            TEST AH,41
		00B706AD     0F8A 5F080000      JPE SpinTire.00B70F12   <-you want this destination   ;  jump to disable damage
		00B706B3   . 8B0D 84C2CA00      MOV ECX,DWORD PTR [CAC284]
		00B706B9   . 8B15 88C2CA00      MOV EDX,DWORD PTR [CAC288]
		00B706BF   . 33C0               XOR EAX,EAX
		*/


		byte Bytes[]=	{0xdf,0xe0,0xf6,0xc4,0x41,0x0f,0x8a,0000,0000,0000,0000,0x8b,0x0d}; //read the wildcards
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[SuspensionDamage1].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//about half a page after SuspensionDamage1, you have the snippet below.
		//you'll want to jmp 00B70F12 if the check at 00B70746 didn't jump
		/*
		00B70746   . 8079 2D 00         CMP BYTE PTR [ECX+2D],0     ;  probably checks if there was an impact
		00B7074A   . 894C24 30          MOV DWORD PTR [ESP+30],ECX
		00B7074E   . 0F84 D0000000      JE SpinTire.00B70824
		00B70754     8B53 0C            MOV EDX,DWORD PTR [EBX+C]   ;  force jump to 00B70F12 to disable speed damage
		00B70757     837A 54 00         CMP DWORD PTR [EDX+54],0
		00B7075B   . 7D 3F              JGE SHORT SpinTire.00B7079C
		00B7075D     807A 2B 00         CMP BYTE PTR [EDX+2B],0
		00B70761     74 39              JE SHORT SpinTire.00B7079C
		00B70763     D942 20            FLD DWORD PTR [EDX+20]
		00B70766     D9E0               FCHS
		00B70768   . D95C24 18          FSTP DWORD PTR [ESP+18]
		*/

		byte Bytes[]=	{0x8B,0x53,0x0C,0x83,0x7A,0x54,0x00}; //overwrite beginning with jmp 00B70F12
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[SuspensionDamage2].Set(Bytes,Mask,sizeof(Bytes));
	}
	{	
		/*
		00597507   . E8 C4EEFFFF    CALL SpinTire.005963D0	<-want this function (basic mov eax, dword [offset]/retn )
		0059750C   . 51             PUSH ECX		<-signature starts here
		0059750D   . 8BCC           MOV ECX,ESP
		0059750F   . C701 03000000  MOV DWORD PTR [ECX],3
		00597515   . 8B10           MOV EDX,DWORD PTR [EAX]
		00597517   . 8B52 0C        MOV EDX,DWORD PTR [EDX+C]
		0059751A   . 68 E41CBD00    PUSH SpinTire.00BD1CE4                                                         ;  ASCII "hkCpuJobThreadPool"
		*/

		byte Bytes[]=	{0x51,0x8B,0xCC,0xC7,0x01,0x03,0x00,0x00,0x00}; //overwrite beginning with jmp 00B70F12
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[GetHavokMemoryManagerCall].Set(Bytes,Mask,sizeof(Bytes));
	}
	

	{	//find the reference to "Game| Can't install truck %d (%s) addon %s - is not an addon"
		//find the previous reference to "AutoContinueLevel"
		//you want to grab the MOV ESI,DWORD PTR [ESP+C8] immediately after the next
		//ret 8.
		byte Bytes[]=	{0x8b,0xb0,0x24,0000,0000,0x00,0x00,0x8b,0xb0,0x24,0000,0000,0x00,0x00,0x8b,0000,0x2b,0000,0xb8};
		byte Mask[]=	{0xFF,0xF0,0xFF,0000,0000,0xFF,0xFF,0xFF,0xF0,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0xFF,0000,0xFF};
		UniqueSignatures[ParseSpawnStartupTrucks].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find any reference to "assets_", the next function called is GetFriendlyName
		//In this instance I took the "asstes_" before "game_uninstall_load"
		byte Bytes[]=	{0x83,0xC4,0x10,0xc6,0000,0x24,0000,0000,0x00,0x00,0x03,0x83,0000,0000,0x08};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF};
		UniqueSignatures[GetFriendlyNameLocator].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	/*
		00B11480  |> E8 EB35FBFF    /CALL SpinTire.00AC4A70
		00B11485  |. 8BF0           |MOV ESI,EAX
		00B11487  |. 8B56 10        |MOV EDX,DWORD PTR [ESI+10]
		00B1148A  |. 2B56 0C        |SUB EDX,DWORD PTR [ESI+C]
		00B1148D  |. C1FA 02        |SAR EDX,2
		00B11490  |. 3BFA           |CMP EDI,EDX
		00B11492  |. 72 06          |JB SHORT SpinTire.00B1149A
		00B11494  |. FF15 A8D5BC00  |CALL DWORD PTR [<&MSVCR90._invalid_para>;  MSVCR90._invalid_parameter_noinfo
		00B1149A  |> 8B46 0C        |MOV EAX,DWORD PTR [ESI+C]
		00B1149D  |. 8B04B8         |MOV EAX,DWORD PTR [EAX+EDI*4]
		00B114A0  |. 8B40 08        |MOV EAX,DWORD PTR [EAX+8]
		00B114A3  |. 83B8 F8020000 >|CMP DWORD PTR [EAX+2F8],10
		00B114AA  |. 72 08          |JB SHORT SpinTire.00B114B4
		00B114AC  |. 8B80 E4020000  |MOV EAX,DWORD PTR [EAX+2E4]
		00B114B2  |. EB 05          |JMP SHORT SpinTire.00B114B9	<-signature begins here
		00B114B4  |> 05 E4020000    |ADD EAX,2E4
		00B114B9  |> 50             |PUSH EAX
		00B114BA  |. E8 210BFCFF    |CALL <SpinTire.GetSourceWhorkshopItem>	<-you want GetSourceWhorkshopItem
		00B114BF  |. 83C4 04        |ADD ESP,4
		00B114C2  |. 85C0           |TEST EAX,EAX
		00B114C4  |. 74 06          |JE SHORT SpinTire.00B114CC
		00B114C6  |. 8078 08 00     |CMP BYTE PTR [EAX+8],0
		00B114CA  |. 74 1A          |JE SHORT SpinTire.00B114E6
		00B114CC  |> 47             |INC EDI
		00B114CD  |. E8 9E35FBFF    |CALL SpinTire.00AC4A70
		00B114D2  |. 8B48 10        |MOV ECX,DWORD PTR [EAX+10]
		00B114D5  |. 2B48 0C        |SUB ECX,DWORD PTR [EAX+C]
		00B114D8  |. C1F9 02        |SAR ECX,2
		00B114DB  |. 3BF9           |CMP EDI,ECX
		00B114DD  |.^72 A1          \JB SHORT SpinTire.00B11480
		00B114DF  |> 68 304FC200    PUSH SpinTire.00C24F30                   ;  ASCII "ui_balance_warning"
		00B114E4  |. EB 15          JMP SHORT SpinTire.00B114FB
		00B114E6  |> 68 204FC200    PUSH SpinTire.00C24F20                   ;  ASCII "ui_mods_warning"
		00B114EB  |. EB 0E          JMP SHORT SpinTire.00B114FB
*/
		byte Bytes[]=	{0xeb,0x05,0x05,0000,0000,0x00,0x00,0x50,0xe8,0000,0000,0000,0000,0x83,0xc4,0x04};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[GetSourceWorkshopItemLocator].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the PeekMessageW/TranslateMessage/DispatchMessageW loop
		//grab the function that is called when PeekMessageW returns 0
		byte Bytes[]=	{0xeb,0x05,0xe8,0000,0000,0000,0000,0x39,0000,0x24};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0xFF};
		UniqueSignatures[MessagPump].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that displays the garage menu (it references "game_garage_caption")
		//the last function called populates this menu.
		//The first loop PopulateGarageMenu walks over a globbal array, grab it.
		/*
		00488704  |. 3BFB           CMP EDI,EBX
		00488706  |. 0F84 50070000  JE SpinTire.00488E5C
		0048870C  |. 8B46 04        MOV EAX,DWORD PTR [ESI+4]
		0048870F  |. 6A 08          PUSH 8
		00488711  |. 8D7B 01        LEA EDI,DWORD PTR [EBX+1]
		00488714  |. E8 77B85B00    CALL SpinTire.00A43F90
		00488719  |. 8BF0           MOV ESI,EAX
		0048871B  |. E8 20FF5B00    CALL SpinTire.00A48640
		00488720  |. 8B0D 88DFCA00  MOV ECX,DWORD PTR [CADF88]
		00488726  |. 2B0D 84DFCA00  SUB ECX,DWORD PTR [<AddonArrayBegin>]	<-you want AddonArrayBegin
		0048872C  |. B8 67666666    MOV EAX,66666667
		00488731  |. F7E9           IMUL ECX
		00488733  |. C1FA 05        SAR EDX,5
		00488736  |. 8BC2           MOV EAX,EDX
		00488738  |. C1E8 1F        SHR EAX,1F
		0048873B  |. 03C2           ADD EAX,EDX
		*/
		byte Bytes[]=	{0x8b,0xf0,0xe8,0000,0000,0000,0000,0x8b,0000,0000,0000,0000,0000,0x2b,0x0d};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[GlobalAddonArray].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//has 2 calls, one from the WindowProc of the truck selection menu, the other:
		/*
		004789FF   . 68 841FC300     PUSH SpinTire.00C31F84                   ; |Arg1 = 00C31F84 ASCII "game/button_hover"
		00478A04   . E8 07CC6500     CALL SpinTire.00AD5610                   ; \SpinTire.00AD5610
		00478A09   . 83C4 10         ADD ESP,10
		00478A0C   > 817D 0C 0202000>CMP DWORD PTR [EBP+C],202
		00478A13   . 75 60           JNZ SHORT SpinTire.00478A75              ;  jump if not clicking
		00478A15   . 8B4C24 0C       MOV ECX,DWORD PTR [ESP+C]
		00478A19   . 80B9 84000000 0>CMP BYTE PTR [ECX+84],0
		00478A20   . 74 53           JE SHORT SpinTire.00478A75
		00478A22   . 8B7C24 10       MOV EDI,DWORD PTR [ESP+10]
		00478A26   . 8BF3            MOV ESI,EBX
		00478A28   . E8 730B0000     CALL SpinTire.004795A0
		00478A2D   . 8B40 20         MOV EAX,DWORD PTR [EAX+20]
		00478A30   . 85C0            TEST EAX,EAX
		00478A32   . 74 41           JE SHORT SpinTire.00478A75
		00478A34   . 8B40 08         MOV EAX,DWORD PTR [EAX+8]
		00478A37   . E8 B438F9FF     CALL SpinTire.0040C2F0
		00478A3C   . 8BF8            MOV EDI,EAX                              ;  eax= char* name of the selected vehicle
		00478A3E   . E8 EDB7FFFF     CALL SpinTire.00474230	<-we also want that for GetModMenuData
		00478A43   . 85FF            TEST EDI,EDI
		00478A45   . 0F84 A7000000   JE SpinTire.00478AF2
		00478A4B   . 57              PUSH EDI                                 ;  char* name of selected truck
		00478A4C   . 8BF0            MOV ESI,EAX            <-signature       ;  truck slot index at esi+60
		00478A4E   . E8 3DBA6800     CALL SpinTire.00B04490 <-grab this       ;  near esi+54-c is a vector where you should store a string of the new truck name
		00478A53   . E9 A1000000     JMP SpinTire.00478AF9
		00478A58   > E8 83DEF8FF     CALL SpinTire.004068E0
		00478A5D   . 8B10            MOV EDX,DWORD PTR [EAX]
		00478A5F   . 8B82 C8000000   MOV EAX,DWORD PTR [EDX+C8]
		00478A65   . 85C0            TEST EAX,EAX
		*/
		byte Bytes[]=	{0x8b,0xf0,0xe8,0000,0000,0000,0000,0xe9,0000,0000,0x00,0x00,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[SaveTruckOverrideCall].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//has 2 calls, one from the WindowProc of the truck selection menu, the other:
		/*
		00B05BA5   . 8B7B 60        MOV EDI,DWORD PTR [EBX+60]
		00B05BA8   . 8D73 48        LEA ESI,DWORD PTR [EBX+48]
		00B05BAB   . E8 F03297FF    CALL SpinTire.00478EA0
		00B05BB0   . 83C0 1C        ADD EAX,1C
		00B05BB3   . 8378 18 10     CMP DWORD PTR [EAX+18],10                ;  check string length (length of original truck name)
		00B05BB7   . 72 13          JB SHORT SpinTire.00B05BCC
		00B05BB9   . 8B40 04        MOV EAX,DWORD PTR [EAX+4]
		00B05BBC   . 8B5424 0C      MOV EDX,DWORD PTR [ESP+C]
		00B05BC0   . 50             PUSH EAX                                 ; /Arg2
		00B05BC1   . 52             PUSH EDX                                 ; |Arg1
		00B05BC2   . E8 B91C97FF    CALL <SpinTire.TruckSelectionMenuConstru>; \TruckSelectionMenuConstructor
		00B05BC7   . E9 D2000000    JMP SpinTire.00B05C9E
		00B05BCC   > 8B5424 0C      MOV EDX,DWORD PTR [ESP+C]
		00B05BD0   . 83C0 04        ADD EAX,4		<-signature
		00B05BD3   . 50             PUSH EAX                                 ; /original truck name
		00B05BD4   . 52             PUSH EDX        v- hook ths 1st instruction of TruckSelectionMenuConstructor
		00B05BD5   . E8 A61C97FF    CALL <SpinTire.TruckSelectionMenuConstru>; \TruckSelectionMenuConstructor
		00B05BDA   . E9 BF000000    JMP SpinTire.00B05C9E
		00B05BDF   > 83EE 01        SUB ESI,1                                ;  we come here when opening the "manage mods" menu
		00B05BE2   . 0F84 AF000000  JE SpinTire.00B05C97
		00B05BE8   . 83EE 01        SUB ESI,1
		00B05BEB   . 74 7B          JE SHORT SpinTire.00B05C68
		00B05BED   . 83EE 03        SUB ESI,3
		00B05BF0   . 0F85 A8000000  JNZ SpinTire.00B05C9E
		*/
		byte Bytes[]=	{0x83,0xC0,0x04,0x50,0x52,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[TruckMenuConstructor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//after a reference to "game/button_click"
		/*
		0047F33C   . C2 1000        RETN 10
		0047F33F   > 8D78 04        LEA EDI,DWORD PTR [EAX+4]
		0047F342   > E8 4952FFFF    CALL SpinTire.00474590	<-this calls GetMapManager(), we'll need it too
		0047F347   . 8B00           MOV EAX,DWORD PTR [EAX]
		0047F349   . 85C0           TEST EAX,EAX
		0047F34B   . 74 05          JE SHORT SpinTire.0047F352
		0047F34D   . 83C0 D8        ADD EAX,-28
		0047F350   . EB 02          JMP SHORT SpinTire.0047F354
		0047F352   > 33C0           XOR EAX,EAX
		0047F354   > 57             PUSH EDI              <-signature begin  ; /map name
		0047F355   . 50             PUSH EAX                                 ; |map manager
		0047F356   . E8 B5E76700    CALL SpinTire.00AFDB10		<-grab destination
		0047F35B   . A1 ACBFCA00    MOV EAX,DWORD PTR [CABFAC]
		0047F360   . 85C0           TEST EAX,EAX
		0047F362   . 0F84 AF000000  JE SpinTire.0047F417
		0047F368   . 8B88 E0000000  MOV ECX,DWORD PTR [EAX+E0]
		0047F36E   . 8B90 E4000000  MOV EDX,DWORD PTR [EAX+E4]
		0047F374   . 8D7424 14      LEA ESI,DWORD PTR [ESP+14]
		0047F378   . 894C24 14      MOV DWORD PTR [ESP+14],ECX
		0047F37C   . 895424 18      MOV DWORD PTR [ESP+18],EDX
		0047F380   . E8 CB0AFBFF    CALL SpinTire.0042FE50
		0047F385   . 84C0           TEST AL,AL
		0047F387   . 0F84 8A000000  JE SpinTire.0047F417
		0047F38D   . 8B35 50D9BC00  MOV ESI,DWORD PTR [<&steam_api.SteamMatc>;  steam_ap.SteamMatchmaking
		0047F393   . FFD6           CALL ESI                                 ;  <&steam_api.SteamMatchmaking>
		0047F395   . 8B4C24 18      MOV ECX,DWORD PTR [ESP+18]
		0047F399   . 8B10           MOV EDX,DWORD PTR [EAX]
		0047F39B   . 8B52 50        MOV EDX,DWORD PTR [EDX+50]
		0047F39E   . 57             PUSH EDI
		0047F39F   . 68 EC20C300    PUSH SpinTire.00C320EC                   ;  ASCII "level"
		*/
		byte Bytes[]=	{0x50,0x50,0xe8,0000,0000,0000,0000,0xa1,0000,0000,0000,0000,0x85};
		byte Mask[]=	{0xF0,0xF0,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0000,0000,0xFF};
		UniqueSignatures[SaveMapSelectionCall].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//After references to "IsMultiplayerActiveAchievements","true","false"
		/*
		00AFD7C0   > 68 E84DC100    PUSH SpinTire.00C14DE8                   ;  ASCII "false"
		00AFD7C5   . 56             PUSH ESI
		00AFD7C6   . FFD7           CALL EDI
		00AFD7C8   . 83C4 08        ADD ESP,8
		00AFD7CB   . 85C0           TEST EAX,EAX
		00AFD7CD   . 75 02          JNZ SHORT SpinTire.00AFD7D1
		00AFD7CF   . 32DB           XOR BL,BL
		00AFD7D1   > 885C24 14      MOV BYTE PTR [ESP+14],BL
		00AFD7D5   . 33DB           XOR EBX,EBX
		00AFD7D7   > 6A 44          PUSH 44
		00AFD7D9   . E8 B2C6A8FF    CALL <JMP.&MSVCR90.operator new>
		00AFD7DE   . 83C4 04        ADD ESP,4
		00AFD7E1   . 894424 1C      MOV DWORD PTR [ESP+1C],EAX
		00AFD7E5   . 895C24 6C      MOV DWORD PTR [ESP+6C],EBX
		00AFD7E9   . 3BC3           CMP EAX,EBX
		00AFD7EB   . 74 0B          JE SHORT SpinTire.00AFD7F8
		00AFD7ED   . 8B4C24 14      MOV ECX,DWORD PTR [ESP+14]	<-signature begin
		00AFD7F1   . 51             PUSH ECX
		00AFD7F2   . 50             PUSH EAX
		00AFD7F3   . E8 D80D98FF    CALL <SpinTire.MapMenuConstructor> <-grab the destination
		00AFD7F8   > C74424 6C FFFF>MOV DWORD PTR [ESP+6C],-1
		00AFD800   . EB 4E          JMP SHORT SpinTire.00AFD850
		*/
		byte Bytes[]=	{0x8b,0000,0x24,0000,0x50,0x50,0xe8,0000,0000,0000,0000,0xc7,0000,0x24,0000,0xff,0xff,0xff,0xff};
		byte Mask[]=	{0xFF,0000,0xFF,0000,0xF0,0xF0,0xFF,0000,0000,0000,0000,0xFF,0000,0xFF,0000,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[MapMenuConstructor].Set(Bytes,Mask,sizeof(Bytes));
	}
	

	{	//find the functions that reference "DOF" and "ScreenEffects", one of them handles water color
		//the other is camera DOF, overwrite its first insturction with "ret 0c"
		byte Bytes[]=	{0x83,0xEC,0000,0x53,0x55,0x8B,0x6C,0x24,0000,0x56,0x57,0xE8,0000,0000,0000,0000,0x6A,0x00}; //write 0x15 bytes before
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[DisableDOF].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the functions that reference "FarFade", way above that there are 4 nested loops
		//near the beginning of the innermost there is a cmp ebp,F9 (F9=250=255-LODDistance)
		//find where the ebp (single byte) comes from
		//find what wrote to this address, you'll land on
		/*
		00B8820E  |. 51             ||||PUSH ECX                             ; /Arg2
		00B8820F  |. 03C7           ||||ADD EAX,EDI                          ; |
		00B88211  |. 8D1C40         ||||LEA EBX,DWORD PTR [EAX+EAX*2]        ; |
		00B88214  |. 8B4424 44      ||||MOV EAX,DWORD PTR [ESP+44]           ; |
		00B88218  |. D91C24         ||||FSTP DWORD PTR [ESP]                 ; |
		00B8821B  |. 035D 00        ||||ADD EBX,DWORD PTR [EBP]              ; |
		00B8821E  |. 51             ||||PUSH ECX                             ; |Arg1
		00B8821F  |. 8BCF           ||||MOV ECX,EDI                          ; |
		00B88221  |. E8 FA54F3FF    ||||CALL SpinTire.00ABD720               ; \SpinTire.00ABD720
		00B88226  |. 47             ||||INC EDI
		00B88227  |. 3B7C24 2C      ||||CMP EDI,DWORD PTR [ESP+2C]
		00B8822B  |. 8803           ||||MOV BYTE PTR [EBX],AL                ;  write object distance
		00B8822D  |.^7E D1          |||\JLE SHORT SpinTire.00B88200
		*/
		//patch 00ABD720's end
		byte Bytes[]=	{0xB8,0xFF,0x00,0x00,0x00,0x2B,0xC1,0x83};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CalculateDistanceForLOD].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find the functions that reference "Spintires/Terrain" (it initializes the shader)
		//Its parent function contains a sequence of calls like 
		//if (InitializeTerrainShader(bool in cl , 1_to_3)
		//{
		//	RunTerrainShader(?,?,0xff,0xff)
		//}
		//You have one call for the tile under you, one for immediate vincinity tiles
		//one for medium range tiles, and one for far tiles.
		//You want to patch the last call (medium range tiles) to InitializeTerrainShader so
		//that its last argument is 3
		byte Bytes[]=	{0x10,0x6a,0x01,0xb1,0x01,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[RunTerrainShaderOnMediumDistanceTiles].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Inside the loop that sweeps all tiles within viewport, returns 2-5 depending
		//on tile distance from camera. Ends with
		/*
		00B87C9E  |. F6C4 05        TEST AH,5
		00B87CA1     7A 0E          JPE SHORT SpinTire.00B87CB1
		00B87CA3  |. DDD8           FSTP ST(0)
		00B87CA5  |. B8 03000000    MOV EAX,3
		00B87CAA  |. 5E             POP ESI
		00B87CAB  |. 83C4 3C        ADD ESP,3C
		00B87CAE  |. C2 0C00        RETN 0C
		00B87CB1  |> D94424 48      FLD DWORD PTR [ESP+48]
		00B87CB5  |. DED9           FCOMPP
		00B87CB7  |. DFE0           FSTSW AX
		00B87CB9  |. F6C4 05        TEST AH,5
		00B87CBC     7A 0C          JPE SHORT SpinTire.00B87CCA
		00B87CBE  |. B8 04000000    MOV EAX,4
		00B87CC3  |. 5E             POP ESI
		00B87CC4  |. 83C4 3C        ADD ESP,3C
		00B87CC7  |. C2 0C00        RETN 0C
		00B87CCA  |> B8 05000000    MOV EAX,5
		00B87CCF  |. 5E             POP ESI
		00B87CD0  |. 83C4 3C        ADD ESP,3C
		00B87CD3  |. C2 0C00        RETN 0C
		00B87CD6  |> B8 02000000    MOV EAX,2
		00B87CDB  |. 5E             POP ESI
		00B87CDC  |. 83C4 3C        ADD ESP,3C
		00B87CDF  \. C2 0C00        RETN 0C
		00B87CE2     CC             INT3
		*/
		//the signature is the beginning of this function, we want to always return 5.


		byte Bytes[]=	{0x83,0xec,0000,0xd9,0x40,0000,0x56,0xd9};
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[GetTileLODLocator].Set(Bytes,Mask,sizeof(Bytes));
	}
	
	{	//2 functions call d3dx9_43.D3DXMatrixPerspectiveFovLH.
		//Find the one that is used during level loading and not repeatedly during
		//gameplay.
		byte Bytes[]=	{0xD9,0x9E,0x04,0x01,0x00,0x00,0x8D,0x46,0x40};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[FOVChange].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//In an huge function that references "game_engine_stalled".
		//The same function that calculates speed/suspension damage.
		//find the only sete byte ptr [esp+**] in there, nop it
		//alternatively find the function that displays damage messages
		//-then the function that add a damage message to the list of dmg msg when you hit something
		//-then where it's called from
		//-the force the next conditional jump
		/*
		00B70C7A   . 8B4E 08        MOV ECX,DWORD PTR [ESI+8]
		00B70C7D   . 899424 F400000>MOV DWORD PTR [ESP+F4],EDX
		00B70C84   . 898C24 F800000>MOV DWORD PTR [ESP+F8],ECX
		00B70C8B   . 894424 28      MOV DWORD PTR [ESP+28],EAX
		00B70C8F   . 0F944424 2F    SETE BYTE PTR [ESP+2F]		<-nop this
		00B70C94   > 8B8B 00010000  MOV ECX,DWORD PTR [EBX+100]
		00B70C9A   . 2B8B FC000000  SUB ECX,DWORD PTR [EBX+FC]
		00B70CA0   . 8B7424 34      MOV ESI,DWORD PTR [ESP+34]
		00B70CA4   . 834424 40 30   ADD DWORD PTR [ESP+40],30
		00B70CA9   . B8 ABAAAA2A    MOV EAX,2AAAAAAB
		*/
		byte Bytes[]=	{0x89,0000,0x24,0000,0x0F,0x94,0x44,0x24,0000,0x8b};
		byte Mask[]=	{0xFF,0000,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[NoVisualDamage].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//In an huge function where forcing jumps disables
		//specific parts of the rendering
		/*
		00B2C55B  |> 803D 2761C900 01          CMP BYTE PTR [C96127],1
		00B2C562     74 15                     JE SHORT SpinTire.00B2C579
		00B2C564  |. 8B35 D4D6BC00             MOV ESI,DWORD PTR [<&USER32.ShowCursor>] ;  USER32.ShowCursor
		00B2C56A  |. 6A 01                     PUSH 1                                   ; /Show = TRUE
		00B2C56C  |. FFD6                      CALL ESI                                 ; \ShowCursor
		00B2C56E  |. 6A 01                     PUSH 1                                   ; /Show = TRUE
		00B2C570  |. FFD6                      CALL ESI                                 ; \ShowCursor
		00B2C572  |. C605 2761C900 01          MOV BYTE PTR [C96127],1    <-signature
		00B2C579  |> 8B8424 80000000           MOV EAX,DWORD PTR [ESP+80]
		00B2C580  |. 8B00                      MOV EAX,DWORD PTR [EAX]
		00B2C582  |. 80B8 88000000 00          CMP BYTE PTR [EAX+88],0                  ;  are we taking a screnshot?
		00B2C589  |. C605 9FBFCA00 00          MOV BYTE PTR [CABF9F],0
		00B2C590     74 09                     JE SHORT SpinTire.00B2C59B  <-nop        ;  jump if screencapping
		00B2C592  |. 80BD 90020000 00          CMP BYTE PTR [EBP+290],0                 ;  disable HUD on screenies?
		00B2C599     74 56                     JE SHORT SpinTire.00B2C5F1  <-force
		*/
		byte Bytes[]=	{0x74,0000,0x8b,0x74,0x24,0000,0x8B,0x4E,0x54,0x2B,0x4E,0x50};
		byte Mask[]=	{0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[Hide3DUI].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Same function as Hide3DUI, but way below. Suggest tracking the ebp+290 (bHideHudOnScreenshot)
		//present near both patches
		/*
		00B2E319  |. FFD0                      CALL EAX
		00B2E31B  |. A3 48DCC800               MOV DWORD PTR [C8DC48],EAX
		00B2E320  |. C68424 48010000 2E        MOV BYTE PTR [ESP+148],2E    <-signature
		00B2E328     833E 00                   CMP DWORD PTR [ESI],0		<-sometimes it can be 391E CMP DWORD PTR [ESI],EBX, so we need to scan for the jump
		00B2E32B     74 23                     JE SHORT SpinTire.00B2E350   <-force     ;  jump to disable steam account on HUD
		00B2E32D  |. 8B8C24 80000000           MOV ECX,DWORD PTR [ESP+80]
		00B2E334  |. 8B01                      MOV EAX,DWORD PTR [ECX]
		00B2E336  |. 80B8 88000000 00          CMP BYTE PTR [EAX+88],0
		00B2E33D     74 09                     JE SHORT SpinTire.00B2E348
		00B2E33F  |. 80BD 90020000 00          CMP BYTE PTR [EBP+290],0
		00B2E346     74 08                     JE SHORT SpinTire.00B2E350
		*/
		byte Bytes[]=	{0xa3,0000,0000,0000,0000,0xc6,0x84,0x24,0000,0000,0x00,0x00,0x2e};
		byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[HideSteamPicName].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Just above the fstp dword [ebx+94] that writes zoom level after 64s.
		//Zoom level varies in the 0 (max zoom) to 1.0 range (max unzoom)
		/*
		00B7AB4A   . DCC8           FMUL ST(0),ST(0)                         ; |
		00B7AB4C   . DEC1           FADDP ST(1),ST(0)                        ; |
		00B7AB4E   . D95C24 34      FSTP DWORD PTR [ESP+34]                  ; |
		00B7AB52   . D94424 34      FLD DWORD PTR [ESP+34]                   ; |
		00B7AB56   . D91C24         FSTP DWORD PTR [ESP]                     ; |
		00B7AB59   . E8 C29988FF    CALL SpinTire.00404520                   ; \SpinTire.00404520
		00B7AB5E   . D81D 48F1C200  FCOMP DWORD PTR [C2F148]
		00B7AB64   . 83C4 04        ADD ESP,4
		00B7AB67   . DFE0           FSTSW AX
		00B7AB69   . F6C4 41        TEST AH,41
		00B7AB6C     75 48          JNZ SHORT SpinTire.00B7ABB6
		00B7AB6E   . D986 D4000000  FLD DWORD PTR [ESI+D4]
		00B7AB74   . D95C24 48      FSTP DWORD PTR [ESP+48]
		00B7AB78   . 8B4C24 48      MOV ECX,DWORD PTR [ESP+48]
		00B7AB7C   . D986 D8000000  FLD DWORD PTR [ESI+D8]
		00B7AB82   . D95C24 4C      FSTP DWORD PTR [ESP+4C]
		00B7AB86   . 8B5424 4C      MOV EDX,DWORD PTR [ESP+4C]
		00B7AB8A   . D986 DC000000  FLD DWORD PTR [ESI+DC]
		00B7AB90   . 898B 9C000000  MOV DWORD PTR [EBX+9C],ECX
		00B7AB96   . D95C24 50      FSTP DWORD PTR [ESP+50]
		00B7AB9A   . 8B4424 50      MOV EAX,DWORD PTR [ESP+50]
		00B7AB9E   . D983 90000000  FLD DWORD PTR [EBX+90]
		00B7ABA4   . 8993 A0000000  MOV DWORD PTR [EBX+A0],EDX
		00B7ABAA   . D99B 94000000  FSTP DWORD PTR [EBX+94]                  ;  write zoom level after after reset timeout
		*/

		//force the jump at +9 bytes
		byte Bytes[]=	{0x83,0xC4,0x04,0xDF,0xE0,0xF6,0xC4,0x41,0x75,0000,0xd9,0x86,0000,0000,0x00,0x00};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0000,0000,0xFF,0xFF};
		UniqueSignatures[AutoZoom].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find what writes zoom level when entering advanced mode
		//Zoom level varies in the 0 (max zoom) to 1.0 range (max unzoom)
		/*
		00B6A00F   > D9EE           FLDZ
		00B6A011   > D95C24 18      FSTP DWORD PTR [ESP+18]
		00B6A015   . B1 01          MOV CL,1
		00B6A017     D94424 18      FLD DWORD PTR [ESP+18]	<-nop
		00B6A01B     D990 90000000  FST DWORD PTR [EAX+90]	<-nop
		00B6A021     D998 94000000  FSTP DWORD PTR [EAX+94]	<-writes zoom level; nop it
		00B6A027   . E8 247089FF    CALL SpinTire.00401050

          ;  write zoom level after after reset timeout
		*/

		byte Bytes[]=	{0xb1,0x01,0xd9,0x44,0x24,0000,0xd9,0x90,0000,0000,0x00,0x00,0xd9,0x98,0000,0000,0x00,0x00};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF};
		UniqueSignatures[AdvancedAutoZoom].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find what writes zoom level when entering a station, force the jump over tha whole function
		byte Bytes[]=	{0x74,0000,0x8b,0x50,0000,0xd9,0xee};
		byte Mask[]=	{0xFF,0000,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[StationAutoZoom].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//In the function that writes zoom level
		//Zoom level varies in the 0 (max zoom) to 1.0 range (max unzoom)
		/*
		00B7658F     CC             INT3
		00B76590  /$ 83EC 0C        SUB ESP,0C
		00B76593  |. D981 94000000  FLD DWORD PTR [ECX+94]                   ;  read zoom level
		00B76599  |. DB4424 10      FILD DWORD PTR [ESP+10]
		00B7659D  |. DC0D 2881C300  FMUL QWORD PTR [C38128]
		00B765A3  |. DEE9           FSUBP ST(1),ST(0)
		00B765A5     D95C24 10      FSTP DWORD PTR [ESP+10]		<-jmp 00B765CB
		00B765A9  |. D9EE           FLDZ
		00B765AB  |. D94424 10      FLD DWORD PTR [ESP+10]
		00B765AF  |. D8D1           FCOM ST(1)
		00B765B1  |. DFE0           FSTSW AX
		00B765B3  |. F6C4 05        TEST AH,5
		00B765B6     7B 11          JPO SHORT SpinTire.00B765C9              ;  jump if zoom level < 0
		00B765B8  |. DDD9           FSTP ST(1)
		00B765BA  |. D9E8           FLD1
		00B765BC  |. D8D1           FCOM ST(1)
		00B765BE  |. DFE0           FSTSW AX
		00B765C0  |. F6C4 05        TEST AH,5
		00B765C3     7A 04          JPE SHORT SpinTire.00B765C9              ;  jump if 0<zoom level <1
		00B765C5  |. DDD9           FSTP ST(1)
		00B765C7  |. EB 02          JMP SHORT SpinTire.00B765CB
		00B765C9  |> DDD8           FSTP ST(0)
		00B765CB  |> 8B81 3C010000  MOV EAX,DWORD PTR [ECX+13C]
		00B765D1  |. D95C24 10      FSTP DWORD PTR [ESP+10]
		00B765D5  |. D94424 10      FLD DWORD PTR [ESP+10]
		00B765D9  |. 05 A4000000    ADD EAX,0A4
		00B765DE  |. D999 94000000  FSTP DWORD PTR [ECX+94]                  ;  write zoom level
		00B765E4  |. D940 30        FLD DWORD PTR [EAX+30]
		00B765E7  |. D91C24         FSTP DWORD PTR [ESP]
		00B765EA  |. D940 34        FLD DWORD PTR [EAX+34]
		00B765ED  |. D95C24 04      FSTP DWORD PTR [ESP+4]
		00B765F1  |. 8B5424 04      MOV EDX,DWORD PTR [ESP+4]
		00B765F5  |. D940 38        FLD DWORD PTR [EAX+38]
		00B765F8  |. 8B0424         MOV EAX,DWORD PTR [ESP]
		00B765FB  |. 8981 9C000000  MOV DWORD PTR [ECX+9C],EAX
		00B76601  |. D95C24 08      FSTP DWORD PTR [ESP+8]
		00B76605  |. 8B4424 08      MOV EAX,DWORD PTR [ESP+8]
		00B76609  |. 8991 A0000000  MOV DWORD PTR [ECX+A0],EDX
		00B7660F  |. 8981 A4000000  MOV DWORD PTR [ECX+A4],EAX
		00B76615  |. 83C4 0C        ADD ESP,0C
		00B76618  \. C2 0400        RETN 4
		00B7661B     CC             INT3
		*/

		byte Bytes[]=	{0x83,0xec,0000,0xd9,0x81,0000,0000,0x00,0x00,0xdb,0x44,0x24};
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[ZoomWriter].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find zoom level, find what write to it when you move
		//the left stick, done.
		/*
		00B76A02     75 2F          JNZ SHORT SpinTire.00B76A33
		00B76A04  |. D987 94000000  FLD DWORD PTR [EDI+94]	<-reads zoom level
		00B76A0A  |. 51             PUSH ECX
		00B76A0B  |. D946 30        FLD DWORD PTR [ESI+30]
		00B76A0E  |. DC0D C083C300  FMUL QWORD PTR [C383C0]
		00B76A14  |. D84C24 1C      FMUL DWORD PTR [ESP+1C]
		00B76A18  |. DEE9           FSUBP ST(1),ST(0)		<-signature
		00B76A1A     D95C24 3C      FSTP DWORD PTR [ESP+3C]	<-jump from here...
		00B76A1E     D94424 3C      FLD DWORD PTR [ESP+3C]
		00B76A22     D91C24         FSTP DWORD PTR [ESP]
		00B76A25     E8 D6CA88FF    CALL SpinTire.00403500                   ;  clamp zoom level
		00B76A2A  |. D99F 94000000  FSTP DWORD PTR [EDI+94] <-...to here     ;  store zoom level modifier by XB controller
		00B76A30  |. 83C4 04        ADD ESP,4
		00B76A33  |> 8BC5           MOV EAX,EBP
		*/
		byte Bytes[]=	{0xDE,0xE9,0xD9,0x5C,0x24,0000,0xD9,0x44,0x24,0000,0xD9,0x1C,0x24,0xE8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[X360ZoomWriter].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find zoom cabin camera angle, (+pi when looking at your back/right,-pi for back/left)
		/* you'll loand on:
		00B791D5   > 807C24 40 00   CMP BYTE PTR [ESP+40],0
		00B791DA   . 74 0E          JE SHORT SpinTire.00B791EA
		00B791DC     D9EE           FLDZ
		00B791DE     D993 F4000000  FST DWORD PTR [EBX+F4]                   ;  reset cabin camera when accelerating
		00B791E4     D99B F8000000  FSTP DWORD PTR [EBX+F8]
		00B791EA   > 8B53 04        MOV EDX,DWORD PTR [EBX+4]
		//nop the 3 float ops
		*/
		byte Bytes[]=	{0x74,0000,0xd9,0xee,0xd9,0x90,0000,0000,0x00,0x00,0xd9,0x90};
		byte Mask[]=	{0xFF,0000,0xFF,0xFF,0xFF,0xF0,0000,0000,0xFF,0xFF,0xFF,0xF0};
		UniqueSignatures[ResetCabinCamera].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Same function as X360ZoomWriter but 18 pages below
		/* you'll loand on:
		00B77672  |. D94424 64      FLD DWORD PTR [ESP+64]
		00B77676  |. D99F F0000000  FSTP DWORD PTR [EDI+F0]
		00B7767C  |. E9 AC000000    JMP SpinTire.00B7772D
		00B77681  |> 807C24 64 00   CMP BYTE PTR [ESP+64],0
		00B77686     0F85 A1000000  JNZ SpinTire.00B7772D		<-signature
		00B7768C  |. 807C24 60 00   CMP BYTE PTR [ESP+60],0
		00B77691     74 08          JE SHORT SpinTire.00B7769B
		00B77693  |. D905 9083C300  FLD DWORD PTR [C38390]			//pi/2, max turn angle when IsHood=true, overwrite it
		00B77699  |. EB 06          JMP SHORT SpinTire.00B776A1
		00B7769B  |> D905 F881C300  FLD DWORD PTR [C381F8]			//p2, max turn angle when IsHood=false
		00B776A1  |> D95C24 64      FSTP DWORD PTR [ESP+64]
		00B776A5  |. 83EC 0C        SUB ESP,0C
		*/
		byte Bytes[]=	{0x0f,0x85,0000,0000,0000,0000,0x80,0x7c,0x24,0000,0x00,0x74,0000,0xd9,0x05};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[CabinCameraMaxTurnAngle].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find camera Y angle (about 0.26 when cam is facing forward, about 2.8 when pointing backward)
		//find what writes on it
		/* you'll loand on 00B788C3:
		00B788BB  |. 7A 14          JPE SHORT SpinTire.00B788D1
		00B788BD  |> D986 14010000  FLD DWORD PTR [ESI+114]                  ;  come here if camera should point forward
		00B788C3  |. D99E 0C010000  FSTP DWORD PTR [ESI+10C]                 ;  write camera y angle
		00B788C9  |. D986 18010000  FLD DWORD PTR [ESI+118]
		00B788CF  |. EB 23          JMP SHORT SpinTire.00B788F4
		00B788D1  |> D94424 14      FLD DWORD PTR [ESP+14]                   ;  backward facing camera
		00B788D5  |. D94424 18      FLD DWORD PTR [ESP+18]
		00B788D9  |. DED9           FCOMPP
		00B788DB  |. DFE0           FSTSW AX
		00B788DD  |. F6C4 41        TEST AH,41
		00B788E0  |.^74 DB          JE SHORT SpinTire.00B788BD
		00B788E2  |> D986 1C010000  FLD DWORD PTR [ESI+11C]
		00B788E8  |. D99E 0C010000  FSTP DWORD PTR [ESI+10C]
		00B788EE  |. D986 20010000  FLD DWORD PTR [ESI+120]
		00B788F4  |> D99E 10010000  FSTP DWORD PTR [ESI+110]
		00B788FA  |. C686 08010000 >MOV BYTE PTR [ESI+108],1	<-signature and hookpoint
		00B78901  |> 6A 0D          PUSH 0D
		00B78903  |. 8BC7           MOV EAX,EDI
		00B78905  |. E8 36DAF5FF    CALL SpinTire.00AD6340
		00B7890A  |. 84C0           TEST AL,AL
		00B7890C  |. 74 6B          JE SHORT SpinTire.00B78979
		00B7890E  |. 32C9           XOR CL,CL
		00B78910  |. EB 5B          JMP SHORT SpinTire.00B7896D
		00B78912  |> 837E 54 02     CMP DWORD PTR [ESI+54],2
		00B78916  |. 74 65          JE SHORT SpinTire.00B7897D
		*/
		byte Bytes[]=	{0xC6,0x86,0x08,0x01,0x00,0x00,0x01};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[WriteCameraYAngle].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find steering angle in radians, find what reads it within the camrea function
		//(the same function as ResetCabinCamera)
		/* 
		00B792CE   . DC0D 7884C300  FMUL QWORD PTR [C38478]
		00B792D4   . DEC1           FADDP ST(1),ST(0)
		00B792D6   . D91D 8CF0CA00  FSTP DWORD PTR [CAF08C]		<-signature
		00B792DC   . 8B7B 04        MOV EDI,DWORD PTR [EBX+4]
		00B792DF   . 8B4F 0C        MOV ECX,DWORD PTR [EDI+C]
		00B792E2   . D941 70        FLD DWORD PTR [ECX+70]  <- put fldz      ;  load steering angle
		00B792E5   . 51             PUSH ECX                                 ; /Arg1
		00B792E6   . D95C24 48      FSTP DWORD PTR [ESP+48]                  ; |
		00B792EA   . D983 EC000000  FLD DWORD PTR [EBX+EC]                   ; |
		00B792F0   . D8A3 1C010000  FSUB DWORD PTR [EBX+11C]                 ; |
		00B792F6   . D95C24 14      FSTP DWORD PTR [ESP+14]                  ; |
		00B792FA   . D94424 14      FLD DWORD PTR [ESP+14]                   ; |
		00B792FE   . D91C24         FSTP DWORD PTR [ESP]                     ; |
		00B79301   . E8 AAAE88FF    CALL <SpinTire.FloatAbs>                 ; \FloatAbs
		00B79306   . DC35 2086C300  FDIV QWORD PTR [C38620]
		*/
		byte Bytes[]=	{0xd9,0x1d,0000,0000,0000,0000,0x8b,0000,0x04,0x8b,0000,0x0c,0xd9};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[AdjustCameraToSteering].Set(Bytes,Mask,sizeof(Bytes));
	}

	

	{	/*Probably easier to redo: find difflock state
		find what writes on it when disengaging auto gearbox
		00AF64F6     C646 0D 00     MOV BYTE PTR [ESI+D],0              ;  disables difflock when auto gearbox is engaged
		00AF64FA     C64424 1E 00   MOV BYTE PTR [ESP+1E],0
		00AF64FF   . EB 0D          JMP SHORT SpinTire.00AF650E
		00AF6501   > 803D 1072C900 >CMP BYTE PTR [C97210],0
		00AF6508   . 74 04          JE SHORT SpinTire.00AF650E
		00AF650A   . C646 0D 01     MOV BYTE PTR [ESI+D],1 <-nop this   ;  disengage difflock when exiting auto gearbox
		00AF650E   > 8B53 04        MOV EDX,DWORD PTR [EBX+4]	<-signature
		00AF6511   . D9E8           FLD1


		*/
		byte Bytes[]=	{0x8B,0x53,0x04,0xD9,0xE8}; //write 0x15 bytes before
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[DifflockAutoEngageFix].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//probably inside the function that writes the state of the button ontop the shifter
		byte Bytes[]=	{0xb2,0000,0x8b,0x44,0x24,0000,0x8b,0x40}; //write 0x15 bytes before
		byte Mask[]=	{0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[ShifterReleaseFix].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//force the jump before a reference to "game_need_to_position"
		byte Bytes[]=	{0x83,0xf8,0x02,0x75,0000,0x83,0x3d,0000,0000,0000,0000,0x00,0000,0000,0x83,0x3d}; //write 0xc bytes after
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0000,0000,0000,0000,0xFF,0000,0000,0xFF,0xFF};
		UniqueSignatures[TrailerLoadFix].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//force the jump before a reference to "game_need_to_position"
		byte Bytes[]=	{0x8B,0x42,0x04,0x51,0x50}; //write 0x14 bytes after
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[TrailerLoadFix2].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//one of the functions that read IsHardcore (the signature is "cmp byte ptr [edx+1C=IsHardcocre],00")
		byte Bytes[]=	{0x80,0x7A,0000,0x00,0x8B,0x34}; //write 7 bytes after
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[HardcoreAutoLogs].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find the function that references "env/icon_cloaking" "gui/socket_trailer__d_a.tga" "gui/repair__a.tga"
		//and force the memsets to write -1 instead of 0xb0
		//(second memset writes 0 for cloaks, you can kill it)
		byte Bytes[]=	{0x33,0xC0,0xC1,0xFA,0x02,0x89,0x44,0x24,0000,0xd8}; //write 0x2f bytes before and 
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[DisableCloaks].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//The/a function that writes damage
		byte Bytes[]=	{0xD9,0xEE,0000,0000,0000,0000,0000,0000,0x3B,0x81,0000,0000,0x00,0x00}; //write 2 bytes after
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0000,0000,0xFF,0xFF,0000,0000,0xFF,0xFF};
		UniqueSignatures[NoDamage].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//The/a function that reads fuel
		byte Bytes[]=	{0xd9,0x5c,0x24,0000,0000,0x80,0000,0000,0x00,0x00,0x8B,0000,0xD9,0x5C,0x24,0000,0xD9,0x44,0x24,0000,0xDD}; //write 4 bytes after
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[UnlimitedFuel].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	/* closely after a reference to "game/winch_remove"
		00B8ED07   . 8A88 FC030000  MOV CL,BYTE PTR [EAX+3FC]                ;  read engine state (1 starting, 3 running)
		00B8ED0D   . 8B5424 4C      MOV EDX,DWORD PTR [ESP+4C]
		00B8ED11   . 8B7424 0C      MOV ESI,DWORD PTR [ESP+C]
		00B8ED15   . 8A4424 20      MOV AL,BYTE PTR [ESP+20]
		00B8ED19     80E1 01        AND CL,1			<-put an OR here
		00B8ED1C   . 83FA FF        CMP EDX,-1                               ;  if cl is 1 then winchpoints are displayed later
		00B8ED1F   . 75 0D          JNZ SHORT SpinTire.00B8ED2E
		00B8ED21   . 83FE FF        CMP ESI,-1
		00B8ED24   . 75 08          JNZ SHORT SpinTire.00B8ED2E
		*/
		byte Bytes[]=	{0x80,0000,0x01,0x83,0000,0xff,0x75};
		byte Mask[]=	{0xFF,0000,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[BatteryPoweredWinch1].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//A function that engine state (1=starting,3 running,0 off) when pressing the winchkey
		//Closely after a reference to "game_startup_engine"
		/*
		00B188FD   . DFE0           FSTSW AX
		00B188FF   . F6C4 41        TEST AH,41
		00B18902   .^0F84 7DFEFFFF  JE SpinTire.00B18785
		00B18908   . 8A81 FC030000  MOV AL,BYTE PTR [ECX+3FC]                ;  check engine state, allow reeling if ( cl and 1 )
		00B1890E   . 24 01          AND AL,1			<-put an OR here
		00B18910    ^0F84 6FFEFFFF  JE SpinTire.00B18785
		00B18916   . C64424 10 01   MOV BYTE PTR [ESP+10],1
		00B1891B   .^E9 69FEFFFF    JMP SpinTire.00B18789
		*/

		byte Bytes[]=	{0x8a,0x81,0000,0000,0000,0000,0x24,0x01,0x0f,0x84,0000,0000,0000,0xff};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0xFF};
		UniqueSignatures[BatteryPoweredWinch2].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find the function that references "WinchSocket" at every frame after clicking on
		//your truck's winch socket. This function takes winch range as its 5th parameter
		byte Bytes[]=	{0x8b,0x17,0xd9,0x05,0000,0000,0000,0000,0x8b,0x42};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[WinchingRange].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//about a page after "game/uncloaking" and before "game/winch_remove"
		/*
		00B6A8D0   . B8 F18FD33D    MOV EAX,3DD38FF1
		00B6A8D5   . F7E9           IMUL ECX
		00B6A8D7   . C1FA 09        SAR EDX,9
		00B6A8DA   . 8BC2           MOV EAX,EDX
		00B6A8DC   . C1E8 1F        SHR EAX,1F
		00B6A8DF   . 43             INC EBX
		00B6A8E0   . 03C2           ADD EAX,EDX
		00B6A8E2   . 3BD8           CMP EBX,EAX
		00B6A8E4   .^0F82 25FEFFFF  JB SpinTire.00B6A70F
		00B6A8EA   > 8B5D 18        MOV EBX,DWORD PTR [EBP+18]	<-signature
		00B6A8ED   . 803B 00        CMP BYTE PTR [EBX],0                                               ;  check if cursor is enabled/visible
		00B6A8F0   . 0F84 19030000  JE SpinTire.00B6AC0F        <-hook                                 ;  jump if not
		00B6A8F6   . 8B4424 7C      MOV EAX,DWORD PTR [ESP+7C]
		00B6A8FA   . 8078 08 00     CMP BYTE PTR [EAX+8],0
		00B6A8FE   . 0F85 0B030000  JNZ SpinTire.00B6AC0F
		00B6A904   . 8B55 08        MOV EDX,DWORD PTR [EBP+8]
		00B6A907   . 8D4C24 5C      LEA ECX,DWORD PTR [ESP+5C]
		*/
		byte Bytes[]=	{0x8b,0x5d,0000,0x80,0x3b,0x00,0x0f,0x84,0000,0000,0x00,0x00,0x8b};
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[MouseOnWinchCrossTest].Set(Bytes,Mask,sizeof(Bytes));
	}

	
	{	//Find the function that references "game/unload_logs", just after
		//the call that uses this string, there are 2 cmp r32,8
		//those 8s are the number of logs to deliver to fill an objective
		//your goal is to overwrite this 8 with the user-defined value
		byte Bytes[]=	{0x83,0xC4,0x10,0x83,0xFF,0000,0x0f,0x83};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[DeliveringLogs1].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Less than one screen below DeliveringLogs1, there is a loop over
		//all stations to see if all objectives are full, with the same cmp r32,8
		//your goal is to overwrite this 8 with the user-defined value
		byte Bytes[]=	{0x83,0x38,0x03,0x75,0000,0x50,0x8b,0x44,0x24,0000,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[DeliveringLogs2].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//There is a "level_" and "level_proving" reference 2-3 screens above
		//This checks if all objectives are full before displaying end-game stats.
		/*
		00B72F0A  |. 8D9B 00000000  LEA EBX,DWORD PTR [EBX]
		00B72F10  |> 8B8F 14010000  /MOV ECX,DWORD PTR [EDI+114]
		00B72F16  |. 2B8F 10010000  |SUB ECX,DWORD PTR [EDI+110]
		00B72F1C  |. C1F9 02        |SAR ECX,2
		00B72F1F  |. 3BF1           |CMP ESI,ECX
		00B72F21  |. 72 06          |JB SHORT SpinTire.00B72F29
		00B72F23  |. FF15 A8D5BC00  |CALL DWORD PTR [<&MSVCR90._invalid_para>;  MSVCR90._invalid_parameter_noinfo
		00B72F29  |> 8B97 10010000  |MOV EDX,DWORD PTR [EDI+110]
		00B72F2F  |. 8B04B2         |MOV EAX,DWORD PTR [EDX+ESI*4]
		00B72F32  |. 8338 03        |CMP DWORD PTR [EAX],3
		00B72F35  |. 75 0F          |JNZ SHORT SpinTire.00B72F46
		00B72F37  |. 50             |PUSH EAX                                ; /Arg1
		00B72F38  |. 8BC5           |MOV EAX,EBP   <-signature here          ; |
		00B72F3A  |. B3 01          |MOV BL,1                                ; |
		00B72F3C  |. E8 0F8C89FF    |CALL SpinTire.0040BB50                  ; \SpinTire.0040BB50
		00B72F41  |. 83F8 08        |CMP EAX,8                               ;  check if objective is complete
		00B72F44  |. 72 18          |JB SHORT SpinTire.00B72F5E
		00B72F46  |> 8B87 14010000  |MOV EAX,DWORD PTR [EDI+114]
		00B72F4C  |. 2B87 10010000  |SUB EAX,DWORD PTR [EDI+110]
		00B72F52  |. 46             |INC ESI
		00B72F53  |. C1F8 02        |SAR EAX,2
		00B72F56  |. 3BF0           |CMP ESI,EAX
		00B72F58  |.^72 B6          \JB SHORT SpinTire.00B72F10
		00B72F5A  |. 84DB           TEST BL,BL
		*/
		byte Bytes[]=	{0x8B,0xC5,0xB3,0x01,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CheckIfObjectiveFull].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Just above a reference to "game_items_delivered" and "(%s %d/%d)"
		//patch a cmp r32,8 and a push 8
		byte Bytes[]=	{0x72,0x05,0xBD,0x80,0x80,0x80,0xFF};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[GameItemDeliveredTextColor].Set(Bytes,Mask,sizeof(Bytes));
	}
	{	//a screen after a reference to "game_locator_objective" and " %s %d"
		//patch a cmp r32,8 and a push 8
		byte Bytes[]=	{0x8b,0x43,0000,0x8B,0xC8,0xC1,0xE9,0x10,0x0F,0xB6,0xD1};
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[ObjectiveNTextColor].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//In the loop after the reference to "game_game_progress" there are two
		//fmul [&0.125] to calculate the progress ratio of progress bars
		byte Bytes[]=	{0x2B,0xCA,0xC1,0xF9,0x03,0x3B,0xF9};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[DrawObjectiveProgressBar].Set(Bytes,Mask,sizeof(Bytes));
	}
	
	{ 
		//find this:
		/*
		00439C0F     CC             INT3
		00439C10  /$ 51             PUSH ECX
		00439C11  |. 56             PUSH ESI
		00439C12  |. 57             PUSH EDI
		00439C13  |. 8BF8           MOV EDI,EAX
		00439C15  |. 8BF1           MOV ESI,ECX
		00439C17  |. 8D4424 10      LEA EAX,DWORD PTR [ESP+10]
		00439C1B  |. 50             PUSH EAX                                 ; /Arg2
		00439C1C  |. 83C6 5C        ADD ESI,5C                               ; |
		00439C1F  |. 56             PUSH ESI                                 ; |Arg1
		00439C20  |. E8 FB260000    CALL SpinTire.0043C320                   ; \SpinTire.0043C320
		00439C25     8338 08        CMP DWORD PTR [EAX],8		<-write a jump here....
		00439C28     73 42          JNB SHORT SpinTire.00439C6C
		00439C2A  |. 8D4C24 10      LEA ECX,DWORD PTR [ESP+10]
		00439C2E  |. 51             PUSH ECX                                 ; /Arg2
		00439C2F  |. 56             PUSH ESI                                 ; |Arg1
		00439C30  |. E8 EB260000    CALL SpinTire.0043C320                   ; \SpinTire.0043C320
		00439C35     BA 08000000    MOV EDX,8
		00439C3A  |. 2B10           SUB EDX,DWORD PTR [EAX]
		00439C3C  |. 3BD7           CMP EDX,EDI
		00439C3E  |. 73 12          JNB SHORT SpinTire.00439C52
		00439C40  |. 8D4424 10      LEA EAX,DWORD PTR [ESP+10]
		00439C44  |. 50             PUSH EAX                                 ; /Arg2
		00439C45  |. 56             PUSH ESI                                 ; |Arg1
		00439C46  |. E8 D5260000    CALL SpinTire.0043C320                   ; \SpinTire.0043C320
		00439C4B     BF 08000000    MOV EDI,8
		00439C50  |. 2B38           SUB EDI,DWORD PTR [EAX]
		00439C52  |> 8D4C24 10      LEA ECX,DWORD PTR [ESP+10]
		00439C56  |. 51             PUSH ECX                                 ; /Arg2
		00439C57  |. 56             PUSH ESI                                 ; |Arg1
		00439C58  |. E8 C3260000    CALL SpinTire.0043C320                   ; \SpinTire.0043C320
		00439C5D  |. 8B5424 14      MOV EDX,DWORD PTR [ESP+14]	<-...to here
		00439C61  |. 0138           ADD DWORD PTR [EAX],EDI                  ;  write number of points in objective
		00439C63  |. 52             PUSH EDX
		00439C64  |. E8 776E6D00    CALL SpinTire.00B10AE0
		00439C69  |. 83C4 04        ADD ESP,4
		00439C6C  |> 5F             POP EDI
		00439C6D  |. 5E             POP ESI
		00439C6E  |. 59             POP ECX
		00439C6F  \. C2 0800        RETN 8
		00439C72     CC             INT3
		It is called from multiple places, including half a screen before DeliveringLogs1, just before a call to
		steam_api.SteamUser
		This function checks if an objective already has 8 loadpoints and refuses to add more if the objective is full.
		You can either replace all 3 checks vs 8, or skip them (easier).
		
		I had to hardcode the 8 loadpoints in the signature since thee is a duplicate function for
		the garages, with a limit at 4 points*/

		byte Bytes[]=	{0xe8,0000,0000,0000,0000,0x83,0x38,0x08,0x73};
		byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[WriteObjectivePoints].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//15 lines above WriteTimeOfDay, find a calculation like 0.05(frame time) * 24(hours par day) / 2700(seconds that make 75% of a game day)
		//the /2700 is from FDIV QWORD PTR [C38B18], only one reference to this constant in the whole game
		byte Bytes[]=	{0xDC,0x35,0000,0000,0000,0000,0xD9,0x5C,0x24,0000,0xEB,0000,0xDD,0xD9};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0000,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[CalcTimeOfDayIncrement].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find time in the 0-24.0f range, find what writes it. That's the fastest way.
		//then hook the fstp [temp]/fld [temp]/fst [time of day] 
		byte Bytes[]=	{0xd9,0x57,0000,0xd9,0x05,0000,0000,0000,0000,0xd8,0xd1};
		byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[WriteTimeOfDay].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Find the reference to "ui_mods_no_truck" used when selecting a map at the new game
		//menu. There is a call to FloatSqrt (distance calculation) just above, nop
		//the distace comparision vs 32.0f
		byte Bytes[]=	{0xF6,0xC4,0x41,0x0f,0x84,0000,0000,0x00,0x00,0x8d,0x4c,0x24};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CheckDistanceForStartupTrucks].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//Same function that calculates suspension damage
		//couple pages before ref to "game_difflock_damage"
		/*
		00B700FA   . 8B00                    MOV EAX,DWORD PTR [EAX]
		00B700FC   . 80B8 8A000000 00        CMP BYTE PTR [EAX+8A],0                  ;  check if hardcore mode
		00B70103     0F84 84050000           JE SpinTire.00B7068D
		00B70109   . 8B4424 44               MOV EAX,DWORD PTR [ESP+44]
		00B7010D   . E8 1EFC8BFF             CALL SpinTire.0042FD30
		00B70112   . 84C0                    TEST AL,AL
		00B70114   . 0F85 73050000           JNZ SpinTire.00B7068D
		00B7011A   . D9EE                    FLDZ
		00B7011C   . 8B8424 94000000         MOV EAX,DWORD PTR [ESP+94]
		00B70123   . 8078 0D 00              CMP BYTE PTR [EAX+D],0                   ;  check if difflock is engaged
		00B70127   . D95C24 24               FSTP DWORD PTR [ESP+24]
		00B7012B   . C74424 30 00000000      MOV DWORD PTR [ESP+30],0	<-signature
		00B70133   . 0F84 7C010000           JE SpinTire.00B702B5       <-hook        ;  jump if diff unlocked
		00B70139   . 8B8B 48010000           MOV ECX,DWORD PTR [EBX+148]
		*/
		byte Bytes[]=	{0xc7,0x44,0x24,0000,0x00,0x00,0x00,0x00,0x0f,0x84,0000,0000,0x00,0x00,0x8b,0000,0000,0000,0x00,0x00,0x2b};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[CalcDifflockStress].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		/*
		00B7047C   . E8 97A8A1FF    CALL <JMP.&d3dx9_43.D3DXVec3Transform>
		00B70481   . D98424 FC02000>FLD DWORD PTR [ESP+2FC]
		00B70488   . 68 4CFDC200    PUSH SpinTire.00C2FD4C                   ; /Arg1 = 00C2FD4C ASCII "game_difflock_damage"
		00B7048D   . D99C24 6801000>FSTP DWORD PTR [ESP+168]                 ; |
		00B70494   . 8B8C24 6801000>MOV ECX,DWORD PTR [ESP+168]              ; |
		00B7049B   . D98424 0403000>FLD DWORD PTR [ESP+304]                  ; |
		00B704A2   . 894C24 4C      MOV DWORD PTR [ESP+4C],ECX               ; |
		00B704A6   . D99C24 6C01000>FSTP DWORD PTR [ESP+16C]                 ; |
		00B704AD   . 8B9424 6C01000>MOV EDX,DWORD PTR [ESP+16C]              ; |
		00B704B4   . D98424 0803000>FLD DWORD PTR [ESP+308]                  ; |
		00B704BB   . 895424 50      MOV DWORD PTR [ESP+50],EDX               ; |
		00B704BF   . D99C24 7001000>FSTP DWORD PTR [ESP+170]                 ; |
		00B704C6   . 8B8424 7001000>MOV EAX,DWORD PTR [ESP+170]              ; |
		00B704CD   . 894424 54      MOV DWORD PTR [ESP+54],EAX               ; |
		00B704D1   . E8 DA2EF6FF    CALL SpinTire.00AD33B0                   ; \SpinTire.00AD33B0
		00B704D6   . 50             PUSH EAX
		00B704D7   . 8D4C24 5C      LEA ECX,DWORD PTR [ESP+5C]
		00B704DB   . FF15 68D2BC00  CALL DWORD PTR [<&MSVCP90.std::basic_str>;  MSVCP90.std::basic_string<wchar_t,std::char_traits<wchar_t>,std::allocator<wchar_t> >::operator=
		00B704E1   . D94424 30      FLD DWORD PTR [ESP+30]
		00B704E5   . DC0D A084C300  FMUL QWORD PTR [C384A0]
		00B704EB   . 8B4E 48        MOV ECX,DWORD PTR [ESI+48]
		00B704EE   . DC05 A083C300  FADD QWORD PTR [C383A0]
		00B704F4   . DB46 48        FILD DWORD PTR [ESI+48]
		00B704F7   . 85C9           TEST ECX,ECX
		00B704F9   . 7D 06          JGE SHORT SpinTire.00B70501
		00B704FB   . D805 707FC300  FADD DWORD PTR [C37F70]
		00B70501   > DEC9           FMULP ST(1),ST(0)			<-signature
		00B70503   . E8 1806D4FF    CALL <SpinTire.FloatToInt>	<-hook to return difflock damage as int
		00B70508   . 83F8 05        CMP EAX,5

		*/
		byte Bytes[]=	{0xDE,0xC9,0xe8,0000,0000,0000,0000,0x83,0xf8,0x05};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[CalcDifflockDamage].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//After CalcDifflockStress and before the part that displays a message if
		//difflock stress is too high
		/*
		00B7039F   . D94424 20               FLD DWORD PTR [ESP+20]		<-signature begin
		00B703A3   . D84C24 24               FMUL DWORD PTR [ESP+24]                  ;  [+24]=[+20]*[+24]
		00B703A7   . D95C24 24               FSTP DWORD PTR [ESP+24]    <-land here   ;  write difflock heat increment
		00B703AB   . D9E8                    FLD1
		00B703AD   . D85E 74                 FCOMP DWORD PTR [ESI+74]                 ;  esi+74=difflock heat
		00B703B0   . DFE0                    FSTSW AX
		00B703B2   . F6C4 05                 TEST AH,5
		00B703B5   . 0F8A 8D020000           JPE SpinTire.00B70648                    ;  jump if difflock heat below threshold
		00B703BB   . 8D4C24 58               LEA ECX,DWORD PTR [ESP+58]
		00B703BF   . FF15 38D2BC00           CALL DWORD PTR [<&MSVCP90.std::basic_str>;  MSVCP90.std::basic_string<wchar_t,std::char_traits<wchar_t>,std::allocator<wchar_t> >::basic_string<wchar_t,std::char_traits<wchar_t>,std::allocator<wchar_t> >
		*/
		byte Bytes[]=	{0xD9,0x44,0x24,0000,0xD8,0x4C,0x24,0000,0xD9,0x5C,0x24,0000,0xD9,0xE8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF,0xFF};
		UniqueSignatures[CheckDiffStress].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//After CalcDifflockStress and before the part that displays a message if
		//difflock stress is too high
		/*
		00B60CD3   . 8985 60010000  MOV DWORD PTR [EBP+160],EAX
		00B60CD9   . 8985 64010000  MOV DWORD PTR [EBP+164],EAX
		00B60CDF   . 899D 68010000  MOV DWORD PTR [EBP+168],EBX
		00B60CE5   . 8985 6C010000  MOV DWORD PTR [EBP+16C],EAX
		00B60CEB   . 8985 70010000  MOV DWORD PTR [EBP+170],EAX
		00B60CF1   . 8985 74010000  MOV DWORD PTR [EBP+174],EAX		<-signature begin
		00B60CF7   . E8 E45B8AFF    CALL spintire.004068E0
		00B60CFC   . 8BF0           MOV ESI,EAX
		00B60CFE   . 8B06           MOV EAX,DWORD PTR [ESI]
		00B60D00   . 68 9CF8C200    PUSH spintire.00C2F89C                   ; /Arg2 = 00C2F89C ASCII "env/icon_cloaking"
		00B60D05   . 50             PUSH EAX                                 ; |Arg1
		00B60D06   . E8 35E3F0FF    CALL <spintire.MeshLoader>  <-hook that  ; \MeshLoader
		00B60D0B   . 8985 F4000000  MOV DWORD PTR [EBP+F4],EAX
		00B60D11   . 8B06           MOV EAX,DWORD PTR [ESI]
		00B60D13   . 68 B0F8C200    PUSH spintire.00C2F8B0                   ; /Arg2 = 00C2F8B0 ASCII "env/icon_truck"
		00B60D18   . 50             PUSH EAX                                 ; |Arg1
		00B60D19   . E8 22E3F0FF    CALL <spintire.MeshLoader>               ; \MeshLoader
		*/
		byte Bytes[]=	{0x89,0x85,0000,0000,0x00,0x00,0xe8,0000,0000,0000,0000,0x8B,0xF0,0x8B,0x06,0x68};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[LoadCloakMesh].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//search for an strncmp(?,"level_",6) followed by a reference to level_proving
		//and a "manual" string comparison, then force the result of the comparison to
		//"string equal" outcome
		/*
		00B72E08  |. 84D2           |TEST DL,DL
		00B72E0A  |.^75 E4          \JNZ SHORT spintire.00B72DF0
		00B72E0C  |> 33C0           XOR EAX,EAX
		00B72E0E  |. EB 05          JMP SHORT spintire.00B72E15
		00B72E10  |> 1BC0           SBB EAX,EAX
		00B72E12  |. 83D8 FF        SBB EAX,-1
		00B72E15  |> 85C0           TEST EAX,EAX
		00B72E17     0F85 C1000000  JNZ spintire.00B72EDE   <-nop this  ;  jump to disable dev menu
		00B72E1D  |. E8 1E238AFF    CALL spintire.00415140
		00B72E22  |. 8BF0           MOV ESI,EAX
		*/
		byte Bytes[]=	{0x85,0xc0,0x0f,0x85,0000,0000,0x00,0x00,0xe8,0000,0000,0000,0000,0x8b,0xf0};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF};
		UniqueSignatures[ShouldWeDisplayDevMenu].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//search for a reference to level_proving
		//and a "manual" string comparison, then force the result of the comparison to
		//"string equal" outcome
		/*
		00B5165D  |. BF 04F2C200    |MOV EDI,spintire.00C2F204               ;  ASCII "level_proving"; Case 5 of switch 00B50EE8
		00B51662  |. E8 89FEF7FF    |CALL spintire.00AD14F0
		00B51667  |> 8A08           |/MOV CL,BYTE PTR [EAX]
		00B51669  |. 3A0F           ||CMP CL,BYTE PTR [EDI]
		00B5166B  |. 75 1A          ||JNZ SHORT spintire.00B51687
		00B5166D  |. 84C9           ||TEST CL,CL
		00B5166F  |. 74 12          ||JE SHORT spintire.00B51683
		00B51671  |. 8A48 01        ||MOV CL,BYTE PTR [EAX+1]
		00B51674  |. 3A4F 01        ||CMP CL,BYTE PTR [EDI+1]
		00B51677  |. 75 0E          ||JNZ SHORT spintire.00B51687
		00B51679  |. 83C0 02        ||ADD EAX,2
		00B5167C  |. 83C7 02        ||ADD EDI,2
		00B5167F  |. 84C9           ||TEST CL,CL
		00B51681  |.^75 E4          |\JNZ SHORT spintire.00B51667
		00B51683  |> 33C0           |XOR EAX,EAX
		00B51685  |. EB 05          |JMP SHORT spintire.00B5168C
		00B51687  |> 1BC0           |SBB EAX,EAX
		00B51689  |. 83D8 FF        |SBB EAX,-1
		00B5168C  |> 85C0           |TEST EAX,EAX			<-signature
		00B5168E     0F85 D3000000  JNZ spintire.00B51767	<-disable jump   ;  jump to disable spawnlocators
		00B51694  |. D943 04        |FLD DWORD PTR [EBX+4]
		00B51697  |. 8B7424 5C      |MOV ESI,DWORD PTR [ESP+5C]
		00B5169B  |. 8D43 04        |LEA EAX,DWORD PTR [EBX+4]
		00B5169E  |. D95C24 20      |FSTP DWORD PTR [ESP+20]
		00B516A2  |. 50             |PUSH EAX
		00B516A3  |. 899C24 FC00000>|MOV DWORD PTR [ESP+FC],EBX
		00B516AA  |. E8 81FF8DFF    |CALL spintire.00431630
		00B516AF  |. D95C24 30      |FSTP DWORD PTR [ESP+30]
		00B516B3  |. D943 08        |FLD DWORD PTR [EBX+8]
		00B516B6  |. C68424 0801000>|MOV BYTE PTR [ESP+108],0
		*/
		byte Bytes[]=	{0x85,0xc0,0x0f,0x85,0000,0000,0x00,0x00,0xd9,0000,0x04};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[ShouldWeDisplaySpawnLocators].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		/*
		00ACA18F  |. 68 7CECC100      |PUSH SpinTire.00C1EC7C                  ;  ASCII "Title"
		00ACA194  |. 8BC6             |MOV EAX,ESI
		00ACA196  |. E8 A511FBFF      |CALL <SpinTire.GetXMLString>
		00ACA19B  |. 83C4 04          |ADD ESP,4
		00ACA19E  |. 85C0             |TEST EAX,EAX
		00ACA1A0  |. 75 18            |JNZ SHORT SpinTire.00ACA1BA
		00ACA1A2  |. 68 7CECC100      |PUSH SpinTire.00C1EC7C                  ;  ASCII "Title"
		00ACA1A7  |. 68 0447C100      |PUSH SpinTire.00C14704                  ;  ASCII "| Cant read string %s"
		00ACA1AC  |. 56               |PUSH ESI
		00ACA1AD  |. E8 0E17FBFF      |CALL SpinTire.00A7B8C0
		00ACA1B2  |. 83C4 0C          |ADD ESP,0C					<-signature begin
		00ACA1B5  |. B8 07ACC000      |MOV EAX,SpinTire.00C0AC07
		00ACA1BA  |> 50               |PUSH EAX
		00ACA1BB  |. 8BCD             |MOV ECX,EBP
		00ACA1BD  |. FF15 2CD2BC00    |CALL DWORD PTR [<&MSVCP90.std::basic_st>;  MSVCP90.std::string::operator=	<-hook this
		00ACA1C3  |. 8BB424 A8000000  |MOV ESI,DWORD PTR [ESP+A8]
		00ACA1CA  |. 8BCE             |MOV ECX,ESI
		00ACA1CC  |. 2B8C24 A4000000  |SUB ECX,DWORD PTR [ESP+A4]
		00ACA1D3  |. C1F9 02          |SAR ECX,2
		00ACA1D6  |. 3BF9             |CMP EDI,ECX
		00ACA1D8  |. 72 0D            |JB SHORT SpinTire.00ACA1E7
		00ACA1DA  |. FF15 A8D5BC00    |CALL DWORD PTR [<&MSVCR90._invalid_para>;  MSVCR90._invalid_parameter_noinfo
		00ACA1E0  |. 8BB424 A8000000  |MOV ESI,DWORD PTR [ESP+A8]
		00ACA1E7  |> 8B9424 A4000000  |MOV EDX,DWORD PTR [ESP+A4]
		00ACA1EE  |. 8D0CBA           |LEA ECX,DWORD PTR [EDX+EDI*4]
		00ACA1F1  |. BF 84ECC100      |MOV EDI,SpinTire.00C1EC84               ;  ASCII "WheelType"
		*/
		byte Bytes[]=	{0x83,0xC4,0x0C,0xb8,0000,0000,0000,0000,0x50,0x8b,0xcd,0xff,0x15};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CreateStdStringForWheelset].Set(Bytes,Mask,sizeof(Bytes));
	}

	{
		/*
		00488CEE  |. 68 AC20C300      |PUSH SpinTire.00C320AC                  ;  ASCII "game_"
		00488CF3  |. 50               |PUSH EAX
		00488CF4  |. 8D5424 6C        |LEA EDX,DWORD PTR [ESP+6C]
		00488CF8  |. 52               |PUSH EDX
		00488CF9  |. E8 32A46400      |CALL <SpinTire.HashStringToFriendlyName>
		00488CFE  |. 83C4 10          |ADD ESP,10
		00488D01  |. C78424 84000000 >|MOV DWORD PTR [ESP+84],4
		00488D0C  |. 8378 18 08       |CMP DWORD PTR [EAX+18],8                ;  wstring length check
		00488D10  |. 72 05            |JB SHORT SpinTire.00488D17
		00488D12  |. 8B40 04          |MOV EAX,DWORD PTR [EAX+4]
		00488D15  |. EB 03            |JMP SHORT SpinTire.00488D1A	<-signature begin
		00488D17  |> 83C0 04          |ADD EAX,4
		00488D1A  |> 47               |INC EDI
		00488D1B     57               |PUSH EDI                                ;  wheelset index (index in list)
		00488D1C     50               |PUSH EAX                                ;  std::wstring localized wheelset name
		00488D1D     56               |PUSH ESI
		00488D1E     E8 5DFB5B00      |CALL SpinTire.00A48880		<-hook this
		00488D23  |. 8D4C24 60        |LEA ECX,DWORD PTR [ESP+60]
		00488D27  |. C78424 84000000 >|MOV DWORD PTR [ESP+84],-1
		00488D32  |. FF15 3CD2BC00    |CALL DWORD PTR [<&MSVCP90.std::basic_st>;  MSVCP90.std::basic_string<wchar_t,std::char_traits<wchar_t>,std::allocator<wchar_t> >::~basic_string<wchar_t,std::char_traits<wchar_t>,std::allocator<wchar_t> >
		00488D38  |. 397B 30          |CMP DWORD PTR [EBX+30],EDI              ;  check mounted wheelset vs index of this set
		00488D3B     75 6F            |JNZ SHORT SpinTire.00488DAC             ;  jump if this set is NOT currently mounted
		00488D3D  |. 68 FC23C300      |PUSH SpinTire.00C323FC                  ; /Arg1 = 00C323FC ASCII "game_post_current"
		00488D42  |. E8 69A66400      |CALL SpinTire.00AD33B0                  ; \SpinTire.00AD33B0
		*/
		byte Bytes[]=	{0xeb,0x03,0x83,0xC0,0x04,0x47,0x57};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[ListWheelsetInGarage].Set(Bytes,Mask,sizeof(Bytes));
	}
	{	//find references to "game_install", "game_change", and "game_uninstall".
		//grab the last global variable (a mov dword [global desired wheelset], -1)
		/*
		00AE9F22  |. B8 3C3C3CFF               MOV EAX,FF3C3C3C
		00AE9F27  |. 894E 60                   MOV DWORD PTR [ESI+60],ECX
		00AE9F2A  |. C686 D0000000 01          MOV BYTE PTR [ESI+D0],1
		00AE9F31  |. C646 78 01                MOV BYTE PTR [ESI+78],1
		00AE9F35  |. 8986 DC000000             MOV DWORD PTR [ESI+DC],EAX
		00AE9F3B  |. 8986 84000000             MOV DWORD PTR [ESI+84],EAX
		00AE9F41  |. C705 28DDC800 FFFFFFFF    MOV DWORD PTR [C8DD28],-1	<-grab this C8DD28
		00AE9F4B  |. E8 E08C91FF               CALL SpinTire.00402C30
		00AE9F50  |. 3918                      CMP DWORD PTR [EAX],EBX
		00AE9F52  |. 74 09                     JE SHORT SpinTire.00AE9F5D
		*/
		byte Bytes[]=	{0x89,0x86,0000,0000,0x00,0x00,0xc7,0x05,0000,0000,0000,0000,0xff,0xff,0xff,0xff,0xe8};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[GlobalDesiredWheelsetLocator].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	
		/*Same function as the shaft loader, just a bit below
		00ACB31B  |. 6A 01           PUSH 1
		00ACB31D  |. 50              PUSH EAX
		00ACB31E  |. B8 04EFC100     MOV EAX,SpinTire.00C1EF04                ;  ASCII "TrailerCanDetach"
		00ACB323  |. E8 68E0A4FF     CALL <SpinTire.GetXMLBool>
		00ACB328  |. 68 94EFC100     PUSH SpinTire.00C1EF94                   ;  ASCII "Constraint"
		00ACB32D  |. 8D4C24 30       LEA ECX,DWORD PTR [ESP+30]
		00ACB331  |. 51              PUSH ECX
		00ACB332  |. 8D5424 68       LEA EDX,DWORD PTR [ESP+68]
		00ACB336  |. 52              PUSH EDX				<-signature
		00ACB337  |. 8883 80040000   MOV BYTE PTR [EBX+480],AL hook this     ;  write bTraileCanDetach
		00ACB33D  |. E8 CE02FBFF     CALL SpinTire.00A7B610
		*/
		byte Bytes[]=	{0x50,0x88,0000,0x80,0x04,0x00,0x00,0xe8};
		byte Mask[]=	{0xF0,0xFF,0000,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[WriteTrailerCanDetach].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	
		//After 2 references to "StopSignals", or just find what reads bTrailerCanDetach
		/*00ACEA2B   . E8 104B93FF        CALL SpinTire.00403540
		00ACEA30   . 51                 PUSH ECX
		00ACEA31   . 8BCE               MOV ECX,ESI
		00ACEA33   . D91C24             FSTP DWORD PTR [ESP]
		00ACEA36   . E8 8506C9FF        CALL SpinTire.0075F0C0
		00ACEA3B   . E9 FD000000        JMP SpinTire.00ACEB3D
		00ACEA40   > 8BCE               MOV ECX,ESI
		00ACEA42   . E8 C906C9FF        CALL SpinTire.0075F110
		00ACEA47   . 80BB 80040000 00   CMP BYTE PTR [EBX+480],0  <-hook from here;  check TrailerCanDetachFlag
		00ACEA4E   . 74 08              JE SHORT SpinTire.00ACEA58  |             ;  apparently ebx+481-483 are not used (file wide no ref)
		00ACEA50   . D905 0838C300      FLD DWORD PTR [C33808]      |              ;  detaching force
		00ACEA56   . EB 02              JMP SHORT SpinTire.00ACEA5A |
		00ACEA58   > D9EE               FLDZ					  <-to here, included. You want to change the BreakOffTreshold (float) parameter
		00ACEA5A   > 8BB3 60040000      MOV ESI,DWORD PTR [EBX+460]	//passed to HandleMalleableAndBreakableConstrain.
		00ACEA60   . D95C24 24          FSTP DWORD PTR [ESP+24]
		00ACEA64   . D9E8               FLD1
		...
		00ACEA7D   . E8 6E520B00        CALL <SpinTire.HandleMalleableAndBreakab>

		*/
		byte Bytes[]=	{0x80,0xBB,0x80,0x04,0x00,0x00,0x00,0x74};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[ReadTrailerCanDetach].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//find areference to "IsLinkedSteering"
		/*
		00AB574C   . B8 08DDC100       MOV EAX,SpinTire.00C1DD08    ;  ASCII "IsLinkedSteering"
		00AB5751   . E8 3A3CA6FF       CALL <SpinTire.GetXMLBool>
		00AB5756   . 83C4 08           ADD ESP,8
		00AB5759   . 84C0              TEST AL,AL
		00AB575B   . 74 09             JE SHORT SpinTire.00AB5766
		00AB575D   . C643 48 01        MOV BYTE PTR [EBX+48],1
		00AB5761   . E9 19010000       JMP SpinTire.00AB587F		<-place your hook at the destination of this jump
		*/
		byte Bytes[]=	{0x83,0xc4,0x08,0x8b,0x84,0x24,0000,0000,0x00,0000,0x2b,0x84,0x24};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0000,0xFF,0000,0xFF,0xFF,0xFF};
		UniqueSignatures[DoneReadingIsLinkedSteering].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//2 pages after a double reference to "Headlight"
		//and near the end of the code that reads IsLinkedSteering when steering
		//(the [EBX+48] from the DoneReadingIsLinkedSteering signature comment)
		/*
		00AF742B   . E8 7010F8FF       CALL SpinTire.00A784A0
		00AF7430   . 8B4B 04           MOV ECX,DWORD PTR [EBX+4]
		00AF7433   . D981 E0030000     FLD DWORD PTR [ECX+3E0]
		00AF7439   . 83C4 0C           ADD ESP,0C
		00AF743C   . DC1D 7080C300     FCOMP QWORD PTR [C38070]
		00AF7442   . DFE0              FSTSW AX
		00AF7444   . F6C4 41           TEST AH,41
		00AF7447   . 74 14             JE SHORT SpinTire.00AF745D
		00AF7449   . 8A81 FC030000     MOV AL,BYTE PTR [ECX+3FC]
		00AF744F   . 24 01             AND AL,1                                 ;  check if engine is running
		00AF7451   . 74 0A             JE SHORT SpinTire.00AF745D
		00AF7453   . C64424 3F 01      MOV BYTE PTR [ESP+3F],1
		00AF7458   . C64424 3E 01      MOV BYTE PTR [ESP+3E],1                  ;  mark to play rev up sound
		00AF745D   > 8B86 30010000     MOV EAX,DWORD PTR [ESI+130]
		00AF7463   . 2B86 2C010000     SUB EAX,DWORD PTR [ESI+12C]
		00AF7469   . 8B8C24 88000000   MOV ECX,DWORD PTR [ESP+88]
		00AF7470   . 41                INC ECX
		00AF7471   . C1F8 02           SAR EAX,2
		00AF7474   . 898C24 88000000   MOV DWORD PTR [ESP+88],ECX
		00AF747B   . 3BC8              CMP ECX,EAX
		00AF747D   .^0F82 FDFEFFFF     JB SpinTire.00AF7380
		*/
		byte Bytes[]=	{0x8A,0x81,0xFC,0x03,0x00,0x00,0x24,0x01,0x74,0000,0xc6,0x44,0x24,0000,0x01};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0000,0xFF,0xFF,0xFF,0000,0xFF};
		UniqueSignatures[CheckEngineStateForLinkedSteeringSound].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//a large fuction that just creates a new texture, Set/Gets a lot of render states
		//and sampler states, and called DrawPrimitiveUP (via subfunction) to copy one texture to another
		/*
		00AA8E1A  |. 68 71020000    PUSH 271                                 ; /n = 271 (625.)
		00AA8E1F  |. 6A 00          PUSH 0                                   ; |c = 00
		00AA8E21  |. 50             PUSH EAX                                 ; |s
		00AA8E22  |. 8985 C4000000  MOV DWORD PTR [EBP+C4],EAX               ; |
		00AA8E28  |. E8 F77BE0FF    CALL <JMP.&MSVCR90.memset>               ; \memset
		00AA8E2D  |. 8B4424 78      MOV EAX,DWORD PTR [ESP+78]
		00AA8E31  |. 8B40 14        MOV EAX,DWORD PTR [EAX+14]
		00AA8E34  |. 8B48 04        MOV ECX,DWORD PTR [EAX+4]
		00AA8E37  |. 0FAF8C24 80000>IMUL ECX,DWORD PTR [ESP+80]
		00AA8E3F  |. 034C24 7C      ADD ECX,DWORD PTR [ESP+7C]
		00AA8E43  |. 8B10           MOV EDX,DWORD PTR [EAX]
		00AA8E45  |. 6BC9 58        IMUL ECX,ECX,58
		00AA8E48  |. 8B7C11 08      MOV EDI,DWORD PTR [ECX+EDX+8]
		00AA8E4C  |. 83C4 0C        ADD ESP,0C
		00AA8E4F  |. 68 0CE7CA00    PUSH SpinTire.00CAE70C                   ; /pCriticalSection = SpinTire.00CAE70C
		00AA8E54  |. C74424 70 0CE7>MOV DWORD PTR [ESP+70],SpinTire.00CAE70C ; |
		00AA8E5C  |. FF15 7CD0BC00  CALL DWORD PTR [<&KERNEL32.EnterCritical>; \EnterCriticalSection
		00AA8E62  |. 6A 00          PUSH 0                                   ;  no shared handle
		00AA8E64  |. 55             PUSH EBP                                 ;  & &Texture
		00AA8E65  |. 6A 00          PUSH 0                                   ;  pool default
		00AA8E67     6A 15          PUSH 15                                  ;  D3DFMT_A8R8G8B8
		00AA8E69  |. 6A 01          PUSH 1                                   ;  D3DUSAGE_RENDERTARGET
		00AA8E6B  |. C74424 78 0100>MOV DWORD PTR [ESP+78],1
		00AA8E73  |. A1 54C0CA00    MOV EAX,DWORD PTR [CAC054]
		00AA8E78  |. 6A 01          PUSH 1                                   ;  levels
		00AA8E7A  |. 68 80000000    PUSH 80                                  ;  height
		00AA8E7F  |. C745 00 000000>MOV DWORD PTR [EBP],0
		00AA8E86  |. 8B08           MOV ECX,DWORD PTR [EAX]
		00AA8E88  |. 8B51 5C        MOV EDX,DWORD PTR [ECX+5C]	<-signature
		00AA8E8B  |. 68 80000000    PUSH 80                                  ;  width
		00AA8E90  |. 50             PUSH EAX
		00AA8E91  |. FFD2           CALL EDX                                 ;  IDirect3DDevice9::CreateTexture
		00AA8E93  |. A3 48DCC800    MOV DWORD PTR [C8DC48],EAX	<-hook
		00AA8E98  |. A1 54C0CA00    MOV EAX,DWORD PTR [CAC054]
		00AA8E9D  |. 8B08           MOV ECX,DWORD PTR [EAX]
		00AA8E9F  |. 8B91 A4000000  MOV EDX,DWORD PTR [ECX+A4]
		00AA8EA5     50             PUSH EAX
		00AA8EA6     FFD2           CALL EDX                                 ;  IDirect3DDevice9::BeginScene
		00AA8EA8  |. A3 48DCC800    MOV DWORD PTR [C8DC48],EAX
		00AA8EAD  |. 85C0           TEST EAX,EAX
		00AA8EAF     0F8C 58020000  JL SpinTire.00AA910D	<-jumps to EOF (leave critical section)
		00AA8EB5  |. 8B45 00        MOV EAX,DWORD PTR [EBP]
		*/
		byte Bytes[]=	{0x5c,0x68,0x80,0x00,0x00,0x00,0x50};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CloneMudHeightmap].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//there are many references to this function, one of them is:
		/*
		00B32D15  |. 68 10ECC200    PUSH spintire.00C2EC10                   ;  ASCII "DayTimeStates"
		00B32D1A  |. 8D7424 14      LEA ESI,DWORD PTR [ESP+14]
		00B32D1E  |. E8 DD3FF8FF    CALL spintire.00AB6D00
		00B32D23  |. 83C4 04        ADD ESP,4
		00B32D26  |. 837C24 10 00   CMP DWORD PTR [ESP+10],0
		00B32D2B  |. 75 0F          JNZ SHORT spintire.00B32D3C
		00B32D2D  |. 68 20ECC200    PUSH spintire.00C2EC20                   ;  ASCII "Game| Cant load day time states, missing node"
		00B32D32  |. E8 E91CF5FF    CALL spintire.00A84A20
		00B32D37  |. E9 C8040000    JMP spintire.00B33204
		00B32D3C  |> E8 DF0390FF    CALL spintire.00433120		<-you want this  00433120
		00B32D41  |. 8B00           MOV EAX,DWORD PTR [EAX]		<-signature
		00B32D43  |. 8B88 8C020000  MOV ECX,DWORD PTR [EAX+28C]
		00B32D49  |. 2B88 88020000  SUB ECX,DWORD PTR [EAX+288]
		*/
		byte Bytes[]=	{0x8B,0x00,0x8B,0x88,0x8C,0x02,0x00,0x00};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[GameManagerAccessorLocator].Set(Bytes,Mask,sizeof(Bytes));
	}
	
	{	//there are many references to this function, one of them is:
		/* code extract
		00B5F914   > 3BC3           CMP EAX,EBX
		00B5F916   . 7E 04          JLE SHORT spintire.00B5F91C
		00B5F918   . 895C24 14      MOV DWORD PTR [ESP+14],EBX
		00B5F91C   > 8B4424 10      MOV EAX,DWORD PTR [ESP+10]
		00B5F920   . 8B80 BC000000  MOV EAX,DWORD PTR [EAX+BC]	<-grab this eax, do you own processing
		00B5F926   . 8B08           MOV ECX,DWORD PTR [EAX]		|-nop all that
		00B5F928   . 6A 00          PUSH 0						|
		00B5F92A   . 6A 00          PUSH 0						|
		00B5F92C   . 8D5424 3C      LEA EDX,DWORD PTR [ESP+3C]	|
		00B5F930   . 52             PUSH EDX					|
		00B5F931   . 6A 00          PUSH 0						|
		00B5F933   . 50             PUSH EAX					|
		00B5F934   . 8B41 4C        MOV EAX,DWORD PTR [ECX+4C]	|
		00B5F937   . FFD0           CALL EAX                    |            ;  texture.lockrect
		00B5F939   . 3B7C24 14      CMP EDI,DWORD PTR [ESP+14]	|
		00B5F93D   . A3 48DCC800    MOV DWORD PTR [C8DC48],EAX  |             ;  store d3d return code
		00B5F942   . 897C24 20      MOV DWORD PTR [ESP+20],EDI	<-keep
		00B5F946   . 0F8F EE000000  JG spintire.00B5FA3A		<-force this jump
		00B5F94C   . D9E8           FLD1
		*/

		/* called from 6 places, like for example:
		00B6A84D   . 8B4D 08        MOV ECX,DWORD PTR [EBP+8]
		00B6A850   . 57             PUSH EDI                                 ;  cloak std::vector at [[0CAE988]+0c]+110
		00B6A851   . 52             PUSH EDX                                 ;  probably &CloakToRemove
		00B6A852   . 50             PUSH EAX
		00B6A853   . E8 784FFFFF    CALL spintire.00B5F7D0		<-inside this function
		00B6A858   . D9EE           FLDZ
		00B6A85A   . 57             PUSH EDI                                 ; /Arg4
		00B6A85B   . 51             PUSH ECX                                 ; |Arg3
		00B6A85C   . 8D8C24 1801000>LEA ECX,DWORD PTR [ESP+118]              ; |
		00B6A863   . D91C24         FSTP DWORD PTR [ESP]                     ; |
		00B6A866   . 51             PUSH ECX                                 ; |Arg2
		00B6A867   . 68 F0FDC200    PUSH spintire.00C2FDF0                   ; |Arg1 = 00C2FDF0 ASCII "game/uncloaking"
		00B6A86C   . E8 9FADF6FF    CALL spintire.00AD5610                   ; \SpinTire.00AD5610
		00B6A871   . A1 ACBFCA00    MOV EAX,DWORD PTR [CABFAC]
		*/

		byte Bytes[]=	{0x8b,0x80,0000,0000,0x00,0x00,0x8B,0x08,0x6A,0x00,0x6A,0x00};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[LockMinimapFogOfWarTexture].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//there are many references to this function, one of them is:
		/* code extract
		00B67070  |. 85C0           |TEST EAX,EAX                            ;  Switch (cases 0..6)
		00B67072  |. 0F84 2C020000  |JE spintire.00B672A4
		00B67078  |. 83F8 01        |CMP EAX,1
		00B6707B  |. 0F84 23020000  |JE spintire.00B672A4
		00B67081  |. 83F8 02        |CMP EAX,2
		00B67084  |. 0F84 1A020000  |JE spintire.00B672A4
		00B6708A  |. 83F8 03        |CMP EAX,3
		00B6708D  |. 0F84 11020000  |JE spintire.00B672A4
		00B67093  |. 83F8 04        |CMP EAX,4
		00B67096  |. 0F85 D8000000  |JNZ spintire.00B67174
		00B6709C  |. E8 2FC68AFF    |CALL spintire.004136D0  <-grab this     ;  if it's a cloak; Case 4 of switch 00B67070
		00B670A1  |. 8B00           |MOV EAX,DWORD PTR [EAX]
		00B670A3  |. 56             |PUSH ESI                                ; /Arg1
		00B670A4  |. E8 174B8AFF    |CALL spintire.0040BBC0  <-and this      ; \SpinTire.0040BBC0
		00B670A9  |. 84C0           |TEST AL,AL
		00B670AB     0F85 7F140000  JNZ spintire.00B68530    <-signature     ;  jump to prevent rendering cloaks
		00B670B1  |. 8D7E 04        |LEA EDI,DWORD PTR [ESI+4]
		00B670B4  |. 6A 00          |PUSH 0                                  ; /Arg1 = 00000000
		00B670B6  |. 8BCF           |MOV ECX,EDI                             ; |
		00B670B8  |. 8BC3           |MOV EAX,EBX                             ; |
		00B670BA  |. 89B424 7C05000>|MOV DWORD PTR [ESP+57C],ESI             ; |
		00B670C1  |. E8 7A4E8AFF    |CALL spintire.0040BF40                  ; \SpinTire.0040BF40
		*/

		byte Bytes[]=	{0x0f,0x85,0000,0000,0x00,0x00,0x8D,0x7E,0x04,0x6A,0x00};
		byte Mask[]=	{0xFF,0xFF,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[CheckIfCloakIsActive].Set(Bytes,Mask,sizeof(Bytes));
	}

	{	//at the beginning of the function that references "game_starting_engine" and "HeadLight"x2
		//there should be a call to D3DXMatrixInverse, hook it
		/*
		00AF4D9F   . 0F84 7E290000  JE SpinTire.00AF7723                                         ;  jump to unstick hud from screen
		00AF4DA5   . 8D8424 A800000>LEA EAX,DWORD PTR [ESP+A8]                                   ;  where to store the std::vector of constraints
		00AF4DAC   . 50             PUSH EAX
		00AF4DAD   . E8 BEAE91FF    CALL SpinTire.0040FC70                                       ;  std::vector<constraint bucket*> constructor
		00AF4DB2   . C78424 CC01000>MOV DWORD PTR [ESP+1CC],0
		00AF4DBD   . 8B4B 04        MOV ECX,DWORD PTR [EBX+4]                                    ;  ecx=HUDData.TruckChassis
		00AF4DC0   . 6A 01          PUSH 1
		00AF4DC2   . 8D8424 AC00000>LEA EAX,DWORD PTR [ESP+AC]                                   ;  eax=&std::vector to populate
		00AF4DC9   . E8 92FDFCFF    CALL SpinTire.00AC4B60                                       ;  populate the above vector
		00AF4DCE   . 8B53 04        MOV EDX,DWORD PTR [EBX+4]
		00AF4DD1   . 8D8C24 C000000>LEA ECX,DWORD PTR [ESP+C0]
		00AF4DD8   . 51             PUSH ECX
		00AF4DD9   . 52             PUSH EDX
		00AF4DDA   . E8 410CFDFF    CALL SpinTire.00AC5A20
		00AF4DDF   . 8D8424 C000000>LEA EAX,DWORD PTR [ESP+C0]
		00AF4DE6   . 50             PUSH EAX
		00AF4DE7   . 6A 00          PUSH 0
		00AF4DE9   . 8D8C24 8801000>LEA ECX,DWORD PTR [ESP+188]
		00AF4DF0   . 51             PUSH ECX
		00AF4DF1   . E8 1C5FA9FF    CALL <JMP.&d3dx9_43.D3DXMatrixInverse> <- signature and hook point		
		*/

		byte Bytes[]=	{0xe8,0000,0000,0000,0000,0x8b,0x53,0x04,0x8b,0x42,0x0c};
		byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[InverseMatrixInHUDFunction].Set(Bytes,Mask,sizeof(Bytes));
	}
	
	{	//at the end of the constraint parser, after "Havok| Cant load constraint: unrecognized type."
		//and "Name"
		/*
		00B861B4   . 68 442FC100    PUSH SpinTire.00C12F44                                       ;  ASCII "Name"
		00B861B9   . 8BC3           MOV EAX,EBX
		00B861BB   . E8 8051EFFF    CALL <SpinTire.GetXMLString>
		00B861C0   . 83C4 04        ADD ESP,4
		00B861C3   . 85C0           TEST EAX,EAX
		00B861C5   . 74 65          JE SHORT SpinTire.00B8622C
		00B861C7   . 50             PUSH EAX
		00B861C8   . 8D8C24 1801000>LEA ECX,DWORD PTR [ESP+118]
		00B861CF   . FF15 1CD2BC00  CALL DWORD PTR [<&MSVCP90.std::basic_string<char,std::char_t>;  MSVCP90.std::basic_string<char,std::char_traits<char>,std::allocator<char> >::basic_string<char,std::char_traits<char>,std::allocator<char> >
		00B861D5   . 8D8424 1401000>LEA EAX,DWORD PTR [ESP+114]
		00B861DC   . C78424 3C01000>MOV DWORD PTR [ESP+13C],5
		00B861E7   . 8B4E 28        MOV ECX,DWORD PTR [ESI+28]		<-signature
		00B861EA   . 50             PUSH EAX                                                     ; /Arg2
		00B861EB   . 51             PUSH ECX                                                     ; |Arg1
		00B861EC   . E8 7F8F8EFF    CALL <SpinTire.std::vector<std::string>::push_back>  <-hook it
		*/

		byte Bytes[]=	{0x8B,0x4E,0x28,0x50,0x51,0xe8};
		byte Mask[]=	{0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
		UniqueSignatures[StoreContraintName].Set(Bytes,Mask,sizeof(Bytes));
	}
}

Hooker::~Hooker()
{
	delete [] UniqueSignatures;
	delete [] TimebombSignatures;

	for (std::list<HMODULE>::iterator i=Plugins.begin(); i!=Plugins.end();i++)
	{
		FreeLibrary(*i);
	}
	Plugins.clear();

	free(GlobalSettings::d3d9DllOverride); //was allocated with strdup
}

//parses configuration options related to the wheelmass bug
void Hooker::ParseConfig_WheelmassBug(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("WheelmassBug");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();

		if (!strcmp(TokenName,"SuspensionFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SuspensionFix=true;
			}
		}
		else if (!strcmp(TokenName,"TireDeformationFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::TireDeformationFix=true;
			}
		}
		else if (!strcmp(TokenName,"MudSinkingFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::MudSinkingFix=true;
			}
		}
		else if (!strcmp(TokenName,"MaxDeformationPressure"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::MaxDeformationPressure);
		}
	}
}

void Hooker::ParseConfig_Winch(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Winch");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();

		if (!strcmp(TokenName,"BatteryPoweredWinch"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::BatteryPoweredWinch=true;
			}
		}
		else if (!strcmp(TokenName,"ReleaseWinchKey"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::ReleaseWinchKey);
		}
		else if (!strcmp(TokenName,"WinchRange"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::WinchingRange);
		}
	}
}

//parses configuration options related to support for extra XML tokens
void Hooker::ParseConfig_Modding(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Modding");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();

		if (!strcmp(TokenName,"UnpresetWheels"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UnpresetWheels=true;
			}
		}
		else if (!strcmp(TokenName,"SupportSkidSteering"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SupportSkidSteering=true;
			}
		}
		else if (!strcmp(TokenName,"UnclampedXML"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UnclampedXML=true;
			}
		}
		else if (!strcmp(TokenName,"UnclampedManualLoads"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UnclampedManualLoads=true;
			}
		}
		else if (!strcmp(TokenName,"SuspensionTunableInWheelsSets"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SuspensionTunableInWheelsSets=true;
			}
		}
		else if (!strcmp(TokenName,"ExtraAddonProperties") ||
				 !strcmp(TokenName,"AddonsCanOverrideDamageSensation")) //depreciated naming
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::ExtraAddonProperties=true;
			}
		}
		else if (!strcmp(TokenName,"SupportAdditionalConstraints"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SupportAdditionalConstraints=true;
			}
		}	
		else if (!strcmp(TokenName,"SupportCustomShafts"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SupportCustomShafts=true;
			}
		}
		else if (!strcmp(TokenName,"OrientableShafts"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::OrientableShafts=true;
			}
		}
		else if (!strcmp(TokenName,"SupportSilentLinkedSteering"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::SupportSilentLinkedSteering=true;
			}
		}
		else if (!strcmp(TokenName,"WheelsetsCanRequireAddons"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::WheelsetsCanRequireAddons=true;
			}
		}
		else if (!strcmp(TokenName,"MapsCanOverrideCloakMesh"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::MapsCanOverrideCloakMesh=true;
			}
		}		
		else if (!strcmp(TokenName,"STPControlledConstraints"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::STPControlledConstraints=true;

				//if STPControlledConstraints are enabled, parse all
				//<ControlledConstraint IncreaseKey="1" DecreaseKey="2" />
				//under the Modding section
				xml_node<>* ControlledConstraintDefinition = node->first_node("ConstraintController");
				while(ControlledConstraintDefinition)
				{
					int IncreaseKey=-1;
					int DecreaseKey=-1;
					int ResetKey=-1;
					xml_attribute<>* Key;
					Key=ControlledConstraintDefinition->first_attribute("IncreaseKey");
					if (Key)
					{
						sscanf(Key->value(),"%d",&IncreaseKey);
					}
					Key=ControlledConstraintDefinition->first_attribute("DecreaseKey");
					if (Key)
					{
						sscanf(Key->value(),"%d",&DecreaseKey);
					}
					Key=ControlledConstraintDefinition->first_attribute("ResetKey");
					if (Key)
					{
						sscanf(Key->value(),"%d",&ResetKey);
					}
					STPConstraintController::Get()->AddController(IncreaseKey,DecreaseKey,ResetKey);
					ControlledConstraintDefinition=ControlledConstraintDefinition->next_sibling("ConstraintController");
				}
				if (STPConstraintController::Get()->GetControllerCount()<1)
				{	//STPControlledConstraints was enabled but no keys were defined, so the feature
					//cannot work, disable it.
					GlobalSettings::STPControlledConstraints=false;
				}
			}
		}
		else if (!strcmp(TokenName,"CustomizableTrailerBreakoffForce"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::CustomizableTrailerBreakoffForce=true;
			}
		}
		
	}
}

//parses configuration options that do not fall into any other category
void Hooker::ParseConfig_Misc(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Misc");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"AllowCustomObjectsInMP"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AllowCustomObjectsInMP=true;
			}
		}
		else if (!strcmp(TokenName,"DisableSuspensionDamage"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::DisableSuspensionDamage=true;
			}
		}
		else if (!strcmp(TokenName,"DifflockAutoEngageFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::DifflockAutoEngageFix=true;
			}
		}
		else if (!strcmp(TokenName,"ShifterReleaseFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::ShifterReleaseFix=true;
			}
		}
		else if (!strcmp(TokenName,"TrailerLoadFix"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::TrailerLoadFix=true;
			}
		}
		else if (!strcmp(TokenName,"HavokBufferSize"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::HavokBufferSize);
		}
		else if (!strcmp(TokenName,"FasterFileAccess"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::FasterFileAccess=true;
			}
		}

		
	}
}

//parses configuration options related to graphics
void Hooker::ParseConfig_Graphics(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Graphics");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"DisableDOF"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::DisableDOF=true;
			}
		}
		else if (!strcmp(TokenName,"RenderDistanceIncrease"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::RenderDistanceIncrease);
			if (GlobalSettings::RenderDistanceIncrease>255)
			{
				GlobalSettings::RenderDistanceIncrease=255;
			}
		}
		else if (!strcmp(TokenName,"IncreaseTerrainLOD"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::IncreaseTerrainLOD=true;
			}
		}
		else if (!strcmp(TokenName,"FOV"))
		{
			float Temp;
			sscanf(TokenValue,"%f",&Temp);
			GlobalSettings::FOV=Temp*(float)M_PI/360.0f;
			
		}
		else if (!strcmp(TokenName,"AspectRatio"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::AspectRatio);				
		}
		else if (!strcmp(TokenName,"NoVisualDamage"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoVisualDamage=true;
			};				
		}
		else if (!strcmp(TokenName,"FreezeTimeOfDayAt"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::FreezeTimeOfDayAt);
		}
	}
}

//parses configuration options related to the HUD
void Hooker::ParseConfig_HUD(xml_node<>* ConfigNode)
{
		xml_node<> *node = ConfigNode->first_node("HUD");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"HideHUD_SteamInfo"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::HideHUD_SteamInfo=true;
			};				
		}
		else if (!strcmp(TokenName,"HideHUD_3dUI"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::HideHUD_3dUI=true;
			};				
		}
	}
}

//parses configuration options related to the camera
void Hooker::ParseConfig_Camera(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Camera");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"NoCabinCameraReset"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoCabinCameraReset=true;
			}
		}
		else if (!strcmp(TokenName,"UncapZoom"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UncapZoom=true;
			}
		}
		else if (!strcmp(TokenName,"UncapX360Zoom"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UncapX360Zoom=true;
			}
		}
		else if (!strcmp(TokenName,"NoAutoZoom"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoAutoZoom=true;
			}
		}
		else if (!strcmp(TokenName,"NoAdvancedAutoZoom"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoAdvancedAutoZoom=true;
			}
		}
		else if (!strcmp(TokenName,"NoAutoZoomOnStations"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoAutoZoomOnStations=true;
			}
		}
		else if (!strcmp(TokenName,"CabinCameraMaxAngle"))
		{
			float Temp;
			sscanf(TokenValue,"%f",&Temp);
			GlobalSettings::CabinCameraMaxAngle=Temp*(float)M_PI/180.0f;
		}
		else if (!strcmp(TokenName,"RightSideCameraKey"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::RightSideCameraKey);
		}
		else if (!strcmp(TokenName,"DetachCameraFromSteering"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::DetachCameraFromSteering=true;
			}
		}
	}
}

//parses configuration options related to the camera
void Hooker::ParseConfig_BugFixes(xml_node<>* ConfigNode)
{
	xml_node<>* node = ConfigNode->first_node("BugFixes");	
	if (!node)
	{
		return;
	}
	ParseConfig_WheelmassBug(node);

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"DPIIndependentCursorSize"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::DPIIndependentCursorSize);
		}
		else if (!strcmp(TokenName,"HandleCursorWithoutDirectX"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::HandleCursorWithoutDirectX=true;
			}
		}
		else if (!strcmp(TokenName,"FixSunkenMud"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::FixSunkenMud=true;
			}
		}
	}
}

//parses configuration options related to the new game menus
void Hooker::ParseConfig_ExternalMinimap(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("ExternalMinimap");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"EnableMinimap"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::MinimapEnabled=true;
			}
		}
		else if (!strcmp(TokenName,"MinimapPositionX"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::MinimapXPosition);
		}
		else if (!strcmp(TokenName,"MinimapPositionY"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::MinimapYPosition);
		}
		else if (!strcmp(TokenName,"MinimapHeight"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::MinimapHeight);
		}
		else if (!strcmp(TokenName,"MinimapWidth"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::MinimapWidth);
		}
		else if (!strcmp(TokenName,"MinimapMarkerSize"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::MinimapMarkerSize);
		}
	}
}

//parses configuration options related to the new game menus
void Hooker::ParseConfig_NewGame(xml_node<>* ConfigNode)
{
	xml_node<> *NewGameNode = ConfigNode->first_node("NewGameOptions");
	if (!NewGameNode)
	{
		return;
	}

	for (xml_attribute<> *attr = NewGameNode->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"DisableCloaks"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::DisableCloaks=true;
			}
		}
		else if (!strcmp(TokenName,"ShowLockedTrucksAtSelectionScreen"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::ShowLockedTrucksAtSelectionScreen=true;
			}
		}
		else if (!strcmp(TokenName,"AlternateTruckMenu"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AlternateTruckMenu=true;

				//the truck menu also requires the map menu to work
				//(to reset addon overrides when changing the map)
				GlobalSettings::AlternateMapMenu=true;
			}
		}
		else if (!strcmp(TokenName,"AlternateMapMenu"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AlternateMapMenu=true;
			}
		}

		
	}

	ParseSTPListCategories(NewGameNode,"TruckGroup",&GlobalSettings::TruckCategories);
	ParseSTPListCategories(NewGameNode,"MapGroup",&GlobalSettings::MapCategories);
}

void Hooker::ParseSTPListCategories(rapidxml::xml_node<>* RootNode,const char* CategoryName, STPListCategories* GroupList)
{
	if (!RootNode || !GroupList || !CategoryName)
	{
		return; //invalid input
	}

	rapidxml::xml_node<>* ConfigNode=RootNode->first_node(CategoryName);
	while (ConfigNode)
	{
		xml_attribute<>* NameAttribute = ConfigNode->first_attribute("Name");
		xml_attribute<>* KeywordAttribute = ConfigNode->first_attribute("Keyword");
		if (NameAttribute && KeywordAttribute)
		{
			//add a dummy entry that has no keywords yet, this reduces the number of
			//copy operation
			GroupList->push_back(STPListCategory());
			STPListCategory& Entry=GroupList->back();
			Entry.SetName(NameAttribute->value());
			
			while(KeywordAttribute)
			{
				Entry.AddKeyword(KeywordAttribute->value());
				KeywordAttribute=KeywordAttribute->next_attribute("Keyword");
			}
		}

		ConfigNode=ConfigNode->next_sibling(CategoryName);
	}
}
//parses configuration options related to gamechangers
void Hooker::ParseConfig_Gameplay(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("Gameplay");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"HardcoreAutoLogs"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::HardcoreAutoLogs=true;
			}
		}
		else if (!strcmp(TokenName,"NoDamage"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::NoDamage=true;
			}
		}
		else if (!strcmp(TokenName,"UnlimitedFuel"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::UnlimitedFuel=true;
			}
		}
		else if (!strcmp(TokenName,"LoadPointsPerObjective"))
		{
			sscanf(TokenValue,"%d",&GlobalSettings::LoadPointsPerObjective);
		}
		else if (!strcmp(TokenName,"AlternateDifflockDamage"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AlternateDifflockDamage=true;
			}
		}
		else if (!strcmp(TokenName,"AlwaysShowDevMenu"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AlwaysShowDevMenu=true;
			}
		}
		else if (!strcmp(TokenName,"AlwaysAllowSpawnLocators"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::AlwaysAllowSpawnLocators=true;
			}
		}
		else if (!strcmp(TokenName,"LegacyCloaks"))
		{
			if (!_stricmp (TokenValue,"true"))
			{
				GlobalSettings::LegacyCloaks=true;
			}
		}
	}
}

//parses configuration options related to game time
void Hooker::ParseConfig_TimeCycle(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("TimeCycle");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"FreezeTimeOfDayAt"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::FreezeTimeOfDayAt);
		}
		else if (!strcmp(TokenName,"GameDayDurationInRealSeconds"))
		{
			sscanf(TokenValue,"%f",&GlobalSettings::GameDayDurationInRealSeconds);
		}
	}
}

//parses configuration options related other dlls
void Hooker::ParseConfig_3rdPartyCompatibility(xml_node<>* ConfigNode)
{
	xml_node<> *node = ConfigNode->first_node("OtherDlls");
	if (!node)
	{
		return;
	}

	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		const char* TokenName=attr->name();
		const char* TokenValue=attr->value();
		if (!strcmp(TokenName,"LoadDll"))
		{
			HMODULE NewLibrary=LoadLibrary(TokenValue);
			if (NewLibrary)
			{
				Plugins.push_back(NewLibrary);
			}
			
		}
		else if (!strcmp(TokenName,"d3d9DllOverride") && GlobalSettings::d3d9DllOverride==NULL)
		{
			GlobalSettings::d3d9DllOverride=strdup(TokenValue);	
		}
	}
}
void Hooker::HandleTime64Bomb(Address& i)
{
	//force the JS after the SUB EAX,5637C032  
	//and grab the 5637C032+00B74080 to display when the bombs were supposed to blow.
	//00A66586  |. FF15 84B5BE00  CALL DWORD PTR [<&MSVCR90._time64>]      ;  get seconds elapsed since 1/1/1970 as a 64bit integer
	//00A6658C  |. 8B4424 0C      MOV EAX,DWORD PTR [ESP+C]                ;  load time lower dword
	//00A66590  |. 8B4C24 10      MOV ECX,DWORD PTR [ESP+10]               ;  load time upper dword
	//00A66594  |. 8B3D 9CB5BE00  MOV EDI,DWORD PTR [<&MSVCR90.rand>]      ;  MSVCR90.rand
	//00A6659A  |. 83C4 04        ADD ESP,4
	//00A6659D  |. 2D 32C03756    SUB EAX,5637C032                         ;  subtract 2nd of november 2015 (23H00?)
	//00A665A2  |. 83D9 00        SBB ECX,0                                ;  decrement upper 32 bits of time if needed
	//00A665A5  |. 894C24 14      MOV DWORD PTR [ESP+14],ECX
	//00A665A9  |. 78 46          JS SHORT SpinTire.00A665F1               ;  go check GBool if we were earlier than 2/nov/2015
	//00A665AB  |. 7F 07          JG SHORT SpinTire.00A665B4               ;  jump if couple billion years in the future
	//00A665AD  |. 3D 8040B700    CMP EAX, 00B74080			             ;  compare to 2/nov/2015+12009600s = 20/mar/2016
	if (GlobalSettings::DefuseTimebombs)
	{	
		for (;i<TimebombSignatures[Timebomb_time64].mFoundAt+0x20;i++) //JS shouldn't be far for the signature, let's say 32 bytes tops.
		{
			if (*reinterpret_cast<byte*>(i)==0x78)
			{
				MakePageWritable(i,1); //make sure we're allowed to write there
				*reinterpret_cast<byte*>(i)=0xEB; //forced jump
				
				char DateBuffer[100];
				time_t Deadline;
				Deadline= *reinterpret_cast<uint*>(TimebombSignatures[Timebomb_time64].mFoundAt+1); //read the 5637C032
				Deadline+=*reinterpret_cast<uint*>(i+5);//grab the 00B74080 supposed to alwway be 5 bytes after the JS.

				ctime_s( DateBuffer,100,&Deadline);
				LOG("Found timebomb v1 at %08x deadline: %s",TimebombSignatures[Timebomb_time64].mFoundAt,DateBuffer);
				
				return; //go back to scanning current page
			}
			else if (*reinterpret_cast<unsigned short*>(i)==0x880F) //jump if signed over more then 128 bytes
			{
				MakePageWritable(i,2); //make sure we're allowed to write there
				*reinterpret_cast<unsigned short*>(i)=0xE990; //nop then forced jump
				
				char DateBuffer[100];
				time_t Deadline;
				Deadline= *reinterpret_cast<uint*>(TimebombSignatures[Timebomb_time64].mFoundAt+1); //read the 5637C032
				Deadline+=*reinterpret_cast<uint*>(i+9);//grab the 00B74080 supposed to alwway be 9 bytes after the long JS.

				ctime_s( DateBuffer,100,&Deadline);
				LOG("Found timebomb v1 at %08x deadline: %s",TimebombSignatures[Timebomb_time64].mFoundAt,DateBuffer);
				
				return; //go back to scanning current page
			}
		}
	}
}


void Hooker::HandleSystemTimebomb(Address& i)
{
	if (GlobalSettings::DefuseTimebombs)
	{
		for (;i<TimebombSignatures[Timebomb_SystemTime].mFoundAt+0x20;i++) //JLE shouldn't be far for the signature, let's say 32 bytes tops.
		{
			if (*reinterpret_cast<byte*>(i)==0x7E)
			{
				MakePageWritable(i,1); //make sure we're allowed to write there
				*reinterpret_cast<byte*>(i)=0xEB; //forced jump
				
				if (*reinterpret_cast<byte*>(i-5)==0x3D) //3d= first byte of 3D 1FEC0C00    CMP EAX,0CEC1F
				{
					uint Deadline=*reinterpret_cast<uint*>(i-4); //read the 0CEC1F
					uint Month=Deadline/35; //'coz Pavel-months have 35 days
					uint Year=Month/12;
					Month=Month-Year*12;
					uint Day=Deadline%35;

					LOG("Found timebomb v2 at %08x deadline: %d/%d/%d\n",TimebombSignatures[Timebomb_SystemTime].mFoundAt,Year,Month,Day);
				}
				else
				{
					LOG("Found timebomb v2 at %08x deadline: based on a file\n",TimebombSignatures[Timebomb_SystemTime].mFoundAt);
				}
				
				return; //go back to scanning current page
			}
		}
	}
}


void Hooker::DisarmExplosiveBundle(Address& i,const Address FoundAt)
{
	/*disarms constructs similar to this
	00A7B661  |. 8B35 9CB5BE00       MOV ESI,DWORD PTR [<&MSVCR90.rand>]      ;  MSVCR90.rand
	00A7B667  |. FFD6                CALL ESI                                 ; [rand
	00A7B669  |. 99                  CDQ                                      ;  crasher V2
	00A7B66A  |. B9 05050000         MOV ECX,505
	00A7B66F  |. F7F9                IDIV ECX
	00A7B671  |. 83FA 01             CMP EDX,1
	00A7B674  |. 75 22               JNZ SHORT SpinTire.00A7B698		<-force this jump
	00A7B676  |. FFD6                CALL ESI				(also rand)
	00A7B678  |. 99                  CDQ
	00A7B679  |. B9 A4030000         MOV ECX,3A4
	00A7B67E  |. F7F9                IDIV ECX
	00A7B680  |. 52                  PUSH EDX
	00A7B681  |. 6A 00               PUSH 0
	00A7B683  |. FFD6                CALL ESI
	00A7B685  |. 99                  CDQ                                      ; |
	00A7B686  |. B9 09030000         MOV ECX,309                              ; |
	00A7B68B  |. F7F9                IDIV ECX                                 ; |
	00A7B68D  |. 03D5                ADD EDX,EBP                              ; |
	00A7B68F  |. 52                  PUSH EDX                                 ; |s
	00A7B690  |. E8 93F4E3FF         CALL <JMP.&MSVCR90.memset>               ; \memset
	00A7B695  |. 83C4 0C             ADD ESP,0C
	*/
	if (GlobalSettings::DefuseTimebombs)
	{
		for (;i<FoundAt+0x20;i++) //JNZ shouldn't be far for the signature, let's say 32 bytes tops.
		{
			if (*reinterpret_cast<byte*>(i)==0x75)
			{
				MakePageWritable(i,1); //make sure we're allowed to write there
				*reinterpret_cast<byte*>(i)=0xEB; //forced jump
				
				LOG("Found corruptor at %08x\n",FoundAt);
				
				return; //go back to scanning current page
			}
			else if (*reinterpret_cast<unsigned short*>(i)==0x850f)
			{
				MakePageWritable(i,1); //make sure we're allowed to write there
				*reinterpret_cast<unsigned short*>(i)=0xE990; //forced jump
				
				LOG("Found corruptor at %08x\n",FoundAt);
				
				return; //go back to scanning current page
			}
		}
	}
}

int __cdecl SortSignatures( const void* Signature1, const void* Signature2 )
{
	const Signature** Sig1=(const Signature**)(Signature1);
	const Signature** Sig2=(const Signature**)(Signature2);
	return (*Sig1)->mBytes[0] - (*Sig2)->mBytes[0];
}

void Hooker::ScanForSignatures()
{
	//building an array of sorted signatures and stopping testing a given byte if it is < the first byte
	//of a signature gives about 30% reduction in scan times
	//note: 0 is still the most frequent byte in the code section (11%) followed by 1 (6%) and 0xCC (6%)
	Signature** SortedSigArray=new Signature*[UNIQUE_SIGNATURE_COUNT];
	for (uint j=0;j<UNIQUE_SIGNATURE_COUNT;++j)
	{
		SortedSigArray[j]=&UniqueSignatures[j];
	}
	qsort(SortedSigArray,UNIQUE_SIGNATURE_COUNT,sizeof(Signature*),SortSignatures);
	uint FirstNotFoundSignatureIndex=0;

	//+0x1000: the first page is always the PE header, skip it.
	int FoundSignatures=0;
	for (Address i=mExeBase+0x1000;i<mExeBase+mImageSize;i+=0x1000) //for all pages in the exe...
	{
		MEMORY_BASIC_INFORMATION PageInfo;
		VirtualQuery(reinterpret_cast<LPCVOID>(i),&PageInfo,sizeof(PageInfo));
		if (PageInfo.Protect==PAGE_EXECUTE_READ ||
			PageInfo.Protect==PAGE_EXECUTE_READWRITE ||
			PageInfo.Protect==PAGE_EXECUTE_WRITECOPY)
		{
			//scan current page
			const Address EndAddress=reinterpret_cast<Address>(PageInfo.BaseAddress)+PageInfo.RegionSize-1;
			for (;i<EndAddress;i++)	
			{
				for (uint j=FirstNotFoundSignatureIndex;j<UNIQUE_SIGNATURE_COUNT;++j)
				{
					if (!SortedSigArray[j]->mFoundAt) //if the signature hasn't already been found
					{
						Signature::MatchesAt_Return Result=SortedSigArray[j]->MatchesAt<char>(i);
						if (Result==Signature::LOWER)
						{	//abord scanning if this byte is before SortedSigArray[j]'s first byte
							//since SortedSigArray is sorted ascendingly
							break;
						}
						else if (Result==Signature::MATCHED)
						{	//if the byte at i matches the signature
							FoundSignatures++;
							//got all we needed?
							if (FoundSignatures==UNIQUE_SIGNATURE_COUNT)
							{
								FirstNotFoundSignatureIndex=UNIQUE_SIGNATURE_COUNT;
								break;
							}

							//if we found the first signature in SortedSigArray that wasn't found
							//we can tell the scanner to skip scanning some sigs at the beginning
							//Oddly doing the same thing with the last index only makes the scan slower.
							if (j==FirstNotFoundSignatureIndex)
							{
								//find the new first not-found signature
								for (uint k=j+1;k<UNIQUE_SIGNATURE_COUNT;k++)
								{
									if (SortedSigArray[k]->mFoundAt==0)
									{
										FirstNotFoundSignatureIndex=k;
										break;
									}
								}
							}
						}
					}
				}
				if(GlobalSettings::DefuseTimebombs)
				{
					for (uint j=0;j<TIMEBOMB_SIGNATURE_COUNT;++j)
					{
						//if the byte at i matches the signature
						if (TimebombSignatures[j].MatchesAt<char>(i))
						{
							if (j==Timebomb_time64)
							{
								HandleTime64Bomb(i);
							}
							else if (j==Timebomb_SystemTime)
							{
								HandleSystemTimebomb(i);
							}
							else if (j==Crasher || j==Crasher_Long)
							{
								DisarmExplosiveBundle(i,TimebombSignatures[j].mFoundAt);
							}							
						}
					}
				}
			}
		}
		else
		{	//exit if page is not executable
			//this is because the first page we scan is the one that contains all our fixes
			break;
		}
	}

	if (GlobalSettings::DebugMode)
	{
		for (uint j=0;j<UNIQUE_SIGNATURE_COUNT;++j)
		{
			if (UniqueSignatures[j].mFoundAt==0)
			{
				printf("Signature %d is MISSING\n",j);
			}
			else
			{
				printf("Found signature %d at %08x\n",j,UniqueSignatures[j].mFoundAt);
			}
		}
	}

	delete [] SortedSigArray;
}

//hooks installed as soon as the dll is loaded
void Hooker::InstallEarlyHooks()
{
	//import all PHYFS functions
	ImportPHYSFX();

	HMODULE MainExe=GetModuleHandle(NULL);
	MODULEINFO ExeProperties;
	GetModuleInformation(GetCurrentProcess(),MainExe,&ExeProperties,sizeof(MODULEINFO));
	mExeBase=reinterpret_cast<Address>(ExeProperties.lpBaseOfDll);
	mImageSize=ExeProperties.SizeOfImage;

	if (GlobalSettings::DPIIndependentCursorSize>0)
	{
		//rewire LoadCursorW to a stub that calls LoadImage instead
		Address* LoadIconImport=GetImportRef("USER32.dll","LoadCursorW");
		if (LoadIconImport)
		{
			MakePageWritable(reinterpret_cast<Address>(LoadIconImport),4);
			*LoadIconImport=reinterpret_cast<Address>(DPIIndependentLoadCursor);
		}
	}
	if (GlobalSettings::HandleCursorWithoutDirectX)
	{
		////rewire ShowCursor to a stub that always shows windows cursor
		//Address* ShowCursorImport=GetImportRef("USER32.dll","ShowCursor");
		//if (ShowCursorImport)
		//{
		//	MakePageWritable(reinterpret_cast<Address>(ShowCursorImport),4);
		//	*ShowCursorImport=reinterpret_cast<Address>(ShowCursorReplacement);
		//}

		//Prevent SetCursor from doing anything
		Address* SetCursorImport=GetImportRef("USER32.dll","SetCursor");
		if (SetCursorImport)
		{ 
			MakePageWritable(reinterpret_cast<Address>(SetCursorImport),4);
			*SetCursorImport=reinterpret_cast<Address>(SetCursorReplacement);
		}
	}
}

void Hooker::InstallHooks()
{
	if (HooksInstalled==false)
	{
		HooksInstalled=true;

		__int64 tic,tac,freq;
		QueryPerformanceCounter ((LARGE_INTEGER*)&tic);
		ScanForSignatures();
		QueryPerformanceCounter ((LARGE_INTEGER*)&tac);
		QueryPerformanceFrequency ((LARGE_INTEGER*)&freq);
		LOG("+++Scanning took: %I64dms \n",(tac - tic)*1000/freq);

		//////////////////////////////////
		//            common functions
		//////////////////////////////////
		if (GlobalSettings::ExtraAddonProperties	||
			GlobalSettings::WheelsetsCanRequireAddons)
		{
			LOG("Located and hooked IsAddonInstalled\n");
			const int Offset2=-5;
			Address CallDestination=GetCallJumpDestination(UniqueSignatures[AreWeInstallingOrRemovingAddon].mFoundAt+Offset2);
			IsAddonInstalled=reinterpret_cast<bool (__cdecl*)(TruckChassis*,const std::string&,void*,uint)>(CallDestination);

			const int Offset3=Offset2;
			WriteCall(UniqueSignatures[AreWeInstallingOrRemovingAddon].mFoundAt+Offset3,reinterpret_cast<Address>(OnInstallOrUninstallAddon),0);
		}

		if (GlobalSettings::UnpresetWheels	||
			GlobalSettings::SuspensionFix	||
			GlobalSettings::SuspensionTunableInWheelsSets ||
			GlobalSettings::ExtraAddonProperties ||
			GlobalSettings::SupportSkidSteering ||
			GlobalSettings::SupportAdditionalConstraints ||
			GlobalSettings::SupportCustomShafts ||
			GlobalSettings::OrientableShafts ||
			GlobalSettings::WheelsetsCanRequireAddons ||
			GlobalSettings::SupportSilentLinkedSteering ||
			GlobalSettings::STPControlledConstraints)
		{
			LOG("Located GetXmlString\n");
			const uint Offset=-0x1D;
			Address CallDestination=GetCallJumpDestination(UniqueSignatures[GetXmlStringFunc].mFoundAt+Offset);
			GetXmlString_Real=reinterpret_cast<char* (__cdecl*)(char*)>(CallDestination);
		}

		//locate the LoadMesh function
		if (GlobalSettings::MinimapEnabled		||
			GlobalSettings::SupportCustomShafts	||
			GlobalSettings::MapsCanOverrideCloakMesh)
		{
			LOG("Located LoadMesh\n");			
			Address LoadMeshCall;
			{
				Signature Sig;

				byte Bytes[]=	{0x68,0000,0000,0000,0000,0x50,0xE8};
				byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));
				
				LoadMeshCall=ScanForNext(UniqueSignatures[MeshLoader].mFoundAt+UniqueSignatures[MeshLoader].mLength,Sig,0x20);
			}
			const int Offset=6;
			const Address LoadMeshBegin=GetCallJumpDestination(LoadMeshCall+Offset);
			LoadMesh_Real=reinterpret_cast<LoadedMesh* (__stdcall*)(void*,const char*)>(LoadMeshBegin);
		}

		if (GlobalSettings::LegacyCloaks || GlobalSettings::MinimapEnabled)
		{	//tools to access the list of KeyModels* on this map
			const int Offset1=-5;
			GetGameManager=reinterpret_cast<GameManager*& (__stdcall*)()>(GetCallJumpDestination(UniqueSignatures[GameManagerAccessorLocator].mFoundAt+Offset1));
			const int Offset3=-7;
			const int Offset4=-15;
			IsModelHidden=reinterpret_cast<bool (__stdcall*)(const Locator*)>(GetCallJumpDestination(UniqueSignatures[CheckIfCloakIsActive].mFoundAt+Offset3));
			GetUnknownObject_LegacyCloaks=reinterpret_cast<void* (__stdcall*)()>(GetCallJumpDestination(UniqueSignatures[CheckIfCloakIsActive].mFoundAt+Offset4));
		}
		
		//overlay requirements
		if (OverlayEnabled())
		{
			//used in the menu manager's render loop
			const int Offset4=2;
			GameMainRenderFunc=reinterpret_cast<void (__cdecl *)()>(GetCallJumpDestination(UniqueSignatures[MessagPump].mFoundAt+Offset4));
		}
		//////////////////////////////////
		//             tests
		//////////////////////////////////
		//if (GlobalSettings::HightlightTest1)
		//{
		//	//full disable
		//	this->MakePageWritable(0x0B653F4,2);
		//	*reinterpret_cast<unsigned short*>(0x0B653F4)=0xe990;
		//}
		//if (GlobalSettings::HightlightTest2)
		//{
		//	//disables shadow+pause icon+???+some placeholder mesh
		//	this->MakePageWritable(0x0B65450,2);
		//	*reinterpret_cast<unsigned short*>(0x0B65450)=0xe990;
		//}
		//if (GlobalSettings::HightlightTest3)
		//{
		//	//disables steam name+some placeholder mesh
		//	this->MakePageWritable(0x0B6639E,2);
		//	*reinterpret_cast<unsigned short*>(0x0B6639E)=0xe990;
		//}
		//if (GlobalSettings::HightlightTest4)
		//{
		//	//disables ???
		//	this->MakePageWritable(0x0B667C7,2);
		//	*reinterpret_cast<unsigned short*>(0x0B667C7)=0xe990;
		//}

		/*if (true)
		{
			WriteJump(0x079DD29,reinterpret_cast<Address>(OnConstraintAddedToWorld));
		}*/

		//////////////////////////////////
		//             bugfixes
		//////////////////////////////////

		if (GlobalSettings::FixSunkenMud)
		{	
			const int Offset=9;
			WriteCall(UniqueSignatures[CloneMudHeightmap].mFoundAt+Offset,reinterpret_cast<Address>(CopyTextureHook),0x15);
		}

		//////////////////////////////////
		//             Wheelmass bug
		//////////////////////////////////

		if (GlobalSettings::MudSinkingFix)
		{	
			//locate the function we use to access the wheel array
			if (GlobalSettings::DebugMode)
			{
				Address RelativeOffset=*reinterpret_cast<Address*>(UniqueSignatures[GlobalVarsLocator].mFoundAt+8);
				GetUnknownStructure1=reinterpret_cast<UnknownStructure1** (*)()>(RelativeOffset+5+UniqueSignatures[GlobalVarsLocator].mFoundAt+8-1);
			}
			
			//patch the sink-into-mud-Y/N-check
			LOG("Changed mud sinking, MaxDeformationPressure=%e Pa\n",GlobalSettings::MaxDeformationPressure);
			const int Offset=8;	
			WriteCall(	UniqueSignatures[MudSinkingCheck].mFoundAt+Offset,
						reinterpret_cast<Address>(&TruckStaysAboveMud),
						7);
		}
		
		if (GlobalSettings::TireDeformationFix	||
			GlobalSettings::UnpresetWheels ||
			GlobalSettings::SuspensionTunableInWheelsSets)
		{	//patch wheel properties when parsing the xml
			LOG("Hooked wheel loader\n");
			const int Offset=3;			
			WriteCall(	UniqueSignatures[WheelLoading].mFoundAt+Offset,
						reinterpret_cast<Address>(&PatchWheelLoader),
						3);
		}

		if (GlobalSettings::SuspensionFix ||
			GlobalSettings::SuspensionTunableInWheelsSets ||
			GlobalSettings::SupportSkidSteering)
		{	//patch suspension properties when parsing the xml
			LOG("Hooked suspension loader\n");
			const int Offset=0;
			WriteCall(	UniqueSignatures[SuspensionLoading].mFoundAt+Offset,
						reinterpret_cast<Address>(&PatchSuspensionLoader),
						0);
		}
		
		if (GlobalSettings::MinimapEnabled || GlobalSettings::MapsCanOverrideCloakMesh)
		{	//also needs the PHYSFS functions
			//the LoadMesh fuction
			//the function that calculates and applies torque on wheels (to get truck position)
			LOG("Hooked LoadCloakMesh\n");
			const int Offset=0x15;
			WriteCall(UniqueSignatures[LoadCloakMesh].mFoundAt+Offset,reinterpret_cast<Address>(Minimap_Hook),0);
		}

		//////////////////////////////////
		//             extra XML
		//////////////////////////////////
		if(GlobalSettings::CustomizableTrailerBreakoffForce)
		{	//also requires IsAddonInstalled and GetXMLString
			LOG("TrailerBreakOffThreshold token supported\n");
			const int Offset1=1;
			const int Offset2=0;
			WriteCall(UniqueSignatures[WriteTrailerCanDetach].mFoundAt+Offset1,reinterpret_cast<Address>(WriteTrailerDetachingForce),1);
			WriteCall(UniqueSignatures[ReadTrailerCanDetach].mFoundAt+Offset2,reinterpret_cast<Address>(ReadTrailerDetachingForce),14);
		}

		if(GlobalSettings::SupportSilentLinkedSteering)
		{	//also requires IsAddonInstalled and GetXMLString
			LOG("IsLinkedSteering can be silenced\n");
			const int Offset1=3;
			const int Offset2=0;
			WriteCall(UniqueSignatures[DoneReadingIsLinkedSteering].mFoundAt+Offset1,reinterpret_cast<Address>(SetSilentLinkedSteering),2);
			WriteCall(UniqueSignatures[CheckEngineStateForLinkedSteeringSound].mFoundAt+Offset2,reinterpret_cast<Address>(ShouldRevEngineForLinkedSteerConstraint),1);
		}
		if(GlobalSettings::WheelsetsCanRequireAddons)
		{	//also requires IsAddonInstalled and GetXMLString
			LOG("Wheelsets can have RequiredAddons\n");
			const int Offset1=0xB;
			const int Offset2=9;
			
			WriteCall(UniqueSignatures[CreateStdStringForWheelset].mFoundAt+Offset1,reinterpret_cast<Address>(&ReadExtraWheelsSetTokens),1);

			AddWheelsetstonGarageMenu=reinterpret_cast<void (__stdcall*)(void*,const std::wstring&,uint)>( GetCallJumpDestination(UniqueSignatures[ListWheelsetInGarage].mFoundAt+Offset2) );
			WriteCall(UniqueSignatures[ListWheelsetInGarage].mFoundAt+Offset2,reinterpret_cast<Address>(&PopulateWheelsetsInGarageMenu),0);
			
			const int Offset3=8;
			DesiredWheelset=*reinterpret_cast<uint**>(UniqueSignatures[GlobalDesiredWheelsetLocator].mFoundAt+Offset3);
			
		}
		if(GlobalSettings::SupportCustomShafts)
		{
			LOG("Supporting custom driveshafts\n");
			//hook the shaft constructor
			const int Offset1=-5;
			Address HookPlace=UniqueSignatures[LoadShaftMesh].mFoundAt+Offset1;
			ShaftConstructor=reinterpret_cast<void* (__stdcall*)(void*,LoadedMesh*)>(GetCallJumpDestination(HookPlace));
			WriteCall(HookPlace,reinterpret_cast<Address>(&LoadShaftModel),0);
		}

		if(GlobalSettings::OrientableShafts)
		{
			LOG("Driveshaft can be oriented\n");
			const Address CreatAShaftHookPoint=UniqueSignatures[CreateShaft].mFoundAt;
			/*WriteCall(0x0ACB27D,reinterpret_cast<Address>(&NoIdeaShaft),0);
			WriteCall(0x0AC6FAA,reinterpret_cast<Address>(&ApplyShaftTransform),2);*/
			OriginalCreateAShaft=reinterpret_cast<void(__stdcall*)(void*)>(GetCallJumpDestination(CreatAShaftHookPoint));
			WriteCall(CreatAShaftHookPoint,reinterpret_cast<Address>(&CreateAShaft),0);

			const int Offset=4;
			WriteCall(UniqueSignatures[TransformShaft].mFoundAt+Offset ,reinterpret_cast<Address>(&RotateShaftAlongY),0);
		}

		if (GlobalSettings::STPControlledConstraints)
		{
			LOG("Supporting STPControllerConstraints\n");

			const int Offset=5;
			WriteCall(UniqueSignatures[InverseMatrixInHUDFunction].mFoundAt,reinterpret_cast<Address>(DriveExtraControlledConstraints),0);
			WriteCall(UniqueSignatures[StoreContraintName].mFoundAt+Offset,reinterpret_cast<Address>(ParseExtraConstraintParams),0);
		}

		if (GlobalSettings::SupportAdditionalConstraints)
		{
			LOG("Hooking constraint parser\n");
			const int Offset0=14;	//BuildContraintFromXML to default case begin
			const int Offset1=-0x15;	//HavocAllocator sig to function begin
			const int Offset2=-0x21;	//hkpPointToPlaneConstraintData_Constructor sig to function begin
			const int Offset3=-5;		//hkpPointToPlaneConstraintData_SetInBodySpace sig to function begin
			const int Offset4=-0x27;	//from ConstraintFromXML_DefaultCase to the call ConstraintFinalizer
			const int Offset5=-0x19;	//hkpPointToPathConstraintData_Constructor sig to function begin
			const int Offset6=-0x1F;	//hkpPointToPathConstraintData_SetInBodySpace sig to function begin
			const int Offset7=-0x24;	//hkpLinearParametricCurve_Constructor sig to function begin
			const int Offset8=-6;		//hkpLinearParametricCurve_AddPoint sig to function begin
			const int Offset9=-0x17;	//hkpLinearParametricCurve_Constructor sig to vftable ptr 
			const int Offset10=-0x44;	//hkpBallSocketChainData_Constructor sig to function begin
			const int Offset11=-4;		//hkpBallSocketChainData_AddConstraintInfoInBodySpace sig to function begin
			const int Offset12=-0x16;	//hkpConstraintChainInstance_Constructor sig to function begin
			const int Offset13=0;		//hkpBallSocketChainData_AddConstraintInfoInBodySpace sig to function begin


			const Address ConstraintFromXML_DefaultCase=UniqueSignatures[BuildContraintFromXML].mFoundAt+Offset0;
			Address DefaultCaseHook;
			{	
				//find the next push 32bit_constant (0x68 opcode)
				Signature Sig;
				byte Bytes[]=	{0x68};
				byte Mask[]=	{0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));

				DefaultCaseHook = ScanForNext(ConstraintFromXML_DefaultCase,Sig,10);
			}
			const Address ConstraintFromXML_HappyEnd=ConstraintFromXML_DefaultCase-0xC; //where all cases converge, except the default one
			const Address ConstraintFinalizer=GetCallJumpDestination(ConstraintFromXML_DefaultCase+Offset4);

			StoreConstraintParserKeyAddresses(
				UniqueSignatures[HavocAllocator].mFoundAt+Offset1,
				DefaultCaseHook+5,	//+5 to land after the jump we place at _DefaultCase
				ConstraintFromXML_HappyEnd,
				UniqueSignatures[hkpPointToPlaneConstraintData_Constructor].mFoundAt+Offset2,
				UniqueSignatures[hkpPointToPlaneConstraintData_SetInBodySpace].mFoundAt+Offset3,
				UniqueSignatures[hkpPointToPathConstraintData_Constructor].mFoundAt+Offset5,
				UniqueSignatures[hkpPointToPathConstraintData_SetInBodySpace].mFoundAt+Offset6,
				UniqueSignatures[hkpLinearParametricCurve_Constructor].mFoundAt+Offset7,
				UniqueSignatures[hkpLinearParametricCurve_AddPoint].mFoundAt+Offset8,
				UniqueSignatures[hkpBallSocketChainData_Constructor].mFoundAt+Offset10,
				UniqueSignatures[hkpBallSocketChainData_AddConstraintInfoInBodySpace].mFoundAt+Offset11,
				UniqueSignatures[hkpConstraintChainInstance_Constructor].mFoundAt+Offset12,
				UniqueSignatures[hkpConstraintChainInstance_AddEntity].mFoundAt+Offset13,
				GetCallJumpDestination(ConstraintFromXML_HappyEnd),	//the "happy end" starts with a call to remove ref
				ConstraintFinalizer);
			WriteJump(DefaultCaseHook,reinterpret_cast<Address>(&ConstraintParser));
			/*
			00B8615C  |. 57             PUSH EDI
			00B8615D  |. E8 8EDBFFFF    CALL <SpinTire.HandleMalleableAndBreakableConstrain>	<-call ConstraintFinalizer
			00B86162  |. 83C4 0C        ADD ESP,0C
			00B86165  |. 8BCE           MOV ECX,ESI
			00B86167  |. 894424 1C      MOV DWORD PTR [ESP+1C],EAX
			00B8616B  |. E8 6051A0FF    CALL <SpinTire.hkReferencedObject::removeReference(vo>
			00B86170  |. 8B4C24 5C      MOV ECX,DWORD PTR [ESP+5C]
			00B86174  |. 85C9           TEST ECX,ECX
			00B86176  |. 74 05          JE SHORT SpinTire.00B8617D
			00B86178  |> E8 5351A0FF    CALL <SpinTire.hkReferencedObject::removeReference()	<-ConstraintFromXML_HappyEnd
			00B8617D  |> 837C24 1C 00   CMP DWORD PTR [ESP+1C],0
			00B86182  |. 75 27          JNZ SHORT SpinTire.00B861AB
			00B86184  |> 68 142FC100    PUSH SpinTire.00C12F14   <-ConstraintFromXML_DefaultCase  ;  ASCII "Havok| Cant load constraint: unrecognized type."
			00B86189  |. 53             PUSH EBX
			00B8618A  |. E8 3157EFFF    CALL SpinTire.00A7B8C0
			*/

			Address* LinearParametricCurveVFTable = *reinterpret_cast<Address**>(UniqueSignatures[hkpLinearParametricCurve_Constructor].mFoundAt+Offset9);
			//3rd virtual member of hkpLinearParametricCurve is getNearestPoint
			MakePageWritable(reinterpret_cast<Address>(&LinearParametricCurveVFTable[3]),8*sizeof(void*));
			LinearParametricCurveVFTable[3]=reinterpret_cast<Address>(&hkpLinearParametricCurve_getNearestPoint2);
			LinearParametricCurveVFTable[4]=reinterpret_cast<Address>(&hkpLinearParametricCurve_getTangent2);
			//LinearParametricCurveVFTable[7]=reinterpret_cast<Address>(&hkpLinearParametricCurve_getLengthFromStart2);
			LinearParametricCurveVFTable[8]=reinterpret_cast<Address>(&hkpLinearParametricCurve_getBinormal2);


		}
		if (GlobalSettings::ExtraAddonProperties)
		{
			//also requires IsAddonInstalled and GetXMLString
			//make sure that IsAddonInstalled is located before we hook it here.
			LOG("Addons can modify more parameters\n");
			const int Offset1=-5;
			WriteCall(UniqueSignatures[IsAddonChassisFullOcclusion].mFoundAt+Offset1,reinterpret_cast<Address>(&ParseTruckAddonHook),0);
		}


		if (GlobalSettings::SuspensionTunableInWheelsSets)
		{
			//also requires GetXMLString
			LOG("Suspension tweakable in WheelsSets\n");
			const int Offset1=-0x14;
			WriteCall(	UniqueSignatures[CreateStdStringForWheelType].mFoundAt+Offset1,
						reinterpret_cast<Address>(&ReadExtraWheelsTypeTokens),
						1);

			const int Offset2=-0x1C;
			WriteCall(	UniqueSignatures[OpenWheelXML].mFoundAt+Offset2,
						reinterpret_cast<Address>(&RetrieveWheelSetPiggyBack),
						1);

		}
		if (GlobalSettings::SupportSkidSteering || GlobalSettings::MinimapEnabled)
		{
			LOG("Hooking the transmission\n");
			const int Offset1=0x18;
			ApplyTorqueOnWheels_Real=reinterpret_cast<void (_stdcall*) (TruckControls* Truck,float AccelRatio,float FrameTime)>(GetCallJumpDestination(UniqueSignatures[CalculateTorqueToApplyOnWheel].mFoundAt+Offset1));
			WriteCall(UniqueSignatures[CalculateTorqueToApplyOnWheel].mFoundAt+Offset1,reinterpret_cast<Address>(&ApplyTorqueOnWheels),0);
		}
		if (GlobalSettings::UnclampedXML)
		{
			LOG("Unclamped XML tokens\n");

			//the float reader
			const uint Offset1=5;
			MakePageWritable(UniqueSignatures[ClampedFloatReader].mFoundAt+Offset1,2);
			//force jump
			*reinterpret_cast<unsigned short*>(UniqueSignatures[ClampedFloatReader].mFoundAt+Offset1)=0xe990;

			//the int reader
			const uint Offset2=5;
			MakePageWritable(UniqueSignatures[ClampedIntReader].mFoundAt+Offset2,1);
			//force jump
			*reinterpret_cast<byte*>(UniqueSignatures[ClampedIntReader].mFoundAt+Offset2)=0xEB;
		}
		if (GlobalSettings::UnclampedManualLoads)
		{
			LOG("Unclamped ManualLoads token\n");
			//the ManualLoads reader
			const uint Offset3=5;
			MakePageWritable(UniqueSignatures[ClampedManualLoadsReader].mFoundAt+Offset3,1);
			//force jump
			*reinterpret_cast<byte*>(UniqueSignatures[ClampedManualLoadsReader].mFoundAt+Offset3)=0xEB;
		}

		//////////////////////////////////
		//             general annoyances
		//////////////////////////////////
		if (GlobalSettings::FasterFileAccess)
		{
			//when symbolic links are disables, PHYSFS wastes a lot of tile checking if each component
			//of paths are symlinks or not. This involves doing a scan for each subdir using FindFirstFileW
			//then reading files attributes.
			//Allowing symlinks removes all those checks.
			//The altenate truck selection menu displays in 1250ms with FasterFileAccess off
			//and 300ms with FasterFileAccess on.
			PHYSFS_permitSymbolicLinks(1);
		}

		if (GlobalSettings::AlternateTruckMenu)
		{
			LOG("Replaced the truck selection menu\n");
			const int Offset0=5;
			const Address TruckMenuConstructorBegin=GetCallJumpDestination(UniqueSignatures[TruckMenuConstructor].mFoundAt+Offset0);
			WriteJump(TruckMenuConstructorBegin,reinterpret_cast<Address>(DoTruckSelection));

			//applies addon overrides during load screens
			const int Offset1=0;
			WriteCall(UniqueSignatures[ParseSpawnStartupTrucks].mFoundAt+Offset1,reinterpret_cast<Address>(&ChangeStartupAddons),2);

			//functions used by the AssetFile class
			const int Offset2=-5;
			GetFriendlyName_Real=reinterpret_cast<void (__cdecl *)(std::wstring&,const char*,const char*,int)>(GetCallJumpDestination(UniqueSignatures[GetFriendlyNameLocator].mFoundAt+Offset2));

			const int Offset3=8;
			GetWorkshopItem_Real=reinterpret_cast<WorkshopMod* (__cdecl *)(const char*)>(GetCallJumpDestination(UniqueSignatures[GetSourceWorkshopItemLocator].mFoundAt+Offset3));

			//the global std::vector of all addons & trailers known to the game
			const int Offset5=15;
			Address VectorLocation=*reinterpret_cast<Address*>(UniqueSignatures[GlobalAddonArray].mFoundAt+Offset5);
			//the compiler hardcodes an access to the base of the array, but the actual
			//std::vector object starts 0xC bytes earlier
			VectorLocation-=0xC;
			AddonDatabase=reinterpret_cast<std::vector<AddonDefinitionFile>*>(VectorLocation);
			//...and immediately after you have the truck database
			TruckDatabase=reinterpret_cast<std::vector<AssetFile>*>(VectorLocation+sizeof(*AddonDatabase));

			//function to access private data of the mod menu (the one with slots for trucks, 
			//and a button to open the workshop browser)
			const int Offset6=2;
			const Address SaveTruckOverrideCalledFrom=UniqueSignatures[SaveTruckOverrideCall].mFoundAt+Offset6;
			SaveTruckOverride_Real = reinterpret_cast<void (__stdcall*)(const char*)>(GetCallJumpDestination(SaveTruckOverrideCalledFrom));

			//once you have SaveTruckOverride, scan for the previous call and
			//grab its destination
			Signature Sig;
			{
				/*
				00478A3C   . 8BF8            MOV EDI,EAX
				00478A3E   . E8 EDB7FFFF     CALL SpinTire.00474230	<-we want this
				00478A43   . 85FF            TEST EDI,EDI
				00478A45   . 0F84 A7000000   JE SpinTire.00478AF2
				00478A4B   . 57              PUSH EDI
				00478A4C   . 8BF0            MOV ESI,EAX
				00478A4E   . E8 3DBA6800     CALL SpinTire.00B04490 <-SaveTruckOverrideCall
				*/
				byte Bytes[]=	{0xE8,0000,0000,0000,0000,0x85,0000,0x0f,0x84};
				byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0000,0xFF,0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));
			}
			const Address GetModMenuCall=ScanForPrevious(SaveTruckOverrideCalledFrom,Sig,0x20);
			GetModMenu = reinterpret_cast<ModMenuData* (*)(void)>(GetCallJumpDestination(GetModMenuCall));	
		}
		
		if (GlobalSettings::AlternateMapMenu)
		{
			LOG("Replaced the map selection menu\n");
			//map menu hook
			const int Offset7=6;
			const Address MapMenuConstructorBegin=GetCallJumpDestination(UniqueSignatures[MapMenuConstructor].mFoundAt+Offset7);
			WriteJump(MapMenuConstructorBegin,reinterpret_cast<Address>(DoMapSelection));

			//function to access private data of the Map menu (the one with a
			//gigantic preview above the casual/hardcore buttons)
			const int Offset8=2;
			const Address SaveMapSelectionCalledFrom=UniqueSignatures[SaveMapSelectionCall].mFoundAt+Offset8;
			SaveMapSelection = reinterpret_cast<void (__stdcall*)(void*, const char*)>(GetCallJumpDestination(SaveMapSelectionCalledFrom));

			//once you have SaveMapSelection, scan for the previous call and
			//grab its destination
			Signature Sig;
			{
				/*
				0047F342   > E8 4952FFFF    CALL <SpinTire.GetMapMenuData>	<-we want this
				0047F347   . 8B00           MOV EAX,DWORD PTR [EAX]
				0047F349   . 85C0           TEST EAX,EAX
				0047F34B   . 74 05          JE SHORT SpinTire.0047F352
				0047F34D   . 83C0 D8        ADD EAX,-28
				0047F350   . EB 02          JMP SHORT SpinTire.0047F354
				0047F352   > 33C0           XOR EAX,EAX
				0047F354   > 57             PUSH EDI                                 ; /map name
				0047F355   . 50             PUSH EAX                                 ; |map manager
				0047F356   . E8 B5E76700    CALL SpinTire.00AFDB10  <-calls SaveMapSelection
				*/
				byte Bytes[]=	{0xE8,0000,0000,0000,0000,0x8b,0x00,0x85,0xc0};
				byte Mask[]=	{0xFF,0000,0000,0000,0000,0xFF,0xFF,0xFF,0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));
			}
			const Address GetMapMenuCall=ScanForPrevious(SaveMapSelectionCalledFrom,Sig,0x20);
			GetMapMenu_Real = reinterpret_cast<void** (*)(void)>(GetCallJumpDestination(GetMapMenuCall));	
		}

		if (GlobalSettings::DifflockAutoEngageFix)
		{
			LOG("Disabled auto engaging difflock when exitting auto gearbox\n");
			const int Offset=-4;
			const Address PatchLocation=UniqueSignatures[DifflockAutoEngageFix].mFoundAt+Offset;

			MakePageWritable(PatchLocation,4);
			*reinterpret_cast<unsigned int*>(PatchLocation)=0x90909090;
		}

		if (GlobalSettings::ShifterReleaseFix)
		{
			LOG("Applied shifter mod\n");
			const int Offset=-7;
			const int SecondaryOffset=1;
			MakePageWritable(UniqueSignatures[ShifterReleaseFix].mFoundAt+Offset,7+2);
			*reinterpret_cast<unsigned short*>(UniqueSignatures[ShifterReleaseFix].mFoundAt+Offset)=0x05EB; //jmp +5
			*reinterpret_cast<byte*>(UniqueSignatures[ShifterReleaseFix].mFoundAt+SecondaryOffset)=0; //say that we did not release (0 instead of 1).
		}

		if (GlobalSettings::TrailerLoadFix)
		{
			LOG("Applied trailer load\n");
			const uint Offset=0xC;
			MakePageWritable(UniqueSignatures[TrailerLoadFix].mFoundAt+Offset,1);
			*reinterpret_cast<byte*>(UniqueSignatures[TrailerLoadFix].mFoundAt+Offset)=0xEb; //force jump to the piece of code that loads the trailer instead of saying "position your truck correctly".

			const uint Offset2=0x14;
			MakePageWritable(UniqueSignatures[TrailerLoadFix2].mFoundAt+Offset2,1);
			*reinterpret_cast<byte*>(UniqueSignatures[TrailerLoadFix2].mFoundAt+Offset2)=0xEb;
		}

		if (GlobalSettings::HavokBufferSize)
		{
			LOG("Resized Havok's temporary buffer to %d bytes\n",GlobalSettings::HavokBufferSize);

			//grab the function telling us where the memory manager is
			//(again, not entierely sure it's the memory manager)
			const uint Offset=-5;
			Address GetHavokMemoryManager=GetCallJumpDestination(UniqueSignatures[GetHavokMemoryManagerCall].mFoundAt+Offset);
			GetHavokMemoryManager_Real=reinterpret_cast<Address (*)()>(GetHavokMemoryManager);

			Address HavokMemoryManager=GetHavokMemoryManager_Real();
			Address HavokBuffer=*reinterpret_cast<Address*>(HavokMemoryManager+0x88+4);

			Address NewBuffer=reinterpret_cast<Address>(_aligned_realloc(reinterpret_cast<void*>(HavokBuffer),GlobalSettings::HavokBufferSize,16));
			*reinterpret_cast<Address*>(HavokMemoryManager+0x88+4)=NewBuffer;
			*reinterpret_cast<Address*>(HavokMemoryManager+0x88+8)=NewBuffer+GlobalSettings::HavokBufferSize;
			*reinterpret_cast<Address*>(HavokMemoryManager+0x88+0xC)=NewBuffer;
		}

		if (GlobalSettings::AllowCustomObjectsInMP)
		{
			LOG("Custom objects should work in MP\n");
			const uint Offset=9;
			MakePageWritable(UniqueSignatures[MPCustomMaps].mFoundAt+Offset,1);
			*reinterpret_cast<byte*>(UniqueSignatures[MPCustomMaps].mFoundAt+Offset)=0xEb;//force jump
		}

		if (GlobalSettings::DisableSuspensionDamage)
		{
			LOG("Killed suspension damage\n");
			const uint Offset=7;
			Address JumpTarget=*reinterpret_cast<Address*>(UniqueSignatures[SuspensionDamage1].mFoundAt+Offset);
			JumpTarget+=Offset+4+UniqueSignatures[SuspensionDamage1].mFoundAt;
			WriteJump(UniqueSignatures[SuspensionDamage2].mFoundAt,JumpTarget);
		}

		//////////////////////////////////
		//             cosmetics
		//////////////////////////////////

		if (GlobalSettings::RenderDistanceIncrease>0)
		{	
			LOG("Increased mud rendering distance by %d units\n",GlobalSettings::RenderDistanceIncrease);
			WriteCall(	UniqueSignatures[ShaderLoader].mFoundAt,
							reinterpret_cast<Address>(&ShaderLoaderHook),
							0);
			WriteCall(	UniqueSignatures[CalculateDistanceForLOD].mFoundAt,
						reinterpret_cast<Address>(&AdjustLOD),
						2);
			const uint Offset=2;
			MakePageWritable(UniqueSignatures[RunTerrainShaderOnMediumDistanceTiles].mFoundAt+Offset,1);
			//push 3 instead of push 1
			*reinterpret_cast<byte*>(UniqueSignatures[RunTerrainShaderOnMediumDistanceTiles].mFoundAt+Offset)=0x3;
			
		}

		if (GlobalSettings::DisableDOF)
		{
			LOG("Killed DOF\n");
			const uint Offset=-0x15;
			MakePageWritable(UniqueSignatures[DisableDOF].mFoundAt+Offset,4);
			*reinterpret_cast<uint*>(UniqueSignatures[DisableDOF].mFoundAt+Offset)=0x90000cC2; //ret 0c
		}

		if (GlobalSettings::IncreaseTerrainLOD)
		{
			LOG("Bumped terrain LOD a bit\n");
			WriteJump(UniqueSignatures[GetTileLODLocator].mFoundAt,reinterpret_cast<Address>(&GetTileLOD));
		}

		if (GlobalSettings::FOV>0 || GlobalSettings::AspectRatio>0)
		{
			LOG("Hooked perspective matrix creator\n");
			WriteCall(UniqueSignatures[FOVChange].mFoundAt,reinterpret_cast<Address>(ChangeFOV),1);
		}

		if (GlobalSettings::NoVisualDamage)
		{
			LOG("Disabled visual damage\n");
			const int Offset=4;
			MakePageWritable(UniqueSignatures[NoVisualDamage].mFoundAt+Offset,5);
			memset(reinterpret_cast<void*>(UniqueSignatures[NoVisualDamage].mFoundAt+Offset),NOP_OPERATOR,5);
			
		}

		if (GlobalSettings::HideHUD_SteamInfo)
		{
			LOG("Removed Steam info from the HUD\n");
			//force jump paststeam name+avatar rendering
			//the problem is that we can't find a good signature at a constant distance from the patch
			//because of a 2byte long cmp dword [esi],ebx that may become a 3 byte cmp dword [esi],0
			//no we locate the cmp and scan for the next je (opcode 0x74)

			Signature Sig;
			{
				byte Bytes[]=	{0x74,0000,0x8b};
				byte Mask[]=	{0xFF,0000,0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));
			}
			const Address PatchLocation=ScanForNext(UniqueSignatures[HideSteamPicName].mFoundAt,Sig,0x20);
			MakePageWritable(PatchLocation,1);
			*reinterpret_cast<byte*>(PatchLocation)=0xEb;
			
			
		}
		if (GlobalSettings::HideHUD_3dUI)
		{
			LOG("Removed compass, shifter, and truckdata from the HUD\n");
			const int Offset1=2;
			Address Destination=GetCallJumpDestination(UniqueSignatures[Hide3DUI].mFoundAt);

			WriteJump(UniqueSignatures[Hide3DUI].mFoundAt+Offset1,Destination);
		}

		//////////////////////////////////
		//             Camera
		//////////////////////////////////

		if (GlobalSettings::NoCabinCameraReset)
		{
			LOG("Disabled cab camera reset\n");
			const uint Offset1=2;
			const uint Offset2=0x10; //distance to the end of the min(max(zoom,0),1) construct.
			WriteJump(UniqueSignatures[ResetCabinCamera].mFoundAt+Offset1,UniqueSignatures[ResetCabinCamera].mFoundAt+Offset2);
		}

		if (GlobalSettings::UncapZoom)
		{
			LOG("Killed zoom limits\n");
			const uint Offset1=0x15;
			const uint Offset2=0x3b; //distance to the end of the min(max(zoom,0),1) construct.
			WriteJump(UniqueSignatures[ZoomWriter].mFoundAt+Offset1,UniqueSignatures[ZoomWriter].mFoundAt+Offset2);
		}

		if (GlobalSettings::UncapX360Zoom)
		{
			LOG("Killed zoom limits for kids\n");
			const uint Offset1=2;
			const uint Offset2=0x12; //jump over a call Clamper(), and some useless float operations.
			WriteJump(UniqueSignatures[X360ZoomWriter].mFoundAt+Offset1,UniqueSignatures[X360ZoomWriter].mFoundAt+Offset2);
		}

		if (GlobalSettings::NoAutoZoom)
		{
			LOG("Killed the autozoom\n");
			const uint Offset=8;
			MakePageWritable(UniqueSignatures[AutoZoom].mFoundAt+Offset,1);
			*reinterpret_cast<byte*>(UniqueSignatures[AutoZoom].mFoundAt+Offset)=0xEb;
		}
		if (GlobalSettings::NoAdvancedAutoZoom)
		{
			LOG("Killed the autozoom in advanced mode\n");
			const uint Offset=2;
			const uint DestinationOffset=0x12;
			WriteJump(UniqueSignatures[AdvancedAutoZoom].mFoundAt+Offset,UniqueSignatures[AdvancedAutoZoom].mFoundAt+DestinationOffset);
		}
		if (GlobalSettings::NoAutoZoomOnStations)
		{
			LOG("Killed camera adjustment on stations\n");
			MakePageWritable(UniqueSignatures[StationAutoZoom].mFoundAt,1);
			*reinterpret_cast<byte*>(UniqueSignatures[StationAutoZoom].mFoundAt)=0xEb;//force jump
		}

		if (GlobalSettings::CabinCameraMaxAngle>=0.0f)
		{
			LOG("Changed cabin camera max angle to %f\n",GlobalSettings::CabinCameraMaxAngle);
			const int Offset=0xF;
			MakePageWritable(UniqueSignatures[CabinCameraMaxTurnAngle].mFoundAt+Offset,4);
			*reinterpret_cast<float**>(UniqueSignatures[CabinCameraMaxTurnAngle].mFoundAt+Offset)=&GlobalSettings::CabinCameraMaxAngle;
		}

		if (GlobalSettings::RightSideCameraKey>=0)
		{
			LOG("Right side camera angles enabled\n");
			WriteCall(UniqueSignatures[WriteCameraYAngle].mFoundAt,reinterpret_cast<Address>(&OnMoveCameraToPreset),2);
		}

		if (GlobalSettings::DetachCameraFromSteering)
		{
			LOG("Steering won't affect camera\n");
			int Offset=0xC;
			const Address Where=UniqueSignatures[AdjustCameraToSteering].mFoundAt+Offset;
			MakePageWritable(Where,3);
			*reinterpret_cast<unsigned short*>(Where)=0xEED9;	//fldz
			*reinterpret_cast<byte*>(Where+2)=0x90;	//nop
		}

		//////////////////////////////////
		//             gameplay tweaks
		//////////////////////////////////
		if (GlobalSettings::LegacyCloaks)
		{	//legacy cloaks
			const int Offset2=6;
			WriteCall(UniqueSignatures[LockMinimapFogOfWarTexture].mFoundAt+Offset2,reinterpret_cast<Address>(RemoveClockFromMap),23);
		}

		if (GlobalSettings::LoadPointsPerObjective>=0)
		{
			static double InversePoints=1.0/( (double)GlobalSettings::LoadPointsPerObjective);
			LOG("Objectives requires %d loadpoints\n",GlobalSettings::LoadPointsPerObjective);

			//these 2 check how many logs are in an objective just after
			//you delivered your load
			const int Offset1=5;
			MakePageWritable(UniqueSignatures[DeliveringLogs1].mFoundAt+Offset1,1);
			*reinterpret_cast<byte*>(UniqueSignatures[DeliveringLogs1].mFoundAt+Offset1)=GlobalSettings::LoadPointsPerObjective;

			const int Offset2=0x14;
			MakePageWritable(UniqueSignatures[DeliveringLogs1].mFoundAt+Offset2,1);
			*reinterpret_cast<byte*>(UniqueSignatures[DeliveringLogs1].mFoundAt+Offset2)=GlobalSettings::LoadPointsPerObjective;

			//In the loop to check if all objectives are full
			const int Offset3=0x11;
			MakePageWritable(UniqueSignatures[DeliveringLogs2].mFoundAt+Offset3,1);
			*reinterpret_cast<byte*>(UniqueSignatures[DeliveringLogs2].mFoundAt+Offset3)=GlobalSettings::LoadPointsPerObjective;

			//check if all objectives full to display stats
			const int Offset4=0xb;
			MakePageWritable(UniqueSignatures[CheckIfObjectiveFull].mFoundAt+Offset4,1);
			*reinterpret_cast<byte*>(UniqueSignatures[CheckIfObjectiveFull].mFoundAt+Offset4)=GlobalSettings::LoadPointsPerObjective;

			//change the color of the "Delivered x/8" text based on how many loadpoints
			//have been delivered
			const int Offset5=-1;
			MakePageWritable(UniqueSignatures[GameItemDeliveredTextColor].mFoundAt+Offset5,1);
			*reinterpret_cast<byte*>(UniqueSignatures[GameItemDeliveredTextColor].mFoundAt+Offset5)=GlobalSettings::LoadPointsPerObjective;

			//change "8" in the "Delivered x/8" text
			const int Offset6=8;
			MakePageWritable(UniqueSignatures[GameItemDeliveredTextColor].mFoundAt+Offset6,1);
			*reinterpret_cast<byte*>(UniqueSignatures[GameItemDeliveredTextColor].mFoundAt+Offset6)=GlobalSettings::LoadPointsPerObjective;

			//change the color of the "Objective N" text based on how many loadpoints
			//have been delivered
			const int Offset7=-7;
			MakePageWritable(UniqueSignatures[ObjectiveNTextColor].mFoundAt+Offset7,1);
			*reinterpret_cast<byte*>(UniqueSignatures[ObjectiveNTextColor].mFoundAt+Offset7)=GlobalSettings::LoadPointsPerObjective;

			//those 2 divide the number of loadpoints in the objective by its
			//capacity to draw progress bars
			const int Offset8=-0x11;
			MakePageWritable(UniqueSignatures[DrawObjectiveProgressBar].mFoundAt+Offset8,4);
			*reinterpret_cast<double**>(UniqueSignatures[DrawObjectiveProgressBar].mFoundAt+Offset8)=&InversePoints;

			const int Offset9=0x29;
			MakePageWritable(UniqueSignatures[DrawObjectiveProgressBar].mFoundAt+Offset9,4);
			*reinterpret_cast<double**>(UniqueSignatures[DrawObjectiveProgressBar].mFoundAt+Offset9)=&InversePoints;

			//this last one refuses to add loadpoints to an objective
			//that already contains 8
			const int Offset10=5;
			const int Offset11=0x38;
			WriteJump(UniqueSignatures[WriteObjectivePoints].mFoundAt+Offset10,UniqueSignatures[WriteObjectivePoints].mFoundAt+Offset10+Offset11);
		}

		if (GlobalSettings::FreezeTimeOfDayAt>=0.0f)
		{
			LOG("Locked time to %fh\n",GlobalSettings::FreezeTimeOfDayAt);
			const uint Offset=-8;
			WriteCall(UniqueSignatures[WriteTimeOfDay].mFoundAt+Offset,reinterpret_cast<Address>(&GetUpdatedTimeOfDay),3);
		}

		if (GlobalSettings::GameDayDurationInRealSeconds>0.0f)
		{
			LOG("Day duretion set to %f s\n",GlobalSettings::GameDayDurationInRealSeconds);
			const uint Offset=2;
			double* DayDuration=*reinterpret_cast<double **>(UniqueSignatures[CalcTimeOfDayIncrement].mFoundAt+Offset);
			MakePageWritable(reinterpret_cast<Address>(DayDuration),sizeof(double));
			*DayDuration=GlobalSettings::GameDayDurationInRealSeconds;
			
		}

		if (GlobalSettings::HardcoreAutoLogs)
		{
			LOG("Applied autologs\n");
			const uint Offset=7;
			MakePageWritable(UniqueSignatures[HardcoreAutoLogs].mFoundAt+Offset,1);
			*reinterpret_cast<byte*>(UniqueSignatures[HardcoreAutoLogs].mFoundAt+Offset)=0xEb; //force jump to the piece of code that spawns the auto log loaders.
		}

		if (GlobalSettings::DisableCloaks)
		{
			LOG("Applied no cloak\n");
			const int Offset=-0x2F;
			const int SecondaryOffset=0x15;
			MakePageWritable(UniqueSignatures[DisableCloaks].mFoundAt+Offset,-Offset+SecondaryOffset+1);
			*reinterpret_cast<uint*>(UniqueSignatures[DisableCloaks].mFoundAt+Offset)=-1;
			*reinterpret_cast<unsigned short*>(UniqueSignatures[DisableCloaks].mFoundAt+SecondaryOffset)=0xe990; //force a jump
		}

		if (GlobalSettings::NoDamage)
		{
			LOG("Applied no dmg\n");
			const uint Offset=2;
			MakePageWritable(UniqueSignatures[NoDamage].mFoundAt+Offset,1);
			*reinterpret_cast<unsigned short*>(UniqueSignatures[NoDamage].mFoundAt+Offset)=0x91d9;
			//we are turning a mov dword [ecx+0FC],eax that writes damage
			//into fst dword [ecx+0FC] who stores the 0.0 loaded just above
		}

		if (GlobalSettings::UnlimitedFuel)
		{
			LOG("Applied ufuel\n");
			const uint Offset=4;
			MakePageWritable(UniqueSignatures[UnlimitedFuel].mFoundAt+Offset,1);
			*reinterpret_cast<byte*>(UniqueSignatures[UnlimitedFuel].mFoundAt+Offset)=0xdb;
			//we are turning a fld dword ptr [eax+xxxx] that reads fuel
			//into fild dword ptr [eax+xxxx] who reads your fuel (a float) as an int
			//and converts it back into a float resulting in a gigantic number.
		}

		if (GlobalSettings::ShowLockedTrucksAtSelectionScreen)
		{
			LOG("Locked trucks appear at selection screen\n");

			const uint Offset=3;
			//6=size of a conditional jump with 32bit offset.
			MakePageWritable(UniqueSignatures[CheckDistanceForStartupTrucks].mFoundAt+Offset,6);
			memset(reinterpret_cast<void*>(UniqueSignatures[CheckDistanceForStartupTrucks].mFoundAt+Offset),NOP_OPERATOR,6);
			

		}

		if (GlobalSettings::AlternateDifflockDamage)
		{
			LOG("Using alternate difflock damage calculation\n");

			const int Offset1=8;
			const int Offset2=8;
			const int Offset3=2;
			const Address HookLocation=UniqueSignatures[CalcDifflockStress].mFoundAt+Offset1;
			
			//MakePageWritable(0x0B70103,2);	
			//*reinterpret_cast<short*>(0x0B70103)=0x04eb; //nop hardcore check


			WriteCall(UniqueSignatures[CalcDifflockStress].mFoundAt+Offset1,reinterpret_cast<Address>(&GetDifflockStress),0);
			WriteCall(UniqueSignatures[CalcDifflockDamage].mFoundAt+Offset3,reinterpret_cast<Address>(&GetDifflockDamage),0);
			WriteJump(HookLocation+SIZEOF_CALL,UniqueSignatures[CheckDiffStress].mFoundAt+Offset2);

		}

		if (GlobalSettings::AlwaysShowDevMenu)
		{
			LOG("Dev menu will work outside proving grounds\n");

			const int Offset=2;
			const Address PatchLocation=UniqueSignatures[ShouldWeDisplayDevMenu].mFoundAt+Offset;
			MakePageWritable(PatchLocation,6);
			memset(reinterpret_cast<void*>(PatchLocation),NOP_OPERATOR,6);	//nop jump

		}

		if (GlobalSettings::AlwaysAllowSpawnLocators)
		{
			LOG("Spawn locators will work outside proving grounds\n");

			const int Offset=2;
			const Address PatchLocation=UniqueSignatures[ShouldWeDisplaySpawnLocators].mFoundAt+Offset;
			MakePageWritable(PatchLocation,6);
			memset(reinterpret_cast<void*>(PatchLocation),NOP_OPERATOR,6);	//nop jump

		}

		//////////////////////////////////
		//             winch stuff
		//////////////////////////////////
		if (GlobalSettings::WinchingRange>=0)
		{
			LOG("Changed winch range to %f\n",GlobalSettings::WinchingRange);

			const uint Offset=4;
			float* WinchRangePtr=*reinterpret_cast<float**>(UniqueSignatures[WinchingRange].mFoundAt+Offset);
			MakePageWritable(reinterpret_cast<Address>(WinchRangePtr),4);
			
			*WinchRangePtr=GlobalSettings::WinchingRange;
		}

		if (GlobalSettings::ReleaseWinchKey>=0)
		{
			LOG("Winch release key enabled\n");

			const int Offset1=6;
			const int Offset2=10;
			const int Offset3=6;	//offset to next instruction after CursorActiveTest = sizeof conditionnal 32bit jump
			const Address CursorActiveTest=UniqueSignatures[MouseOnWinchCrossTest].mFoundAt+Offset1;
			const Address OriginalJumpDest=GetCallJumpDestination(CursorActiveTest);
			Address ClickTestLocation;
			{
				/*
				00B6AA33   . E8 E89A89FF    CALL <SpinTire.FloatSqrt>                                          ; \FloatSqrt
				00B6AA38   . D94424 18      FLD DWORD PTR [ESP+18]
				00B6AA3C   . 83C4 04        ADD ESP,4
				00B6AA3F   . DED9           FCOMPP
				00B6AA41   . DFE0           FSTSW AX
				00B6AA43   . F6C4 41        TEST AH,41
				00B6AA46     0F85 C3010000  JNZ SpinTire.00B6AC0F                                              ;  check mouse cursor to winch cross distance
				00B6AA4C   . 807D 14 00     CMP BYTE PTR [EBP+14],0    <-signature                             ;  is mouse lbutton down
				00B6AA50     0F84 AF010000  JE SpinTire.00B6AC05                                               ;  jump if not
				00B6AA56   . 8B55 08        MOV EDX,DWORD PTR [EBP+8]  <-we want that
				00B6AA59   . 8B72 04        MOV ESI,DWORD PTR [EDX+4]
				00B6AA5C   . 8B86 58040000  MOV EAX,DWORD PTR [ESI+458]
				00B6AA62   . 85C0           TEST EAX,EAX
				00B6AA64   . 74 1E          JE SHORT SpinTire.00B6AA84
				*/
				Signature Sig;
				byte Bytes[]=	{0x80,0x7D,0000,0x00,0x0f,0x84};
				byte Mask[]=	{0xFF,0xFF,0000,0xFF,0xFF,0xFF};
				Sig.Set(Bytes,Mask,sizeof(Bytes));
				ClickTestLocation=ScanForNext(CursorActiveTest,Sig,OriginalJumpDest-CursorActiveTest);
			}
			WinchHooks::Get()->Init(CursorActiveTest+Offset3,OriginalJumpDest,ClickTestLocation+Offset2);

			WriteJump(CursorActiveTest,reinterpret_cast<Address>(WinchHooks::MustReleaseWinch));
		}

		if (GlobalSettings::BatteryPoweredWinch)
		{
			LOG("Switched to battery powered winches\n");

			const uint Offset1=1;
			MakePageWritable(UniqueSignatures[BatteryPoweredWinch1].mFoundAt+Offset1,1);
			*reinterpret_cast<byte*>(UniqueSignatures[BatteryPoweredWinch1].mFoundAt+Offset1)=0xC9;//or operator instead of and
			
			const uint Offset2=6;
			MakePageWritable(UniqueSignatures[BatteryPoweredWinch2].mFoundAt+Offset2,1);
			*reinterpret_cast<byte*>(UniqueSignatures[BatteryPoweredWinch2].mFoundAt+Offset2)=0x0C;//or operator instead of and
		}
	}
}

void Hooker::MakePageWritable(Address Where, std::size_t Length) const
{
	DWORD Dummy;
	VirtualProtect(reinterpret_cast<LPVOID>(Where),Length,PAGE_EXECUTE_READWRITE,&Dummy);
}

void Hooker::WriteJump(Address Source, Address Destination) const
{
	if (abs((int)(Destination-Source-2))<127)
	{
		MakePageWritable(Source,2);
		*reinterpret_cast<byte*>(Source)=0xEB; //jmp opcode
		*reinterpret_cast<byte*>(Source+1)=Destination-Source-2;
	}
	else //write 32 bit jump
	{
		MakePageWritable(Source,5);
		*reinterpret_cast<byte*>(Source)=0xE9; //jmp opcode
		*reinterpret_cast<uint*>(Source+1)=Destination-Source-5;
	}
}

//NopCount: the number of nops to add AFTER the call
void Hooker::WriteCall(Address Source, Address Destination,uint NopCount) const
{
	MakePageWritable(Source,5);
	*reinterpret_cast<byte*>(Source)=0xE8; //call opcode
	*reinterpret_cast<uint*>(Source+1)=Destination-Source-5;

	memset(reinterpret_cast<void*>(Source+5),0x90,NopCount);
}

void Hooker::EnableConsole()
{
    AllocConsole();
    freopen("CONOUT$", "w", stdout);
    //freopen("CONIN$", "r", stdin);
}

void Hooker::RunPlugins()
{
	for (std::list<HMODULE>::iterator i=Plugins.begin(); i!=Plugins.end();i++)
	{
		void (__stdcall *PluginFunction)(void)=reinterpret_cast<void (__stdcall *)(void)>(GetProcAddress(*i,"OnD3DCreate9"));
		if (PluginFunction)
		{
			PluginFunction();
		}
	}
}

//will return the address where the instruction at OpcodeBegin jumps to.
//Currently only supports calls and unconditionnal jumps with 32bit offsets
Address Hooker::GetCallJumpDestination(Address OpcodeBegin) const
{
	byte Opcode=*reinterpret_cast<byte*>(OpcodeBegin);
	if (Opcode==JMP_IMM32_OPERATOR || Opcode==CALL_IMM32_OPERATOR)
	{
		return OpcodeBegin+5+*reinterpret_cast<int*>(OpcodeBegin+1);
	}
	else if (JCC8_START<Opcode && Opcode<JCC8_END)
	{
		return OpcodeBegin+2+*reinterpret_cast<signed char*>(OpcodeBegin+1);
	}
	else if (Opcode==JCC32_PREFIX)
	{
		Opcode=*reinterpret_cast<byte*>(OpcodeBegin+1);
		if (JCC32_START<Opcode && Opcode<JCC32_END)
		{
			return OpcodeBegin+6+*reinterpret_cast<int*>(OpcodeBegin+2);
		}
		else
		{
			LOG("Unsupported opcode in GetCallJumpDestination\n");
			DebugBreak();
			return 0xBAADC0DE;
		}
	}
	else
	{
		LOG("Unsupported opcode in GetCallJumpDestination\n");
		DebugBreak();
		return 0xBAADC0DE;
	}
}

//write the assembly instruction Test byte ptr [Register+Offset],Mask
//at the specified location
void Hooker::WriteTest(Address Where, RegisterCodes Register, char Offset, unsigned char Mask) const
{
	MakePageWritable(Where,4);
	uint Opcode=0x000040F6;
	Opcode|=Register<<8;
	Opcode|=Offset<<16;
	Opcode|=Mask<<24;
	*reinterpret_cast<uint*>(Where)=Opcode;
}

//Scans for a signature, starting at From
//If the signature wasn't found between From and From+MaxDistance the function returns 0
//otherwise it returns the address when the signature was found
Address Hooker::ScanForNext(Address From, Signature &Pattern, uint MaxDistance) const
{
	for (Address i=From;i<From+MaxDistance;++i)
	{
		if(Pattern.MatchesAt<char>(i)==Signature::MATCHED)
		{
			return Pattern.mFoundAt;
		}
	}
	return 0;
}

//Like ScanForNext, but scans backward
Address Hooker::ScanForPrevious(Address From, Signature &Pattern, uint MaxDistance) const
{
	for (Address i=From;i>From-MaxDistance;--i)
	{
		if(Pattern.MatchesAt<char>(i)==Signature::MATCHED)
		{
			return Pattern.mFoundAt;
		}
	}
	return 0;
}

//walks the import table of the exe, looking for where ProcName is imported from ModuleName
//and returns a reference to the pointer containing the address of the function.
Address* Hooker::GetImportRef(const char* ModuleName,const char* ProcName) const
{
	IMAGE_DOS_HEADER* DosHeader=reinterpret_cast<IMAGE_DOS_HEADER*>(mExeBase);
    IMAGE_NT_HEADERS* ImageHeader=reinterpret_cast<IMAGE_NT_HEADERS*>(mExeBase+DosHeader->e_lfanew);
	IMAGE_IMPORT_DESCRIPTOR* ImportDescriptor=reinterpret_cast<IMAGE_IMPORT_DESCRIPTOR*>(ImageHeader->OptionalHeader.DataDirectory[1].VirtualAddress + mExeBase);
	
	while(ImportDescriptor->OriginalFirstThunk)
	{	//for every imported dll
		char* DllName=reinterpret_cast<char*>(ImportDescriptor->Name+mExeBase);
		//printf("ImportDescriptor %x dll:%s \n",ImportDescriptor,DllName);
		if (!stricmp(DllName,ModuleName))
		{
			IMAGE_THUNK_DATA* NameArray=reinterpret_cast<IMAGE_THUNK_DATA*>(ImportDescriptor->OriginalFirstThunk+mExeBase);
			IMAGE_THUNK_DATA* FunctionPtrTable=reinterpret_cast<IMAGE_THUNK_DATA*>(ImportDescriptor->FirstThunk+mExeBase);
			
			while(NameArray->u1.AddressOfData)
			{	//for every import from this module

				IMAGE_IMPORT_BY_NAME* NameImage=reinterpret_cast<IMAGE_IMPORT_BY_NAME*>(NameArray->u1.AddressOfData+mExeBase);
				//printf("NameArray %x FunctionPtrTable %x import:%s \n",NameArray,FunctionPtrTable,NameImage->Name);
				if (!stricmp(reinterpret_cast<char*>(&(NameImage->Name[0])),ProcName))
				{
					return reinterpret_cast<Address*>( &(FunctionPtrTable->u1.AddressOfData) );
				}

				NameArray++;
				FunctionPtrTable++;
			}

			return NULL; //not found
		}


		ImportDescriptor++;
	}
	return NULL; //not found
}