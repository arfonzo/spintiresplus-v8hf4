/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "STPListDialogWithPreview.h"

class STPTruckSelectionDialog :
	public STPListDialogWithPreview
{
public:
	STPTruckSelectionDialog(void);
	~STPTruckSelectionDialog(void);

	virtual void Restore();

	const std::string* GetSelectedTruckName() const;
	const STPListDialogWithPreview::EntryData* GetSelectedTruck() const {return mSelectedTruck;}

	//returns the filename of the currently seleted truck, or
	//NULL if no truck is selected
	const std::string* GetSelectedTruckFileName() const;

	void SetTruckChecked(const STPListDialogWithPreview::EntryData* TruckEntry,bool Checked);
private:
	virtual void AddEntryToMenu(const char* VarName,const STPListDialogWithPreview::EntryData& Data,std::string& Label);
	const STPListDialogWithPreview::EntryData* mSelectedTruck;
};