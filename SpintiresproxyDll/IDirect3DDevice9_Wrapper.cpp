/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "IDirect3DDevice9_Wrapper.h"
#include "STPMenuManager.h"
#include "Common.h"
#include "Settings.h"
#include "ExternalMinimap.h"
#include "bugfixes.h"

//only display in wireframe mode the models with more than this many polys.
//otherwise you won't see anything since the game is rendered offsreen and then redrawn
//of a plane (2 triangles) in from of your screen
#define WIREFRAME_POLY_TRESHOLD 3

//We only actually display in wireframe mode when this is true, and we allow
//toggling this by pressing F2 only when GlobalSettings::WireframeMode is true
static bool WireframeEnabled=false;

IDirect3DDevice9_Wrapper::IDirect3DDevice9_Wrapper(IDirect3DDevice9* IDirect3DDevice9_Real)
:mRefCount(1)
{
	mIDirect3DDevice9_Real=IDirect3DDevice9_Real;
}

IDirect3DDevice9_Wrapper::~IDirect3DDevice9_Wrapper(void)
{
	//delete mIDirect3DDevice9_Real; //done by Release()
}

///////////////// modified functions
HRESULT IDirect3DDevice9_Wrapper::BeginScene() 
{
	//toggle wireframe mode when F2 is down and we enter here
	if (GlobalSettings::WireframeMode)
	{
		if (WireframeEnabled)
		{
			if (GetKeyState(VK_F2)<0)
			{
				WireframeEnabled=false;
			}
		}
		else if (GetKeyState(VK_F3)<0)
		{
			WireframeEnabled=true;
		}
	}

	//if the wireframe option is enabled in the config AND the user wants wrireframe display NOW.
	if (WireframeEnabled)
	{
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME) ;
	}
	return mIDirect3DDevice9_Real->BeginScene() ;
}


HRESULT IDirect3DDevice9_Wrapper::EndScene() 
{
	if (GlobalSettings::MinimapEnabled)
	{
		STPMinimapManager::Get()->CheckForLostDevice();
	}
	return mIDirect3DDevice9_Real->EndScene() ;
}

HRESULT IDirect3DDevice9_Wrapper::Reset(D3DPRESENT_PARAMETERS* pPresentationParameters) 
{
	HRESULT ReturnValue= mIDirect3DDevice9_Real->Reset(pPresentationParameters) ;
	return ReturnValue;
}


ULONG IDirect3DDevice9_Wrapper::AddRef()
{
	InterlockedIncrement(&mRefCount);
	return mIDirect3DDevice9_Real->AddRef();
}

ULONG IDirect3DDevice9_Wrapper::Release()
{
	//We need to count references ourself to call
	//STPMenuManager::Get()->OnDeviceLost() (ie: TwTerminate) before refcount
	//reaches 0 on the real device or the GUI manager will fail its deallocation
	//and crash when quitting the game
	InterlockedDecrement(&mRefCount);
	if (!mRefCount && OverlayEnabled())
	{	//minimap doesn't need to be notified from here since it grabs the
		//WM_DESTROY message
		STPMenuManager::Get()->Destroy();
		ULONG ref=mIDirect3DDevice9_Real->Release();
		if (ref)
		{
			LOG ("BUG wrong reference count %d  \n",ref);
		}
		
		delete this;
		return 0;
	}
	else
	{
		return mIDirect3DDevice9_Real->Release();
	}
}

HRESULT IDirect3DDevice9_Wrapper::DrawPrimitive(D3DPRIMITIVETYPE PrimitiveType,UINT StartVertex,UINT PrimitiveCount) 
{
	
	if (WireframeEnabled && PrimitiveCount<WIREFRAME_POLY_TRESHOLD)
	{
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID) ;
		HRESULT ReturnValue=mIDirect3DDevice9_Real->DrawPrimitive(PrimitiveType, StartVertex,PrimitiveCount) ;
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME) ;
		return ReturnValue;
	}	
	return mIDirect3DDevice9_Real->DrawPrimitive(PrimitiveType, StartVertex,PrimitiveCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::DrawIndexedPrimitive(D3DPRIMITIVETYPE PrimitiveType,INT BaseVertexIndex,UINT MinVertexIndex,UINT NumVertices,UINT startIndex,UINT primCount) 
{
	
	if (WireframeEnabled && primCount<WIREFRAME_POLY_TRESHOLD)
	{
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID) ;
		HRESULT ReturnValue=mIDirect3DDevice9_Real->DrawIndexedPrimitive(PrimitiveType,BaseVertexIndex, MinVertexIndex,NumVertices,startIndex,primCount) ;
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME) ;
		return ReturnValue;
	}
	return mIDirect3DDevice9_Real->DrawIndexedPrimitive(PrimitiveType,BaseVertexIndex, MinVertexIndex,NumVertices,startIndex,primCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType,UINT PrimitiveCount,CONST void* pVertexStreamZeroData,UINT VertexStreamZeroStride) 
{
	
	if (WireframeEnabled && PrimitiveCount<WIREFRAME_POLY_TRESHOLD)
	{
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID) ;
		HRESULT ReturnValue=mIDirect3DDevice9_Real->DrawPrimitiveUP(PrimitiveType, PrimitiveCount,pVertexStreamZeroData,VertexStreamZeroStride) ;
		mIDirect3DDevice9_Real->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME) ;
		return ReturnValue;
	}
	return mIDirect3DDevice9_Real->DrawPrimitiveUP(PrimitiveType, PrimitiveCount,pVertexStreamZeroData,VertexStreamZeroStride) ;
}

BOOL IDirect3DDevice9_Wrapper::ShowCursor(BOOL bShow) 
{
	if (GlobalSettings::HandleCursorWithoutDirectX)
	{	//hide cursor if we want to disable directx cursor
		return mIDirect3DDevice9_Real->ShowCursor(FALSE) ;
	}
	else
	{
		return mIDirect3DDevice9_Real->ShowCursor(bShow) ;
	}
}

HRESULT IDirect3DDevice9_Wrapper::SetCursorProperties(UINT XHotSpot,UINT YHotSpot,IDirect3DSurface9* pCursorBitmap) 
{
	if (GlobalSettings::HandleCursorWithoutDirectX)
	{	//do nothing if we want to disable directx cursor
		return D3D_OK;
	}
	else
	{
		return mIDirect3DDevice9_Real->SetCursorProperties(XHotSpot, YHotSpot,pCursorBitmap) ;
	}
}
///////////////// All function below are just stubs that pass the call to mIDirect3DDevice9_Real
/*** IUnknown methods ***/
HRESULT IDirect3DDevice9_Wrapper::QueryInterface(const IID &riid, void **ppvObj)
{
	return mIDirect3DDevice9_Real->QueryInterface(riid,ppvObj);
}

/*** IDirect3DDevice9 methods ***/
HRESULT IDirect3DDevice9_Wrapper::TestCooperativeLevel() 
{
	return mIDirect3DDevice9_Real->TestCooperativeLevel() ;
}

UINT IDirect3DDevice9_Wrapper::GetAvailableTextureMem() 
{
	return mIDirect3DDevice9_Real->GetAvailableTextureMem() ;
}

HRESULT IDirect3DDevice9_Wrapper::EvictManagedResources() 
{
	return mIDirect3DDevice9_Real->EvictManagedResources() ;
}

HRESULT IDirect3DDevice9_Wrapper::GetDirect3D(IDirect3D9** ppD3D9) 
{
	return mIDirect3DDevice9_Real->GetDirect3D(ppD3D9) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetDeviceCaps(D3DCAPS9* pCaps) 
{
	return mIDirect3DDevice9_Real->GetDeviceCaps(pCaps) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetDisplayMode(UINT iSwapChain,D3DDISPLAYMODE* pMode) 
{
	return mIDirect3DDevice9_Real->GetDisplayMode(iSwapChain, pMode) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetCreationParameters(D3DDEVICE_CREATION_PARAMETERS *pParameters) 
{
	return mIDirect3DDevice9_Real->GetCreationParameters(pParameters) ;
}

void IDirect3DDevice9_Wrapper::SetCursorPosition(int X,int Y,DWORD Flags) 
{
	 mIDirect3DDevice9_Real->SetCursorPosition(X, Y,Flags) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateAdditionalSwapChain(D3DPRESENT_PARAMETERS* pPresentationParameters,IDirect3DSwapChain9** pSwapChain) 
{
	return mIDirect3DDevice9_Real->CreateAdditionalSwapChain(pPresentationParameters, pSwapChain) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetSwapChain(UINT iSwapChain,IDirect3DSwapChain9** pSwapChain) 
{
	return mIDirect3DDevice9_Real->GetSwapChain(iSwapChain, pSwapChain) ;
}

UINT IDirect3DDevice9_Wrapper::GetNumberOfSwapChains() 
{
	return mIDirect3DDevice9_Real->GetNumberOfSwapChains() ;
}

HRESULT IDirect3DDevice9_Wrapper::Present(CONST RECT* pSourceRect,CONST RECT* pDestRect,HWND hDestWindowOverride,CONST RGNDATA* pDirtyRegion) 
{
	return mIDirect3DDevice9_Real->Present(pSourceRect, pDestRect,hDestWindowOverride,pDirtyRegion) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetBackBuffer(UINT iSwapChain,UINT iBackBuffer,D3DBACKBUFFER_TYPE Type,IDirect3DSurface9** ppBackBuffer) 
{
	return mIDirect3DDevice9_Real->GetBackBuffer(iSwapChain, iBackBuffer,Type,ppBackBuffer) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetRasterStatus(UINT iSwapChain,D3DRASTER_STATUS* pRasterStatus) 
{
	return mIDirect3DDevice9_Real->GetRasterStatus(iSwapChain, pRasterStatus) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetDialogBoxMode(BOOL bEnableDialogs) 
{
	return mIDirect3DDevice9_Real->SetDialogBoxMode(bEnableDialogs) ;
}

void IDirect3DDevice9_Wrapper::SetGammaRamp(UINT iSwapChain,DWORD Flags,CONST D3DGAMMARAMP* pRamp) 
{
	mIDirect3DDevice9_Real->SetGammaRamp(iSwapChain, Flags, pRamp) ;
}

void IDirect3DDevice9_Wrapper::GetGammaRamp(UINT iSwapChain,D3DGAMMARAMP* pRamp) 
{
	mIDirect3DDevice9_Real->GetGammaRamp(iSwapChain, pRamp) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateTexture(UINT Width,UINT Height,UINT Levels,DWORD Usage,D3DFORMAT Format,D3DPOOL Pool,IDirect3DTexture9** ppTexture,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateTexture(Width, Height,Levels,Usage,Format,Pool,ppTexture,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateVolumeTexture(UINT Width,UINT Height,UINT Depth,UINT Levels,DWORD Usage,D3DFORMAT Format,D3DPOOL Pool,IDirect3DVolumeTexture9** ppVolumeTexture,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateVolumeTexture(Width, Height,Depth,Levels,Usage,Format,Pool,ppVolumeTexture,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateCubeTexture(UINT EdgeLength,UINT Levels,DWORD Usage,D3DFORMAT Format,D3DPOOL Pool,IDirect3DCubeTexture9** ppCubeTexture,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateCubeTexture(EdgeLength, Levels, Usage, Format, Pool, ppCubeTexture,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateVertexBuffer(UINT Length,DWORD Usage,DWORD FVF,D3DPOOL Pool,IDirect3DVertexBuffer9** ppVertexBuffer,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateVertexBuffer(Length, Usage,FVF,Pool,ppVertexBuffer,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateIndexBuffer(UINT Length,DWORD Usage,D3DFORMAT Format,D3DPOOL Pool,IDirect3DIndexBuffer9** ppIndexBuffer,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateIndexBuffer(Length, Usage,Format,Pool,ppIndexBuffer,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateRenderTarget(UINT Width,UINT Height,D3DFORMAT Format,D3DMULTISAMPLE_TYPE MultiSample,DWORD MultisampleQuality,BOOL Lockable,IDirect3DSurface9** ppSurface,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateRenderTarget(Width, Height,Format,MultiSample,MultisampleQuality, Lockable, ppSurface,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateDepthStencilSurface(UINT Width,UINT Height,D3DFORMAT Format,D3DMULTISAMPLE_TYPE MultiSample,DWORD MultisampleQuality,BOOL Discard,IDirect3DSurface9** ppSurface,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateDepthStencilSurface(Width, Height,Format,MultiSample,MultisampleQuality,Discard,ppSurface,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::UpdateSurface(IDirect3DSurface9* pSourceSurface,CONST RECT* pSourceRect,IDirect3DSurface9* pDestinationSurface,CONST POINT* pDestPoint) 
{
	return mIDirect3DDevice9_Real->UpdateSurface(pSourceSurface, pSourceRect,pDestinationSurface,pDestPoint) ;
}

HRESULT IDirect3DDevice9_Wrapper::UpdateTexture(IDirect3DBaseTexture9* pSourceTexture,IDirect3DBaseTexture9* pDestinationTexture) 
{
	return mIDirect3DDevice9_Real->UpdateTexture(pSourceTexture, pDestinationTexture) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetRenderTargetData(IDirect3DSurface9* pRenderTarget,IDirect3DSurface9* pDestSurface) 
{
	return mIDirect3DDevice9_Real->GetRenderTargetData(pRenderTarget, pDestSurface) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetFrontBufferData(UINT iSwapChain,IDirect3DSurface9* pDestSurface) 
{
	return mIDirect3DDevice9_Real->GetFrontBufferData(iSwapChain, pDestSurface) ;
}

HRESULT IDirect3DDevice9_Wrapper::StretchRect(IDirect3DSurface9* pSourceSurface,CONST RECT* pSourceRect,IDirect3DSurface9* pDestSurface,CONST RECT* pDestRect,D3DTEXTUREFILTERTYPE Filter) 
{
	return mIDirect3DDevice9_Real->StretchRect(pSourceSurface, pSourceRect,pDestSurface,pDestRect,Filter) ;
}

HRESULT IDirect3DDevice9_Wrapper::ColorFill(IDirect3DSurface9* pSurface,CONST RECT* pRect,D3DCOLOR color) 
{
	return mIDirect3DDevice9_Real->ColorFill(pSurface, pRect,color) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateOffscreenPlainSurface(UINT Width,UINT Height,D3DFORMAT Format,D3DPOOL Pool,IDirect3DSurface9** ppSurface,HANDLE* pSharedHandle) 
{
	return mIDirect3DDevice9_Real->CreateOffscreenPlainSurface(Width, Height,Format,Pool,ppSurface,pSharedHandle) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetRenderTarget(DWORD RenderTargetIndex,IDirect3DSurface9* pRenderTarget) 
{
	return mIDirect3DDevice9_Real->SetRenderTarget(RenderTargetIndex, pRenderTarget) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetRenderTarget(DWORD RenderTargetIndex,IDirect3DSurface9** ppRenderTarget) 
{
	return mIDirect3DDevice9_Real->GetRenderTarget(RenderTargetIndex, ppRenderTarget) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetDepthStencilSurface(IDirect3DSurface9* pNewZStencil) 
{
	return mIDirect3DDevice9_Real->SetDepthStencilSurface(pNewZStencil) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetDepthStencilSurface(IDirect3DSurface9** ppZStencilSurface) 
{
	return mIDirect3DDevice9_Real->GetDepthStencilSurface(ppZStencilSurface) ;
}

HRESULT IDirect3DDevice9_Wrapper::Clear(DWORD Count,CONST D3DRECT* pRects,DWORD Flags,D3DCOLOR Color,float Z,DWORD Stencil) 
{
	return mIDirect3DDevice9_Real->Clear(Count, pRects,Flags,Color,Z,Stencil) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetTransform(D3DTRANSFORMSTATETYPE State,CONST D3DMATRIX* pMatrix) 
{
	return mIDirect3DDevice9_Real->SetTransform(State, pMatrix) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetTransform(D3DTRANSFORMSTATETYPE State,D3DMATRIX* pMatrix) 
{
	return mIDirect3DDevice9_Real->GetTransform(State, pMatrix) ;
}

HRESULT IDirect3DDevice9_Wrapper::MultiplyTransform(D3DTRANSFORMSTATETYPE Type,CONST D3DMATRIX* Matrix) 
{
	return mIDirect3DDevice9_Real->MultiplyTransform(Type,Matrix) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetViewport(CONST D3DVIEWPORT9* pViewport) 
{
	return mIDirect3DDevice9_Real->SetViewport(pViewport) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetViewport(D3DVIEWPORT9* pViewport) 
{
	return mIDirect3DDevice9_Real->GetViewport(pViewport) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetMaterial(CONST D3DMATERIAL9* pMaterial) 
{
	return mIDirect3DDevice9_Real->SetMaterial(pMaterial) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetMaterial(D3DMATERIAL9* pMaterial) 
{
	return mIDirect3DDevice9_Real->GetMaterial(pMaterial) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetLight(DWORD Index,CONST D3DLIGHT9* Light) 
{
	return mIDirect3DDevice9_Real->SetLight(Index, Light) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetLight(DWORD Index,D3DLIGHT9* Light) 
{
	return mIDirect3DDevice9_Real->GetLight(Index,Light) ;
}

HRESULT IDirect3DDevice9_Wrapper::LightEnable(DWORD Index,BOOL Enable) 
{
	return mIDirect3DDevice9_Real->LightEnable(Index, Enable) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetLightEnable(DWORD Index,BOOL* pEnable) 
{
	return mIDirect3DDevice9_Real->GetLightEnable(Index, pEnable) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetClipPlane(DWORD Index,CONST float* pPlane) 
{
	return mIDirect3DDevice9_Real->SetClipPlane(Index, pPlane) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetClipPlane(DWORD Index,float* pPlane) 
{
	return mIDirect3DDevice9_Real->GetClipPlane(Index, pPlane) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetRenderState(D3DRENDERSTATETYPE State,DWORD Value) 
{
	return mIDirect3DDevice9_Real->SetRenderState(State, Value) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetRenderState(D3DRENDERSTATETYPE State,DWORD* pValue) 
{
	return mIDirect3DDevice9_Real->GetRenderState(State, pValue) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateStateBlock(D3DSTATEBLOCKTYPE Type,IDirect3DStateBlock9** ppSB) 
{
	return mIDirect3DDevice9_Real->CreateStateBlock(Type, ppSB) ;
}

HRESULT IDirect3DDevice9_Wrapper::BeginStateBlock() 
{
	return mIDirect3DDevice9_Real->BeginStateBlock() ;
}

HRESULT IDirect3DDevice9_Wrapper::EndStateBlock(IDirect3DStateBlock9** ppSB) 
{
	return mIDirect3DDevice9_Real->EndStateBlock(ppSB) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetClipStatus(CONST D3DCLIPSTATUS9* pClipStatus) 
{
	return mIDirect3DDevice9_Real->SetClipStatus(pClipStatus) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetClipStatus(D3DCLIPSTATUS9* pClipStatus) 
{
	return mIDirect3DDevice9_Real->GetClipStatus(pClipStatus) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetTexture(DWORD Stage,IDirect3DBaseTexture9** ppTexture) 
{
	return mIDirect3DDevice9_Real->GetTexture(Stage, ppTexture) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetTexture(DWORD Stage,IDirect3DBaseTexture9* pTexture) 
{
	return mIDirect3DDevice9_Real->SetTexture(Stage, pTexture) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetTextureStageState(DWORD Stage,D3DTEXTURESTAGESTATETYPE Type,DWORD* pValue) 
{
	return mIDirect3DDevice9_Real->GetTextureStageState(Stage, Type,pValue) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetTextureStageState(DWORD Stage,D3DTEXTURESTAGESTATETYPE Type,DWORD Value) 
{
	return mIDirect3DDevice9_Real->SetTextureStageState(Stage, Type,Value) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetSamplerState(DWORD Sampler,D3DSAMPLERSTATETYPE Type,DWORD* pValue) 
{
	return mIDirect3DDevice9_Real->GetSamplerState(Sampler, Type,pValue) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetSamplerState(DWORD Sampler,D3DSAMPLERSTATETYPE Type,DWORD Value) 
{
	return mIDirect3DDevice9_Real->SetSamplerState(Sampler, Type,Value) ;
}

HRESULT IDirect3DDevice9_Wrapper::ValidateDevice(DWORD* pNumPasses) 
{
	return mIDirect3DDevice9_Real->ValidateDevice(pNumPasses) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetPaletteEntries(UINT PaletteNumber,CONST PALETTEENTRY* pEntries) 
{
	return mIDirect3DDevice9_Real->SetPaletteEntries(PaletteNumber, pEntries) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetPaletteEntries(UINT PaletteNumber,PALETTEENTRY* pEntries) 
{
	return mIDirect3DDevice9_Real->GetPaletteEntries(PaletteNumber, pEntries) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetCurrentTexturePalette(UINT PaletteNumber) 
{
	return mIDirect3DDevice9_Real->SetCurrentTexturePalette(PaletteNumber) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetCurrentTexturePalette(UINT *PaletteNumber) 
{
	return mIDirect3DDevice9_Real->GetCurrentTexturePalette(PaletteNumber) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetScissorRect(CONST RECT* pRect) 
{
	return mIDirect3DDevice9_Real->SetScissorRect( pRect) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetScissorRect(RECT* pRect) 
{
	return mIDirect3DDevice9_Real->GetScissorRect(pRect) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetSoftwareVertexProcessing(BOOL bSoftware) 
{
	return mIDirect3DDevice9_Real->SetSoftwareVertexProcessing(bSoftware) ;
}

BOOL IDirect3DDevice9_Wrapper::GetSoftwareVertexProcessing() 
{
	return mIDirect3DDevice9_Real->GetSoftwareVertexProcessing() ;
}

HRESULT IDirect3DDevice9_Wrapper::SetNPatchMode(float nSegments) 
{
	return mIDirect3DDevice9_Real->SetNPatchMode(nSegments) ;
}

float IDirect3DDevice9_Wrapper::GetNPatchMode() 
{
	return mIDirect3DDevice9_Real->GetNPatchMode() ;
}

HRESULT IDirect3DDevice9_Wrapper::DrawIndexedPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType,UINT MinVertexIndex,UINT NumVertices,UINT PrimitiveCount,CONST void* pIndexData,D3DFORMAT IndexDataFormat,CONST void* pVertexStreamZeroData,UINT VertexStreamZeroStride) 
{
	//interestingly, this function is only called when we touch mud
	return mIDirect3DDevice9_Real->DrawIndexedPrimitiveUP(PrimitiveType, MinVertexIndex,NumVertices,PrimitiveCount,pIndexData,IndexDataFormat,pVertexStreamZeroData,VertexStreamZeroStride) ;
}

HRESULT IDirect3DDevice9_Wrapper::ProcessVertices(UINT SrcStartIndex,UINT DestIndex,UINT VertexCount,IDirect3DVertexBuffer9* pDestBuffer,IDirect3DVertexDeclaration9* pVertexDecl,DWORD Flags) 
{
	return mIDirect3DDevice9_Real->ProcessVertices(SrcStartIndex, DestIndex,VertexCount,pDestBuffer,pVertexDecl,Flags) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateVertexDeclaration(CONST D3DVERTEXELEMENT9* pVertexElements,IDirect3DVertexDeclaration9** ppDecl) 
{
	return mIDirect3DDevice9_Real->CreateVertexDeclaration(pVertexElements, ppDecl) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetVertexDeclaration(IDirect3DVertexDeclaration9* pDecl) 
{
	return mIDirect3DDevice9_Real->SetVertexDeclaration(pDecl) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetVertexDeclaration(IDirect3DVertexDeclaration9** ppDecl) 
{
	return mIDirect3DDevice9_Real->GetVertexDeclaration(ppDecl) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetFVF(DWORD FVF) 
{
	return mIDirect3DDevice9_Real->SetFVF(FVF) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetFVF(DWORD* pFVF) 
{
	return mIDirect3DDevice9_Real->GetFVF(pFVF) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateVertexShader(CONST DWORD* pFunction,IDirect3DVertexShader9** ppShader) 
{
	return mIDirect3DDevice9_Real->CreateVertexShader(pFunction, ppShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetVertexShader(IDirect3DVertexShader9* pShader) 
{
	return mIDirect3DDevice9_Real->SetVertexShader(pShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetVertexShader(IDirect3DVertexShader9** ppShader) 
{
	return mIDirect3DDevice9_Real->GetVertexShader(ppShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetVertexShaderConstantF(UINT StartRegister,CONST float* pConstantData,UINT Vector4fCount) 
{
	return mIDirect3DDevice9_Real->SetVertexShaderConstantF(StartRegister, pConstantData,Vector4fCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetVertexShaderConstantF(UINT StartRegister,float* pConstantData,UINT Vector4fCount) 
{
	return mIDirect3DDevice9_Real->GetVertexShaderConstantF(StartRegister, pConstantData,Vector4fCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetVertexShaderConstantI(UINT StartRegister,CONST int* pConstantData,UINT Vector4iCount) 
{
	return mIDirect3DDevice9_Real->SetVertexShaderConstantI(StartRegister, pConstantData,Vector4iCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetVertexShaderConstantI(UINT StartRegister,int* pConstantData,UINT Vector4iCount) 
{
	return mIDirect3DDevice9_Real->GetVertexShaderConstantI(StartRegister, pConstantData,Vector4iCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetVertexShaderConstantB(UINT StartRegister,CONST BOOL* pConstantData,UINT  BoolCount) 
{
	return mIDirect3DDevice9_Real->SetVertexShaderConstantB(StartRegister, pConstantData, BoolCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetVertexShaderConstantB(UINT StartRegister,BOOL* pConstantData,UINT BoolCount) 
{
	return mIDirect3DDevice9_Real->GetVertexShaderConstantB(StartRegister, pConstantData,BoolCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetStreamSource(UINT StreamNumber,IDirect3DVertexBuffer9* pStreamData,UINT OffsetInBytes,UINT Stride) 
{
	return mIDirect3DDevice9_Real->SetStreamSource(StreamNumber, pStreamData,OffsetInBytes,Stride) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetStreamSource(UINT StreamNumber,IDirect3DVertexBuffer9** ppStreamData,UINT* pOffsetInBytes,UINT* pStride) 
{
	return mIDirect3DDevice9_Real->GetStreamSource(StreamNumber, ppStreamData,pOffsetInBytes,pStride) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetStreamSourceFreq(UINT StreamNumber,UINT Setting) 
{
	return mIDirect3DDevice9_Real->SetStreamSourceFreq(StreamNumber, Setting) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetStreamSourceFreq(UINT StreamNumber,UINT* pSetting) 
{
	return mIDirect3DDevice9_Real->GetStreamSourceFreq(StreamNumber, pSetting) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetIndices(IDirect3DIndexBuffer9* pIndexData) 
{
	return mIDirect3DDevice9_Real->SetIndices(pIndexData) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetIndices(IDirect3DIndexBuffer9** ppIndexData) 
{
	return mIDirect3DDevice9_Real->GetIndices(ppIndexData) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreatePixelShader(CONST DWORD* pFunction,IDirect3DPixelShader9** ppShader) 
{
	return mIDirect3DDevice9_Real->CreatePixelShader(pFunction, ppShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetPixelShader(IDirect3DPixelShader9* pShader) 
{
	return mIDirect3DDevice9_Real->SetPixelShader(pShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetPixelShader(IDirect3DPixelShader9** ppShader) 
{
	return mIDirect3DDevice9_Real->GetPixelShader(ppShader) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetPixelShaderConstantF(UINT StartRegister,CONST float* pConstantData,UINT Vector4fCount) 
{
	return mIDirect3DDevice9_Real->SetPixelShaderConstantF(StartRegister, pConstantData,Vector4fCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetPixelShaderConstantF(UINT StartRegister,float* pConstantData,UINT Vector4fCount) 
{
	return mIDirect3DDevice9_Real->GetPixelShaderConstantF(StartRegister, pConstantData,Vector4fCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetPixelShaderConstantI(UINT StartRegister,CONST int* pConstantData,UINT Vector4iCount) 
{
	return mIDirect3DDevice9_Real->SetPixelShaderConstantI(StartRegister, pConstantData,Vector4iCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetPixelShaderConstantI(UINT StartRegister,int* pConstantData,UINT Vector4iCount) 
{
	return mIDirect3DDevice9_Real->GetPixelShaderConstantI(StartRegister, pConstantData,Vector4iCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::SetPixelShaderConstantB(UINT StartRegister,CONST BOOL* pConstantData,UINT  BoolCount) 
{
	return mIDirect3DDevice9_Real->SetPixelShaderConstantB(StartRegister, pConstantData, BoolCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::GetPixelShaderConstantB(UINT StartRegister,BOOL* pConstantData,UINT BoolCount) 
{
	return mIDirect3DDevice9_Real->GetPixelShaderConstantB(StartRegister, pConstantData,BoolCount) ;
}

HRESULT IDirect3DDevice9_Wrapper::DrawRectPatch(UINT Handle,CONST float* pNumSegs,CONST D3DRECTPATCH_INFO* pRectPatchInfo) 
{
	return mIDirect3DDevice9_Real->DrawRectPatch(Handle, pNumSegs,pRectPatchInfo) ;
}

HRESULT IDirect3DDevice9_Wrapper::DrawTriPatch(UINT Handle,CONST float* pNumSegs,CONST D3DTRIPATCH_INFO* pTriPatchInfo) 
{
	return mIDirect3DDevice9_Real->DrawTriPatch(Handle, pNumSegs, pTriPatchInfo) ;
}

HRESULT IDirect3DDevice9_Wrapper::DeletePatch(UINT Handle) 
{
	return mIDirect3DDevice9_Real->DeletePatch(Handle) ;
}

HRESULT IDirect3DDevice9_Wrapper::CreateQuery(D3DQUERYTYPE Type,IDirect3DQuery9** ppQuery) 
{
	return mIDirect3DDevice9_Real->CreateQuery(Type, ppQuery) ;
}