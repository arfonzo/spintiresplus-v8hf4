/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPMenuManager.h"
#include "Common.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "STPDialog.h"
#include "GameFunctions.h"

///////////////////// windowproc hook
LRESULT CALLBACK HookedWindowProc(HWND hwnd,UINT uMsg, WPARAM wParam,LPARAM lParam)
{
	STPMenuManager* Manager=STPMenuManager::Get();
	if (Manager->IsVisible())
	{
		if(uMsg==WM_CLOSE)
		{
			Manager->StopRenderingLoop();
			//return 0; //also forward the message to the base game
		}
		else if(TwEventWin(hwnd, uMsg, wParam, lParam) )
		{	// Send event message to AntTweakBar
			return 0; // Event has been handled by AntTweakBar
		}
		else if (uMsg>=WM_MOUSEFIRST && uMsg<=WM_MOUSELAST)
		{	//don't forward mouse input to base game
			return 0;
		}
		else if (uMsg>=WM_KEYFIRST && uMsg<=WM_KEYLAST)
		{	//don't forward keyboard input to base game
			return 0;
		}
		
		//return DefWindowProc(hwnd, uMsg, wParam, lParam);
		return CallWindowProc(STPMenuManager::Get()->GetGameWindowProc(),hwnd,uMsg, wParam,lParam);
	}
	else
	{
		return CallWindowProc(STPMenuManager::Get()->GetGameWindowProc(),hwnd,uMsg, wParam,lParam);
	}
}

/////////////////// STPMenuManager class implemantation
STPMenuManager::STPMenuManager(void)
:mWindow(0)
,mD3DDevice(NULL)
,mMenuVisible(false)
,mOriginalWindowProc(NULL)
{
}

void STPMenuManager::Init(HWND Window,IDirect3DDevice9* D3DDevice)
{
	mD3DDevice=D3DDevice;
	mWindow=Window;

	//hook this window's WindowProc
	if (!mOriginalWindowProc)
	{	//the game will never create a second d3d window, so we must not
		//hook several times
		mOriginalWindowProc=reinterpret_cast<WNDPROC>(GetWindowLong(Window,GWL_WNDPROC));
		SetWindowLong(Window,GWL_WNDPROC,reinterpret_cast<LONG>(&HookedWindowProc));
	}

	if(!TwInit(TW_DIRECT3D9, mD3DDevice) )
	{	//first time going through the function?
		LOG("Overlay Init Failed : %s\n",TwGetLastError());
	}	
}

STPMenuManager::~STPMenuManager(void)
{
	TwTerminate();
}

STPMenuManager* STPMenuManager::Get()
{
	
	static STPMenuManager Instance;
	return &Instance;
}

bool STPMenuManager::Draw()
{
	// Clear screen and begin draw
	if (FAILED(mD3DDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, D3DCOLOR_ARGB(255, 0, 0, 0), 1.0f, 0)))
	{
		return true;
	}
	if (FAILED(mD3DDevice->BeginScene()))
	{
		return true;
	}
	//draw scene
	//my dialogs need gamma correction
	mD3DDevice->SetRenderState(D3DRS_SRGBWRITEENABLE,TRUE);
	const uint DialogCount=mDialogs.size();
	for (uint i=0;i<DialogCount;++i)
	{
		mDialogs[i]->Render(mD3DDevice);
	}

	//...but not ant tweak bar
	mD3DDevice->SetRenderState(D3DRS_SRGBWRITEENABLE,FALSE);
	if (TwDraw()==0)
	{	//TwDraw returns 0 on error
		return true;
	}

	if (FAILED(mD3DDevice->EndScene()))
	{
		return true;
	}

	// Present frame buffer
	if(FAILED(mD3DDevice->Present(NULL, NULL, NULL, NULL)))
	{
		return true;
		
	}

	//no error
	return false;
}

void STPMenuManager::StopRenderingLoop()
{
	mMenuVisible=false;
}

//basic message pump and rendering loop for when the game won't call Begin/EndScene,
//and Present() itself. Call StopRenderingLoop() to exit this function
void STPMenuManager::RenderingLoop()
{
	//__int64 tic,tac,freq;
	//QueryPerformanceFrequency ((LARGE_INTEGER*)&freq);

	//initialize all registered dialogs
	OnDeviceReset();

	mMenuVisible=true;
	bool DeviceLost=false;
	while( IsVisible() )
    {
		const DWORD StartTime=GetTickCount();

		// Process windows messages
        MSG msg;
		while( PeekMessage(&msg, mWindow, 0, 0, PM_REMOVE) )
        {
			//actually those 3 never seem to come through here
			//but they are in HookedWindowProc
            if( msg.message==WM_QUIT ||  msg.message==WM_DESTROY ||  msg.message==WM_CLOSE)
			{
                StopRenderingLoop();
				//also let the base game handle those messages
			}
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

		if (!DeviceLost)
		{
			if (Draw())
			{	//if there was an error when drawing
				DeviceLost=true;
				OnDeviceLost();
			}
		}
		else
		{
			GameMainRenderFunc(); //this is the game's function to handle lost d3d devices
			HRESULT hr=mD3DDevice->TestCooperativeLevel();
			if (hr==D3D_OK)
			{
				DeviceLost=false;
				OnDeviceReset();
			}
		}

		//frame pacing
		const DWORD EndTime=GetTickCount();	
		DWORD TargetFrameTime=1000/60; //cap at 60 fps
		if (DeviceLost)
		{
			//just 5 polls/s when we lost focus should be enough
			Sleep(200); 
		}
		else if (StartTime<EndTime && EndTime<StartTime+TargetFrameTime )
		{
			Sleep(TargetFrameTime - (EndTime-StartTime) );
		}
	}

	//free AntTweakBar's ressources, since it's not used anymore.
	TwWindowSize(0, 0); 
}

WNDPROC STPMenuManager::GetGameWindowProc() const
{
	return mOriginalWindowProc;
}

void STPMenuManager::OnDeviceLost()
{
	const uint DialogCount=mDialogs.size();
	for (uint i=0;i<DialogCount;++i)
	{
		mDialogs[i]->OnDeviceLost();
	}
	// anttweakbar doc says it's how to handle lost devices
	TwWindowSize(0, 0);
}

void STPMenuManager::OnDeviceReset()
{
	RECT ClientArea;
	GetClientRect (mWindow,&ClientArea);
	TwWindowSize(ClientArea.right-ClientArea.left,ClientArea.bottom-ClientArea.top);

	const uint DialogCount=mDialogs.size();
	for (uint i=0;i<DialogCount;++i)
	{
		mDialogs[i]->Restore();
	}
}

//Adds the specified STPDialog to our list of windows, so that we can
//save/restore their state when we lose focus/d3d device.
void STPMenuManager::RegisterDialog(const STPDialog* Dialog)
{
	mDialogs.push_back(const_cast<STPDialog*>(Dialog));
}

//Removes the specified STPDialog from our list of windows.
void STPMenuManager::UnregisterDialog(const STPDialog* Dialog)
{
	std::vector<STPDialog*>::iterator End=mDialogs.end();
	for (std::vector<STPDialog*>::iterator i=mDialogs.begin();i!=End;++i)
	{
		if ((*i)==Dialog)
		{
			mDialogs.erase(i);
			return;
		}
	}
}

void STPMenuManager::Destroy()
{
	TwTerminate();
}

int STPMenuManager::GetScreenWidth() const
{
	if (mWindow)
	{
		RECT ClientArea;
		GetClientRect(mWindow,&ClientArea);
		return ClientArea.right; //should do right-left, but MSDN says left is always 0.
	}
	else
	{
		return 0;
	}
}

int STPMenuManager::GetScreenHeight() const
{
	if (mD3DDevice)
	{
		RECT ClientArea;
		GetClientRect(mWindow,&ClientArea);
		return ClientArea.bottom; //should do bottom-top, but MSDN says top is always 0.
	}
	else
	{
		return 0;
	}
}

void STPMenuManager::OnLostFocus()
{
	if (mD3DDevice && FAILED(mD3DDevice->TestCooperativeLevel()))
	{
		OnDeviceLost();
	}
}

void STPMenuManager::OnGainedFocus()
{
	//if (mD3DDevice)
	//{
	//	mD3DDevice->ValidateDevice
	//	OnDeviceLost();
	//}
}