/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <string.h>


#include "LODHooks.h"
#include "Settings.h"

//dumps a shader to file, overwriting existing files
void __stdcall SaveShader(const char* Name,const void* Content,size_t Size)
{
	static int Counter;
	Counter++;
	char Filename[64];
	sprintf(Filename,"%s_%d",Name,Counter);


	while(char* temp=strchr(Filename,'/'))
	{
		*temp='_';
	}

	FILE* Dump=fopen(Filename,"w+b");
	fwrite(Content,Size,1,Dump);
	fclose(Dump);
}

//this function replaces a call memcpy(ShaderBuffer,ShaderCache,ShaderSize)
void __cdecl ShaderLoaderHook(void* dest,void* src, size_t count)
{
	if (GlobalSettings::DumpShaders)
	{
		const char* ShaderName;
		__asm
		{
			lea eax,dword ptr [esp+0x194]
			mov [ShaderName],eax
		}
		SaveShader(ShaderName,src,count);
	}

	//extract spintires/mus.fx, find the first vertex shader
	//find where it is doing (Distance_to_EyePos.z-32)*0.125
	//and there you have Pattern.
	//The same construct is present in Terrain.fx and has to be patched too.
	const float Pattern[]={-32,0.125,1};
	const Address SourceAddress=reinterpret_cast<Address>(src);

	for (Address i=SourceAddress;i<SourceAddress+count;++i)
	{
		if (memcmp(reinterpret_cast<void*>(i),&Pattern, sizeof(Pattern))==0)
		{ //if the pattern was found
			*reinterpret_cast<float*>(i)=((float)GlobalSettings::RenderDistanceIncrease+6.0f) * -32.0f/(255.0f-249.0f); //249 is the default render distance setting
		}
	}
	
	memcpy(dest,src,count);
}


//should return 255-Distance.
//Later on, object are NOT drawn if 255-Distance<=249
unsigned char __fastcall AdjustLOD(uint Distance)
{	//the distance happened to be in ecx, so...fastcall!
	uint ReturnValue=255-Distance+GlobalSettings::RenderDistanceIncrease;
	return __min(255,ReturnValue);
}

uint __stdcall GetTileLOD(void *, float, float)
{
	return 5; //5 is the max LOD afaik
}