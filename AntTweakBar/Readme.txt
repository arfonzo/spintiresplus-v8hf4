AntTweakBar development library
-------------------------------


AntTweakBar is a small and easy-to-use C/C++ library that allows programmers
to quickly add a light and intuitive GUI into OpenGL and DirectX based 
graphic programs to interactively tweak parameters.

For installation and documentation please refer to:
http://anttweakbar.sourceforge.net/doc

-------------------------------
This version has been slightly modified to adapt SpintiresPlus needs, specificaly:
-Only directx9 support was needed, so support (and dependency) on other rendering
engines has been removed.
-The built-in unremovable help bar has been removed.
-Added support for callbacks when menu selection (highlighted item) changes.

