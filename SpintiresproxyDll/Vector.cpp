/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "Vector.h"
#include <math.h>
/////////////////// Vector 3 class
Vector3::Vector3(float x, float y, float z)
{
	X=x;
	Y=y;
	Z=z;
}
void Vector3::operator-=(const Vector3& Other)
{
	X-=Other.X;
	Y-=Other.Y;
	Z-=Other.Z;
}

void Vector3::operator+=(const Vector3& Other)
{
	X+=Other.X;
	Y+=Other.Y;
	Z+=Other.Z;
}

float Vector3::Length3D() const
{
	return sqrt(X*X + Y*Y + Z*Z);
}

float Vector3::ScalarProduct3D(const Vector3& Other) const
{
	return X*Other.X + Y*Other.Y + Z*Other.Z;
}

//assumes that *this* holds the coordinates of a point and returns
//the distance to Point.
float Vector3::PointToPoint3DDistance(const Vector3& Point) const
{
	float XDist=X-Point.X;
	float YDist=Y-Point.Y;
	float ZDist=Z-Point.Z;
	return sqrt(XDist*XDist + YDist*YDist + ZDist*ZDist);
}

void Vector3::Scale(const float ScaleFactor)
{
	X*=ScaleFactor;
	Y*=ScaleFactor;
	Z*=ScaleFactor;
}

void Vector3::Normalize3D()
{
	float Length=Length3D();
	X/=Length;
	Y/=Length;
	Z/=Length;
}
/////////////////// Vector 4 class
Vector4::Vector4(float x, float y, float z, float w)
:Vector3(x,y,z)
{
	W=w;
}
void Vector4::operator-=(const Vector4& Other)
{
	Vector3::operator -=(Other);
	W-=Other.W;
}

void Vector4::operator+=(const Vector4& Other)
{
	Vector3::operator +=(Other);
	W+=Other.W;
}

void Vector4::Scale(const float ScaleFactor)
{
	Vector3::Scale(ScaleFactor);
	W*=ScaleFactor;
}

//*this* becomes Vector transformed by Transform
//Transform is a 3 row 4 column matrix
void Vector4::TransformedVector(const Vector4& Vector,const float* Transform)
{
	const int Columns=4;
	X=Vector.X*Transform[0+Columns*0] + Vector.Y*Transform[0+Columns*1] + Vector.Z*Transform[0+Columns*2] + Transform[0+Columns*3];
	Y=Vector.X*Transform[1+Columns*0] + Vector.Y*Transform[1+Columns*1] + Vector.Z*Transform[1+Columns*2] + Transform[1+Columns*3];
	Z=Vector.X*Transform[2+Columns*0] + Vector.Y*Transform[2+Columns*1] + Vector.Z*Transform[2+Columns*2] + Transform[2+Columns*3];
	W=Vector.X*Transform[3+Columns*0] + Vector.Y*Transform[3+Columns*1] + Vector.Z*Transform[3+Columns*2] + Transform[3+Columns*3];
}

float Vector4::Dot(const Vector4 & Other) const
{
	return X*Other.X + Y*Other.Y + Z*Other.Z + W*Other.W;
}

/////////////////////////////////// Old Vector manipulation ///////////////////////////////
//Computes Output=Vector1 x Vector2
void VectorProduct(const Vector3& Vector1,const Vector3& Vector2,Vector3& Output)
{
	Output.X=Vector1.Y*Vector2.Z - Vector1.Z*Vector2.Y;
	Output.Y=Vector1.Z*Vector2.X - Vector1.X*Vector2.Z;
	Output.Z=Vector1.X*Vector2.Y - Vector1.Y*Vector2.X;
}

float VectorScalarProduct(const Vector3& Vector1,const Vector3& Vector2)
{
	return Vector1.X*Vector2.X + Vector1.Y*Vector2.Y + Vector1.Z*Vector2.Z;
}

void VectorNormalize(Vector3& Input)
{
	float Length=Input.Length3D();
	Input.X/=Length;
	Input.Y/=Length;
	Input.Z/=Length;
}

void ProjectVectorOnPlane(const Vector3& Input,const Vector3& PlaneNormalVector,Vector3& Output)
{
	float Scale=VectorScalarProduct(Input,PlaneNormalVector);
	Output.X=Input.X-Scale*PlaneNormalVector.X;
	Output.Y=Input.Y-Scale*PlaneNormalVector.Y;
	Output.Z=Input.Z-Scale*PlaneNormalVector.Z;
}

void MultiplyBy3x3Matrix(const Vector3& Input,const float BaseChangeMatrix[3*3],Vector3& Output)
{
	Output.X=Input.X*BaseChangeMatrix[0+0*3] + Input.Y*BaseChangeMatrix[1+0*3] + Input.Z*BaseChangeMatrix[2+0*3];
	Output.Y=Input.X*BaseChangeMatrix[0+1*3] + Input.Y*BaseChangeMatrix[1+1*3] + Input.Z*BaseChangeMatrix[2+1*3];
	Output.Z=Input.X*BaseChangeMatrix[0+2*3] + Input.Y*BaseChangeMatrix[1+2*3] + Input.Z*BaseChangeMatrix[2+2*3];
}