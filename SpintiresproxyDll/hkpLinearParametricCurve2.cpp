/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include  <limits>
#include <Physics/Dynamics/Constraint/Bilateral/PointToPath/hkpLinearParametricCurve.h>
#include <Physics/Dynamics/Constraint/Bilateral/PointToPath/hkpPointToPathConstraintData.h>

#include "hkpLinearParametricCurve2.h"
#include "Common.h"
#include "typedefs.h"

/*
Ideally those functions would be members of a hkpLinearParametricCurve subclass, but the problem is that
virtual function indexes aren't the same in my and Pavel's version of havok, and for example getTangent maps
to a function that just "return 0.0f".
*/

/*Quadratic interpolation between 3 points, where
Ratio=0 returns PointA
Ratio=1 returns PointC
and anything inbetween leans more or less toward PointB */
void __stdcall QuadraticInterpolate(const hkVector4& PointA,const hkVector4& PointB,const hkVector4& PointC,hkReal Ratio, hkVector4& Output)
{
	hkReal u=1.0f-Ratio;
	Output.setMul4(u * u, PointA);
	Output.addMul4(2.0f * Ratio * u, PointB);
	Output.addMul4(Ratio * Ratio, PointC);
}

//fetches the tolerance stored for a specific point
hkReal ExtractTolerance(const hkVector4& Point)
{
	return Point.getSimdAt(3);
}

//returns the tolerance at a specified abcissa, linearily interpolated with the tolerance
//of previous & next nodes
hkReal hkpLinearParametricCurve_getToleranceAt(hkpLinearParametricCurve* This,hkReal t)
{
	int Index=static_cast<int>(t);
	int PrevNode=Index-1;
	int NextNode=Index+1;
	const int MaxIndex=This->m_points.getSize()-1;
	const hkReal SegmentProgression=t - ((float)Index);
	if (This->m_closedLoop)
	{
		if (PrevNode<0)
		{
			PrevNode=MaxIndex-2;
		}
		if (NextNode>MaxIndex)
		{
			NextNode=2;
		}
	}
	else
	{
		if (PrevNode<0)
		{
			PrevNode=0;
		}
		if (NextNode>MaxIndex)
		{
			NextNode=MaxIndex;
		}
	}

	if ( SegmentProgression < 0.5f) //first half of the segment
	{
		const hkReal JunctionTolerance=0.5f*( ExtractTolerance(This->m_points[PrevNode]) + ExtractTolerance(This->m_points[Index]) );
		return Lerp(SegmentProgression,0.0f,0.5f, JunctionTolerance, ExtractTolerance(This->m_points[Index]) );
	}
	else
	{
		const hkReal JunctionTolerance=0.5f*( ExtractTolerance(This->m_points[NextNode]) + ExtractTolerance(This->m_points[Index]) );
		return Lerp(SegmentProgression,0.5f,1.0f, ExtractTolerance(This->m_points[Index]), JunctionTolerance);
	}
}

/*returns the point at CurveRatio belonging to the Index-th segment
CurveRatio should always be in the 0-1 range
*/
void __stdcall hkpLinearParametricCurve_getPointOnSegment(hkpLinearParametricCurve* This,int Index,hkReal CurveRatio,hkVector4& Output)
{
	const int MaxIndex=This->m_points.getSize()-1;
	//preliminary filter, makes sure the required indexes exist for the rest of the process
	if (MaxIndex==1)
	{
		Output.setInterpolate4(This->m_points[0],This->m_points[1],CurveRatio);
		return;
	}
	else if (Index==MaxIndex)
	{
		if (This->m_closedLoop)
		{
			Index=1;
		}
		else
		{	//open path, index=last index, we can only return the last point here
			Output=This->m_points[MaxIndex];
			return;
		}
	}

	//if we're here, we are guaranteed than Index+1 exists
	if (CurveRatio < This->m_smoothingFactor)
	{
		if (Index==0 )
		{
			if (This->m_closedLoop)
			{
				Index=MaxIndex-1;
			}
			else
			{
				Output.setInterpolate4(This->m_points[0],This->m_points[1],CurveRatio);
				return;
			}
		}
		hkVector4 PointA,PointB,PointC;
		PointB=This->m_points[Index];
		PointC.setInterpolate4(PointB, This->m_points[Index+1], This->m_smoothingFactor);
		PointA.setInterpolate4(PointB, This->m_points[Index-1], This->m_smoothingFactor);

		//when CurveRatio=m_smoothingFactor we want to get PointC (the one on this segment)
		//when CurveRatio=0 we want the halfway interpolation between A and C (and B).
		QuadraticInterpolate(PointA, PointB, PointC, 0.5f+ 0.5f*CurveRatio/This->m_smoothingFactor, Output);

	}
	else if (CurveRatio> 1.0f-This->m_smoothingFactor)
	{
		//Index==MaxIndex cannot happen, due to the preliminary filter
		if (Index==MaxIndex-1)
		{
			if (This->m_closedLoop)
			{
				Index=0;
			}
			else
			{
				Output.setInterpolate4(This->m_points[MaxIndex-1],This->m_points[MaxIndex],CurveRatio);
				return;
			}
		}
		hkVector4 PointA,PointB,PointC;
		PointB=This->m_points[Index+1];
		PointC.setInterpolate4(PointB, This->m_points[Index+2], This->m_smoothingFactor);
		PointA.setInterpolate4(PointB, This->m_points[Index], This->m_smoothingFactor);

		//when CurveRatio=1-m_smoothingFactor we want to get PointA (the one on this segment)
		//when CurveRatio=1 we want the halfway interpolation between A and C (and B).
		QuadraticInterpolate(PointC, PointB, PointA, 0.5f+ 0.5f*(1.0f-CurveRatio)/This->m_smoothingFactor, Output);
	}
	else
	{
		Output.setInterpolate4(This->m_points[Index],This->m_points[Index+1],CurveRatio);
	}
}



void hkpLinearParametricCurve_getPoint2(hkpLinearParametricCurve* This, hkReal t, hkVector4& pointOnPath)
{
	/*
	//-The asm bit below does: Curve->getPoint(t,pointOnPath);
	//This construct is required because the index of getPoint in the vftable ins't the same
	//in my version of havok and Pavel's
	__asm 
	{
		mov ecx,[This]
		//pass arguments
		push [pointOnPath]

		push [t]

		mov eax,[ecx]	//eax=vftable ptr
		mov eax,[eax+8]	//eax=3rd virtual function=getPoint
		call eax
	}
	*/

	int Index=static_cast<int>(t);
	float CurveRatio=t-((float)Index);
	Index=__clamp(0,Index,This->m_points.getSize()-1);
	hkpLinearParametricCurve_getPointOnSegment(This, Index, CurveRatio, pointOnPath);
}

//////////// hkpLinearParametricCurve::getTangent rewrite
//returns the tangent in the middle of the segment starting at Index
void __stdcall hkpLinearParametricCurve_GetMidpointTangent(hkpLinearParametricCurve* This, int Index, hkVector4& tangent ) 
{
	//handling last point
	if (Index==This->m_points.getSize()-1)
	{
		if (This->m_closedLoop)
		{
			Index=1;
		}
		else
		{
			Index--;
		}
	}

	tangent.setSub4(This->m_points[Index+1],This->m_points[Index]);
	tangent.mul4(1.0f/(This->m_distance[Index+1]-This->m_distance[Index])); //same as normalizing
}


//returns the tangent in the middle of the segment starting at Index
void __stdcall hkpLinearParametricCurve_GetMidpointTangent(hkpLinearParametricCurve* This, hkReal t, hkVector4& tangent ) 
{
	int Index=__clamp(0,static_cast<int>(t),This->m_points.getSize()-1);
	hkpLinearParametricCurve_GetMidpointTangent(This,Index,tangent);
}


//returns the tangent at abscissa t of the path
void __stdcall hkpLinearParametricCurve_getSmoothTangent(hkpLinearParametricCurve* This, hkReal t, hkVector4& tangent ) 
{
	if (!This->m_smoothingFactor)
	{	//smoothing disabled
		hkpLinearParametricCurve_GetMidpointTangent(This,t,tangent);
	}
	else	
	{	//smoothing enabled
		int Index=static_cast<int>(t);
		float CurveRatio=abs(t-((float)Index));

		//we could do index=clamp(0,t,...) and then calculate CurveRatio
		//but CurveRatio would be very wrong if t were negative (which CAN happen).
		//Index=__clamp(0,Index,This->m_points.getSize()-1);

		if (CurveRatio<This->m_smoothingFactor)
		{
			hkVector4 TangentHere;
			hkpLinearParametricCurve_GetMidpointTangent(This,Index,TangentHere);


			hkVector4 PrevTangent;
			if (Index==0)
			{
				if (This->m_closedLoop)
				{
					hkpLinearParametricCurve_GetMidpointTangent(This,This->m_points.getSize()-3,PrevTangent);
				}
				else
				{	//at the beginning of the first segment and not looping
					//return the midpoint tangent
					tangent=TangentHere;
					return;
				}
			}
			else
			{
				hkpLinearParametricCurve_GetMidpointTangent(This,Index-1,PrevTangent);
			}

			//at curve ratio 0, we want a 50% mix of TangentHere and PrevTangent
			//at curve ratio=m_smoothingFactor, we want TangentHere
			tangent.setInterpolate4(PrevTangent,TangentHere, 0.5f + 0.5f*CurveRatio/This->m_smoothingFactor);
		}
		else if (CurveRatio>1.0f-This->m_smoothingFactor)
		{
			hkVector4 TangentHere;
			hkpLinearParametricCurve_GetMidpointTangent(This,Index,TangentHere);

			hkVector4 NextTangent;
			if (Index==This->m_points.getSize()-1 && This->m_closedLoop)
			{
				if (This->m_closedLoop)
				{
					hkpLinearParametricCurve_GetMidpointTangent(This,1,NextTangent);
				}
				else
				{	//at the beginning of the first segment and not looping
					//return the midpoint tangent
					tangent=TangentHere;
					return;
				}
			}
			else
			{
				hkpLinearParametricCurve_GetMidpointTangent(This,Index+1,NextTangent);
			}

			//when CurveRatio = 1, we want a 50% mix of TangentHere and NextTangent
			//at CurveRatio= 1-m_smoothingFactor, we want TangentHere
			tangent.setInterpolate4(NextTangent,TangentHere, 0.5f + 0.5f*(1.0f-CurveRatio)/This->m_smoothingFactor );
		}
		else
		{	//CurveRatio between m_smoothingFactor and 1-m_smoothingFactor
			hkpLinearParametricCurve_GetMidpointTangent(This,Index,tangent);
		}
	}
}

void __stdcall hkpLinearParametricCurve_getTangent2( hkReal t, hkVector4& tangent ) 
{
	//DO NOT put any code before the __asm part!!
	//store this pointer
	hkpLinearParametricCurve* This;
	__asm 
	{
		mov [This],ecx
	}

	hkpLinearParametricCurve_getSmoothTangent(This,t,tangent);
	//LOG("tan: %f %f at %f\n",tangent.getSimdAt(0),tangent.getSimdAt(1),t);

}

//rewrite of hkpLinearParametricCurve::getNearestPoint because this function may
//crash (infinite recusrion->stack overflow) and fails at finding the nearest point if t isn't a good hint already.
//also when changing trucks, the "t" of each body gets reset to 0 but not their position resulting in items
//trying to magnet to faraway position (often through other bodies).
//getNearestPoint2 does NOT use t.
hkReal __stdcall hkpLinearParametricCurve_getNearestPoint2_Internal(hkpLinearParametricCurve* This, hkReal t, const hkVector4& nearPoint, hkVector4& pointOnPath )
{
	hkReal ClosestDistance=std::numeric_limits<float>::max();
	hkReal ClosestPointAbscissa;
	hkVector4 ClosestPoint(nearPoint);

	//on closed loops the last 2 points are clones of the first 2
	//so we ignore the last node to avoid processing the first segment twice.
	int MaxIndex=This->m_points.getSize()-1;
	if (This->m_closedLoop)
	{
		MaxIndex--;
	}

	//loop over all points in this curve
	for (int i=0;i<MaxIndex;i++)
	{
		
		//get tangent at point i
		hkVector4 Segment(This->m_points[i+1]);
		Segment.sub4(This->m_points[i]);

		//get segment length
		hkSimdReal SegmentLength=This->m_distance[i+1]-This->m_distance[i]; //the loop guarantees i+1 exists.

		//normalize tangent
		//Segment.mul4(1.0f/SegmentLength); //we'll do that later
		

		//calculate the point i to nearPoint vector
		hkVector4 Target(nearPoint);
		Target.sub4(This->m_points[i]);

		//find at what percentage of the segment is the projection of nearPoint

		//divide by SegmentLength once because Segment wasn't normalized
		//divide by SegmentLength twice to get a 0-1 ratio
		hkReal CurveRatio=Segment.dot3(Target)/(SegmentLength * SegmentLength );

			
		// make sure the abscissa stays within the segment
		CurveRatio=__clamp(0.0f,CurveRatio,1.0f);

		hkVector4 ClosestPointOnThisSegment;
		hkpLinearParametricCurve_getPointOnSegment(This,i,CurveRatio,ClosestPointOnThisSegment);
		

		hkReal SquaredDistanceToTarget;
		{
			hkVector4 Distance;
			Distance.setSub4(ClosestPointOnThisSegment,nearPoint);
			SquaredDistanceToTarget=Distance.lengthSquared3();
		}

		if (SquaredDistanceToTarget<ClosestDistance)
		{
			ClosestDistance=SquaredDistanceToTarget;
			ClosestPointAbscissa=((float)i) + CurveRatio;
			ClosestPoint=ClosestPointOnThisSegment;
		}
	}

	if (This->m_smoothingFactor)
	{
		//due to smoothing and the resulting curves (portion of circles) we
		//use basic iterative solver to find where the Path_point->near_point vector
		//is perpendicular to the tangent (scalar product=0) which is where the true closest point.

		float Min=-0.5f;
		float Max=0.5f;
		float Prev=0;
		for (int i=0; i<7 ; i++)	//7 steps should be enough (splits the segment into 128 chunks)
		{
			hkVector4 Target(nearPoint);
			Target.sub4(ClosestPoint);

			hkVector4 Tangent;
			hkpLinearParametricCurve_getSmoothTangent(This,ClosestPointAbscissa+Prev,Tangent);

			float ScalarProduct=Tangent.dot3(Target);
			if (abs(ScalarProduct)<INSIGNIFICANT)
			{	//close enough
				break;
			}
			if (ScalarProduct>0)
			{
				Min=Prev;
			}
			else
			{
				Max=Prev;
			}
			Prev=0.5f*(Min+Max);
			hkpLinearParametricCurve_getPoint2(This,ClosestPointAbscissa+Prev,ClosestPoint);
		}
		ClosestPointAbscissa+=Prev;		
	}

	const hkReal Tolerance=hkpLinearParametricCurve_getToleranceAt(This,ClosestPointAbscissa);
	const hkReal SqTolerance=Tolerance*Tolerance;
	if (Tolerance)
	{
		if ( This->m_dirNotParallelToTangentAlongWholePath.equals3(hkVector4(0,0,0)))
			/*This->m_dirNotParallelToTangentAlongWholePath.getSimdAt(0) ||
			 This->m_dirNotParallelToTangentAlongWholePath.getSimdAt(1) ||
			 This->m_dirNotParallelToTangentAlongWholePath.getSimdAt(2) )*/
		{	//if it is set then tolerance only applies perpendicularly to NeverParallelDir

			//Project nearPoint on the plane normal to m_dirNotParallelToTangentAlongWholePath.
			hkVector4 nearPointProjection;
			nearPointProjection.setCross(nearPoint,This->m_dirNotParallelToTangentAlongWholePath);
			nearPointProjection.setCross(This->m_dirNotParallelToTangentAlongWholePath,nearPointProjection);

			hkReal ProjectedDistanceToTarget;
			{
				hkVector4 Temp;
				Temp.setSub4(ClosestPoint,nearPointProjection);
				ProjectedDistanceToTarget=Temp.lengthSquared3();
			}
			if (ProjectedDistanceToTarget>SqTolerance) 
			{	//if a tolerance is defined for this node and we are further
				//set ClosestPoint to the projection_on_path + tolerance meters * projection_to_nearPoint vector
				hkVector4 Offset;
				Offset.setSub4(nearPointProjection,ClosestPoint);
				Offset.normalize3IfNotZero();
				ClosestPoint.addMul4(Tolerance,Offset);
			}
			else
			{	//there is a tolerance but we are close enough -> stay at nearPointProjection
				//but keep scanning since we might be even closer to another point on the path
				ClosestPoint=nearPointProjection;
			}
		}
		else
		{	//there is a tolerance but m_dirNotParallelToTangentAlongWholePath is all zero
			//->apply tolerance in any direction

			hkReal DistanceToTarget=ClosestPoint.lengthSquared3();
			{
				hkVector4 Temp;
				Temp.setSub4(ClosestPoint,nearPoint);
				DistanceToTarget=Temp.lengthSquared3();
			}
			if (DistanceToTarget>SqTolerance) 
			{	//if a tolerance is defined for this node and we are further
				//set ClosestPoint to the projection + tolerance meters * projection_to_nearPoint vector
				hkVector4 Offset;
				Offset.setSub4(nearPoint,ClosestPoint);
				Offset.normalize3IfNotZero();
				ClosestPoint.addMul4(Tolerance,Offset);
			}
			else
			{	//there is a tolerance but we are close enough -> no need to move
				//but keep scanning since we might be even closer to another point on the path
				ClosestPoint=pointOnPath;
			}
		}
	}
//	LOG("pt: %f %f res %f %f dist %f\n",pointOnPath.getSimdAt(0),pointOnPath.getSimdAt(1),ClosestPoint.getSimdAt(0),ClosestPoint.getSimdAt(1),ClosestPoint.distanceTo3(pointOnPath));
	pointOnPath=ClosestPoint;
	
	
	return ClosestPointAbscissa;
}

//just a stub to hkpLinearParametricCurve_getNearestPoint2_Internal
hkReal __stdcall hkpLinearParametricCurve_getNearestPoint2( hkReal t, const hkVector4& nearPoint, hkVector4& pointOnPath )
{
	//DO NOT put any code before the __asm part!!
	//store this pointer
	hkpLinearParametricCurve* This;
	hkpPointToPathConstraintData* Constraint;
	__asm 
	{
		mov [This],ecx
		mov [Constraint],esi
	}

	
	hkReal ReturnValue=hkpLinearParametricCurve_getNearestPoint2_Internal(This,t,nearPoint, pointOnPath );
	return ReturnValue;

}

//////////// hkpLinearParametricCurve::getBinormal rewrite
void __stdcall hkpLinearParametricCurve_getBinormal2_Internal(hkpLinearParametricCurve* This, hkReal t, hkVector4& up ) 
{
	hkpLinearParametricCurve_getSmoothTangent(This,t,up);
	up.setCross(up,This->m_dirNotParallelToTangentAlongWholePath);
}

void __stdcall hkpLinearParametricCurve_getBinormal2( hkReal t, hkVector4& up ) 
{
	//DO NOT put any code before the __asm part!!
	//store this pointer
	hkpLinearParametricCurve* This;
	__asm 
	{
		mov [This],ecx
	}

	
	hkpLinearParametricCurve_getBinormal2_Internal(This,t,up);
}