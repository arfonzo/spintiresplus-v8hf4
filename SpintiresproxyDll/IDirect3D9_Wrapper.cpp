/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "IDirect3D9_Wrapper.h"
#include "IDirect3DDevice9_Wrapper.h"
#include "STPMenuManager.h"
#include "Settings.h"
#include "ExternalMinimap.h"

IDirect3D9_Wrapper::IDirect3D9_Wrapper(IDirect3D9* IDirect3D9_Real)
{
	mIDirect3D9_Real=IDirect3D9_Real;
}

IDirect3D9_Wrapper::~IDirect3D9_Wrapper(void)
{
	//delete mIDirect3D9_Real; //done by Release()
}

///////////////// Modified members

HRESULT IDirect3D9_Wrapper::CreateDevice(UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow, DWORD BehaviorFlags, D3DPRESENT_PARAMETERS *pPresentationParameters, IDirect3DDevice9 **ppReturnedDeviceInterface)
{
		
	HRESULT ReturnValue;
	if (GlobalSettings::MinimapEnabled)
	{
		//create the game's device first , so that it gets steam's overlay
		ReturnValue=mIDirect3D9_Real->CreateDevice(Adapter, DeviceType, hFocusWindow, BehaviorFlags, pPresentationParameters, ppReturnedDeviceInterface);

		//then create the minimap's window
		STPMinimapManager* Minimap=STPMinimapManager::Get();
		Minimap->SetD3D(mIDirect3D9_Real);
		if (pPresentationParameters->Windowed)
		{	//in windowed mode both the minimap's and the main game's device can be
			//on the same adapter...
			Minimap->SetMainGameAdapter(-1);	//tells the minimap all adapters are allowed.
		}
		else
		{	//...but in fullscreen mode this is impossible.
			Minimap->SetMainGameAdapter(Adapter);
		}
		Minimap->StartMinimap(hFocusWindow);		

		ShowWindow(hFocusWindow,SW_SHOWNORMAL);	//and give focus only to the main window
	}
	else
	{
		ReturnValue=mIDirect3D9_Real->CreateDevice(Adapter, DeviceType, hFocusWindow, BehaviorFlags, pPresentationParameters, ppReturnedDeviceInterface);
	}
	IDirect3DDevice9_Wrapper* Device=new IDirect3DDevice9_Wrapper(*ppReturnedDeviceInterface);

	*ppReturnedDeviceInterface=Device;
	
	if (OverlayEnabled())
	{
		STPMenuManager::Get()->Init(hFocusWindow,Device);
	}

	return ReturnValue;
}

ULONG IDirect3D9_Wrapper::Release()
{
	if (GlobalSettings::MinimapEnabled)
	{	
		//kill the mininmap when the game tries to free its direct3D object
		//In turn this will deallocate the Direct3dDevice and remove a reference
		//to this d3d object.
		//...but this release doesn't happen through IDirect3D9_Wrapper::Release ?! 
		STPMinimapManager::Get()->Terminate();
	}

	ULONG RefCount=mIDirect3D9_Real->Release();
	
	if (!RefCount)
	{
		delete this;
	}
	return RefCount;
}


///////////////// All function below are just stubs that pass the call to mIDirect3D9_Real

/*** IUnknown methods ***/
HRESULT IDirect3D9_Wrapper::QueryInterface(const IID &riid, void **ppvObj)
{
	return mIDirect3D9_Real->QueryInterface(riid,ppvObj);
}

ULONG IDirect3D9_Wrapper::AddRef()
{
	return mIDirect3D9_Real->AddRef();
}

/*** IDirect3D9 methods ***/
HRESULT IDirect3D9_Wrapper::RegisterSoftwareDevice(void *pInitializeFunction)
{
	return mIDirect3D9_Real->RegisterSoftwareDevice(pInitializeFunction);
}

UINT IDirect3D9_Wrapper::GetAdapterCount()
{
	return mIDirect3D9_Real->GetAdapterCount();
}

HRESULT IDirect3D9_Wrapper::GetAdapterIdentifier(UINT Adapter, DWORD Flags, D3DADAPTER_IDENTIFIER9 *pIdentifier)
{
	return mIDirect3D9_Real->GetAdapterIdentifier(Adapter, Flags, pIdentifier);
}

UINT IDirect3D9_Wrapper::GetAdapterModeCount(UINT Adapter, D3DFORMAT Format)
{
	return mIDirect3D9_Real->GetAdapterModeCount(Adapter, Format);
}

HRESULT IDirect3D9_Wrapper::EnumAdapterModes(UINT Adapter, D3DFORMAT Format, UINT Mode, D3DDISPLAYMODE *pMode)
{
	return mIDirect3D9_Real->EnumAdapterModes(Adapter, Format, Mode, pMode);
}

HRESULT IDirect3D9_Wrapper::GetAdapterDisplayMode(UINT Adapter, D3DDISPLAYMODE *pMode)
{
	return mIDirect3D9_Real->GetAdapterDisplayMode(Adapter,pMode);
}

HRESULT IDirect3D9_Wrapper::CheckDeviceType(UINT Adapter, D3DDEVTYPE DevType, D3DFORMAT AdapterFormat, D3DFORMAT BackBufferFormat, BOOL bWindowed)
{
	return mIDirect3D9_Real->CheckDeviceType(Adapter, DevType, AdapterFormat, BackBufferFormat, bWindowed);
}

HRESULT IDirect3D9_Wrapper::CheckDeviceFormat(UINT Adapter, D3DDEVTYPE DeviceType, D3DFORMAT AdapterFormat, DWORD Usage, D3DRESOURCETYPE RType, D3DFORMAT CheckFormat)
{
	return mIDirect3D9_Real->CheckDeviceFormat(Adapter, DeviceType, AdapterFormat, Usage, RType, CheckFormat);
}

HRESULT IDirect3D9_Wrapper::CheckDeviceMultiSampleType(UINT Adapter, D3DDEVTYPE DeviceType, D3DFORMAT SurfaceFormat, BOOL Windowed, D3DMULTISAMPLE_TYPE MultiSampleType, DWORD *pQualityLevels)
{
	return mIDirect3D9_Real->CheckDeviceMultiSampleType(Adapter, DeviceType, SurfaceFormat, Windowed, MultiSampleType, pQualityLevels);
}

HRESULT IDirect3D9_Wrapper::CheckDepthStencilMatch(UINT Adapter, D3DDEVTYPE DeviceType, D3DFORMAT AdapterFormat, D3DFORMAT RenderTargetFormat, D3DFORMAT DepthStencilFormat)
{
	return mIDirect3D9_Real->CheckDepthStencilMatch(Adapter, DeviceType, AdapterFormat, RenderTargetFormat, DepthStencilFormat);
}

HRESULT IDirect3D9_Wrapper::CheckDeviceFormatConversion(UINT Adapter, D3DDEVTYPE DeviceType, D3DFORMAT SourceFormat, D3DFORMAT TargetFormat)
{
	return mIDirect3D9_Real->CheckDeviceFormatConversion(Adapter, DeviceType, SourceFormat, TargetFormat);
}

HRESULT IDirect3D9_Wrapper::GetDeviceCaps(UINT Adapter, D3DDEVTYPE DeviceType, D3DCAPS9 *pCaps)
{
	return mIDirect3D9_Real->GetDeviceCaps(Adapter, DeviceType, pCaps);
}

HMONITOR IDirect3D9_Wrapper::GetAdapterMonitor(UINT Adapter)
{
	return mIDirect3D9_Real->GetAdapterMonitor(Adapter);
}