/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <Physics/Dynamics/Constraint/hkpConstraintInstance.h>
#include <Physics/Dynamics/Constraint/hkpConstraintData.h>
#include <Physics/Dynamics/Constraint/Bilateral/Prismatic/hkpPrismaticConstraintData.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "STPControlledConstraints.h"
#include "Common.h"
#include "GameStructures.h"
#include "HavokCompat.h"
#include "TruckControls.h"
#include "STPConstraintNamePiggyBack.h"
#include "Hooks.h"
#include "TruckChassis.h"

/////////////////////////////////////
////////////////  STPConstraintController
///////////////////////////////////// 
STPConstraintController::STPConstraintController()
:mLastSeenTruck(NULL)
{
}
STPConstraintController::~STPConstraintController()
{
}

STPConstraintController* STPConstraintController::Get()
{
	static STPConstraintController Instance;
	return &Instance;
}

void STPConstraintController::Update(const TruckChassis* Truck)
{
	signed char KeyboardState[256];
	GetKeyboardState(reinterpret_cast<BYTE*>(KeyboardState));

	const int ControllerCount=mControllers.size();
	if (Truck!=mLastSeenTruck)
	{	//if we changed truck, reset the ratios
		mLastSeenTruck=const_cast<TruckChassis*>(Truck);
		mLastUpdateTime=GetTickCount();
	}
	else
	{
		//update frame time
		const uint Now=GetTickCount();
		mFrameTime=float(Now-mLastUpdateTime)/1000.0f;
		mFrameTime=__clamp(0.0f,mFrameTime,1.0f);	//To prevent catastrophic cases when mLastUpdateTime
													//hasn't been updated in a while.
													//Game isn't playable at 1fps anyway.
		mLastUpdateTime=Now;

		//mark that the constraints should backsteer if the truck is moving
		const TruckControls* TransmissionData=Truck->Constrols;
		if (TransmissionData->IsAccelerating()	&& 
			!TransmissionData->IsInNeutral()	&&
			!TransmissionData->HandbrakeOn)
		{
			mBackSteering=true;
		}
		else
		{
			mBackSteering=false;
		}

		//update all controller states
		for (int i=0; i<ControllerCount;++i)
		{
			SetData& Set=mControllers[i];
			
			Set.KeyStates=STPConstraintController::NO_KEY_DOWN;

			if (KeyboardState[Set.ResetKey] < 0)  //if ResetKey is down
			{
				Set.KeyStates=STPConstraintController::RESET_DOWN;
			}
			else if (KeyboardState[Set.IncreaseKey] < 0) //if IncreaseKey is down
			{
				Set.KeyStates=STPConstraintController::INCREASE_DOWN;
			}
			else if (KeyboardState[Set.DecreaseKey] < 0) //if DecreaseKey is down
			{
				Set.KeyStates=STPConstraintController::DECREASE_DOWN;
			}
		}
	}
}

void STPConstraintController::AddController(int IncreaseKey,int DecreaseKey,int ResetKey)
{
	if (IncreaseKey>=0 && IncreaseKey<256 &&
		DecreaseKey>=0 && DecreaseKey<256 &&
		ResetKey>=0 && ResetKey<256)
	{	//make sure the virtual key code is in a valid range
		//codes above 255 will cause an array oiverflow when we use GetKeyboardState
		SetData NewSet;
		NewSet.IncreaseKey=IncreaseKey;
		NewSet.DecreaseKey=DecreaseKey;
		NewSet.ResetKey=ResetKey;
		mControllers.push_back(NewSet);
	}
}

STPConstraintController::KeyState STPConstraintController::GetControllerState(uint ControllerIndex) const
{
	if (ControllerIndex > mControllers.size()-1)
	{	//out of bounds
		return STPConstraintController::NO_KEY_DOWN;
	}
	return mControllers[ControllerIndex].KeyStates;
}
/////////////////////////////////////
////////////////  hooks
/////////////////////////////////////
//returns the angle or position this contraint should go to
float GetSTPConstraintTarget(STPConstraintNamePiggyBack& ConstraintData,float MinPos,float MaxPos,float CurrentPos)
{
	const STPConstraintController* Constroller=STPConstraintController::Get();
		
	STPConstraintController::KeyState State=Constroller->GetControllerState(ConstraintData.GetController());

	float Delta=0.0f;
	if (State==STPConstraintController::INCREASE_DOWN)
	{
		Delta=ConstraintData.GetSpeed() * Constroller->GetFrameTime();
	}
	else if (State==STPConstraintController::DECREASE_DOWN)
	{
		Delta=-ConstraintData.GetSpeed() * Constroller->GetFrameTime();
	}
	else if ( (Constroller->IsBackSteering() && ConstraintData.AutoResets()) ||
				State==STPConstraintController::RESET_DOWN )
	{	//we should start resetting the constraints

		//by how many position units we can move during this frame if we are resetting
		const float ResetDelta=ConstraintData.GetResetSpeed() * Constroller->GetFrameTime();

		if (abs(CurrentPos - ConstraintData.GetResetPosition()) < ResetDelta)
		{	//if we are close to the reset position, return the reset pos
			return ConstraintData.GetResetPosition();
		}
		else if (CurrentPos > ConstraintData.GetResetPosition())
		{
			Delta=-ResetDelta;
		}
		else //current pos < reset pos
		{
			Delta=ResetDelta;
		}
	}
	return __clamp(MinPos,CurrentPos+Delta,MaxPos);
}

void __stdcall DriveExtraControlledConstraints_Internal(const HUDData* HUD,std::vector<ConstraintBucket*>& ContraintGroups)
{
	if (!HUD->ActiveTruck)
	{	//might happen at loadtime, I guess when there is no truck yet
		return;
	}

	//update key/button state
	STPConstraintController::Get()->Update(HUD->ActiveTruck);

	//sweep over all groupd of constraints in this truck
	//there seem to be one bucket per addon (+1 for the truck itself).
	const uint BucketCount=ContraintGroups.size();
	for (uint j=0;j<BucketCount;j++)
	{
		ConstraintBucket* Bucket=ContraintGroups[j];
		if (!Bucket)
		{
			continue;
		}

		std::vector<hkpConstraintInstance*>& TruckConstraints=Bucket->Constraints;
		if (TruckConstraints.empty())
		{	//no constraints, no problem
			continue;
		}

		//sweep over all constraints in thius group
		const uint ConstraintCount=TruckConstraints.size();
		for (uint i=0;i<ConstraintCount;++i)
		{
			const hkpConstraintInstance* Constraint=TruckConstraints[i];
			hkpConstraintData2* ConstraintData=reinterpret_cast<hkpConstraintData2*>(Constraint->getDataRw());
			
			if (ConstraintData->getType()==hkpConstraintData::CONSTRAINT_TYPE_LIMITEDHINGE)
			{
				//for some odd reason Constraint->getName() often points to the 2nd char of the string
				//and internally the game does an and 0xFFFFFFFE to retriev the base pointer
				const char* ConstraintName=reinterpret_cast<const char*>(reinterpret_cast<const Address>(Constraint->getName()) & -2);
				if ( ConstraintName && !strncmp(ConstraintName,STP_CONSTRAINT_NAME_WITH_SEPARATOR,strlen(STP_CONSTRAINT_NAME_WITH_SEPARATOR)) )
				{	//if the constraint is named "STPControlledConstraint" it contains a piggy back
					//go get it
					STPConstraintNamePiggyBack Piggy;
					Piggy.InitFromChars(ConstraintName+strlen(STP_CONSTRAINT_NAME));

					if (Piggy.IsValid())
					{	//successfully retrieved the piggyback

						hkpLimitedHingeConstraintData2* Hinge2=static_cast<hkpLimitedHingeConstraintData2*>(ConstraintData);
						
						//only drive constraints with motors
						if (Hinge2->getMotor())
						{
							const float Target=GetSTPConstraintTarget(Piggy,
								Hinge2->getMinAngularLimit(),
								Hinge2->getMaxAngularLimit(),
								Hinge2->getMotorTargetAngle());
							Hinge2->setMotorTargetAngle(Target);
						}
					}
				}
			}
			else if (ConstraintData->getType()==hkpConstraintData::CONSTRAINT_TYPE_PRISMATIC)
			{
				//for some odd reason Constraint->getName() often points to the 2nd char of the string
				//and internally the game does an and 0xFFFFFFFE to retriev the base pointer
				const char* ConstraintName=reinterpret_cast<const char*>(reinterpret_cast<const Address>(Constraint->getName()) & -2);
				if ( ConstraintName && !strncmp(ConstraintName,STP_CONSTRAINT_NAME_WITH_SEPARATOR,strlen(STP_CONSTRAINT_NAME_WITH_SEPARATOR)) )
				{	//if the constraint is named "STPControlledConstraint" it contains a piggy back
					//go get it
					STPConstraintNamePiggyBack Piggy;
					Piggy.InitFromChars(ConstraintName+strlen(STP_CONSTRAINT_NAME));

					if (Piggy.IsValid())
					{	//successfully retrieved the piggyback

						hkpPrismaticConstraintData* Primatic=reinterpret_cast<hkpPrismaticConstraintData*>(ConstraintData);
						
						//only drive constraints with motors
						if (Primatic->getMotor())
						{
							const float Target=GetSTPConstraintTarget(Piggy,
								Primatic->getMinLinearLimit(),
								Primatic->getMaxLinearLimit(),
								Primatic->getMotorTargetPosition());
							Primatic->setMotorTargetPosition(Target);
						}
					}
				}
			}
		}
	}
}

__declspec(naked) D3DXMATRIX* DriveExtraControlledConstraints(D3DXMATRIX *pOut,FLOAT *pDeterminant,const D3DXMATRIX *pM)
{
	__asm
	{
		lea eax, [esp+0xa8+4+3*4] //a8: original 4:return address 34*:D3DXMatrixInverse takes 3 parameters
		push eax
		push ebx					//hold the HUDData pointer throughout the whole function
		call DriveExtraControlledConstraints_Internal

		//do the original function
		jmp D3DXMatrixInverse
	}
}
