/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
class hkVector4;

class Vector3
{
public:
	Vector3(float x=0, float y=0, float z=0);
public:
	float X;	//havok vector3 is 16byte aligned, but mine MUST stay 4 byte aligned (it can mess WheelParams's memebers)
	float Y;
	float Z;

public:
	void operator-=(const Vector3&);
	void operator+=(const Vector3&);
	float Length3D() const;
	float ScalarProduct3D(const Vector3&) const;
	float PointToPoint3DDistance(const Vector3& Point) const;
	void Scale(const float ScaleFactor);
	void Normalize3D(); 
};

__declspec(align(16)) class Vector4 : public Vector3
{
public:
	Vector4(float x=0, float y=0, float z=0, float w=0);
public:
	float W;

public:
	void operator-=(const Vector4&);
	void operator+=(const Vector4&);
	void Scale(const float ScaleFactor);
	void TransformedVector(const Vector4& Vector,const float* Transform); //Transform is a 3 row 4 column matrix
	float Dot(const Vector4&) const;
	hkVector4* AsHkVector() {return reinterpret_cast<hkVector4*>(this);}
};

void VectorProduct(const Vector3& Vector1,const Vector3& Vector2,Vector3& Output);
float VectorScalarProduct(const Vector3& Vector1,const Vector3& Vector2);
void VectorNormalize(Vector3& Input);
void ProjectVectorOnPlane(const Vector3& Input,const Vector3& PlaneNormalVector,Vector3& Output);
void MultiplyBy3x3Matrix(const Vector3& Input,const float BaseChangeMatrix[3*3],Vector3& Output);