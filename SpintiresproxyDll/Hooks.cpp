/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "Common.h"
#include "Hooks.h"
#include "GameStructures.h"
#include "GameFunctions.h"
#include "RigidBody.h"
#include "WheelParams.h"
#include "STPTruckNamePiggyBack.h"
#include "TruckControls.h"
#include "STPWheelsSetPiggyBack.h"
#include "ExternalMinimap.h"
#include "PHYSFS_minimal.h"
#include <RapidXml\RapidXml.hpp>
#include "TruckChassis.h"

//00 1D C0DE in memory
//the least significant byte must always be a 0 because it also serves as null terminator
#define ID_CODE 0xDEC01D00

//all defined in hooker.cpp
extern uint* DesiredWheelset;
extern void (__stdcall* AddWheelsetstonGarageMenu)(void* Menu,const std::wstring& SetName,uint Index);
extern LoadedMesh* (__stdcall* LoadMesh_Real)(void* MeshLoaderParam,const char* MeshName);

/////////////////////////////////// Game functions and their stubs /////////////////////
__declspec(naked) char* __stdcall GetXMLString(void* DOMTree,char* NodeName)
{
	__asm
	{	
		push ecx	//save ecx
		push edx	//save edx

		mov eax, dword  ptr [esp+4+4*2] //=mov eax,[DOMTree]
		mov ecx, eax //for editor compatibility
		push dword ptr [esp+8+4*2]		//push dword ptr [NodeName]
		call GetXmlString_Real
		add esp,4

		pop edx
		pop ecx
		ret 8
	}
}

//reads a group of 3 floats in the format Name="(%f; %f; %f)"
//returns true if all 3 floats were successfully read.
bool GetXMLFloat3(void* XMLTree,char* Name,float& X,float& Y,float& Z)
{
	char* Value=GetXMLString(XMLTree,Name);
	if (Value)
	{
		if (sscanf(Value,"(%f; %f; %f)",&X,&Y,&Z)==3)
		{
			return true;
		}
	}
	return false;
}

//reads a float
//returns true if was successfully read.
bool GetXMLFloat(void* XMLTree,char* Name,float& Output)
{
	char* Value=GetXMLString(XMLTree,Name);
	if (Value)
	{
		if (sscanf(Value,"%f",&Output)==1)
		{
			return true;
		}
	}
	return false;
}

bool GetXMLBool(void* XMLTree,char* Name,bool DefaultValue)
{
	char* Value=GetXMLString(XMLTree,Name);
	if (Value)
	{
		if (!_stricmp (Value,"true"))
		{
			return true;
		}
		else if (!_stricmp (Value,"false"))
		{
			return false;
		}
	}
	return DefaultValue;
}

bool GetXMLInt(void* XMLTree,char* Name,int& Output)
{
	char* Value=GetXMLString(XMLTree,Name);
	if (Value)
	{
		if (sscanf(Value,"%d",&Output)==1)
		{
			return true;
		}
	}
	return false;
}

__declspec(naked) LoadedMesh* __stdcall LoadMesh(void* MeshLoaderParam,const char* MeshName)
{
	//The fun thing with LoadMesh_Real is that it takes 2 parameters when used in the game
	//and 3 in the editor, the last paramter being 1 or 0, we'll use 0.
	//Anyway we need to save the stack pointer call LoadMesh_Real and expect it not to clean up
	//all its arguments
	__asm
	{
		push ebp
		mov ebp,esp

		push 0
		push dword ptr [ebp+0xC] //our 2nd argument
		push dword ptr [ebp+0x8] //our 1st argument

		call [LoadMesh_Real]	//who might pop 2 OR 3 arguments...
		mov esp,ebp
		pop ebp
		ret 8
	}
}

/////////////////////////////////// Mud deformation hooks ///////////////////////
struct MudCalc_RootHub
{
	uint Unknown1;	//likely an union, float if Unknown5 is NULL
	RigidBody* Wheel;
	RigidBody* Ground;
	void* Unknown2;
	uint Type;		//typically 2, might be one of the SPU_SEND_* enum
	ContactData* ContactPoint;
	byte Unknown4[8];
	void* Unknown5;
};

float CalcGroundPressure(MudCalc_RootHub* RootNode,WheelParams* WheelInfo)
{
	//distance between ground and wheel z axis, or 0 if sunk past the axle.
	const float RideHeight=__max(RootNode->Wheel->m_shape->Radius - WheelInfo->mudDepth,0);
	//Remember that RootNode->Wheel->m_shape->Radius = WheelInfo->fRadius - fRadiusOffset
	//with fRadiusOffset being defined by the softness preset
	const float Length=2*sqrt(WheelInfo->fRadius * WheelInfo->fRadius - RideHeight*RideHeight);
	const float ContactSurface=Length * WheelInfo->Width;
	//We are not calculating the actual contact surface here, but its horizontal projection
	//which is all that matters for ground pressure.

	float Weight=WheelInfo->TotalWeightOnWheel+1.0f/RootNode->Wheel->invMass;
	float GroundPressure=Weight*GRAVITY_CONSTANT/ContactSurface;

#ifndef DEBUG_DRIVETRAIN
	if (GlobalSettings::DebugMode)
	{
		const std::vector<WheelParams*>& WheelArray=(*GetUnknownStructure1())->UnknownS->Wheels;
		COORD ConsoleOrigin;
		ConsoleOrigin.X=0;
		//find out index in WheelArray
		for (uint i=0; i<WheelArray.size();++i)
		{
			if (WheelArray[i]==WheelInfo)
			{
				ConsoleOrigin.Y=i;
				break;
			}
		}

		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),ConsoleOrigin);
		LOG("Radius: %.3f/%.3fm\tPressure: %.3f kPa\tmudDepth: %.3fm\tTWoW: %f kg \n",RootNode->Wheel->m_shape->Radius,WheelInfo->fRadius,GroundPressure/1000.0f,WheelInfo->mudDepth,WheelInfo->TotalWeightOnWheel)
		//LOG("Address %8x angvel %.3f\t fric: %f \n",WheelInfo,WheelInfo->AngularVelocity,WheelInfo->Wheel->m_friction)
	}
#endif	
	return GroundPressure;

}

//Should return 1 as integer if the truck does NOT sink, 0 otherwise.
uint __stdcall TruckStaysAboveMud_Internal(MudCalc_RootHub* RootNode,WheelParams* WheelInfo,float& GroundPressure)
{
	GroundPressure=CalcGroundPressure(RootNode,WheelInfo);

	//Converting ground pressure to game deformation (or pressure ?) units.
	//The divisor here is a balance factor aiming to give the largest(?) deformation
	//for an enormous pressure.
	GroundPressure=__min(255.0f,255.0f*GroundPressure/GlobalSettings::MaxDeformationPressure);
	
	//to be verified: if GroundPressure is not at least 1.0 here then no deformation
	//will actually be applied

	//if ground pressure is too low, don't even bother calculating
	//ground deformation
	if (GroundPressure>1.0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//Should return 1 as integer if the truck does NOT sink, 0 otherwise.
__declspec(naked) uint __stdcall TruckStaysAboveMud()
{
	__asm
	{
		lea eax,[esp+4+0x10] //0x10: original, 4:one call
		push eax	//GroundPressure

		push esi	//WheelInfo
		push edi	//RootNode
		call TruckStaysAboveMud_Internal

		retn
	}
}

/////////////////////////////////// Suspension tuning from the WheelsSets  ///////////////////////////////////
struct ExtraWheelsSetsParameters
{
	uint IDCode;
	float SuspensionStrength_Normalized;
	float SuspensionDamping_Normalized;
	float SuspensionMin_Offset;
	float HardpointOffsetX;
	float HardpointOffsetY;
	float HardpointOffsetZ;
};

ExtraWheelsSetsParameters* LastKnownExtraWheelsSetsParameters=NULL;


//This function replaces (std::string) WheelType=Type, but adds extra data after the null terminator
//later referenced as piggyback. The piggyback is still inside the buffer owned by the string
//so it will be deallocated with the string, but since it's past the null terminator, regular 
//string operations (compare, concatenation, strcpy) will not "see" it.
//std::string copy constructor should see it though.
//This function MUST add a piggyback to the input string, even if it is a dummy one
//or buffer overflows will happen, because I can't tell which strings have a payload
//and which do not.
void __stdcall ReadExtraWheelsTypeTokens_Internal(std::string& Input,const char* StringContent,void* DOMTree)
{
	ExtraWheelsSetsParameters Params;
	Params.IDCode=ID_CODE;

	if (!GetXMLFloat(DOMTree,"SuspensionStrength_Normalized",Params.SuspensionStrength_Normalized))
	{
		Params.SuspensionStrength_Normalized=-1.0f;
	}
	if (!GetXMLFloat(DOMTree,"SuspensionDamping_Normalized",Params.SuspensionDamping_Normalized))
	{
		Params.SuspensionDamping_Normalized=-1.0f;
	}

	if (!GetXMLFloat(DOMTree,"SuspensionMin_Offset",Params.SuspensionMin_Offset))
	{
		Params.SuspensionMin_Offset=0.0f;
	}

	if (!GetXMLFloat(DOMTree,"HardpointOffsetX",Params.HardpointOffsetX))
	{
		Params.HardpointOffsetX=0.0f;
	}
	if (!GetXMLFloat(DOMTree,"HardpointOffsetY",Params.HardpointOffsetY))
	{
		Params.HardpointOffsetY=0.0f;
	}
	if (!GetXMLFloat(DOMTree,"HardpointOffsetZ",Params.HardpointOffsetZ))
	{
		Params.HardpointOffsetZ=0.0f;
	}

	Input=StringContent;
	Input.append(reinterpret_cast<char*>(&Params),sizeof(Params));
}

//WheelType: pointer to a string determining the type of wheel we are bout to construct.
//WheelType is like "kraz_front" or NULL if we are loading the default wheel set.
//if this function is hooked into the game then ALL wheel type strings that enter it
//contain a piggy back payload.
void __fastcall RetrieveWheelSetPiggyBack_Internal(const char* WheelType)
{
	if (WheelType)
	{
		const ExtraWheelsSetsParameters* Temp=reinterpret_cast<const ExtraWheelsSetsParameters*>(WheelType+strlen(WheelType));
		if (Temp->IDCode==ID_CODE)
		{
			LastKnownExtraWheelsSetsParameters=const_cast<ExtraWheelsSetsParameters*>(Temp);
		}
		else
		{
			//should never happen
			LastKnownExtraWheelsSetsParameters=NULL;
		}
	}
	else
	{
		//happens when switching back to default wheels
		LastKnownExtraWheelsSetsParameters=NULL;
	}
	
}

__declspec(naked) void RetrieveWheelSetPiggyBack()
{
	__asm
	{
		pushad
		//at this point the pointer we want to retrieve is in ecx, sooo...
		call RetrieveWheelSetPiggyBack_Internal
		popad
		mov eax,dword ptr FS:[0]
		ret
	}
}

//thir replaces std::string::operator =
__declspec(naked) std::string* _fastcall ReadExtraWheelsTypeTokens(std::string& Input)
{
	__asm
	{
		lea eax,[esp+0x20+4+4] //original code:+20, return address:+4, one argument pushed for us:+4
		push eax	//DOMTree

		push dword ptr [esp+4+4] //push our first argument 

		push ecx	//output std::string
		call ReadExtraWheelsTypeTokens_Internal

		ret 4
	}
}

/////////////////////////////////// Wheel loading hook ///////////////////////////////////
//WheelCoordinates: pointer to a float[3] representing the wheel's coordinates as specified in the .xml
void __stdcall WheelLoader(void* DOMTree,hkpRigidBodyCinfo* WheelBodyInfo,WheelParams* pWheelParams,float* WheelCoordinates)
{
	LOG("----New wheel----\n");
	
	if (GlobalSettings::UnpresetWheels)
	{
		//parse new xml tokens
		char* Temp=GetXMLString(DOMTree,"Mass_Numeric");
		if (Temp)
		{
			LOG("Mass_Numeric: %s\n",Temp);
			sscanf(Temp,"%f",&(WheelBodyInfo->m_mass));
		}

		Temp=GetXMLString(DOMTree,"Softness_Numeric");
		if (Temp)
		{
			LOG("Softness_Numeric: %s\n",Temp);
			sscanf(Temp,"%f",&(pWheelParams->fSoftForceScale));
		}

		Temp=GetXMLString(DOMTree,"Friction_Numeric");
		if (Temp)
		{
			LOG("Friction_Numeric: %s\n",Temp);
			sscanf(Temp,"%f",&(WheelBodyInfo->m_friction));
		}

		Temp=GetXMLString(DOMTree,"SubstanceFriction_Numeric");
		if (Temp)
		{
			LOG("SubstanceFriction_Numeric: %s\n",Temp);
			sscanf(Temp,"%f",&(pWheelParams->fSubstanceFriction));
		}
		Temp=GetXMLString(DOMTree,"Havok_AllowedPenetrationDepth");
		if (Temp)
		{
			LOG("AllowedPenetrationDepth: %s\n",Temp);

			//havok version mismatch, Pavel's version probably has m_allowedPenetrationDepth 8 bytes later than mine.
			byte* PenDepthPtr=reinterpret_cast<byte*>(&WheelBodyInfo->m_allowedPenetrationDepth);
			PenDepthPtr+=8;

			sscanf(Temp,"%f",PenDepthPtr);
		}
	}

	if (GlobalSettings::SuspensionTunableInWheelsSets && LastKnownExtraWheelsSetsParameters)
	{
		WheelCoordinates[0]+=LastKnownExtraWheelsSetsParameters->HardpointOffsetX;
		WheelCoordinates[1]+=LastKnownExtraWheelsSetsParameters->HardpointOffsetY;
		WheelCoordinates[2]+=LastKnownExtraWheelsSetsParameters->HardpointOffsetZ;
		LOG("Offsets from WheelsSets x:%.3f\ty:%.3f\tz:%.3f\n",LastKnownExtraWheelsSetsParameters->HardpointOffsetX,LastKnownExtraWheelsSetsParameters->HardpointOffsetY,LastKnownExtraWheelsSetsParameters->HardpointOffsetZ);
	}

	if (GlobalSettings::TireDeformationFix)
	{
		//make softness APPARENTLY independent from wheelmass
		//this is an empirical formula, known to cause softer suspension
		//with huge wheelmasses (5+t).
		pWheelParams->fSoftForceScale*=20.0f/sqrt(WheelBodyInfo->m_mass);
		
		//make sure forcescale doesn't get past 1.0 or the tires will push the truck
		//upward with more force than its weight
		pWheelParams->fSoftForceScale=__min(1.0f,pWheelParams->fSoftForceScale);	
	}

	if (GlobalSettings::DebugMode)
	{
		LOG("Tire softness: %f\n",pWheelParams->fSoftForceScale);
	}
}

_declspec(naked) void PatchWheelLoader()
{

	void* DOMTree;
	hkpRigidBodyCinfo* bodyInfo;
	WheelParams* pWheelParams;
	float* WheelCoordinates; //relative to truck frame

	//you'll have to look at your own disassembly to see how many local variables
	//are optimized away
#define LOCAL_VARIABLE_COUNT 4
#define LOCAL_VARIABLE_BYTES 4*LOCAL_VARIABLE_COUNT
	__asm	//grab some local variables from parent function
	{
		pushad
		
		//grab the 2nd argument to the original wheel loader before overwriting.
		mov eax,dword ptr [EBP+0xC]

		mov ebp,esp
		sub esp,LOCAL_VARIABLE_BYTES
		//writing to local variables must be done past this point
		mov dword ptr [pWheelParams],ebx

		mov dword ptr [DOMTree],eax

		lea eax,[esp+0x1C+4+32+LOCAL_VARIABLE_BYTES] //+4: return address, +32: pushad
		mov dword ptr [WheelCoordinates],eax

		lea eax,[esp+0x120+4+32+LOCAL_VARIABLE_BYTES] //+4: return address, +32: pushad
		mov dword ptr [bodyInfo],eax		
		
	}
	WheelLoader(DOMTree,bodyInfo,pWheelParams,WheelCoordinates);
	__asm
	{
		add esp,LOCAL_VARIABLE_BYTES
		popad

		cmp byte ptr [ebx+0x70],0	//original code
		fld dword ptr [esp+0x14]	//original code

		ret
	}

#undef LOCAL_VARIABLE_COUNT
#undef LOCAL_VARIABLE_SIZE
}

/////////////////////////////////// Suspension loading hook ///////////////////////////////////
//returns by what you should multiply suspension stuffness/damping for mass-independent behavior
float GetSuspensionMassFactor(const WheelParams* pWheelParams)
{
	//in this calculation, inertias have their X axis along the axle (x points right)
	//the truck's forward/backward vector is along Z (Z=forward)
	//and Y is along the height

	//sum of inveres masses
	float MassTerm=pWheelParams->ParentFrame->invMass + pWheelParams->Wheel->invMass+(float)EPSILON;

	//sum of [inverse inertias * (distance to suspension joint)^2]
	float WheelInertiaTermX=0.4f*pWheelParams->Width; //apparently suspension is at 40% width from center
	WheelInertiaTermX*= WheelInertiaTermX * pWheelParams->Wheel->invInertia.X;
	//float WheelInertiaTermY= presumed 0;
	//float WheelInertiaTermZ=0; (null distance)


	Vector4 ParentToWheel=pWheelParams->Wheel->Position;
	ParentToWheel-=pWheelParams->ParentFrame->Position;

	//X/Y/ZVectors are in the truck's standard fame (ie:Z sideway)
	float SidewayProjection=pWheelParams->ParentFrame->ZVector.ScalarProduct3D(ParentToWheel);
	//float UpwardProjection=pWheelParams->ParentFrame->YVector.ScalarProduct3D(ParentToWheel);
	float ForwardProjection=pWheelParams->ParentFrame->XVector.ScalarProduct3D(ParentToWheel);

	float ChassisInertiaTermX;
	if (pWheelParams->isRightSided)
	{
		ChassisInertiaTermX=SidewayProjection + 0.4f*pWheelParams->Width;
	}
	else
	{
		ChassisInertiaTermX=SidewayProjection - 0.4f*pWheelParams->Width;
	}
	ChassisInertiaTermX*= ChassisInertiaTermX * pWheelParams->ParentFrame->invInertia.X;
	//float ChassisInertiaTermY=0;
	float ChassisInertiaTermZ=ForwardProjection * ForwardProjection * pWheelParams->ParentFrame->invInertia.Z;
	

	//LOG("**** %f\n",1.0f/(MassTerm+WheelInertiaTermX+ChassisInertiaTermX+ChassisInertiaTermZ));
	return MassTerm+WheelInertiaTermX+ChassisInertiaTermX+ChassisInertiaTermZ;
}

//parse suspension-specific XML tokens upon wheel creation
void __stdcall SuspensionLoader(void* DOMTree,WheelParams* pWheelParams)
{
	//This is to preserve order of magniture with SpintirePlus v1 and v2.
	const float CompatibilityFactor=0.6f;

	hkpWheelConstraintData* Suspension=pWheelParams->Suspension;
	//parse new xml tokens

	//////////////////////////// SuspensionStrength_Normalized
	if (GlobalSettings::SuspensionTunableInWheelsSets	&&
		LastKnownExtraWheelsSetsParameters				&&
		LastKnownExtraWheelsSetsParameters->SuspensionStrength_Normalized>=0.0f)
	{
		Suspension->m_atoms.m_lin0Soft.m_tau=CompatibilityFactor*LastKnownExtraWheelsSetsParameters->SuspensionStrength_Normalized*GetSuspensionMassFactor(pWheelParams);
		LOG("StiffnessN from wheelset: %f\n",Suspension->m_atoms.m_lin0Soft.m_tau);
	}
	else
	{
		const char* Temp=GetXMLString(DOMTree,"SuspensionStrength_Normalized");
		if (Temp)
		{
			float Stiffness;
			sscanf(Temp,"%f",&Stiffness);
			
			Suspension->m_atoms.m_lin0Soft.m_tau=CompatibilityFactor*Stiffness*GetSuspensionMassFactor(pWheelParams);
			LOG("StiffnessN: %f\n",Suspension->m_atoms.m_lin0Soft.m_tau);
		}
	}

	//////////////////////////// SuspensionDamping_Normalized
	if (GlobalSettings::SuspensionTunableInWheelsSets	&&
		LastKnownExtraWheelsSetsParameters				&&
		LastKnownExtraWheelsSetsParameters->SuspensionDamping_Normalized>=0.0f)
	{
		Suspension->m_atoms.m_lin0Soft.m_damping=CompatibilityFactor*LastKnownExtraWheelsSetsParameters->SuspensionDamping_Normalized*GetSuspensionMassFactor(pWheelParams);
		LOG("DampingN from wheelset: %f\n",Suspension->m_atoms.m_lin0Soft.m_damping);
	}
	else
	{
		const char* Temp=GetXMLString(DOMTree,"SuspensionDamping_Normalized");
		if (Temp)
		{
			float Damping;
			sscanf(Temp,"%f",&Damping);
			Suspension->m_atoms.m_lin0Soft.m_damping=CompatibilityFactor*Damping*GetSuspensionMassFactor(pWheelParams);
			LOG("DampingN %f\n",Suspension->m_atoms.m_lin0Soft.m_damping);
		}
	}

	//////////////////////////// SuspensionMin from wheelset
	if (GlobalSettings::SuspensionTunableInWheelsSets	&&
		LastKnownExtraWheelsSetsParameters				&&
		LastKnownExtraWheelsSetsParameters->SuspensionMin_Offset)
	{
		Suspension->m_atoms.m_lin0Limit.m_min+=LastKnownExtraWheelsSetsParameters->SuspensionMin_Offset;
		LOG("Suspension min from wheelset: %f\n",Suspension->m_atoms.m_lin0Limit.m_min);
	}

	if (GlobalSettings::SupportSkidSteering)
	{
		//set the uninitialized variables
		pWheelParams->SetSkidSteerMode(WheelParams::SkidSteer_None);

		//parse new xml tokens
		char* Temp=GetXMLString(DOMTree,"SkidSteeringMode");
		if (Temp)
		{
			if (!stricmp(Temp,"ForwardReverse"))
			{
				pWheelParams->SetSkidSteerMode(WheelParams::SkidSteer_ForwardReverse);
				//pWheelParams->TorqueType=WheelParams::Torque_Free;
				LOG("Skid steer: ForwardReverse\n");
			}
			else if (!stricmp(Temp,"ForwardBrake"))
			{
				pWheelParams->SetSkidSteerMode(WheelParams::SkidSteer_ForwardBrake);
				//pWheelParams->TorqueType=WheelParams::Torque_Free;
				LOG("Skid steer: ForwardBrake\n");
			}
		}
	}	
}

_declspec(naked) void PatchSuspensionLoader()
{
	void* DOMTree;
	WheelParams* RootHub;
	__asm
	{
		pushad
		mov edi,ebp //we will need ebp later

		mov ebp,esp
		sub esp,8	//8=4*number of local variables
		//writing to local variables must be done PAST this point

		mov edi,dword ptr [edi+8] //access paren't function ebp+8=1st argument
		mov dword ptr [DOMTree], edi
		mov dword ptr [RootHub], ebx
		
		
	}
	SuspensionLoader(DOMTree,RootHub);
	__asm
	{
		add esp,8	//8=4*number of local variables
		popad

		mov ecx, dword ptr [ebx+0x10] //original code
		mov edx, dword ptr [ecx]
		ret
	}
}


/////////////////////////////////// Addon loading hook ///////////////////////////////////
//converts AngvelString to a float and returns it if WantGearRatio is false
//converts RatioString to a float and returns it if WantGearRatio is true
//and performs Angvel<->ratio conversion if either is NULL
//if both AngvelString and RatioString are NULL, the function returns 0.0f
float ConvertGearboxAngvelAndRatio(const char* AngvelString,const char* RatioString,bool WantGearRatio)
{
	const float ENGINE_DEFAULT_OPTIMAL_RPM=1500;
	float ReturnValue;
	if (WantGearRatio)
	{	//we want a gear ratio
		if (RatioString)
		{
			sscanf(RatioString,"%f",&ReturnValue);
		}
		else if (AngvelString)
		{
			sscanf(AngvelString,"%f",&ReturnValue);
			ReturnValue/=RPM2RADS( ENGINE_DEFAULT_OPTIMAL_RPM );

		}
		else
		{
			return 0.0f;
		}
	}
	else 
	{	//we want an angvel
		if (AngvelString)
		{
			sscanf(AngvelString,"%f",&ReturnValue);
		}
		else if (RatioString)
		{
			sscanf(RatioString,"%f",&ReturnValue);
			ReturnValue*=RPM2RADS( ENGINE_DEFAULT_OPTIMAL_RPM );
		}
		else
		{
			return 0.0f;
		}
	}
	return ReturnValue;
}

//tries to parse a gearbox on TruckData/Motor and fills the Gearbox vector with wheel optimal angvels
//or gearbox ration depending on UseGearboxRatio.
//returns true if the gearbox is valid, false otherwise.
//a valid gearbox has at least a reverse, high and first gear.
bool ParseAddonGearbox(void* XMLTree,std::vector<float>& Gearbox,bool UseGearboxRatio)
{
	char* Angvel;
	char* GearRatio;
	float Gear;

	//check reverse gear
	Angvel=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/ReverseGear/AngVel");
	GearRatio=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/ReverseGear/GearRatio");	
	Gear=ConvertGearboxAngvelAndRatio(Angvel,GearRatio,UseGearboxRatio);
	if (!Gear)
	{	//Gear=0 means invalid/missing ratio, and reverse gear is critical
		//so we signal that this gearbox is invalid.
		return false;
	}
	Gearbox.push_back(Gear);

	//check first gear
	Angvel=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/Gear_1/AngVel");
	GearRatio=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/Gear_1/GearRatio");	
	Gear=ConvertGearboxAngvelAndRatio(Angvel,GearRatio,UseGearboxRatio);
	if (!Gear)
	{	//Gear=0 means invalid/missing ratio, and 1st gear is critical
		//so we signal that this gearbox is invalid.
		return false;
	}
	//Gearbox.push_back(Gear); //we be done in the loop below

	//check high gear
	Angvel=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/HighGear/AngVel");
	GearRatio=GetXMLString(XMLTree,"TruckData/TruckMotorOverrides/HighGear/GearRatio");	
	const float HighGear=ConvertGearboxAngvelAndRatio(Angvel,GearRatio,UseGearboxRatio);
	if (!HighGear)
	{	//Gear=0 means invalid/missing ratio, and HighGear is critical
		//so we signal that this gearbox is invalid.
		return false;
	}
	//if we are here then all mandatory gears are present, check for others
	{
		char Query[sizeof("TruckData/TruckMotorOverrides/Gear_0123456789/AngVel")];
		int i=2;
		do
		{
			Gearbox.push_back(Gear);	//on the first iteration of the loop, this will
										//store the first gear

			sprintf(Query,"TruckData/TruckMotorOverrides/Gear_%d/AngVel",i);
			Angvel=GetXMLString(XMLTree,Query);
			sprintf(Query,"TruckData/TruckMotorOverrides/Gear_%d/GearRatio",i);
			GearRatio=GetXMLString(XMLTree,Query);

			Gear=ConvertGearboxAngvelAndRatio(Angvel,GearRatio,UseGearboxRatio);

			++i;
		}while(Gear);
	}

	Gearbox.push_back(HighGear); //high gear must be pushed last
	return true;
}

bool _stdcall ParseTruckAddonHook_Internal(void* DOMTree,const char* AddonName,const Addon* Truck,TruckChassis* Base)
{
	if (Truck!=NULL)	//if we are an addon
	{
		if (Base && Base->TruckBase)
		{
			STPTruckNamePiggyBack Piggy;
			Piggy.InitFromStdString(Base->TruckBase->Name);
			bool TruckModified=false;

			//damage sensation modifiers
			if (!Piggy.HasSensationOverride())
			{	//if no piggyback is currently attached to this truck
				const char* SensationMinString=GetXMLString(DOMTree,"TruckData/Damage/SensationMin");
				const char* SensationMaxString=GetXMLString(DOMTree,"TruckData/Damage/SensationMax");
				if (SensationMinString)
				{
					TruckModified=true;

					Piggy.SetSensationOverrideHash(AddonName);
					Piggy.SaveSensationMin(Base->HealthInfo.SensationMin);
					Piggy.SaveSensationMax(Base->HealthInfo.SensationMax);

					sscanf(SensationMinString,"(%f; %f; %f)",&Base->HealthInfo.SensationMin.X,&Base->HealthInfo.SensationMin.Y,&Base->HealthInfo.SensationMin.Z);

					if (SensationMaxString)
					{	//modifies sensation min and max
						sscanf(SensationMaxString,"(%f; %f; %f)",&Base->HealthInfo.SensationMax.X,&Base->HealthInfo.SensationMax.Y,&Base->HealthInfo.SensationMax.Z);
					}
				}
				else if (SensationMaxString)
				{
					TruckModified=true;

					Piggy.SetSensationOverrideHash(AddonName);
					Piggy.SaveSensationMin(Base->HealthInfo.SensationMin);
					Piggy.SaveSensationMax(Base->HealthInfo.SensationMax);

					sscanf(SensationMaxString,"(%f; %f; %f)",&Base->HealthInfo.SensationMax.X,&Base->HealthInfo.SensationMax.Y,&Base->HealthInfo.SensationMax.Z);
				}				
			}

			//torque modifiers
			{
				float TorqueFactor;
				if (GetXMLFloat(DOMTree,"TruckData/TruckMotorOverrides/TorqueFactor",TorqueFactor))
				{
					TruckModified=true;
					Piggy.AddTorqueMod(AddonName,TorqueFactor);
					Base->Constrols->MaxTorque*=TorqueFactor;
				}
			}

			//gearbox replacement
			if (!Piggy.HasGearboxOverride())
			{
				//check if the truck uses gearbox ratios or gearbox angvels
				bool UseGearRatio=false;
				if (GlobalSettings::SupportSkidSteering)
				{	//if skid steering is enabled, then some vehicles might use
					//gearboxes that need gear ratios instead of angvels

					const uint WheelCount=Base->Wheels.size();
					for (uint i=0; i<WheelCount; ++i)
					{
						const WheelParams* Wheel=Base->Wheels[i]->Wheel;
						if (Wheel->GetSkidSteerMode() != WheelParams::SkidSteer_None)
						{	//if at least one wheel uses skid steering, then the gearbox
							//uses gear ratios
							UseGearRatio=true;
							break;
						}
					}
				}
				std::vector<float> Gearbox; //angvels or ratios depending on what the vehicle needs
				Gearbox.reserve(6); //reverse, 1, 1+, and 3 auto gears

				if (ParseAddonGearbox(DOMTree,Gearbox,UseGearRatio))
				{	//if the gearbox is valid
					Piggy.SaveGearbox(AddonName,Base->Constrols->GearBox);
					Base->Constrols->GearBox=Gearbox;
					Base->Constrols->SelectedGear=0;
					TruckModified=true;
				}
			}

			if (TruckModified)	//if we overwrote sensation min or max
			{
				Piggy.SaveInString(Base->TruckBase->Name);
			}
		}
	}

	//original code
	char *Value=GetXMLString(DOMTree,"IsChassisFullOcclusion");
	if (Value)
	{
		if (!stricmp(Value,"true"))
		{
			return true;
		}
	}
	return false;
}

//this function should return true if GetXMLBoo("IsChassisFullOcclusion")==true;
_declspec(naked) bool ParseTruckAddonHook(void* DOMTree,void*)
{
	__asm
	{
		push dword ptr [esp+0x10+4]	//TruckChassis* Base is at esp+0x10 when calling this function
		push dword ptr [ebp+0x14]	//grab 4th argument passed on to parent
		push dword ptr [ebp+0x80]	//char* used in the paren't parent, holds this addon's name (ex:kraz_tractor)
		push dword ptr [esp+4+0xC]	//+4: return address, +C: 2 pushes before
		call ParseTruckAddonHook_Internal //ParseTruckAddonHook_Internal(DOMTree,Truck,Base);
		ret
		
	}
}

bool _cdecl OnInstallOrUninstallAddon(TruckChassis* Chassis,const std::string& AddonName,void* Arg3,uint Arg4)
{
	bool ReturnValue=IsAddonInstalled(Chassis,AddonName,Arg3,Arg4);

	if (ReturnValue) //if we are uninstalling an addon
	{
		if (GlobalSettings::ExtraAddonProperties)
		{
			STPTruckNamePiggyBack Piggy;
			Piggy.InitFromStdString(Chassis->TruckBase->Name);
			bool PiggyModified=false;

			//damage sensation override
			if (Piggy.RemoveSensationOverride(AddonName,Chassis->HealthInfo.SensationMin,Chassis->HealthInfo.SensationMax))
			{ //if we are removing the addon that overrodde Sensation data..
				PiggyModified=true;	
			}

			//engine torque factors
			{
				float TorqueMod;
				if (Piggy.RemoveTorqueMod(AddonName,TorqueMod))
				{
					PiggyModified=true;
					Chassis->Constrols->MaxTorque/=TorqueMod;
				}
			}

			//gearbox mod
			if (Piggy.RemoveGearboxOverride(AddonName,Chassis->Constrols->GearBox))
			{ //if we are removing the addon that overroddethe gearbox
				Chassis->Constrols->SelectedGear=0;
				PiggyModified=true;		
			}

			if (PiggyModified)
			{	//save modified piggy
				Piggy.SaveInString(Chassis->TruckBase->Name);
			}
		}

		if (GlobalSettings::WheelsetsCanRequireAddons)
		{
			if (Chassis->CurrentWheelset!=0)
			{
				STPWheelsSetPiggyBack Piggy;
				if (Piggy.InitFromStdString(Chassis->WheelSets[Chassis->CurrentWheelset-1].Title))
				{
					std::list<std::string> RequiredAddons;
					Piggy.FillAddonList(RequiredAddons);

					const std::list<std::string>::const_iterator End=RequiredAddons.end();
					for (std::list<std::string>::const_iterator i=RequiredAddons.begin(); i!= End;++i)
					{
						if ((*i)==AddonName)
						{
							//The addon we are uninstalling is required for our current wheelset
							//so change wheelse to default wheels
							*DesiredWheelset=0;

							break;
						}
					}
				}
			}
		}
	}

	return ReturnValue;
}

/////////////////////////////////// time of day change
_declspec(naked) float GetUpdatedTimeOfDay(void)
{
	__asm
	{
		fstp st(0) //flush the value already in st0
		fld dword ptr [GlobalSettings::FreezeTimeOfDayAt] //load desired time
		ret //return
	}
}

/////////////////////////////////// New difflock damage
float __stdcall GetDifflockStress_Internal(TruckChassis* Chassis,TruckState* State)
{
	float ReturnValue;
	if (State->DiffLocked)
	{
		//first calculate the average speed of all powered wheels
		float AverageSpeed=0;
		int ConstrainedWheels=0;
		for (uint i=0;i<Chassis->Wheels.size();++i)
		{
			const WheelParams* CurrentWheel=(Chassis->Wheels[i])->Wheel;
			if ( CurrentWheel->TorqueType==WheelParams::Torque_Free ||
				(CurrentWheel->TorqueType==WheelParams::Torque_AWDOnly && State->AWDMode==false) ||
				 CurrentWheel->mudDepth )
			{	//skip wheels that aren't powered
				//and wheels that touch mud
				continue;
			}

			//tracked vehicles are immune to difflock damage, since there is no locker
			if (GlobalSettings::SupportSkidSteering && CurrentWheel->GetSkidSteerMode()!=WheelParams::SkidSteer_None )
			{
				continue;
			}
			AverageSpeed+=CurrentWheel->AngularVelocity;
			ConstrainedWheels++;
		}
		if (ConstrainedWheels<=1)
		{	//if there is only one powered wheel outside mud, assume that all other wheels
			//slip to loosen diff stress.
			//Same if all wheels are in the mud.
			return -1.0f;
		}
		
		AverageSpeed/=ConstrainedWheels;
		

		//then calculate how far each wheel is from the average
		float DifflockStress=0;
		for (uint i=0;i<Chassis->Wheels.size();++i)
		{
			const WheelParams* CurrentWheel=(Chassis->Wheels[i])->Wheel;
			if ( CurrentWheel->TorqueType==WheelParams::Torque_Free ||
				(CurrentWheel->TorqueType==WheelParams::Torque_AWDOnly && State->AWDMode==false) ||
//				  abs(CurrentWheel->AccelerationDamped)>0.5	||
				  CurrentWheel->mudDepth)
			{	//skip wheels that aren't powered
				//and wheels that touch mud
				continue;
			}

			//tracked vehicles are immune to difflock damage, since there is no locker
			if (GlobalSettings::SupportSkidSteering && CurrentWheel->GetSkidSteerMode()!=WheelParams::SkidSteer_None )
			{
				continue;
			}
			float Delta=abs(CurrentWheel->AngularVelocity-AverageSpeed);
			
			
			if (Delta>0.1f) //even in a flat straight line, each wheel's speed isn't exactly the average
			{
				DifflockStress+=Delta;
			}
		}

		ReturnValue=DifflockStress/450.0f; //balance factor
	}
	else
	{
		ReturnValue=-1.0f; //instant diff heal
	}
	//here ReturnValue should be an increment to the difflock's stress
	//where stress=1.0 means a broken differential.

	//those counter operations the game applies to the result
	return (ReturnValue*20.0f*20.0f)+2.8f;
}

float __stdcall GetDifflockStress()
{
	TruckChassis* Chassis;
	TruckState* State;

	/*
	00B700FC   . 80B8 8A000000 >CMP BYTE PTR [EAX+8A],0                  ;  check if hardcore mode
	00B70103     0F84 84050000  JE SpinTire.00B7068D
	00B70109   . 8B4424 44      MOV EAX,DWORD PTR [ESP+44]
	00B7010D   . E8 1EFC8BFF    CALL SpinTire.0042FD30
	00B70112   . 84C0           TEST AL,AL
	00B70114   . 0F85 73050000  JNZ SpinTire.00B7068D
	00B7011A   . D9EE           FLDZ
	00B7011C   . 8B8424 9400000>MOV EAX,DWORD PTR [ESP+94]	<-you want this eax (State)
	00B70123   . 8078 0D 00     CMP BYTE PTR [EAX+D],0                   ;  check if difflock is engaged
	00B70127   . D95C24 24      FSTP DWORD PTR [ESP+24]
	00B7012B   . C74424 30 0000>MOV DWORD PTR [ESP+30],0
	00B70133   . 0F84 7C010000  JE SpinTire.00B702B5      <-hook here    ;  jump if diff unlocked
	00B70139   . 8B8B 48010000  MOV ECX,DWORD PTR [EBX+148]	<-you want this ebx (Chassis)
	00B7013F   . 2B8B 44010000  SUB ECX,DWORD PTR [EBX+144]
	00B70145   . 33F6           XOR ESI,ESI
	00B70147   . C1F9 02        SAR ECX,2
	00B7014A   . 85C9           TEST ECX,ECX
	*/

	__asm	//grab some local variables from parent function
	{
		mov dword ptr [State],eax
		mov dword ptr [Chassis],ebx
	
		
	}
	return GetDifflockStress_Internal(Chassis,State);
}

__declspec(naked) int __stdcall GetDifflockDamage()
{
	__asm
	{
		fstp st(0)	//we are given truck damage capacity/125
					//that we are supposed to pop and return as int.
					//however our difflock damage is different
		mov eax,500
		retn
	}
}

/////////////////////////////////// Wheelsets that require adons
void __stdcall ReadExtraWheelsSetTokens_Internal(void* XMLTree,std::string& This,const char* Input)
{
	This=Input; //original code

	const char* RequiredAddons=GetXMLString(XMLTree,"RequiredAddons");
	if (RequiredAddons)
	{
		STPWheelsSetPiggyBack Piggy;
		Piggy.SetRequiredAddons(RequiredAddons);
		Piggy.SaveInString(This);
	}
}
//should match std::string::operator=(const char*)
__declspec(naked) void __stdcall ReadExtraWheelsSetTokens(std::string& This,const char* Input)
{
	__asm
	{
		push [esp+4]	//our first argument, Input
		push ecx		//this pointer
		push ESI
		call ReadExtraWheelsSetTokens_Internal
		ret 4
	}
}

void __stdcall PopulateWheelsetsInGarageMenu(void* Menu,const std::wstring& SetName,uint Index)
{
	TruckChassis* Chassis;
	__asm
	{
		mov [Chassis],ebx
	}

	STPWheelsSetPiggyBack Piggy;
	
	if (Piggy.InitFromStdString(Chassis->WheelSets[Index-1].Title))
	{
		std::list<std::string> RequiredAddons;
		Piggy.FillAddonList(RequiredAddons);

		const std::list<std::string>::const_iterator End=RequiredAddons.end();
		for (std::list<std::string>::const_iterator i=RequiredAddons.begin(); i!= End;++i)
		{
			if (!IsAddonInstalled(Chassis,(*i),NULL,0))
			{
				return;
			}
		}
	}

	//original code: add wheelset to the garage menu
	AddWheelsetstonGarageMenu(Menu,SetName,Index);
}

/////////////////////////////////// Silent IsLinkedSteering
//sets Constraint->IsLinkedSteering to the proper mode 
void __stdcall SetSilentLinkedSteering_Internal(IKConstraint* Constraint,void* XMLTree)
{
	if (Constraint->IsLinkedSteering == IKConstraint::LS_EnginePowered)
	{
		if (GetXMLBool(XMLTree,"TriggerEngineSound",true) == false)
		{
			Constraint->IsLinkedSteering = IKConstraint::LS_Silent;
		}
	}
}
__declspec(naked) void SetSilentLinkedSteering()
{
	__asm
	{
		pushad
		lea eax, dword ptr [esp+0x20+4+0x14] //14:original 4:a call 20:pushad
		push eax
		push ebx
		call SetSilentLinkedSteering_Internal
		popad
		mov eax,dword ptr [esp+0xF8+4]	//original code +4(a call)
		ret
	}
}

//returns true if the engine is on and linked steering is engine powered
bool ShouldRevEngineForLinkedSteerConstraint_Internal (const TruckChassis* Truck, const IKConstraint* Constraint)
{
	if ( (Truck->EngineStateFlags & TruckChassis::ES_Running) &&
		Constraint->IsLinkedSteering == IKConstraint::LS_EnginePowered)
	{
		return true;
	}
	else
	{
		return false;
	}
}

__declspec(naked) bool ShouldRevEngineForLinkedSteerConstraint()
{
	__asm
	{
		push dword ptr [esp+0xC+4] //c: original, +4 call
		push ecx
		call ShouldRevEngineForLinkedSteerConstraint_Internal
		ret
	}
}

/////////////////////////////////// External Minimap and cloakmesh override
//hook to get the .dds for current map
LoadedMesh* __stdcall Minimap_Hook_Internal(void* MeshLoaderParam, const char* MeshToLoad, const std::string& MapName)
{
	if (GlobalSettings::MinimapEnabled)
	{
		STPMinimapManager::Get()->SetMapName(MapName);
	}

	if (GlobalSettings::MapsCanOverrideCloakMesh)
	{
		//open texture file
		std::string MapXMLFilePath="levels/"+MapName+".xml";
		PHYSFS_File* MapXMLFile=PHYSFS_openRead(MapXMLFilePath.data());
		if (MapXMLFile)
		{
			//read levels/MapName.xml
			unsigned int FileSize=static_cast<unsigned int>(PHYSFS_fileLength(MapXMLFile)); //this file can't get bigger than 2Gb anyway, or the game would run out of memory just with one texture
			char* Buffer=static_cast<char*>(malloc(FileSize+1));
			PHYSFS_read(MapXMLFile,Buffer,FileSize,1);
			PHYSFS_close(MapXMLFile);
			Buffer[FileSize]=0;	//add null terminator

			//parse the XML in levels/MapName.xml
			rapidxml::xml_document<> doc;
			doc.parse<0>(Buffer);
			rapidxml::xml_node<> *node = doc.first_node("SpintiresPlusConfig");
			
			char* CloakMeshName=NULL;

			for (rapidxml::xml_attribute<> *attr = node->first_attribute();
			attr; attr = attr->next_attribute())
			{
				const char* TokenName=attr->name();
				const char* TokenValue=attr->value();
				if (!strcmp(TokenName,"CloakMesh"))
				{
					CloakMeshName=const_cast<char*>(TokenValue);
				}
			}

			if (CloakMeshName)
			{
				LoadedMesh* ReturnValue=LoadMesh(MeshLoaderParam,CloakMeshName);
				free(Buffer);
				return ReturnValue;
			}
			else
			{
				free(Buffer);
			}

		}
		
	}
	return LoadMesh(MeshLoaderParam,MeshToLoad); //original code
}

_declspec(naked) LoadedMesh* Minimap_Hook(void* MeshLoaderParam, const char* MeshToLoad)
{
	__asm
	{
		push dword ptr [esp+4+0xc8]
		push dword ptr [esp+4+4+4] //4: previous push, 4:one call, 4: we want the 2nd argument
		push dword ptr [esp+4+4+4] //we want the following arument
		call Minimap_Hook_Internal
		ret 8
	}
}