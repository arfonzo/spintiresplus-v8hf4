/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "Typedefs.h"

class Signature
{
public:
	enum MatchesAt_Return : byte
	{
		LOWER,
		MATCHED,
		HIGHER,
	};

	Signature(void);
	~Signature(void);

	//Sets the bytes of this signature
	//for wildcards, set byte and mask to 0, otherwise set mask to 0xFF
	//max signature length is 32 bytes.
	void Set(const byte* SeekPattern,const byte* Mask,uint Size);
	 template <typename Block> MatchesAt_Return MatchesAt(Address Location);

	byte mBytes[32];
	byte mMask[32];
	uint mLength;
	Address mFoundAt;
};


//this function checks if a signature matches at the supplied location
//block defines the datatype used for comparison, ie:number of bytes checked at once.
//Disappointingly, char gives the best results
//char 2.7s
//short & int: 4.4s
//__int64: 6+s
template <typename Block>
Signature::MatchesAt_Return Signature::MatchesAt(Address Location)
{
	Block* BlockPtr=reinterpret_cast<Block*>(mBytes);
	Block* BlockMask=reinterpret_cast<Block*>(mMask);
	//early test that will filter out a lot of things before setting up the loops
	//cuts down scanning time by 10-25%
	if( *reinterpret_cast<byte*>(Location) < mBytes[0])
	{
		return Signature::LOWER;
	}

	const uint NumberOfBlocks=mLength/sizeof(Block);
	const uint RemainingBytes=mLength%sizeof(Block);	
	for (uint i=NumberOfBlocks; i--;)
	{

		if( (*reinterpret_cast<Block*>(Location+i*sizeof(Block)) & BlockMask[i]) != BlockPtr[i])
		{
			return Signature::HIGHER;
		}
	}

	const uint BytesScannedYet=NumberOfBlocks*sizeof(Block);
	for (uint i=RemainingBytes;i--;)
	{
		if( (*reinterpret_cast<Block*>(Location+i+BytesScannedYet) & mMask[BytesScannedYet+i]) != mBytes[BytesScannedYet+i])
		{
			return Signature::HIGHER;
		}
	}
	//if we are here everything matched
	mFoundAt=Location;
	return Signature::MATCHED;

}