/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include  <math.h>
#include <Physics/Dynamics/Entity/hkpRigidBody.h>
#include <Physics/Dynamics/Constraint/Bilateral/PointToPlane/hkpPointToPlaneConstraintData.h>
#include <Physics/Dynamics/Constraint/Bilateral/PointToPath/hkpPointToPathConstraintData.h>
#include <Physics/Dynamics/Constraint/Bilateral/PointToPath/hkpLinearParametricCurve.h>
#include <Physics/Dynamics/Constraint/Chain/BallSocket/hkpBallSocketChainData.h>
#include <Physics/Dynamics/Constraint/Chain/hkpConstraintChainInstance.h>

#include "ConstraintHooks.h"
#include "Common.h"
#include "Hooks.h"
#include "RigidBody.h"
#include "hkpLinearParametricCurve2.h"
#include "HavokCompat.h"
#include "STPControlledConstraints.h"
#include "STPConstraintNamePiggyBack.h"
#include "WheelParams.h"

//if the userdata of a constraint point to this, then it is considered
//mass independant
//static const int MassIndependantConstraintMarker=0;

const char* UnknownContraintType="Havok| Cant load constraint: unrecognized type.";
Address ContinueDefaultCase;
Address HappyEndAddress;

/////////////Havok functions located in the game
Address AllocateFromHavokPool_Real;

namespace hkpPointToPlaneConstraintDataPtrs
{
	void (hkpPointToPlaneConstraintData::* Init)(); //actually the constructor
	
	//implemented just after the constructor and just before SetInBodySpace
	//void (hkpPointToPlaneConstraintData::* SetInWorldSpace)(const hkTransform& bodyATransform, const hkTransform& bodyBTransform, const hkVector4& pivotW, const hkVector4& planeNormalW);
	
	void (hkpPointToPlaneConstraintData::* SetInBodySpace)(	const hkVector4& pivotA, const hkVector4& pivotB, const hkVector4& planeNormalB);
}

namespace hkpPointToPathConstraintDataPtrs
{
	void (hkpPointToPathConstraintData::* Init)(); //actually the constructor
	
	//implemented just after the constructor and just before SetInBodySpace
	//void (hkpPointToPathConstraintData::* SetInWorldSpace)(const hkTransform& bodyATransform, const hkTransform& bodyBTransform, const hkVector4& pivotW, const hkVector4& planeNormalW);
	
	void (hkpPointToPathConstraintData::* SetInBodySpace)( const hkVector4& pivotA, const hkVector4& pivotB, hkpParametricCurve* path);
}

namespace hkpLinearParametricCurvePtrs
{
	void (hkpLinearParametricCurve::* Init)(); //actually the constructor
	
	//implemented just after the constructor and just before SetInBodySpace
	//void (hkpPointToPathConstraintData::* SetInWorldSpace)(const hkTransform& bodyATransform, const hkTransform& bodyBTransform, const hkVector4& pivotW, const hkVector4& planeNormalW);
	
	void (hkpLinearParametricCurve::* AddPoint)( const hkVector4& p );
}

namespace hkpBallSocketChainDataPtrs
{
	void (hkpBallSocketChainData::* Init)(); //actually the constructor
	
	//immediately before the constructor
	void (hkpBallSocketChainData::* AddConstraintInfoInBodySpace)(const hkVector4& pivotInA, const hkVector4& pivotInB );
}

namespace hkpConstraintChainInstancePtrs
{
	void (hkpConstraintChainInstance::* Init)(hkpConstraintChainData* data); //actually the constructor
	
	//immediately before the constructor
	void (hkpConstraintChainInstance::* AddEntity)(hkpEntity* entity);
}

namespace hkpReferencedObjectPtrs
{
	void (hkReferencedObject::* RemoveReference)();
}
/////////////Other functions located in the game
//warning: needs hkpConstraintData* pointer in ecx (the constraint to be wrappend in a malleable container)
void* (_cdecl* CreateConstraint_Real)(hkpRigidBody* ParentBody,float BreakOffThreshold,float Malleability);

/////////////Stubs to game functions

_declspec(naked) byte* _fastcall HavokMalloc(uint ByteCount)
{
	__asm
	{
		push esi
		mov esi, ecx //first argument goes in esi
		call [AllocateFromHavokPool_Real] //already puts buffer address in eax
		pop esi
		retn
	}
}

hkpConstraintInstance* _stdcall CreateConstraint(hkpConstraintData* Constraint,hkpRigidBody* ChildBody, hkpRigidBody* ParentBody,float BreakOffThreshold,float Malleability)
{
	//Actually CreateConstraint_Real is neither __thiscall nor cdecl (probably an implicit function)
	//stack cleanup is done by caller like a cdecl, but class pointer is on ecx like a thiscall
	//so let do the call manually
	__asm
	{
		//prepare arguments
		push [Malleability]
		push [BreakOffThreshold]
		push [ParentBody]
		mov ecx,dword ptr [Constraint]
		mov edx,dword ptr [ChildBody]

		//call
		call [CreateConstraint_Real]
		// return value in eax, keep it there

		//cdecl cleanup
		add esp,0xc
	}
}

///////////// Initialization
void StoreConstraintParserKeyAddresses(
		Address HavokAlloc,					//havok memory allocator
		Address DefaultCase,				//where to jump if we did not construct a constraint
		Address AddConstraintToWorld,		//where to jump if we built one
		Address Point2Plane_Constructor,	//address of hkpPointToPlaneConstraintData constructor
		Address Point2Plane_SetInBodySpace,	//address of hkpPointToPlaneConstraintData::setInBodySpace
		Address Point2Path_Constructor,		//address of hkpPointToPathConstraintData constructor
		Address Point2Path_SetInBodySpace,	//address of hkpPointToPathConstraintData::setInBodySpace
		Address LinearCurve_Constructor,	//address of hkpLinearParametricCurve's constructor
		Address LinearCurve_Addpoint,		//address of hkpLinearParametricCurve::addPoint
		Address BallSocketChainData_Constructor,		//address of hkpBallSocketChainData's constructor
		Address BallSocketChainData_addConstrain,		//address of hkpBallSocketChainData::addConstraintInfoInBodySpace
		Address ConstraintChainInstance_Constructor,	//address of hkpConstraintChainInstance's constructor
		Address ConstraintChainInstance_AddEntity,		//address of hkpConstraintChainInstance::addEntity
		Address RemoveReference,			//address of hkpReferencedObject::removeReference
		Address ConstrainFinalizer)			//function that takes the constrain data, builds the constaint and optionnally
											//wraps it in a malleable container.
											//typically starts with "Havok| Ignoring Malleable Strength because constraint is breakable. Is there sense in using both?"
{
	AllocateFromHavokPool_Real=HavokAlloc;
	ContinueDefaultCase=DefaultCase;
	HappyEndAddress=AddConstraintToWorld;
	
	//when even reinterpret_casts aren't enough...
	memcpy(&hkpPointToPlaneConstraintDataPtrs::Init,&Point2Plane_Constructor,sizeof(void*));
	memcpy(&hkpPointToPlaneConstraintDataPtrs::SetInBodySpace,&Point2Plane_SetInBodySpace,sizeof(void*));
	CreateConstraint_Real=reinterpret_cast<void* (_cdecl*)(hkpRigidBody*,float,float)>(ConstrainFinalizer);

	memcpy(&hkpPointToPathConstraintDataPtrs::Init,&Point2Path_Constructor,sizeof(void*));
	memcpy(&hkpPointToPathConstraintDataPtrs::SetInBodySpace,&Point2Path_SetInBodySpace,sizeof(void*));

	memcpy(&hkpLinearParametricCurvePtrs::Init,&LinearCurve_Constructor,sizeof(void*));
	memcpy(&hkpLinearParametricCurvePtrs::AddPoint,&LinearCurve_Addpoint,sizeof(void*));

	memcpy(&hkpBallSocketChainDataPtrs::Init,&BallSocketChainData_Constructor,sizeof(void*));
	memcpy(&hkpBallSocketChainDataPtrs::AddConstraintInfoInBodySpace,&BallSocketChainData_addConstrain,sizeof(void*));

	memcpy(&hkpConstraintChainInstancePtrs::Init,&ConstraintChainInstance_Constructor,sizeof(void*));
	memcpy(&hkpConstraintChainInstancePtrs::AddEntity,&ConstraintChainInstance_AddEntity,sizeof(void*));

	memcpy(&hkpReferencedObjectPtrs::RemoveReference,&RemoveReference,sizeof(void*));

	//__asm
	//{
	//	mov [hkpBallSocketChainDataPtrs::Init],0x0792600
	//	mov [hkpBallSocketChainDataPtrs::AddConstraintInfoInBodySpace],0x0792590

	//	mov [hkpConstraintChainInstancePtrs::Init],0x079B040
	//	mov [hkpConstraintChainInstancePtrs::AddEntity],0x079AFE0

	//}
}
///////////// Constraint parsers
void ParseP2PathNodes(void* XMLNode,hkpLinearParametricCurve* Path)
{
	//read additional path parameters
	if (GetXMLBool(XMLNode,"Loop"))
	{
		Path->m_closedLoop=true;
	}
	else
	{
		Path->m_closedLoop=false;
	}

	//set smoothing
	char* Temp=GetXMLString(XMLNode,"SmoothFactor");
	if (Temp)
	{
		sscanf(Temp,"%f",&(Path->m_smoothingFactor));		
	}

	int Index=0;
	char Node[15];
	char ToleranceString[30];
	sprintf(Node,"Node%d",Index);
	sprintf(ToleranceString,"Node%d_Tolerance",Index);
	Vector4 NodeCoordinates;

	if (Path->m_closedLoop)
	{
		while (GetXMLFloat3(XMLNode,Node,NodeCoordinates.X,NodeCoordinates.Y,NodeCoordinates.Z))
		{
			//does this node have a tolerance?
			if (GetXMLFloat(XMLNode,ToleranceString,NodeCoordinates.W)==false)
			{
				NodeCoordinates.W=0;
			}

			(Path->*hkpLinearParametricCurvePtrs::AddPoint)(*NodeCoordinates.AsHkVector());

			Index++;
			sprintf(Node,"Node%d",Index);
			sprintf(ToleranceString,"Node%d_Tolerance",Index);
		}

		//make the last segment identical to the first (havok requirement)
		hkVector4 Temp=Path->m_points[0];
		(Path->*hkpLinearParametricCurvePtrs::AddPoint)(Temp);
		Temp=Path->m_points[1];
		(Path->*hkpLinearParametricCurvePtrs::AddPoint)(Temp);
		
		//The construct below will cause an array reallocation
		//then the pointer to Path->m_points[0] (reference) will become invalid
		//thus the 2 last points will be botched
		//(Path->*hkpLinearParametricCurvePtrs::AddPoint)(Path->m_points[0]);
	}
	else	//open path
	{
		while (GetXMLFloat3(XMLNode,Node,NodeCoordinates.X,NodeCoordinates.Y,NodeCoordinates.Z))
		{
			//does this node have a tolerance?
			if (GetXMLFloat(XMLNode,ToleranceString,NodeCoordinates.W)==false)
			{
				NodeCoordinates.W=0;
			}

			(Path->*hkpLinearParametricCurvePtrs::AddPoint)(*NodeCoordinates.AsHkVector());

			Index++;
			sprintf(Node,"Node%d",Index);
			sprintf(ToleranceString,"Node%d_Tolerance",Index);
		}
	}
}

//if Chain_Mode is found in the current xml node it will create/append/terminate a chain constraint
//return value is true when it terminated the constraint and you should return the chain constraint instead of
//whatever you were building.
//*Output points to the created constraint instance when the function returns true, but is unchanged in other cases.
bool HandleChainConstraint(void* XMLTree,hkpRigidBody* ChildBody,hkpConstraintData*& ConstraintData, hkpConstraintInstance*& Output)
{
	char* Mode=GetXMLString(XMLTree,"Chain_Mode");
	static hkpBallSocketChainData* CurrentChainData;
	static hkpConstraintChainInstance* CurrentChain;

	if (Mode)
	{
		if (!stricmp(Mode,"StartChain"))
		{
			LOG("StartChain\n");
			//CurrentChainData = new hkpBallSocketChainData;
			CurrentChainData=reinterpret_cast<hkpBallSocketChainData*>(HavokMalloc(sizeof(hkpBallSocketChainData)));
			(CurrentChainData->*hkpBallSocketChainDataPtrs::Init)();

			//read position of the joint
			Vector4 CoordinatesAToLink,CoordinatesBToLink;
			GetXMLFloat3(XMLTree,"Chain_PreviousLinkToJointOffset",CoordinatesAToLink.X,CoordinatesAToLink.Y,CoordinatesAToLink.Z);
			GetXMLFloat3(XMLTree,"Chain_NextLinkToJointOffset",CoordinatesBToLink.X,CoordinatesBToLink.Y,CoordinatesBToLink.Z);
			(CurrentChainData->*hkpBallSocketChainDataPtrs::AddConstraintInfoInBodySpace)(*CoordinatesAToLink.AsHkVector(),*CoordinatesBToLink.AsHkVector());

			//CurrentChain = new hkpConstraintChainInstance;
			CurrentChain=reinterpret_cast<hkpConstraintChainInstance*>(HavokMalloc(sizeof(hkpConstraintChainInstance)+4)); //+4: Pavel's hkpBallSocketChainData are 4 bytes bigger than mine
			(CurrentChain->*hkpConstraintChainInstancePtrs::Init)(CurrentChainData);
			

			(CurrentChain->*hkpConstraintChainInstancePtrs::AddEntity)(ChildBody);
			GetXMLFloat(XMLTree,"Chain_MaxErrorDistance",CurrentChainData->m_maxErrorDistance);
			GetXMLFloat(XMLTree,"Chain_Stiffness",CurrentChainData->m_tau);
			GetXMLFloat(XMLTree,"Chain_Damping",CurrentChainData->m_damping);
			GetXMLFloat(XMLTree,"Chain_CFM",CurrentChainData->m_cfm);
		}
		else if (!stricmp(Mode,"ContinueChain"))
		{

			//read position of the joint
			Vector4 CoordinatesAToLink,CoordinatesBToLink;
			GetXMLFloat3(XMLTree,"Chain_PreviousLinkToJointOffset",CoordinatesAToLink.X,CoordinatesAToLink.Y,CoordinatesAToLink.Z);
			GetXMLFloat3(XMLTree,"Chain_NextLinkToJointOffset",CoordinatesBToLink.X,CoordinatesBToLink.Y,CoordinatesBToLink.Z);
			(CurrentChainData->*hkpBallSocketChainDataPtrs::AddConstraintInfoInBodySpace)(*CoordinatesAToLink.AsHkVector(),*CoordinatesBToLink.AsHkVector());

			(CurrentChain->*hkpConstraintChainInstancePtrs::AddEntity)(ChildBody);
		}
		else if (!stricmp(Mode,"EndChain"))
		{
			LOG("EndChain\n");
			(CurrentChain->*hkpConstraintChainInstancePtrs::AddEntity)(ChildBody);
			Output=CurrentChain;
			ConstraintData=CurrentChainData;

			CurrentChainData=NULL;
			CurrentChain=NULL;
			return true; 
		}
		//else if (!stricmp(Mode,"RestartChain"))
		//{
		//	(CurrentChain->*hkpConstraintChainInstancePtrs::AddEntity)(ChildBody);
		//	Output=CurrentChain;
		//	ConstraintData=CurrentChainData;

		//	//CurrentChainData = new hkpBallSocketChainData;
		//	CurrentChainData=reinterpret_cast<hkpBallSocketChainData*>(HavokMalloc(sizeof(hkpBallSocketChainData)));
		//	(CurrentChainData->*hkpBallSocketChainDataPtrs::Init)();

		//	//read position of the joint
		//	Vector4 CoordinatesAToLink,CoordinatesBToLink;
		//	GetXMLFloat3(XMLTree,"Chain_PreviousLinkToJointOffset",CoordinatesAToLink.X,CoordinatesAToLink.Y,CoordinatesAToLink.Z);
		//	GetXMLFloat3(XMLTree,"Chain_NextLinkToJointOffset",CoordinatesBToLink.X,CoordinatesBToLink.Y,CoordinatesBToLink.Z);
		//	(CurrentChainData->*hkpBallSocketChainDataPtrs::AddConstraintInfoInBodySpace)(*CoordinatesAToLink.AsHkVector(),*CoordinatesBToLink.AsHkVector());

		//	//CurrentChain = new hkpConstraintChainInstance;
		//	CurrentChain=reinterpret_cast<hkpConstraintChainInstance*>(HavokMalloc(sizeof(hkpConstraintChainInstance)+4)); //+4: Pavel's hkpBallSocketChainData are 4 bytes bigger than mine
		//	(CurrentChain->*hkpConstraintChainInstancePtrs::Init)(CurrentChainData);
		//	

		//	(CurrentChain->*hkpConstraintChainInstancePtrs::AddEntity)( ((hkpConstraintChainInstance*)Output)->m_chainedEntities[0]);
		//	GetXMLFloat(XMLTree,"Chain_MaxErrorDistance",CurrentChainData->m_maxErrorDistance);
		//	GetXMLFloat(XMLTree,"Chain_Stiffness",CurrentChainData->m_tau);
		//	GetXMLFloat(XMLTree,"Chain_Damping",CurrentChainData->m_damping);
		//	GetXMLFloat(XMLTree,"Chain_CFM",CurrentChainData->m_cfm);
		//	
		//	return true; 
		//}
	}

	return false;
}

//
void HandleCommonSTPConstraintParameters(hkpRigidBody* ParentBody,hkpRigidBody* ChildBody,void* XMLTree)
{
	int Quality=3;
	if (GetXMLInt(XMLTree,"SetChildQuality",Quality))
	{
		ChildBody->setQualityType(static_cast<hkpCollidableQualityType>(Quality));
	}
	if (GetXMLInt(XMLTree,"SetParentQuality",Quality))
	{
		ParentBody->setQualityType(static_cast<hkpCollidableQualityType>(Quality));
	}
}

//hooked constraint parser that may create our new constraints
hkpConstraintData* _stdcall ConstraintParser_Internal(hkpRigidBody* ParentBody,hkpRigidBody* ChildBody,void* XMLTree,float BreakOffThreshold,float Malleability, hkpConstraintInstance*& Output)
{
	char* ConstrantType=GetXMLString(XMLTree,"Type");
	HandleCommonSTPConstraintParameters(ParentBody,ChildBody,XMLTree);

	///////////////////////////////////////////
	//////////////////////p2plane
	///////////////////////////////////////////
	if (ConstrantType && strcmp(ConstrantType,"PointToPlane")==0)
	{
		////////chain stuff
		hkpConstraintData* ChainData;
		if(HandleChainConstraint(XMLTree,ChildBody,ChainData,Output))
		{
			return ChainData;
		}
		////////chain stuff


		////////p2plane stuff
		hkpPointToPlaneConstraintData* Constraint=reinterpret_cast<hkpPointToPlaneConstraintData*>(HavokMalloc(sizeof(hkpPointToPlaneConstraintData)));
		(Constraint->*hkpPointToPlaneConstraintDataPtrs::Init)();	//actually calls its constructor

		Vector4 Axis;
		Vector4 Pivot;
		GetXMLFloat3(XMLTree,"AxisLocal",Axis.X,Axis.Y,Axis.Z);
		GetXMLFloat3(XMLTree,"PivotOffset",Pivot.X,Pivot.Y,Pivot.Z);
		Axis.W=0.0f;
		Pivot.W=0.0f;

		(Constraint->*hkpPointToPlaneConstraintDataPtrs::SetInBodySpace)(*Vector4().AsHkVector(),*Pivot.AsHkVector(),*Axis.AsHkVector());
		Output=CreateConstraint(Constraint,ParentBody,ChildBody,BreakOffThreshold,Malleability);

		
		return Constraint;
	}
	///////////////////////////////////////////
	//////////////////////p2path
	///////////////////////////////////////////
	else if (ConstrantType && strcmp(ConstrantType,"PointToPath")==0)
	{
		////////chain stuff
		hkpConstraintData* ChainData;
		if(HandleChainConstraint(XMLTree,ChildBody,ChainData,Output))
		{
			return ChainData;
		}
		////////chain stuff


		////////p2path stuff
		//doing Path=new hkpLinearParametricCurve();
		hkpLinearParametricCurve* Path=reinterpret_cast<hkpLinearParametricCurve*>(HavokMalloc(sizeof(hkpLinearParametricCurve)));
		(Path->*hkpLinearParametricCurvePtrs::Init)();


		//Constraint=new hkpPointToPathConstraintData();
		hkpPointToPathConstraintData* Constraint=reinterpret_cast<hkpPointToPathConstraintData*>(HavokMalloc(sizeof(hkpPointToPathConstraintData)));
		(Constraint->*hkpPointToPathConstraintDataPtrs::Init)();	//actually calls its constructor
		
		//extra path options
		if(GetXMLFloat(XMLTree,"FrictionForceAlongPath",Constraint->m_maxFrictionForce))
		{	//Havok wants a friction IMPULSE = force * frame time
			Constraint->m_maxFrictionForce/=TYPICAL_HAVOK_FRAMERATE;
		}

		Vector4 Pivot,NotParallelDir;
		GetXMLFloat3(XMLTree,"PivotOffset",Pivot.X,Pivot.Y,Pivot.Z);
		GetXMLFloat3(XMLTree,"NeverParallelDir",NotParallelDir.X,NotParallelDir.Y,NotParallelDir.Z);
		NotParallelDir.AsHkVector()->normalize3IfNotZero();

		char* PathMode=GetXMLString(XMLTree,"Path_Mode");
		Path->m_dirNotParallelToTangentAlongWholePath=*NotParallelDir.AsHkVector();

		if (!PathMode || !strcmp(PathMode,"PositionOnly"))
		{
			Constraint->setOrientationType(hkpPointToPathConstraintData::CONSTRAIN_ORIENTATION_NONE);
		}
		else if (!strcmp(PathMode,"FullyConstrained"))
		{
			Constraint->setOrientationType(hkpPointToPathConstraintData::CONSTRAIN_ORIENTATION_TO_PATH);
		}
		else if (!strcmp(PathMode,"AlignXToPath"))
		{
			Constraint->setOrientationType(hkpPointToPathConstraintData::CONSTRAIN_ORIENTATION_ALLOW_SPIN);
		} 

		
		ParseP2PathNodes(XMLTree,Path);
		//ChildBody->setGravityFactor(0);
		//ChildBody->getMaterial().setRestitution(0);

		(Constraint->*hkpPointToPathConstraintDataPtrs::SetInBodySpace)(*Vector4().AsHkVector(),*Pivot.AsHkVector(),Path);
		Output=CreateConstraint(Constraint,ParentBody,ChildBody,BreakOffThreshold,Malleability);

		if (GlobalSettings::DebugMode)
		{
			hkVector4 ChildPosInParentFrame(ChildBody->getPosition());
			ChildPosInParentFrame._setTransformedInversePos(ParentBody->getTransform(),ChildPosInParentFrame);
			printf("Path Node=\"(%f ; %f ; %f)\"\n",(float)ChildPosInParentFrame.getSimdAt(0),(float)ChildPosInParentFrame.getSimdAt(1),(float)ChildPosInParentFrame.getSimdAt(2));
		}


		(Path->*hkpReferencedObjectPtrs::RemoveReference)();
		return Constraint;
	}
	return NULL;
}

_declspec(naked) void ConstraintParser()
{
	__asm
	{
		lea eax,[esp+0x1C]
		push eax					//Output
		push dword ptr [EBP+0x18]	//Malleability
		push dword ptr [EBP+0x14]	//BreakOffThreshold
		push dword ptr [EBP+0x10]	//XMLTree, 3rd argument
		push dword ptr [EBP+0xC]	//ParentBody
		push dword ptr [EBP+0x8]	//ChildBody
		call ConstraintParser_Internal
		mov ecx,eax		//put constrain ptr in *this* (so that is gets deallocated)
		test eax,eax
		je ToDefault	//jump if ConstraintParser_Internal returned false
		jmp [HappyEndAddress]

ToDefault:
		push UnknownContraintType	//we overwrote that when coming to here
		mov ecx, dword ptr [ebp+10]	//for editor compatibility
		jmp [ContinueDefaultCase]
	}
}
//
//bool IsConstrainsMassIndependant(const hkpConstraintInstance* Constraint)
//{
//	//if (Constraint->getUserData() == reinterpret_cast<hkUlong>(&MassIndependantConstraintMarker))
//	//{
//	//	return true;
//	//}
//	//ignore breakable and malleable constraint wrappers
//	const hkpConstraintData2* ConstraintData=reinterpret_cast<const hkpConstraintData2*>(Constraint->getData());
//	int ConstraintType=ConstraintData->getType();
//
//	if (ConstraintType == hkpConstraintData::CONSTRAINT_TYPE_BREAKABLE ||
//		ConstraintType == hkpConstraintData::CONSTRAINT_TYPE_MALLEABLE )
//	{	
//		return false;
//	}
//
//	return true;
//	////handle all wheel constraints
//	//if (ConstraintType==hkpConstraintData::CONSTRAINT_TYPE_WHEEL)
//	//{
//	//	return true;
//	//}
//
//	
//	return false;
//}
//void OnConstraintAddedToWorld_Internal(hkpConstraintInstance* Constraint)
//{
//	if (!IsConstrainsMassIndependant(Constraint))
//	{
//		return;
//	}
//
//	RigidBody* BodyA=reinterpret_cast<RigidBody*>(Constraint->getRigidBodyA());
//	RigidBody* BodyB=reinterpret_cast<RigidBody*>(Constraint->getRigidBodyB());
//	hkReal MassA=1.0f;
//	hkReal MassB=1.0f;
//	
//
//	if (!BodyA || !BodyB)
//	{
//		return;
//	}
//
//	bool Modify=false;
//
//	if (BodyA->CollidesWithGroup(0x64) && BodyA->invMass)
//	{
//		hkUlong Temp;
//		BodyA->GetCollisionUserData(0x64,&Temp);
//		WheelParams* WheelData=reinterpret_cast<WheelParams*>(Temp);
//		if (WheelData)
//		{
//			MassA=1.0f/(1.0f + WheelData->TotalWeightOnWheel * BodyA->invMass);
//			Modify=true;
//		}
//	}
//
//	
//	if (BodyB->CollidesWithGroup(0x64) && BodyB->invMass)
//	{
//		hkUlong Temp;
//		BodyB->GetCollisionUserData(0x64,&Temp);
//		WheelParams* WheelData=reinterpret_cast<WheelParams*>(Temp);
//		if (WheelData)
//		{
//			MassB=1.0f/(1.0f + WheelData->TotalWeightOnWheel * BodyB->invMass);
//
//			Modify=true;
//		}
//	}
//
//
//	if (Modify && MassA!=MassB)
//	{
//		LOG("massA %f massB %f\n",MassA,MassB);
//		Address SetVirtualMassInverse=0x0075DF60;
//		hkVector4 MassAVector,MassBVector;
//		MassAVector.setAll(MassA);
//		MassBVector.setAll(MassB);
//
//		__asm
//		{
//			lea ecx, [MassBVector]
//			push ecx
//
//			lea eax, [MassAVector]
//			push eax
//
//			mov ecx, dword ptr [Constraint]
//			call SetVirtualMassInverse
//		}
//	}
//
//}
//_declspec(naked) void OnConstraintAddedToWorld()
//{
//
//	__asm
//	{
//		push eax //save eax
//		push dword ptr [ebp+0xC]	//constraint instance ptr
//		call OnConstraintAddedToWorld_Internal
//
//		pop eax
//		//original code below
//		pop ebx
//		mov esp,ebp
//		pop ebp
//		retn
//	}
//}


void __stdcall ParseExtraConstraintParams_Internal(hkpConstraintInstance* Constraint,void* XMLTree,std::vector<std::string>& Vector,const std::string& String)
{
	Vector.push_back(String);	//original code

	//if (GetXMLBool(XMLTree,"MassIndependantConstraint",false))
	//{
	//	Constraint->setUserData(reinterpret_cast<hkUlong>(&MassIndependantConstraintMarker));
	//}

	if (String==STP_CONSTRAINT_NAME)
	{
		std::string& AddedString=Vector.back();

		STPConstraintNamePiggyBack Piggy;
		float Speed=1.0f;
		float ResetSpeed=1.0f;
		float ResetPosition=0.0f;
		int ControllerIndex=0;
		GetXMLFloat(XMLTree,"Speed",Speed);
		GetXMLFloat(XMLTree,"ResetSpeed",ResetSpeed);
		GetXMLFloat(XMLTree,"ResetPosition",ResetPosition);
		GetXMLInt(XMLTree,"ControllerIndex",ControllerIndex);
		bool AutoReset=GetXMLBool(XMLTree,"ResetWithTruckMovement",false);

		char* ConstraintType=GetXMLString(XMLTree,"Type");

		//convert angles to radians for hinges
		if (ConstraintType && !strcmp(ConstraintType,"Hinge"))
		{
			Speed=static_cast<float>(DEG2RAD(Speed));
			ResetSpeed=static_cast<float>(DEG2RAD(ResetSpeed));
			ResetPosition=static_cast<float>(DEG2RAD(ResetPosition));
		}

		Piggy.SetController(ControllerIndex);
		Piggy.SetSpeed(Speed);
		Piggy.SetResetSpeed(ResetSpeed);
		Piggy.SetResetPosition(ResetPosition);
		Piggy.SetAutoResetMode(AutoReset);

		//save additional parameters
		Piggy.SaveInString(AddedString);
	}
}

__declspec(naked) void __stdcall ParseExtraConstraintParams(std::vector<std::string>& Vector,const std::string& String)
{
	__asm
	{
		push dword ptr [esp+4+4] //push our 2nd argument (String). 4:return address 4: 2nd argument
		push dword ptr [esp+4+4] //push our 1st argument (Vector). 4:return address 4: 1st argument
		push ebx			 //XMLTree

		//the constraint
		push dword ptr [esp+0x1c+4+4*3+4*2]	//1c: original +4:return address +4*3:pushed 3 things before
											//+4*2: 2args were pushed to us
		call ParseExtraConstraintParams_Internal

		ret 8
	}
}