#include "STPWheelsSetPiggyBack.h"

STPWheelsSetPiggyBack::STPWheelsSetPiggyBack(void)
{
}

STPWheelsSetPiggyBack::~STPWheelsSetPiggyBack(void)
{
}

void STPWheelsSetPiggyBack::SaveInString(std::string &String)
{
	ClearStream();
	mDataStream<<mRequiredAddons;
	PiggyBack::SaveInString(String);
}

void STPWheelsSetPiggyBack::Init()
{
	mDataStream>>mRequiredAddons;
}

void STPWheelsSetPiggyBack::FillAddonList(std::list<std::string>& AddonList)
{
	if (this->IsValid() && !mRequiredAddons.empty())
	{
		//splits a tring like "kraz_protector,beacons,kraz_carriage"
		//into "kraz_protector" "beacons" "kraz_carriage" and adds those 3
		//strings to AddonList
		std::string::size_type Start=0;
		std::string::size_type End=mRequiredAddons.find(',',0);
		while(End!=std::string::npos)
		{
			AddonList.push_back(mRequiredAddons.substr(Start,End-Start));

			Start=End+1;
			End=mRequiredAddons.find(',',Start);
		}
		AddonList.push_back(mRequiredAddons.substr(Start,mRequiredAddons.length()-Start));
		
	}
}