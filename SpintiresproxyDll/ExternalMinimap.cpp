/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <d3d9.h>
#pragma comment(lib,"d3dx9.lib")
//#include <DxErr.h>
//#pragma comment(lib, "dxerr.lib")	//this adds 270kb to the final executable
#include <string>
#include <windowsX.h> //for GET_X_LPARAM

#include "Common.h"
#include "Settings.h"
#include "ExternalMinimap.h"
#include "PHYSFS_minimal.h"
#include "Locator.h"

#define MINIMAP_CLASS_NAME "SpintirePlus Minimap class"
#define DX_MAP_VERTEX_FORMAT			TEXTURED_VERTEX_FORMAT
#define DX_TRUCK_MARKER_VERTEX_FORMAT	SOLIDFILL_VERTEX_FORMAT
#define DX_OBJECT_VERTEX_FORMAT			TEXTURED_VERTEX_FORMAT

//scale factor from truck world to full-sized map
#define WORLD_TO_MAP_SCALE	2.0f

//congratz if you manage to fit that many trucks
//in the game's physics bubble
#define MAX_MARKER_COUNT	10

///////////////////// C functions
//windowproc for our map
LRESULT CALLBACK MinimapWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static POINT DragStartPos;	//mouse coordinates relative to window origin when the dragging started
	static bool Dragging;
	STPMinimapManager* Minimap=STPMinimapManager::Get();
	if (hwnd == Minimap->GetWindowHandle())
	{	
		switch(uMsg)
		{
		case WM_MOUSEMOVE:
			{
				if (wParam & MK_LBUTTON)
				{	//if left button down and moving->we are dragging
					if (!Dragging)
					{	//the first time, initialize LastKnownMouseX/Y
						//to prevent weird movement if the user enters the window
						//while having the lbutton already down
						Dragging=true;
						DragStartPos.x=GET_X_LPARAM(lParam);
						DragStartPos.y=GET_Y_LPARAM(lParam);
					}
					else
					{
						POINT CurrentMousePos;
						CurrentMousePos.x=GET_X_LPARAM(lParam);
						CurrentMousePos.y=GET_Y_LPARAM(lParam);

						int DeltaX=CurrentMousePos.x-DragStartPos.x;
						int DeltaY=CurrentMousePos.y-DragStartPos.y;
						Minimap->MoveWindow(DeltaX,DeltaY);
					}
				}
				else
				{
					Dragging=false;
				}
				return 0;	//message handled
				break;
			}
		case WM_DESTROY:
			{
				Minimap->OnWMDestroy();
				//Minimap->Terminate();	//must be done from the main thread
				return 0;	//message handled
				break;
			}
		}
	}
	return DefWindowProc( hwnd,  uMsg,  wParam,  lParam);
}

//message pump for the minimap window
DWORD WINAPI MinimapMainLoop(LPVOID lpParameter)
{
	STPMinimapManager::Get()->MessagePump();
	return 0;
}

/////////////////////STPMinimapManager implementation
//singleton access
STPMinimapManager* STPMinimapManager::Get()
{
	static STPMinimapManager Instance;
	return &Instance;
}

STPMinimapManager::STPMinimapManager()
:mWindow(NULL)
,mMapTexture(NULL)
,mBackPlaneVertexBuffer(NULL)
,mEnableRendering(false)
,mAlive(false)
,mMapHeight(0)
,mMapWidth(0)
,mActiveMarkerCount(0)
,mMarkersDirty(true)
,mMapNameChanged(false)
,mThread(NULL)
,mDeviceJustReset(false)
,mGarageIcon(NULL)
,mLumberYardIcon(NULL)
,mObjectiveIcon(NULL)
,mFuelIcon(NULL)
,mLogKioskIcon(NULL)
,mLastKnownLocatorCount(0)
,mMainGameAdapter(-1)
{
	mProgamInstance=GetModuleHandle(NULL);
	ZeroMemory(&mWindowClass,sizeof(WNDCLASS));
	mWindowClass.style=CS_NOCLOSE | CS_HREDRAW | CS_VREDRAW;
	mWindowClass.lpfnWndProc=MinimapWindowProc;
	mWindowClass.hInstance=mProgamInstance;
	mWindowClass.lpszClassName=MINIMAP_CLASS_NAME;
	mWindowClass.hbrBackground= (HBRUSH)GetStockObject(LTGRAY_BRUSH);

	RegisterClass(&mWindowClass);

	mWindowX=0;
	mWindowY=0;
	mWindowWidth=GlobalSettings::MinimapWidth;
	mWindowHeight=GlobalSettings::MinimapHeight;
	mMarkers.reserve(1); //reserve rom for 1 marker. Usually there won't be more than 1.

	MountSTPResources();
}

STPMinimapManager::~STPMinimapManager()
{
	//mD3D->Release();	//we use the main game's d3d object
	//DestroyWindow(mWindow);	//the game's main window will destroy ours
	mWindow=NULL;

	UnregisterClass(MINIMAP_CLASS_NAME,mProgamInstance);
}

void STPMinimapManager::OnWMDestroy()
{
	mAlive=false;
	mEnableRendering=false;
}

void STPMinimapManager::StartMinimap(HWND FocusWindow)
{

	if (!mWindow)
	{
		mFocusWindow=FocusWindow;

		HRESULT hr;
		//setup window
		{
			mWindowHeight=GlobalSettings::MinimapHeight;
			mWindowWidth=GlobalSettings::MinimapWidth;
			mWindowX=GlobalSettings::MinimapXPosition;
			mWindowY=GlobalSettings::MinimapYPosition;

			mWindow=CreateWindowEx(
				WS_EX_TOOLWINDOW | WS_EX_NOACTIVATE | WS_EX_NOPARENTNOTIFY,
				MINIMAP_CLASS_NAME,
				"STP Minimap",
				WS_POPUP,
				mWindowX,
				mWindowY,
				mWindowWidth,
				mWindowHeight,
				FocusWindow,	//parent window, so that we get the close/quit messages
				NULL,
				mProgamInstance,
				NULL);

			ShowWindow(mWindow,SW_SHOWNOACTIVATE);
		}

		//create d3d device
		{
			ZeroMemory(&mPresent, sizeof(mPresent));
			mPresent.BackBufferCount=1;
			mPresent.MultiSampleType=D3DMULTISAMPLE_NONE;
			mPresent.MultiSampleQuality=0;
			mPresent.SwapEffect= D3DSWAPEFFECT_DISCARD;
			mPresent.hDeviceWindow=mWindow;
			mPresent.Flags=0;
			mPresent.FullScreen_RefreshRateInHz=D3DPRESENT_RATE_DEFAULT;
			mPresent.PresentationInterval=D3DPRESENT_INTERVAL_DEFAULT;
			mPresent.BackBufferFormat=D3DFMT_UNKNOWN;
			mPresent.EnableAutoDepthStencil=false;	
			mPresent.Windowed=true;
			mPresent.BackBufferHeight=0;	//fetch size from window size
			mPresent.BackBufferWidth=0;

			UINT AdapterCount=mD3D->GetAdapterCount();
			hr=D3DERR_NOTFOUND;
			for (UINT i=0;i<AdapterCount;++i)
			{
				if (i==mMainGameAdapter)
				{	//prevents the minimap from using the same adapter as the 
					//main game. (MS says it shouldn't work. Indeed it causes crashes most of the time)
					continue;
				}
				hr=mD3D->CreateDevice(
							i,
							D3DDEVTYPE_HAL,
							mFocusWindow,
							D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
							&mPresent,
							&mDevice);
				if (SUCCEEDED(hr))
				{
					break;
				}
			}
			if (FAILED(hr))
			{
				LOG("Couldn't create the minimap window on any of your display adapters\n");
				ShowWindow(FocusWindow,SW_MINIMIZE);
				MessageBox(NULL,"Couldn't create the minimap window on any of your display adapters.\nLikely all your screens are used up by another fullscreen application, or you're trying to use the ExternalMinimap on a computer that doesn't support it.\nThe program will now exit.","SpintiresPlus critical error",MB_OK);
				ExitProcess(0);
			}
		}

		mDeviceJustReset=true;
		mThread=CreateThread(NULL,NULL,MinimapMainLoop,NULL,NULL,&mThreadID);
		//WaitForSingleObject(mInitializationComplete,INFINITE);
	}
}

////////////////////////////////////////////////////////////
//functions below must be called by the dedicated minimap thread
////////////////////////////////////////////////////////////
void STPMinimapManager::Render()
{
	if (mEnableRendering)
	{
		//check for dirty buffers
		if (mMapNameChanged)
		{
			UpdateBackgroundTexture();
		}

		{
			//unfortunately, the locator list is NOT full when UpdateBackgroundTexture is
			//called, because log kiosks are models that generate a locator, and they are
			//loaded much later.
			//Result: we poll the list for changes.
			std::vector<Locator*>* Locators = GetLocatorList();
			if ( Locators && Locators->size() != mLastKnownLocatorCount )
			{
				DrawLocatorsOnMap();
			}
		}

		const int MarkersInArray=mMarkers.size();
		if (mMarkersDirty && MarkersInArray)
		{
			mActiveMarkerCount=MarkersInArray;
			//update marker position
			void* VerticleBufferInRAM;
			mTruckMarkerBuffer->Lock(0,0,&VerticleBufferInRAM,0);	
			memcpy(VerticleBufferInRAM, &mMarkers[0], sizeof(MinimapTruckMarker) * mActiveMarkerCount);
			mTruckMarkerBuffer->Unlock();
			
			mMarkersDirty=false;
			mMarkers.clear();
		}

		//clear scene
		mDevice->Clear(0,NULL,D3DCLEAR_TARGET,0x00000000,0,0);
		mDevice->BeginScene();

		if (mBackPlaneVertexBuffer)
		{//render background map
			//Bind our Vertex Buffer
			mDevice->SetStreamSource(0,	mBackPlaneVertexBuffer,0,sizeof(TexturedVertex));

			//stream must be set before using SetFVF
			mDevice->SetTexture(0,mMapTexture);
			mDevice->SetFVF(DX_MAP_VERTEX_FORMAT);

			//Render from our Vertex Buffer
			mDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
		}
		
		if (mActiveMarkerCount && GlobalSettings::MinimapMarkerSize>0.0f)
		{//draw the marker(s)
			//Bind our Vertex Buffer
			mDevice->SetStreamSource(0, mTruckMarkerBuffer,0,sizeof(SolidFillVertex));
			mDevice->SetTexture(0,NULL);
			mDevice->SetFVF(DX_TRUCK_MARKER_VERTEX_FORMAT);

			//Render from our Vertex Buffer
			//each marker being a single triangle, D3DPT_TRIANGLELIST is the most efficient solution
			mDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, mActiveMarkerCount);
		}

		mDevice->EndScene();
		if (FAILED(mDevice->Present(NULL,NULL,mWindow,NULL)))
		{
			FlushGFXObjects();
			mEnableRendering=false;
		}
	}
}

void STPMinimapManager::UpdateBackplaneGeometry()
{
	TexturedVertex BackPlane[4];
	//topleft
	mMinimapTopLeftX=((float)mWindowWidth-(float)mMapWidth*mMapScaleFactor)/2.0f;
	mMinimapTopLeftY=((float)mWindowHeight-(float)mMapHeight*mMapScaleFactor)/2.0f;
	BackPlane[0].x=mMinimapTopLeftX;
	BackPlane[0].y=mMinimapTopLeftY;
	BackPlane[0].tu=0.0f;
	BackPlane[0].tv=1.0f;

	//topright
	BackPlane[1].x=((float)mWindowWidth+(float)mMapWidth*mMapScaleFactor)/2.0f;
	BackPlane[1].y=BackPlane[0].y;
	BackPlane[1].tu=1.0f;
	BackPlane[1].tv=1.0f;

	//bottom left
	BackPlane[2].x=BackPlane[0].x;
	BackPlane[2].y=((float)mWindowHeight+(float)mMapHeight*mMapScaleFactor)/2.0f;
	BackPlane[2].tu=0.0f;
	BackPlane[2].tv=0.0f;

	//bottom right
	BackPlane[3].x=BackPlane[1].x;
	BackPlane[3].y=BackPlane[2].y;
	BackPlane[3].tu=1.0f;
	BackPlane[3].tv=0.0f;

	if (!mBackPlaneVertexBuffer)
	{	//create vertex buffer if needed
		HRESULT hr=mDevice->CreateVertexBuffer(sizeof(BackPlane),	//Length
									D3DUSAGE_WRITEONLY,		//Usage
									DX_MAP_VERTEX_FORMAT,	//FVF
									D3DPOOL_DEFAULT,		//Pool
									&mBackPlaneVertexBuffer,//ppVertexBuffer
									NULL);
		
		if (FAILED(hr))
		{
			LOG("CreateVertexBuffer failed for background map\n");
		}
	}

	//send updated vertices to GPU
	void* VerticleBufferInRAM;
	mBackPlaneVertexBuffer->Lock(0,0,&VerticleBufferInRAM,0);
	memcpy(VerticleBufferInRAM, &BackPlane, sizeof(BackPlane));
	mBackPlaneVertexBuffer->Unlock();
}

void STPMinimapManager::DrawLocatorsOnMap()
{
	std::vector<Locator*>* Locators=GetLocatorList();
	if (!Locators || !Locators->size() || ! mMapTexture)
	{	//no locator, no thing to draw
		return;
	}

	//make sure icons are loaded
	if (!mGarageIcon || !mFuelIcon || !mObjectiveIcon || !mLumberYardIcon || !mLogKioskIcon)
	{
		LoadIcons();
	}

	IDirect3DSurface9* MapSurface;
	IDirect3DSurface9* PreviousRenderTarget;
	mMapTexture->GetSurfaceLevel(0,&MapSurface);
	mDevice->GetRenderTarget(0,&PreviousRenderTarget);	//this increases refcount on PreviousRenderTarget

	//tell the device to draw on the map's texture
	mDevice->SetRenderTarget(0,MapSurface);
	//enable alpha blending
	mDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
	mDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
	mDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);

	

	
	const uint IconCount=Locators->size();

	mDevice->BeginScene();
	for (uint i=0;i<IconCount;++i)
	{
		const Locator* Object=(*Locators)[i];
		switch(Object->Type)
		{
			case Locator::Garage:
			{
				mDevice->SetTexture(0,mGarageIcon);
				break;
			}
			case Locator::Fuel:
			{
				mDevice->SetTexture(0,mFuelIcon);
				break;
			}
			case Locator::AutoLumber:
			{
				mDevice->SetTexture(0,mLumberYardIcon);
				break;
			}
			case Locator::LogKiosk:
			{	//log kisosk aren't in this list
				mDevice->SetTexture(0,mLogKioskIcon);
				break;
			}
			case Locator::Objective:
			{
				mDevice->SetTexture(0,mObjectiveIcon);
				break;
			}
			default:
			{
				//LOG("type %d \n",Object->Type);
				continue; //don't draw any icon for cloaks and spawn locators
			}
		};

		//place icon
		D3DSURFACE_DESC SurfaceInfo;
		MapSurface->GetDesc(&SurfaceInfo);

		const float MarkerCenterX=Object->X * WORLD_TO_MAP_SCALE +SurfaceInfo.Height/2;
		const float MarkerCenterY=Object->Y * WORLD_TO_MAP_SCALE +SurfaceInfo.Width/2;

		TexturedVertex ObjectMarkerVertices[4];
		//topleft
		ObjectMarkerVertices[0].x=MarkerCenterX - GlobalSettings::MinimapMarkerSize / mMapScaleFactor /2.0f;
		ObjectMarkerVertices[0].y=MarkerCenterY - GlobalSettings::MinimapMarkerSize / mMapScaleFactor /2.0f;
		ObjectMarkerVertices[0].tu=0.0f;
		ObjectMarkerVertices[0].tv=1.0f;

		//topright
		ObjectMarkerVertices[1].x=ObjectMarkerVertices[0].x + GlobalSettings::MinimapMarkerSize / mMapScaleFactor;
		ObjectMarkerVertices[1].y=ObjectMarkerVertices[0].y;
		ObjectMarkerVertices[1].tu=1.0f;
		ObjectMarkerVertices[1].tv=1.0f;

		//bottom left
		ObjectMarkerVertices[2].x=ObjectMarkerVertices[0].x;
		ObjectMarkerVertices[2].y=ObjectMarkerVertices[0].y + GlobalSettings::MinimapMarkerSize / mMapScaleFactor;
		ObjectMarkerVertices[2].tu=0.0f;
		ObjectMarkerVertices[2].tv=0.0f;

		//bottom right
		ObjectMarkerVertices[3].x=ObjectMarkerVertices[1].x;
		ObjectMarkerVertices[3].y=ObjectMarkerVertices[2].y;
		ObjectMarkerVertices[3].tu=1.0f;
		ObjectMarkerVertices[3].tv=0.0f;

		//no need to bother/advantage over creating a vertex buffer, since this is a one shot operation.
		//with only one shape
		mDevice->SetFVF(DX_OBJECT_VERTEX_FORMAT);
		mDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP,2,ObjectMarkerVertices,sizeof(TexturedVertex));
	}

	//restore render target
	mDevice->EndScene();
	mDevice->Present(NULL,NULL,NULL,NULL);
	mDevice->SetRenderTarget(0,PreviousRenderTarget);
	MapSurface->Release();
	PreviousRenderTarget->Release();

	//save mLastKnownLocatorCount.
	mLastKnownLocatorCount=IconCount;
}

void STPMinimapManager::UpdateBackgroundTexture(bool ForceFullReload)
{
	mDevice->SetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_SELECTARG1);
	mDevice->SetTextureStageState(0,D3DTSS_COLORARG1,D3DTA_TEXTURE);
	mDevice->SetTextureStageState(0,D3DTSS_COLORARG2,D3DTA_DIFFUSE);   //Ignored
		
	UnloadTexture(mMapTexture);

	//open texture file
	std::string TextureFilePath="levels/"+mMapName+".dds";
	mMapTexture=LoadTexture(TextureFilePath.c_str(),true);
	if (!mMapTexture)
	{
		LOG("Failed to load map texture %s",TextureFilePath.c_str());
		return;
	}

	D3DSURFACE_DESC SurfaceInfo;
	mMapTexture->GetLevelDesc(0,&SurfaceInfo);

	//scale map to fit on the window
	const float OldAspectRatio=float(mMapHeight)/float(mMapWidth);
	const float NewAspectRatio=float(SurfaceInfo.Height)/float(SurfaceInfo.Width);
	mMapHeight=SurfaceInfo.Height;
	mMapWidth=SurfaceInfo.Width;
	mMapScaleFactor=__min(float(mWindowWidth) / float(mMapWidth) , float(mWindowHeight)/float(mMapHeight));

	if (ForceFullReload || OldAspectRatio!=NewAspectRatio || !mMapWidth || !mMapHeight )
	{
		UpdateBackplaneGeometry();
	}

	//reset the number of markers drawn on the map
	mLastKnownLocatorCount=0;

	//current texture is up to date with regards to mapname
	mMapNameChanged=false;

}

void STPMinimapManager::UnloadTexture(IDirect3DTexture9*& Texture)
{
	if (Texture)
	{
		if (Texture->Release())
		{
			LOG("STPMinimapManager::UnloadTexture refcount is still non 0\n");
		}
		Texture=NULL;
	}
}

IDirect3DTexture9* STPMinimapManager::LoadTexture(const char* FileName,bool RenderTarget)
{
	return LoadTextureFromPHYSFS(FileName,mDevice,RenderTarget);
}

void STPMinimapManager::LoadIcons()
{
	UnloadTexture(mGarageIcon);
	UnloadTexture(mFuelIcon);
	UnloadTexture(mObjectiveIcon);
	UnloadTexture(mLumberYardIcon);
	UnloadTexture(mLogKioskIcon);

	//open texture file
	mGarageIcon=LoadTexture(STP_RESOURCE_DIRECTORY"/Icons/STPGarageIcon.png");
	mFuelIcon=LoadTexture(STP_RESOURCE_DIRECTORY"/Icons/STPFuelIcon.png");
	mObjectiveIcon=LoadTexture(STP_RESOURCE_DIRECTORY"/Icons/STPObjectiveIcon.png");
	mLumberYardIcon=LoadTexture(STP_RESOURCE_DIRECTORY"/Icons/STPLumberIcon.png");
	mLogKioskIcon=LoadTexture(STP_RESOURCE_DIRECTORY"/Icons/STPLogKioskIcon.png");
}

//should be run by the dedicated minimap thread
void STPMinimapManager::MessagePump()
{
	mAlive=true;
	while(Alive())
	{
		const DWORD StartTime=GetTickCount();

		MSG msg;
		while( PeekMessage(&msg, GetWindowHandle(), 0, 0, PM_REMOVE) )
        {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
        }
		
		if (mDeviceJustReset)
		{	//if the device was just reset, we must reload all
			//vertices and textures
			ReinitGFXObjects();
			mDeviceJustReset=false;
			mEnableRendering=true;
		}
		Render();

		//frame pacing
		const DWORD EndTime=GetTickCount();	
		const DWORD TargetFrameTime=1000/60; //cap at 60 fps
		if (StartTime<EndTime && EndTime<StartTime+TargetFrameTime )
		{
			Sleep(TargetFrameTime - (EndTime-StartTime) );
		}
	}
	FlushGFXObjects();
}


void STPMinimapManager::ReinitGFXObjects()
{
	HRESULT hr;
	hr=mDevice->CreateVertexBuffer(sizeof(MinimapTruckMarker) * MAX_MARKER_COUNT,	//Length
								D3DUSAGE_WRITEONLY,		//Usage
								DX_TRUCK_MARKER_VERTEX_FORMAT,//FVF
								D3DPOOL_DEFAULT,		//Pool
								&mTruckMarkerBuffer,//ppVertexBuffer
								NULL);
	if (FAILED(hr))
	{
		LOG("CreateVertexBuffer failed for markers\n");
	}
	mMarkersDirty=true; //marker data should be reloaded into gpu

	//initialize texture
	{
		UpdateBackgroundTexture(true); //will also update vertex buffer content
	}	
}

//deallocates all graphic elements of the minimap
//(textures, vertex buffers,...)
void STPMinimapManager::FlushGFXObjects()
{
	UnloadTexture(mMapTexture);
	UnloadTexture(mGarageIcon);
	UnloadTexture(mFuelIcon);
	UnloadTexture(mObjectiveIcon);
	UnloadTexture(mLumberYardIcon);
	UnloadTexture(mLogKioskIcon);

	if (mBackPlaneVertexBuffer)
	{
		mBackPlaneVertexBuffer->Release();
		mBackPlaneVertexBuffer=NULL;
	}
	if (mTruckMarkerBuffer)
	{
		mTruckMarkerBuffer->Release();
		mTruckMarkerBuffer=NULL;
	}
}

////////////////////////////////////////////////////////////
//functions above must be called by the dedicated minimap thread
////////////////////////////////////////////////////////////

//this function can be called by any thread
void STPMinimapManager::AddTruckMarker(float X, float Y,float Angle)
{
	if (mMarkers.size()<MAX_MARKER_COUNT)
	{
		//top left corner coordiantes
		const float MinimapX=mMinimapTopLeftX;
		const float MinimapY=mMinimapTopLeftY;
		const float MarkerCenterX=MinimapX + (X* WORLD_TO_MAP_SCALE +float(mMapWidth)/2.0f) * mMapScaleFactor;
		const float MarkerCenterY=mWindowHeight-( MinimapY + (Y* WORLD_TO_MAP_SCALE +float(mMapHeight)/2.0f) * mMapScaleFactor );

		
		const float a=2.0f*GlobalSettings::MinimapMarkerSize/3.0f;	// 2/3 * triangle height
		//place 3 vertices to make an triangle more or less centered on MarkerCenter
		//first rotate the triangle (it's not work implementing 2d transform matrices)
		MinimapTruckMarker NewMarker;
		
		NewMarker.Vertex[0].x=a * cos(Angle);
		NewMarker.Vertex[0].y=a * sin(Angle);

		NewMarker.Vertex[1].x=a * cos(Angle+ 2.0f*float(M_PI)/3.0f) /2.0f;
		NewMarker.Vertex[1].y=a * sin(Angle+ 2.0f*float(M_PI)/3.0f) /2.0f;

		NewMarker.Vertex[2].x=a * cos(Angle- 2.0f*float(M_PI)/3.0f) /2.0f;
		NewMarker.Vertex[2].y=a * sin(Angle- 2.0f*float(M_PI)/3.0f) /2.0f;

		//place 3 vertices to make a triangle centered on MarkerCenter
		//the add offset and color
		NewMarker.Vertex[0].x+=MarkerCenterX;
		NewMarker.Vertex[0].y+=MarkerCenterY;
		NewMarker.Vertex[0].Color=0xFFFF0000; //full red

		NewMarker.Vertex[1].x+=MarkerCenterX;
		NewMarker.Vertex[1].y+=MarkerCenterY;
		NewMarker.Vertex[1].Color=0xFFFF0000; //full red

		NewMarker.Vertex[2].x+=MarkerCenterX;
		NewMarker.Vertex[2].y+=MarkerCenterY;;
		NewMarker.Vertex[2].Color=0xFFFF0000; //full red


		mMarkers.push_back(NewMarker);
		mMarkersDirty=true;
	}
}

//signales the Minimap to change the map to display, if needed.
void STPMinimapManager::SetMapName(const std::string &NewMap)
{
	if (NewMap!=mMapName)
	{
		mMapName=NewMap;
		mMapNameChanged=true;
	}
}

//moves the window BY the specified amount of pixels in each direction
void STPMinimapManager::MoveWindow(int DeltaX, int DeltaY)
{
	mWindowX+=DeltaX;
	mWindowY+=DeltaY;
	SetWindowPos(mWindow,NULL,mWindowX,mWindowY,0,0,SWP_NOSIZE);
}

//This function will check if the minimap's device is in the lost state
//and will reset it if needed and possible.
//This function MUST be called by the main thread
//calling from any other thread will cause a failure
void STPMinimapManager::CheckForLostDevice()
{
	if (Alive() && (mEnableRendering==false) && (GetCurrentThreadId() != mThreadID))
	{
		HRESULT hr=mDevice->TestCooperativeLevel();
		if (hr==D3DERR_DEVICENOTRESET)
		{
			hr=mDevice->Reset(&mPresent);
			if (SUCCEEDED(hr))
			{
				mDeviceJustReset=true;
			}
		}
	}
}

void STPMinimapManager::Terminate()
{
	if (mDevice)
	{	//if we have not already deallocated
		//signal the rendering thread that game's over
		mEnableRendering=false;
		mAlive=false;

		//wait for the render thread to finish
		if (GetCurrentThreadId() != mThreadID)
		{
			WaitForSingleObject(mThread,INFINITE);
		}
		CloseHandle(mThread);

		mDevice->Release();
		mDevice=NULL;
	}
}