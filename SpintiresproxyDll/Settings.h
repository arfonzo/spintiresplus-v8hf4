/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <stdio.h>
#include "STPListCategory.h"

/////////////////////////////////// Config ////////////////////////////////////
//in m/s^2
#define GRAVITY_CONSTANT 9.81f

/////////////////////////////////// global vars ///////////////////////////////
#define GLOBAL __declspec( selectany )

namespace GlobalSettings
{	//default values, overwritten at startup when parsing SpintiresPlus_config.xml

	//special
	GLOBAL bool DebugMode=false;
	GLOBAL bool DumpShaders=false;
	GLOBAL bool DefuseTimebombs=false;
	GLOBAL char* d3d9DllOverride=NULL;
	GLOBAL bool WireframeMode=false;
	//GLOBAL bool HightlightTest1=false;
	//GLOBAL bool HightlightTest2=false;
	//GLOBAL bool HightlightTest3=false;
	//GLOBAL bool HightlightTest4=false;
	

	//bugfixes
	GLOBAL int DPIIndependentCursorSize=-1;
	GLOBAL bool HandleCursorWithoutDirectX=false;	
	GLOBAL bool FixSunkenMud=false;	

	//wheelmass bug
	GLOBAL bool SuspensionFix=false;
	GLOBAL bool TireDeformationFix=false;
	GLOBAL bool MudSinkingFix=false;
	GLOBAL float MaxDeformationPressure=25e6;

	//XML additions
	GLOBAL bool UnpresetWheels=false;
	GLOBAL bool UnclampedXML=false;
	GLOBAL bool UnclampedManualLoads=false;
	GLOBAL bool SuspensionTunableInWheelsSets=false;
	GLOBAL bool ExtraAddonProperties=false;
	GLOBAL bool SupportSkidSteering=false;
	GLOBAL bool SupportAdditionalConstraints=false;
	GLOBAL bool STPControlledConstraints=false;
	GLOBAL bool SupportCustomShafts=false;
	GLOBAL bool OrientableShafts=false;
	GLOBAL bool SupportSilentLinkedSteering=false;
	GLOBAL bool CustomizableTrailerBreakoffForce=false;
	GLOBAL bool WheelsetsCanRequireAddons=false;
	GLOBAL bool MapsCanOverrideCloakMesh=false;
	
	//general annoyances
	GLOBAL bool AllowCustomObjectsInMP=false;
	GLOBAL bool DisableSuspensionDamage=false;
	GLOBAL bool DifflockAutoEngageFix=false;
	GLOBAL bool ShifterReleaseFix=false;
	GLOBAL bool TrailerLoadFix=false;
	GLOBAL unsigned int HavokBufferSize=0;	
	GLOBAL bool FasterFileAccess=false;
	

	//cosmetics
	GLOBAL bool DisableDOF=false;
	GLOBAL int RenderDistanceIncrease=0;
	GLOBAL bool IncreaseTerrainLOD=false;
	GLOBAL bool NoVisualDamage=false;

	//fov
	GLOBAL float FOV=-1.0f;
	GLOBAL float AspectRatio=-1.0f;

	//hud
	GLOBAL bool HideHUD_SteamInfo=false;
	GLOBAL bool HideHUD_3dUI=false;

	//Camera
	GLOBAL bool NoCabinCameraReset=false;
	GLOBAL bool UncapZoom=false;
	GLOBAL bool UncapX360Zoom=false;
	GLOBAL bool NoAutoZoom=false;
	GLOBAL bool NoAdvancedAutoZoom=false;
	GLOBAL bool NoAutoZoomOnStations=false;
	GLOBAL float CabinCameraMaxAngle=-1.0f;
	GLOBAL int RightSideCameraKey=-1;
	GLOBAL bool DetachCameraFromSteering=false;

	//new game
	GLOBAL bool AlternateTruckMenu=false;
	GLOBAL bool AlternateMapMenu=false;
	GLOBAL bool ShowLockedTrucksAtSelectionScreen=false;
	GLOBAL STPListCategories TruckCategories;
	GLOBAL STPListCategories MapCategories;

	//gameplay tweaks
	GLOBAL int LoadPointsPerObjective=-1;
	GLOBAL bool HardcoreAutoLogs=false;
	GLOBAL bool DisableCloaks=false;
	GLOBAL bool NoDamage=false;
	GLOBAL bool UnlimitedFuel=false;
	GLOBAL bool AlwaysShowDevMenu=false;
	GLOBAL bool AlwaysAllowSpawnLocators=false;
	GLOBAL bool LegacyCloaks=false;
	GLOBAL bool AlternateDifflockDamage=false;

	//time cycle
	GLOBAL float GameDayDurationInRealSeconds=-1.0f;
	GLOBAL float FreezeTimeOfDayAt=-1.0f;

	//winch
	GLOBAL bool BatteryPoweredWinch=false;
	GLOBAL int ReleaseWinchKey=-1;
	GLOBAL float WinchingRange=-1.0f;

	//minimap
	GLOBAL bool MinimapEnabled=false;
	GLOBAL int MinimapXPosition=-1280;
	GLOBAL int MinimapYPosition=0;
	GLOBAL int MinimapWidth=1280;
	GLOBAL int MinimapHeight=1024;
	GLOBAL float MinimapMarkerSize=20.0f;
};
#undef GLOBAL

static bool OverlayEnabled()
{
	if (GlobalSettings::AlternateMapMenu ||
		GlobalSettings::AlternateTruckMenu)
	{
		return true;
	}
	return false;
}