/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "PHYSFS_Init.h"
#include "PHYSFS_minimal.h"


////////////////////////function prototypes for all PHYSFS stuff
void  (_cdecl* PHYSFS_getLinkedVersion) (PHYSFS_Version *ver);
int  (_cdecl* PHYSFS_init) (const char *argv0);
int  (_cdecl* PHYSFS_deinit) (void);
const PHYSFS_ArchiveInfo** (_cdecl* PHYSFS_supportedArchiveTypes) (void);
void  (_cdecl* PHYSFS_freeList) (void *listVar);
const char* (_cdecl* PHYSFS_getLastError)  (void);
const char* (_cdecl* PHYSFS_getDirSeparator)  (void);
void  (_cdecl* PHYSFS_permitSymbolicLinks) (int allow);
char**  (_cdecl* PHYSFS_getCdRomDirs) (void);
const char* (_cdecl* PHYSFS_getBaseDir)  (void);
const char* (_cdecl* PHYSFS_getUserDir)(void);
const char* (_cdecl* PHYSFS_getWriteDir)(void);
int  (_cdecl* PHYSFS_setWriteDir) (const char *newDir);
int  (_cdecl* PHYSFS_addToSearchPath) (const char *newDir, int appendToPath);
int  (_cdecl* PHYSFS_removeFromSearchPath) (const char *oldDir);
char**  (_cdecl* PHYSFS_getSearchPath) (void);
int  (_cdecl* PHYSFS_setSaneConfig) (const char *organization,
                                    const char *appName,
                                    const char *archiveExt,
                                    int includeCdRoms,
                                    int archivesFirst);
int  (_cdecl* PHYSFS_mkdir) (const char *dirName);
int  (_cdecl* PHYSFS_delete) (const char *filename);
const char* (_cdecl* PHYSFS_getRealDir) (const char *filename);
char**  (_cdecl* PHYSFS_enumerateFiles) (const char *dir);
int  (_cdecl* PHYSFS_exists) (const char *fname);
int  (_cdecl* PHYSFS_isDirectory) (const char *fname);
int  (_cdecl* PHYSFS_isSymbolicLink) (const char *fname);
PHYSFS_sint64  (_cdecl* PHYSFS_getLastModTime) (const char *filename);
PHYSFS_File*  (_cdecl* PHYSFS_openWrite) (const char *filename);
PHYSFS_File*  (_cdecl* PHYSFS_openAppend) (const char *filename);
PHYSFS_File*  (_cdecl* PHYSFS_openRead) (const char *filename);
int  (_cdecl* PHYSFS_close) (PHYSFS_File *handle);
PHYSFS_sint64  (_cdecl* PHYSFS_read) (PHYSFS_File *handle,
                                     void *buffer,
                                     PHYSFS_uint32 objSize,
                                     PHYSFS_uint32 objCount);
PHYSFS_sint64  (_cdecl* PHYSFS_write) (PHYSFS_File *handle,
                                      const void *buffer,
                                      PHYSFS_uint32 objSize,
                                      PHYSFS_uint32 objCount);
int  (_cdecl* PHYSFS_eof) (PHYSFS_File *handle);
PHYSFS_sint64  (_cdecl* PHYSFS_tell) (PHYSFS_File *handle);
int  (_cdecl* PHYSFS_seek) (PHYSFS_File *handle, PHYSFS_uint64 pos);
PHYSFS_sint64  (_cdecl* PHYSFS_fileLength) (PHYSFS_File *handle);
int  (_cdecl* PHYSFS_setBuffer) (PHYSFS_File *handle, PHYSFS_uint64 bufsize);
int  (_cdecl* PHYSFS_flush) (PHYSFS_File *handle);
PHYSFS_sint16  (_cdecl* PHYSFS_swapSLE16) (PHYSFS_sint16 val);
PHYSFS_uint16  (_cdecl* PHYSFS_swapULE16) (PHYSFS_uint16 val);
PHYSFS_sint32  (_cdecl* PHYSFS_swapSLE32) (PHYSFS_sint32 val);
PHYSFS_uint32  (_cdecl* PHYSFS_swapULE32) (PHYSFS_uint32 val);
PHYSFS_sint64  (_cdecl* PHYSFS_swapSLE64) (PHYSFS_sint64 val);
PHYSFS_uint64  (_cdecl* PHYSFS_swapULE64) (PHYSFS_uint64 val);
PHYSFS_sint16  (_cdecl* PHYSFS_swapSBE16) (PHYSFS_sint16 val);
PHYSFS_uint16  (_cdecl* PHYSFS_swapUBE16) (PHYSFS_uint16 val);
PHYSFS_sint32  (_cdecl* PHYSFS_swapSBE32) (PHYSFS_sint32 val);
PHYSFS_uint32  (_cdecl* PHYSFS_swapUBE32) (PHYSFS_uint32 val);
PHYSFS_sint64  (_cdecl* PHYSFS_swapSBE64) (PHYSFS_sint64 val);
PHYSFS_uint64  (_cdecl* PHYSFS_swapUBE64) (PHYSFS_uint64 val);
int  (_cdecl* PHYSFS_readSLE16) (PHYSFS_File *file, PHYSFS_sint16 *val);
int  (_cdecl* PHYSFS_readULE16) (PHYSFS_File *file, PHYSFS_uint16 *val);
int  (_cdecl* PHYSFS_readSBE16) (PHYSFS_File *file, PHYSFS_sint16 *val);
int  (_cdecl* PHYSFS_readUBE16) (PHYSFS_File *file, PHYSFS_uint16 *val);
int  (_cdecl* PHYSFS_readSLE32) (PHYSFS_File *file, PHYSFS_sint32 *val);
int  (_cdecl* PHYSFS_readULE32) (PHYSFS_File *file, PHYSFS_uint32 *val);
int  (_cdecl* PHYSFS_readSBE32) (PHYSFS_File *file, PHYSFS_sint32 *val);
int  (_cdecl* PHYSFS_readUBE32) (PHYSFS_File *file, PHYSFS_uint32 *val);
int  (_cdecl* PHYSFS_readSLE64) (PHYSFS_File *file, PHYSFS_sint64 *val);
int  (_cdecl* PHYSFS_readULE64) (PHYSFS_File *file, PHYSFS_uint64 *val);
int  (_cdecl* PHYSFS_readSBE64) (PHYSFS_File *file, PHYSFS_sint64 *val);
int  (_cdecl* PHYSFS_readUBE64) (PHYSFS_File *file, PHYSFS_uint64 *val);
int  (_cdecl* PHYSFS_writeSLE16) (PHYSFS_File *file, PHYSFS_sint16 val);
int  (_cdecl* PHYSFS_writeULE16) (PHYSFS_File *file, PHYSFS_uint16 val);
int  (_cdecl* PHYSFS_writeSBE16) (PHYSFS_File *file, PHYSFS_sint16 val);
int  (_cdecl* PHYSFS_writeUBE16) (PHYSFS_File *file, PHYSFS_uint16 val);
int  (_cdecl* PHYSFS_writeSLE32) (PHYSFS_File *file, PHYSFS_sint32 val);
int  (_cdecl* PHYSFS_writeULE32) (PHYSFS_File *file, PHYSFS_uint32 val);
int  (_cdecl* PHYSFS_writeSBE32) (PHYSFS_File *file, PHYSFS_sint32 val);
int  (_cdecl* PHYSFS_writeUBE32) (PHYSFS_File *file, PHYSFS_uint32 val);
int  (_cdecl* PHYSFS_writeSLE64) (PHYSFS_File *file, PHYSFS_sint64 val);
int  (_cdecl* PHYSFS_writeULE64) (PHYSFS_File *file, PHYSFS_uint64 val);
int  (_cdecl* PHYSFS_writeSBE64) (PHYSFS_File *file, PHYSFS_sint64 val);
int  (_cdecl* PHYSFS_writeUBE64) (PHYSFS_File *file, PHYSFS_uint64 val);
int  (_cdecl* PHYSFS_isInit) (void);
int  (_cdecl* PHYSFS_symbolicLinksPermitted) (void);
int  (_cdecl* PHYSFS_setAllocator) (const PHYSFS_Allocator *allocator);
int  (_cdecl* PHYSFS_mount) (const char *newDir, const char *mountPoint, int appendToPath);
const char* (_cdecl* PHYSFS_getMountPoint) (const char *dir);
void  (_cdecl* PHYSFS_getCdRomDirsCallback) (PHYSFS_StringCallback c, void *d);
void  (_cdecl* PHYSFS_getSearchPathCallback) (PHYSFS_StringCallback c, void *d);
void  (_cdecl* PHYSFS_enumerateFilesCallback) (const char *dir, PHYSFS_EnumFilesCallback c, void *d);
void  (_cdecl* PHYSFS_utf8FromUcs4) (const PHYSFS_uint32 *src, char *dst, PHYSFS_uint64 len);
void  (_cdecl* PHYSFS_utf8ToUcs4) (const char *src, PHYSFS_uint32 *dst, PHYSFS_uint64 len);
void  (_cdecl* PHYSFS_utf8FromUcs2) (const PHYSFS_uint16 *src, char *dst, PHYSFS_uint64 len);
void  (_cdecl* PHYSFS_utf8ToUcs2) (const char *src, PHYSFS_uint16 *dst, PHYSFS_uint64 len);
void  (_cdecl* PHYSFS_utf8FromLatin1) (const char *src, char *dst,PHYSFS_uint64 len);
////////////////////////end of prototypes

//macro to import functions with GetProcAddress without having to type a zillion
//reinterpret_casts
#define IMPORT_FUNCTION(Module,Function) *reinterpret_cast<void**>(&Function)=reinterpret_cast<void*>(GetProcAddress(Module,#Function))

//function that will import all the above from the .exe
void ImportPHYSFX()
{
	HMODULE MainExe=GetModuleHandle(0);
	IMPORT_FUNCTION(MainExe,PHYSFS_getLinkedVersion);
	IMPORT_FUNCTION(MainExe,PHYSFS_init);
	IMPORT_FUNCTION(MainExe,PHYSFS_deinit);
	IMPORT_FUNCTION(MainExe,PHYSFS_supportedArchiveTypes);
	IMPORT_FUNCTION(MainExe,PHYSFS_freeList);
	IMPORT_FUNCTION(MainExe,PHYSFS_getLastError);
	IMPORT_FUNCTION(MainExe,PHYSFS_getDirSeparator);
	IMPORT_FUNCTION(MainExe,PHYSFS_permitSymbolicLinks);
	IMPORT_FUNCTION(MainExe,PHYSFS_getCdRomDirs);
	IMPORT_FUNCTION(MainExe,PHYSFS_getBaseDir);
	IMPORT_FUNCTION(MainExe,PHYSFS_getUserDir);
	IMPORT_FUNCTION(MainExe,PHYSFS_getWriteDir);
	IMPORT_FUNCTION(MainExe,PHYSFS_setWriteDir);
	IMPORT_FUNCTION(MainExe,PHYSFS_addToSearchPath);
	IMPORT_FUNCTION(MainExe,PHYSFS_removeFromSearchPath);
	IMPORT_FUNCTION(MainExe,PHYSFS_getSearchPath);
	IMPORT_FUNCTION(MainExe,PHYSFS_setSaneConfig);
	IMPORT_FUNCTION(MainExe,PHYSFS_mkdir);
	IMPORT_FUNCTION(MainExe,PHYSFS_delete);
	IMPORT_FUNCTION(MainExe,PHYSFS_getRealDir);
	IMPORT_FUNCTION(MainExe,PHYSFS_enumerateFiles);
	IMPORT_FUNCTION(MainExe,PHYSFS_exists);
	IMPORT_FUNCTION(MainExe,PHYSFS_isDirectory);
	IMPORT_FUNCTION(MainExe,PHYSFS_isSymbolicLink);
	IMPORT_FUNCTION(MainExe,PHYSFS_getLastModTime);
	IMPORT_FUNCTION(MainExe,PHYSFS_openWrite);
	IMPORT_FUNCTION(MainExe,PHYSFS_openAppend);
	IMPORT_FUNCTION(MainExe,PHYSFS_openRead);
	IMPORT_FUNCTION(MainExe,PHYSFS_close);
	IMPORT_FUNCTION(MainExe,PHYSFS_read);
	IMPORT_FUNCTION(MainExe,PHYSFS_write);
	IMPORT_FUNCTION(MainExe,PHYSFS_eof);
	IMPORT_FUNCTION(MainExe,PHYSFS_tell);
	IMPORT_FUNCTION(MainExe,PHYSFS_seek);
	IMPORT_FUNCTION(MainExe,PHYSFS_fileLength);
	IMPORT_FUNCTION(MainExe,PHYSFS_setBuffer);
	IMPORT_FUNCTION(MainExe,PHYSFS_flush);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSLE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapULE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSLE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapULE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSLE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapULE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapUBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapUBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapSBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_swapUBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSLE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_readULE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_readUBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSLE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_readULE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_readUBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSLE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_readULE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_readSBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_readUBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSLE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeULE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeUBE16);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSLE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeULE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeUBE32);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSLE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeULE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeSBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_writeUBE64);
	IMPORT_FUNCTION(MainExe,PHYSFS_isInit);
	IMPORT_FUNCTION(MainExe,PHYSFS_symbolicLinksPermitted);
	IMPORT_FUNCTION(MainExe,PHYSFS_setAllocator);
	IMPORT_FUNCTION(MainExe,PHYSFS_mount);
	IMPORT_FUNCTION(MainExe,PHYSFS_getMountPoint);
	IMPORT_FUNCTION(MainExe,PHYSFS_getCdRomDirsCallback);
	IMPORT_FUNCTION(MainExe,PHYSFS_getSearchPathCallback);
	IMPORT_FUNCTION(MainExe,PHYSFS_enumerateFilesCallback);
	IMPORT_FUNCTION(MainExe,PHYSFS_utf8FromUcs4);
	IMPORT_FUNCTION(MainExe,PHYSFS_utf8ToUcs4);
	IMPORT_FUNCTION(MainExe,PHYSFS_utf8FromUcs2);
	IMPORT_FUNCTION(MainExe,PHYSFS_utf8ToUcs2);
	IMPORT_FUNCTION(MainExe,PHYSFS_utf8FromLatin1);
}