/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <D3dx9math.h>

#include "typedefs.h"

struct LoadedMesh
{
	byte Unknown0[4];
	uint ReferenceCount;
	//more stuff
};

struct ModelFrame
{
	byte Unknown0[8];
	D3DXMATRIX Transform;
	//more stuff
};

void* __stdcall LoadShaftModel(void* DestBuffer,LoadedMesh* Mesh);
void __stdcall CreateAShaft(void* Param);
D3DXMATRIX* __stdcall RotateShaftAlongY(D3DXMATRIX *pOut,FLOAT Angle);//must match the prototype of D3DXMatrixRotationY


void __stdcall WriteTrailerDetachingForce();
float __stdcall ReadTrailerDetachingForce();