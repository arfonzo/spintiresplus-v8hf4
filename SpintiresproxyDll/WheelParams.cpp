/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include  <limits>
#include <float.h>
#include "WheelParams.h"
#include "RigidBody.h"
#include "GameStructures.h"
#include "Common.h"
#include "Physics/Dynamics/Entity/hkpRigidBody.h"

#define WHEEL_VISCOUS_FRICTION 10.0f
#define MAX_ANGVEL 80.0f

void WheelParams::SetSkidSteerMode(WheelParams::STPFlags Mode)
{
	//clear tank steer mode
	TankTrackParams&=~(WheelParams::SkidSteer_Mask);
	//write new one
	TankTrackParams|=Mode;
}

WheelParams::STPFlags WheelParams::GetSkidSteerMode() const
{ 
	return static_cast<WheelParams::STPFlags> (TankTrackParams&(WheelParams::SkidSteer_Mask));
}

//applies the give torque along the wheel's axis
//positive toques make the truck go forward and increase angvel
//Torque is  in N.m
void WheelParams::ApplyTorque(float _Torque, float FrameTime)
{
	if (!_finite(_Torque)) //remove error cases
	{
		return;
	}
	STPPreviousTorque=_Torque; //store torque at last step

	Vector4 TorqueVector=Wheel->ZVector;
	//negative torques = forward
	TorqueVector.Scale(-_Torque * FrameTime);	//negative torques = forward

	//reminder: ApplyAngularImpulse takes a Torque.Time as input, not a torque.
	Wheel->ApplyAngularImpulse(TorqueVector);
}

float WheelParams::GetPreviousAngvel() const
{
	//repurposed local that was previously only used by ApplyTorqueOnWheels() 
	return STPPreviousAngvel;
}

void WheelParams::SavePreviousAngvel()
{
	STPPreviousAngvel=AngularVelocity;
}

float WheelParams::GetInvInertia() const
{
	return Wheel->GetInvInertiaZ();
}

//returns the angular velocity around the wheel's main axis (Z)
//float WheelParams::GetAngVel() const
//{
//	return -Wheel->getAngularVelocity().Dot(Wheel->ZVector);
//}


void WheelParams::Brake(float FrameTime)
{

	float Torque=ServoToAngvel(0.0f,FrameTime,std::numeric_limits<float>::max());	
	ApplyTorque(Torque,FrameTime);
	TorqueRepartition=0.0f;
}
//returns the torque to apply on a wheel so that it theorically reaches the TargetVel, sooner or later.
//MaxTorque in N.m can be negative when reversing
float WheelParams::ServoToAngvel(float TargetVel,float FrameTime,float MaxTorque) const
{
	const float ResistiveTorque=GetResistiveTorque(FrameTime); //Torque at last step
	const float Error=TargetVel - AngularVelocity;

	LOG_DT("Resist %.1f\tcur %.2f\tprevS %.2f\tprevT %.1f ||\n",ResistiveTorque,AngularVelocity,STPPreviousAngvel,STPPreviousTorque);

	//average new suggest torque with previous value to smooth torque variations
	//and reduce the "smoking tire effect".
	return (Error / (FrameTime * GetInvInertia()) - ResistiveTorque );
}

//returns the mass of this wheel in kg
float WheelParams::GetMass() const
{
	return 1.0f/Wheel->invMass;
}

//returns the mass of this wheel in kg
//positive "resisitve torques" push the truck forward, regardless of current mortion direction.
float WheelParams::GetResistiveTorque( float FrameTime) const
{
	const float EffectiveTorqueImpulse=(AngularVelocity-GetPreviousAngvel()) /( FrameTime * GetInvInertia() );
	return (EffectiveTorqueImpulse - STPPreviousTorque) * GroundContactFactor; 
}

//returns the mass of this wheel in kg
float WheelParams::GetPeripheralSpeed() const
{
	return AngularVelocity * fRadius;
}