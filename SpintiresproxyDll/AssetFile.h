/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>
#include <vector>
#include "typedefs.h"

class AssetFile
{
public:
	AssetFile(void);
	~AssetFile(void);

	//takes internal FilePath (ex: "classes/trucks/myaddon.xml")
	//and puts "myaddon" in Output.
	//This function returns mangled names for workshop items.
	void GetFileName(std::string& Output)const;

	//the 2 argument variants take the result from GetFileName
	//as an input, making them a bit faster
	static void GetFriendlyName(std::string& Output,const std::string& FileName);
	void GetFriendlyName(std::string& Output)const;
	static const std::string* GetWorkshopItem(const std::string& FileName);
	const std::string* GetWorkshopItem()const;

	bool IsTrailer() const {return false;}

	void* GetGameDataXML() const {return &(const_cast<AssetFile*>(this)->mGameDataXMLNode);}

private: //total size: 0x50 bytes
	//DO NOT ADD ANY MEMBER
	//this would change the size, making it useless
	std::string mFilePath;
	byte mUnknown0[0x1C];
	void* mGameDataXMLNode;		//used in GetXMLString(&mGameDataXMLNode,...)
	byte mUnknown1[0x10];
};
