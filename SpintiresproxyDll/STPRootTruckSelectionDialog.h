#pragma once
#include "stpdialog.h"
#include "STPTruckSelectionDialog.h"
#include "STPAddonListDialog.h"
#include "STPTrailerListDialog.h"

class STPRootTruckSelectionDialog :
	public STPDialog
{
public:
	STPRootTruckSelectionDialog(void);
	~STPRootTruckSelectionDialog(void);

	void DoModal();

	virtual void Restore();

	virtual void OnQuit();
	virtual void OnCancel();
private:
	STPTruckSelectionDialog mTruckList;
	unsigned int mWheelset;

	STPAddonListDialog mAddonDialog;
	STPTrailerListDialog mTrailerDialog;
	std::vector<std::string> mAddons;
	std::string mTrailer;
};

bool __stdcall DoTruckSelection(void*, const char* OriginaTruck);