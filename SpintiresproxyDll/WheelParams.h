/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "typedefs.h"
#include "Vector.h"

class RigidBody;
class hkpWheelConstraintData;

class WheelParams
{	//WARNING: this is a game's class
	//you are not allowed ot add data members
	//(member functions are ok)
public:
	enum TorqueEnum:uint
	{
		Torque_Free=	0,	//this wheel is never powered
		Torque_Always=	1,	//"default" in the xml
		Torque_AWDOnly=	2,	//Only powered in AWD mode
	};

	enum STPFlags:byte
	{
		SkidSteer_None=0,
		SkidSteer_ForwardBrake=1,
		SkidSteer_ForwardReverse=2,

		SkidSteer_Mask=3,
	};

	void* Unknown0[2];
	RigidBody* ParentFrame;
	RigidBody* Wheel;
	hkpWheelConstraintData* Suspension;
	byte Unknown1[0x4C];
	//offset 0x60
	float fSoftForceScale;	
	uint Unknown2;
	float Width;
	float fRadius;				//radius as defined in the .xml
	//offset 0x70
	bool isRightSided;

	////////////SpintiresPlus Data
	byte SpinTiresPlusReserved1;
	byte SpinTiresPlusReserved2;
	byte TankTrackParams;
	///////////End SpintiresPlus Data

	//offset 0x74
	float fSubstanceFriction;	
	float SteeringAngle;		//in radians
	TorqueEnum TorqueType ;		//offset 0x7C
	byte Unknown38[0x8];
	void* Unknown39;	//might be the constraint motor used for handbraking.
	//offset 0x8C
	float TorqueRepartition;	//torque on this wheel/total truck torque
	union
	{
		float AngularVelocity_AbsClamped;	//absolute value
		float STPPreviousAngvel;
	};
										//is 0 for negative angvels
	//offset 0x94
	union
	{
		//float STPErrorAccumulator;
		float STPPreviousTorque;	//does not count resistive torque (ie: brakes)
		float AccelerationDamped;	//formerly labelled "Slip"
	};
	void* Unknown40;
	//offset 0x9c
	float GroundContactFactor;		//chassis to wheel pressure factor, 0 when in the air, 1 when on the ground
	byte Unknown42[0x10];
	//offset 0xB0
	Vector3 Origin;
	//offset 0xBC
	Vector3 WheelDir;
	//offset 0xC8
	Vector3 AxleDir;
	//offset 0xD4
	Vector3 SuspensionDir;
	//offset 0xE0
	float AngularVelocity;		//in rad/s
	float LinearVelocity;		//in m/s
	float Unknown5;				//used to be called torque, but no
	float mudDepth;	
	float waterDepth;	
	byte Unknown6[0xC];
	//offset 0x100
	float mudCoef;			//amount of mud stuck on the wheel (cosmetic)
	float Unknown69;
	byte Unknown70[0x8];
	Vector3 Unknown74;

	float Unknown75;	
	//offset 0x120
	float dustCoef;			//amount of dust/debris stuck on the wheel (cosmetic)
	float debrisCoef;
	float TotalWeightOnWheel;	//in kg, does not include the wheel's own weight
	//offset 12c
	byte Unknown8;	//smells like a bunch of flags
					//if Unknown8 & 1 then mud is not deformed
	uint ID;		//starts at 3

	//////Spintires plus additional methods
public:
	float GetMass() const;
	void SetSkidSteerMode(STPFlags Mode);
	WheelParams::STPFlags GetSkidSteerMode() const;

	float GetPreviousAngvel() const;
	void SavePreviousAngvel();

	void ApplyTorque(float Torque,float FrameTime); //positive torque=forward
	void Brake(float FrameTime);
	float GetInvInertia() const; //returns the inertia along the wheel's rotation axis
	float GetPeripheralSpeed() const; //in m/s
	//float GetAngVel() const;
	

	//computes the torque required to reach the specified angvel
	//MaxTorque doesn't need to be accurate but it helps prevent instabilities
	//to control system-litterates: it's the limit on the integrator
	float ServoToAngvel(float TargetVel,float FrameTime, float Maxtorque) const;
	float GetResistiveTorque(float FrameTime) const;
};