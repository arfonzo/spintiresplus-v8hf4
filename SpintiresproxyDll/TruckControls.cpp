/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>

#include "Common.h"
#include "WheelParams.h"
#include "GameStructures.h"
#include "TruckControls.h"
#include "RigidBody.h"
#include "TruckChassis.h"

//based on cat C15 engine
//in percentages of max torque

#define ENGINE_TORQUE_AT_MAX_RPM	0.7f
#define ENGINE_TORQUE_AT_OPT_RPM	1.0f
#define ENGINE_TORQUE_AT_LOW_RPM	1.0f
#define ENGINE_TORQUE_AT_IDLE_RPM	0.5f
//idle rpm=0rpm

#define ENGINE_MAX_RPM			2100.0f
#define ENGINE_OPTIMAL_RPM		1500.0f
#define ENGINE_LOW_RPM			1000.0f
#define ENGINE_STALLING_RPM		 500.0f
//linear approx of the rpm that gives no torque
#define ENGINE_NOTORQUE_RPM Lerp(0, ENGINE_TORQUE_AT_OPT_RPM, ENGINE_TORQUE_AT_MAX_RPM, ENGINE_OPTIMAL_RPM, ENGINE_MAX_RPM)

//in seconds
#define DELAY_BEFORE_STALL_WARNING	1.0f

bool TruckControls::IsReversing() const
{
	return SelectedGear==-1;
}

bool TruckControls::IsInHighGear() const
{
	return SelectedGear==GearBox.size();
}

bool TruckControls::JustShifted() const
{
	return SelectedGear!=PreviousGear;
}

bool TruckControls::IsInNeutral() const
{
	return SelectedGear==0;
}

bool TruckControls::IsAccelerating() const
{
	if (AccelRatio<0.0f && !(AutoGearboxEngaged && IsReversing()))
	{
		return true;
	}
	else if (AccelRatio>0.0f && AutoGearboxEngaged && IsReversing())
	{
		return true;
	}
	else
	{
		return false;
	}
}

//returns the AngVel value seen in the truck xml for selected gear
//but the output is negative when reversing
float TruckControls::GetCurrentGearRatio() const
{
	if (IsInNeutral())
	{
		return 0.0;	//neutral
	}
	else if (IsReversing())
	{
		return -GearBox[0];
	}
	else
	{
		return GearBox[SelectedGear];
	}
}


//returns the max angular speed outside the gearbox for the current
//gear
float TruckControls::GetMaxSpeed() const
{
	return GetCurrentGearRatio()*RPM2RADS(ENGINE_NOTORQUE_RPM);
}

//AverageWheelAngVel is in rad/s
//rpm is really in revolution per minute
float TruckControls::GetEngineRPM(float AverageWheelAngVel) const
{
	float GearRatio=GetCurrentGearRatio();
	float EngineRPM=RADS2RPM(AverageWheelAngVel/GearRatio);
	return __max(0,EngineRPM);
}

//returns the torque leaving then engine at a given engine rpm
//this torque is always positive
float  TruckControls::GetEngineTorque(float EngineRPM) const
{
	if (EngineRPM<=ENGINE_LOW_RPM)
	{
		return MaxTorque * Lerp(EngineRPM, 0, ENGINE_LOW_RPM, ENGINE_TORQUE_AT_IDLE_RPM, ENGINE_TORQUE_AT_LOW_RPM);
	}
	else if (EngineRPM<=ENGINE_OPTIMAL_RPM)
	{
		return MaxTorque * Lerp(EngineRPM, ENGINE_LOW_RPM, ENGINE_OPTIMAL_RPM , ENGINE_TORQUE_AT_LOW_RPM, ENGINE_TORQUE_AT_OPT_RPM);;
	}
	else if (EngineRPM<ENGINE_NOTORQUE_RPM)
	{	//interpolate between torques for optimal and notorque rpm
		return MaxTorque * Lerp(EngineRPM, ENGINE_OPTIMAL_RPM, ENGINE_NOTORQUE_RPM, ENGINE_TORQUE_AT_OPT_RPM, 0.0f);
	}
	else
	{
		return 0.0f;
	}
}


float TruckControls::GetTorqueForAngvel(float Angvel,float& MaxAngvel)
{
	float GearRatio=GetCurrentGearRatio();
	float EngineRPM=GetEngineRPM(Angvel);
	MaxAngvel=RPM2RADS( float(ENGINE_NOTORQUE_RPM) * GearRatio );	

	return GetEngineTorque(EngineRPM) / GearRatio;
}

float TruckControls::GetFuelConsumptionFactor() const
{
	return STPFuelConsumptionFactor;
}

void TruckControls::ShakeTruck(bool Backward)
{
	RigidBody* MyBody=Chassis->TruckBase->TruckBody;
	Vector4 Force=MyBody->XVector;
	if (Backward)
	{
		Force.Scale(-0.5f/MyBody->invMass);
	}
	else
	{
		Force.Scale(0.5f/MyBody->invMass);
	}

	MyBody->ApplyPointImpulse(Force,MyBody->Position);
}

//You must call UpdateStallRatio at the end of every run of the Transmission function so as to update internal variables
void TruckControls::UpdateStallRatio(bool IsLackingTorque,float AverageWheelAngvel,float FrameTime)
{
	uint CurrentRPM=(uint)GetEngineRPM(AverageWheelAngvel);
	if (CurrentRPM > (uint)ENGINE_STALLING_RPM || !IsAccelerating()) //not pressing the accel <-> clutch disengaged
	{
		mStallingCount=0;
		Chassis->HealthInfo.StallRatio=0.0f;
	}
	else if (IsLackingTorque && CurrentRPM<=mLastEngineRPM) //and engine RPM <= ENGINE_STALLING_RPM
	{
		mStallingCount++;
	}
	else	//still below ENGINE_STALLING_RPM be we have enough torque or are accelerating
	{		//we just decrement the counter instead of resetting because the noise on speeds/torques
			//can briefly yield a situation where we have enough torque or a speed spike.
		mStallingCount=__max(0,mStallingCount-1);
		if(!mStallingCount)
		{
			Chassis->HealthInfo.StallRatio=0.0f;
		}
	}
	 
	mLastEngineRPM=CurrentRPM;
	
	if ((float)mStallingCount>DELAY_BEFORE_STALL_WARNING/FrameTime)
	//if (mStallingCount>20)
	{
		const float StallDecay=0.1f; //StallRatio decreases by this amount at 20Hz (ie: every 3 havok frame)
		Chassis->HealthInfo.StallRatio+= FrameTime*3.0f;
	}
#ifdef DEBUG_DRIVETRAIN
			LOG("Stall info count %d sr%f ||\n",(int) mStallingCount, Chassis->StallRatio);
#endif
}

bool TruckControls::IsRPMLow(float AverageWheelAngVel) const
{
	return GetEngineRPM(AverageWheelAngVel)<ENGINE_LOW_RPM;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void TruckControls::PZ_GetGearRatioData(float& MinAngvel,float& MaxAngvel) const
{
	float GearAngvel=abs(GetCurrentGearRatio());
	if (IsInHighGear())
	{
		MinAngvel=0.4f;
		MaxAngvel=GearAngvel+2.0f;
	}
	else
	{
		MinAngvel=GearAngvel*0.5f-2.0f;
		MaxAngvel=GearAngvel*2.0f+5.0f;
	}
	
	if (IsReversing())
	{
		MinAngvel=-MinAngvel;
		MaxAngvel=-MaxAngvel;
	}
}