/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once


typedef struct CTwBar TwBar;
struct IDirect3DDevice9;

class STPDialog
{
public:
	STPDialog(void);
	~STPDialog(void);

	//called when display device is lost
	//should save position/size states
	//The default implementatinoonly saves position,
	//size and visibility for the main bar, then deletes
	//mMenu
	virtual void OnDeviceLost();
	virtual void Restore();
	virtual void Render(IDirect3DDevice9* Device);
	void SetVisible(bool Visible);
	bool IsVisible() const {return mVisible;}

	void DoModal();

	void SetPosition(int TopLeftX,int TopLeftY);
	void SetSize(int Width, int Height);
	int GetWidth() const {return mSize[0];}
	int GetHeight() const {return mSize[1];}
	
protected:
	TwBar* mMenu;
	
private:
	int mPos[2];
	int mSize[2];
	bool mVisible;
};
