/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPTruckSelectionDialog.h"

#include "Common.h"
#include "Settings.h"
#include "AssetFile.h"
#include "AntTweakBar.h"
#include "STPAddonListDialog.h"
#include "STPTrailerListDialog.h"
#include "STPMenuManager.h"
#include "Hooks.h"


//defined in hooker.cpp
extern std::vector<AssetFile>* TruckDatabase;



//called by qsort to sort truck names in the truck menu
int __cdecl SortTruckNames(const void* Param1,const void* Param2)
{
	const STPListDialogWithPreview::EntryData* Element1=static_cast<const STPListDialogWithPreview::EntryData*>(Param1);
	const STPListDialogWithPreview::EntryData* Element2=static_cast<const STPListDialogWithPreview::EntryData*>(Param2);
	return stricmp(Element1->mDisplayName.c_str(),Element2->mDisplayName.c_str());
}

/////////////////////////////////
// Callbacks
/////////////////////////////////
void TW_CALL STPTruckSelectionDialog_SetSelectedTruck(const void *value, void *clientData)
{
	bool Checked=*(const bool *)value;
	STPListDialogWithPreview::EntryData* Data=static_cast<STPListDialogWithPreview::EntryData*>(clientData);
	if (Data)
	{
		STPTruckSelectionDialog* Dialog=static_cast<STPTruckSelectionDialog*> (Data->mDialog);
		if (Dialog)
		{
			Dialog->SetTruckChecked(Data,Checked);
		}
	}
}

void TW_CALL STPTruckSelectionDialog_GetSelectedTruck(void *value, void *clientData)
{ 
	bool* Checked=(bool *)value;
	STPListDialogWithPreview::EntryData* Data=static_cast<STPListDialogWithPreview::EntryData*>(clientData);
	if (Data)
	{
		STPTruckSelectionDialog* Dialog=static_cast<STPTruckSelectionDialog*> (Data->mDialog);
		if (Dialog)
		{
			//the checkbox is checked if its client data points to the same thing
			//as the dialog's selected truck
			if (Dialog->GetSelectedTruck()==Data)
			{
				*Checked=true;
			}
			else
			{
				*Checked=false;
			}
		}
	}
}

/////////////////////////////////
// STPTruckSelectionDialog
/////////////////////////////////
STPTruckSelectionDialog::STPTruckSelectionDialog(void)
{
	mSelectedTruck=NULL;

	//populate the list of trucks
	const uint KnownTruckCount=TruckDatabase->size();
	mEntryData.resize(KnownTruckCount);
	for (uint i=0;i<KnownTruckCount;++i)
	{
		STPListDialogWithPreview::EntryData& Item=mEntryData[i];

		const AssetFile& Truck=(*TruckDatabase)[i];
		Item.mDialog=this;
		Truck.GetFileName(Item.mReturnValue);
		Truck.GetFriendlyName(Item.mDisplayName,Item.mReturnValue);
		if (Truck.GetGameDataXML())
		{
			int BalancePoints=0;
			GetXMLInt(Truck.GetGameDataXML(),"BalancePoints",BalancePoints);
			Item.mDisplayName.reserve(Item.mDisplayName.size()+2+BalancePoints+2);
			Item.mDisplayName+=" (";
			Item.mDisplayName.append(BalancePoints,'*');
			Item.mDisplayName+=")";
		}
		Item.mPreviewFile="billboards/trucks/"+Item.mReturnValue+".dds";
	}

	//alphabetically sort trucks
	qsort(&mEntryData.front(),KnownTruckCount,sizeof(STPListDialogWithPreview::EntryData),SortTruckNames);

	mSortingCategories=&GlobalSettings::TruckCategories;
}

STPTruckSelectionDialog::~STPTruckSelectionDialog(void)
{
}

void STPTruckSelectionDialog::Restore()
{
	STPListDialogWithPreview::Restore(); //restore from parent class
	TwSetParam(mMenu,NULL,"label",TW_PARAM_CSTRING,1,"Select truck");
}

void STPTruckSelectionDialog::AddEntryToMenu(const char* VarName,const STPListDialogWithPreview::EntryData& Data,std::string& Label)
{
	TwAddVarCB(
		mMenu,
		VarName,
		TW_TYPE_BOOLCPP,
		STPTruckSelectionDialog_SetSelectedTruck,
		STPTruckSelectionDialog_GetSelectedTruck,
		const_cast<STPListDialogWithPreview::EntryData*>(&Data),
		Label.c_str());
}

void STPTruckSelectionDialog::SetTruckChecked(const STPListDialogWithPreview::EntryData* TruckEntry, bool Checked)
{
	if (Checked)
	{	//we changed selection
		mSelectedTruck=TruckEntry;
	}
	else if (/*!Checked &&*/ TruckEntry==mSelectedTruck)
	{	//we just unchecked the currently selected truck
		mSelectedTruck=NULL;
	}
}

const std::string* STPTruckSelectionDialog::GetSelectedTruckFileName() const
{
	if (mSelectedTruck)
	{
		return &(mSelectedTruck->mReturnValue);
	}
	else
	{
		return NULL;
	}
}