/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <algorithm>	//for std::transform

#include "STPAddonListDialogAbstract.h"
#include "AntTweakBar.h"
#include "AddonDefinitionFile.h"

//defined in hooker.cpp
extern std::vector<AddonDefinitionFile>* AddonDatabase;

//helper to sort a list of StringPair
//returns true if the first string of the first StringPair is alphebetically before
//the first string of the other StringPair.
//result is case INsensitive.
struct STPAddonListDialogAbstractSorter
	: public std::binary_function<StringPair, StringPair, bool>
{	
	bool operator()(const StringPair& Left, const StringPair& Right) const
	{
		return stricmp(Left.first.c_str(),Right.first.c_str())<0;
	}
};

STPAddonListDialogAbstract::STPAddonListDialogAbstract()
:mAddonNames(NULL)
,mCheckedAddons(NULL)
{	
}

STPAddonListDialogAbstract::~STPAddonListDialogAbstract(void)
{
	TwDeleteBar(mMenu);
	mMenu=NULL;
	delete [] mCheckedAddons;
	mCheckedAddons=NULL;
}

void STPAddonListDialogAbstract::Show()
{
	SetVisible(true);
}

bool STPAddonListDialogAbstract::BelongsToList(const AddonDefinitionFile* Addon) const 
{
	return true;
}

void STPAddonListDialogAbstract::SortList()
{
	mAddonNames.sort(STPAddonListDialogAbstractSorter());
}

void STPAddonListDialogAbstract::PopulateList()
{
	mAddonNames.clear();

	//for all addons...
	for (uint i=0;i<AddonDatabase->size();++i)
	{
		const AddonDefinitionFile& Addon=(*AddonDatabase)[i];
	
		//if it should be added to this list
		if(this->BelongsToList(&Addon))
		{
			//add a dummy entry (smaller std::string copy)
			StringPair* NewEntry;
			mAddonNames.push_back(StringPair());

			//get a pointer to the newly added entry
			NewEntry=&mAddonNames.back();

			//fill NewEntry
			Addon.GetFileName(NewEntry->second);

			const std::string* WorkshopItem=AddonDefinitionFile::GetWorkshopItem(NewEntry->second);
			AddonDefinitionFile::GetFriendlyName(NewEntry->first,NewEntry->second);

			
			if (WorkshopItem)
			{
				NewEntry->first=(*WorkshopItem)+":"+NewEntry->first;
			}
			else
			{
				NewEntry->first.reserve(NewEntry->first.size() + NewEntry->second.size() + 4);
				NewEntry->first+=" (";
				NewEntry->first+=NewEntry->second;
				NewEntry->first+=")";
			}
		}
	}

	this->SortList();
	InitializeCheckBoxStates();
}

//creates the mCheckedAddons array and initializes it
void STPAddonListDialogAbstract::InitializeCheckBoxStates()
{
	const uint AddonCount=mAddonNames.size();
	mCheckedAddons=new bool[AddonCount];

	{	//loop over all known addons
		uint j=0;
		std::list<StringPair>::const_iterator End=mAddonNames.end();
		for ( std::list<StringPair>::const_iterator i=mAddonNames.begin();	i!=End; ++i,++j)
		{
			mCheckedAddons[j]=IsAddonInstalled((*i).second);
		}
	}
}

void STPAddonListDialogAbstract::FilterDisplay()
{
	bool ShowAll=false;
	if (mSearchFilter.empty())
	{
		ShowAll=true;
	}

	if (mSearchFilter=="*") //show only installed addons
	{
		uint j=0;
		std::list<StringPair>::const_iterator End=mAddonNames.end();
		for ( std::list<StringPair>::const_iterator i=mAddonNames.begin();	i!=End; ++i,++j)
		{
			//make the addon visible if it is checked
			int VarVisible=mCheckedAddons[j] ? 1 : 0;
			TwSetParam(mMenu,(*i).first.c_str(),"visible",TW_PARAM_INT32,1,&VarVisible);
		}
	}
	else
	{	//show all addons or only those installed
		
		//convert search filter to lower case
		std::transform(mSearchFilter.begin(),mSearchFilter.end(),mSearchFilter.begin(),::tolower);

		const std::list<StringPair>::const_iterator End=mAddonNames.end();
		for ( std::list<StringPair>::const_iterator i=mAddonNames.begin();	i!=End; ++i)
		{
			int VarVisible=0;
			
			if (ShowAll)
			{
				VarVisible=1;
			}
			else
			{
				std::string LowerCaseEntry=(*i).first;
				std::transform(LowerCaseEntry.begin(),LowerCaseEntry.end(),LowerCaseEntry.begin(),::tolower);
				if (LowerCaseEntry.find(mSearchFilter) != std::string::npos)
				{
					VarVisible=1;
				}
				else
				{
					VarVisible=0;
				}
			}		
			TwSetParam(mMenu,(*i).first.c_str(),"visible",TW_PARAM_INT32,1,&VarVisible);
		}
	}
}

void STPAddonListDialogAbstract::OnChangeSearchFilter(const std::string* Filter)
{
	mSearchFilter=*Filter;
	FilterDisplay();
}

//////////// Callbacks
void TW_CALL STPAddonListDialogAbstract_OnChangeSearchFilter(const void* value, void* Dialog)
{ 
	static_cast<STPAddonListDialogAbstract*>(Dialog)->OnChangeSearchFilter(static_cast<const std::string*>(value));
}

void TW_CALL STPAddonListDialogAbstract_UpdateFilterBox(void *value, void *Dialog)
{ 
//	*static_cast<std::string*>(value)=static_cast<STPAddonListDialogAbstract*>(Dialog)->GetSearchFilter();
	TwCopyStdStringToLibrary(*static_cast<std::string*>(value),static_cast<STPAddonListDialogAbstract*>(Dialog)->GetSearchFilter());
}

//////////// save / restore
void STPAddonListDialogAbstract::Restore()
{
	if (mMenu)
	{
		TwDeleteBar(mMenu);
	}
	mMenu=TwNewBar(mUniqueIdentifier.c_str(),NULL);

	STPDialog::Restore();

	TwAddVarCB(mMenu,"Search_filter",TW_TYPE_STDSTRING,STPAddonListDialogAbstract_OnChangeSearchFilter,STPAddonListDialogAbstract_UpdateFilterBox,this,"label='Search'");
	TwAddSeparator(mMenu,"Separator", NULL);


	{	//loop over all known addons
		uint j=0;
		std::list<StringPair>::const_iterator End=mAddonNames.end();
		for ( std::list<StringPair>::const_iterator i=mAddonNames.begin();	i!=End; ++i,++j)
		{
			TwAddVarRW(mMenu,(*i).first.c_str(),TW_TYPE_BOOLCPP,&(mCheckedAddons[j]),NULL);
		}
	}
}