/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <d3d9.h>
#include <algorithm>	//for std::transform

#include "STPListDialogWithPreview.h"
#include "Common.h"
#include "STPMenuManager.h"
#include "D3DTools.h"

#define DEFAULT_PREVIEW_FILE	"STPMissingPreview.png"
////////////////////////////////////////////////////////////
////////////	STPListDialogWithPreview::EntryData
////////////////////////////////////////////////////////////
STPListDialogWithPreview::EntryData::EntryData()
{
	mPreview=NULL;
}

STPListDialogWithPreview::EntryData::~EntryData()
{
	UnloadTexture();
}

void STPListDialogWithPreview::EntryData::UnloadTexture()
{
	if (mPreview)
	{
		if (mPreview->Release() == 0)
		{
			mPreview=NULL;
		}
	}
}

IDirect3DTexture9* STPListDialogWithPreview::EntryData::GetPreview()
{
	if (mPreview)
	{
		return mPreview;
	}
	else if (!mPreviewFile.empty())
	{	//preview not loaded
		IDirect3DDevice9* Device=STPMenuManager::Get()->GetD3DDevice();
		mPreview=LoadTextureFromPHYSFS(mPreviewFile.c_str(),Device,false);
		
		if (mPreview==NULL)
		{	//preview still null->file not found, so use a default preview
			mPreview=LoadTextureFromPHYSFS(STP_RESOURCE_DIRECTORY"/"DEFAULT_PREVIEW_FILE,Device,false);
		}
		return mPreview;
	}
	else
	{
		return NULL;
	}
}
////////////////////////////////////////////////////////////
////////////	STPListDialogWithPreview
////////////////////////////////////////////////////////////
STPListDialogWithPreview::STPListDialogWithPreview(void)
{
	mDefaultCallbackParam.mDialog=this;
	mDefaultCallbackParam.mDisplayName="";
	mDefaultCallbackParam.mReturnValue="";
	mSortingCategories=NULL;

	//add our resource directory at the end of the search path since it won't be use often
	MountSTPResources();

	{
		//place preview window

		STPMenuManager* Manager=STPMenuManager::Get();
		IDirect3DDevice9* Device=Manager->GetD3DDevice();
		//get screen size
		const int WindowWidth=Manager->GetScreenWidth();
		const int WindowHeight=Manager->GetScreenHeight();
		const int PreviewSize=__min(WindowWidth/3,WindowHeight);

		mPreviewWindow.SetDevice(Device);
		mPreviewWindow.SetPosSize(
			WindowWidth-PreviewSize,
			(WindowHeight-PreviewSize)/2,
			PreviewSize,
			PreviewSize,
			false);
	}
}

STPListDialogWithPreview::~STPListDialogWithPreview(void)
{
	uint EntryCount=mEntryData.size();
	for (uint i=0;i<EntryCount;++i)
	{
		mEntryData[i].UnloadTexture();
	}
}

////////// callbacks (begin)
void TW_CALL STPListDialogWithPreview_OnHighlightChange(void* EntryData)
{
	STPListDialogWithPreview::EntryData* Params=static_cast<STPListDialogWithPreview::EntryData*>(EntryData);
	if (Params)
	{
		Params->mDialog->OnHighlightChange(Params);
	}
}

void STPListDialogWithPreview::OnChangeSearchFilter(const std::string* Filter)
{
	mSearchFilter=*Filter;
	FilterDisplay();
}

void TW_CALL STPListDialogWithPreview_OnChangeSearchFilter(const void* value, void* EntryData)
{ 
	STPListDialogWithPreview::EntryData* Params=static_cast<STPListDialogWithPreview::EntryData*>(EntryData);
	if (Params)
	{
		Params->mDialog->OnChangeSearchFilter(static_cast<const std::string*>(value));
	}
}

void TW_CALL STPListDialogWithPreview_UpdateFilterBox(void *value, void *EntryData)
{ 
	STPListDialogWithPreview::EntryData* Params=static_cast<STPListDialogWithPreview::EntryData*>(EntryData);
	if (Params)
	{
		TwCopyStdStringToLibrary(*static_cast<std::string*>(value),Params->mDialog->GetSearchFilter());
	}
	
}
////////// callbacks (end)
void STPListDialogWithPreview::Restore()
{
	if (mMenu)
	{
		TwDeleteBar(mMenu);
	}
	mMenu=TwNewBar("List",STPListDialogWithPreview_OnHighlightChange);

	STPDialog::Restore(); //restore position/size/visibility
	AddControlsOnTopOfList();

	mPreviewWindow.OnDeviceReset();

	//TwAddButton(mMenu,"Cancel",&STPListDialogWithPreview_QuitCallback,&mDefaultCallbackParam,NULL);
	TwAddVarCB(mMenu,"Search_filter",TW_TYPE_STDSTRING,STPListDialogWithPreview_OnChangeSearchFilter,STPListDialogWithPreview_UpdateFilterBox,&mDefaultCallbackParam,"label='Search'");
	TwAddSeparator(mMenu,"-1",NULL);

	//////////////////////////////
	//populate the list's entries
	//////////////////////////////
	const uint EntryCount=mEntryData.size();
	uint ButtonCount=0;
	for (uint i=0;i<EntryCount;++i)	//fo each truck we know
	{
		STPListDialogWithPreview::EntryData& Item=mEntryData[i];

		std::string ButtonDefinition("label='");
		ButtonDefinition+=Item.mDisplayName+"' group='All'";
		char Temp[12];
		sprintf(Temp,"%d",ButtonCount); //generate an unique name for AntTweakBar
		AddEntryToMenu(Temp,Item,ButtonDefinition);

		++ButtonCount;
	}

	//fold the "All" category
	if (EntryCount)
	{
		const int Opened=0;
		TwSetParam(mMenu,"All","opened",TW_PARAM_INT32, 1, &Opened);
	}

	//add other categories if defined
	if (mSortingCategories && !mSortingCategories->empty())
	{
		const STPListCategories::const_iterator End=mSortingCategories->end();

		//for each category...
		for (STPListCategories::const_iterator i=mSortingCategories->begin();
			i != End; ++i)
		{
			bool GroupEmpty=true;
			const std::string DefinitionTail="' group='"+(*i).GetName()+"'";

			//for each truck...
			for (uint j=0;j<EntryCount;++j)
			{
				STPListDialogWithPreview::EntryData& Item=mEntryData[j];
				

				//see if the truck's name matches any of the category's keywords
				if ((*i).BelongsToThisCategory(Item.mDisplayName))
				{
					std::string ButtonDefinition("label='");
					ButtonDefinition+=Item.mDisplayName+DefinitionTail;
					//we end up with something like:
					//label='Type-C 255' group='3 Axle'


					char Temp[12];
					sprintf(Temp,"%d",ButtonCount); //generate an unique name for AntTweakBar
					AddEntryToMenu(Temp,Item,ButtonDefinition);

					GroupEmpty=false;
					++ButtonCount;
				}
			}

			//fold the truck group if it was created
			if (!GroupEmpty)
			{
				const int Opened=0;
				TwSetParam(mMenu,(*i).GetName().c_str(),"opened",TW_PARAM_INT32, 1, &Opened);
			}
		}
	}
}

void STPListDialogWithPreview::OnDeviceLost()
{
	//when device is lost all textures must be freed before we can reset
	uint EntryCount=mEntryData.size();
	for (uint i=0;i<EntryCount;++i)
	{
		mEntryData[i].UnloadTexture();
	}

	mPreviewWindow.OnDeviceLost();
	STPDialog::OnDeviceLost();
}

void STPListDialogWithPreview::SetPreview(const IDirect3DTexture9* Preview)
{
	mPreviewWindow.SetTexture(Preview);
}

void STPListDialogWithPreview::Render(IDirect3DDevice9* Device)
{
	//we want the preview to be opaque
	Device->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
	mPreviewWindow.Render();
}


void STPListDialogWithPreview::FilterDisplay()
{
	const int BUFFER_SIZE=256;
	char Buffer[BUFFER_SIZE];
	Buffer[BUFFER_SIZE-1]=0;

	char VarName[12];
	uint i=0;
	if (mSearchFilter.empty()) //show all items
	{
		const int VarVisible=1;
		while(true) 
		{
			sprintf(VarName,"%d",i);

			//buffer is used here as a dummy storage for an int32
			if (TwGetParam(mMenu,VarName,"visible",TW_PARAM_INT32,1,&Buffer) == 0)
			{	//when TwGetParam returns an error, it means we've swept over all buttons
				break;
			}
			TwSetParam(mMenu,VarName,"visible",TW_PARAM_INT32,1,&VarVisible);
			++i;
		}

		//fold the "All" category
		const int Opened=0;
		TwSetParam(mMenu,"All","opened",TW_PARAM_INT32, 1, &Opened);
	}
	else //don't show all items
	{
		//convert search filter to lower case
		std::transform(mSearchFilter.begin(),mSearchFilter.end(),mSearchFilter.begin(),::tolower);
		
		while(true) 
		{
			int VarVisible=0;
			sprintf(VarName,"%d",i);

			//the -2 guarantees we'll never overwrite the last character of the buffer
			//which is set to 0.
			if (TwGetParam(mMenu,VarName,"label",TW_PARAM_CSTRING,BUFFER_SIZE-2,&Buffer) == 0)
			{	//when TwGetParam returns an error, it means we've swept over all buttons
				break;
			}

			//make entry name lower case too for seemingly case insensitive comparison
			strlwr(Buffer);
			if (strstr(Buffer,mSearchFilter.c_str()))
			{
				VarVisible=1;
			}
			
			TwSetParam(mMenu,VarName,"visible",TW_PARAM_INT32,1,&VarVisible);
			++i;
		}

		//unfold the "All" category
		const int Opened=1;
		TwSetParam(mMenu,"All","opened",TW_PARAM_INT32, 1, &Opened);
	}
}

void STPListDialogWithPreview::AddControlsOnTopOfList()
{
	//nothing to do by default
}

void STPListDialogWithPreview::OnHighlightChange(STPListDialogWithPreview::EntryData *Params)
{
	SetPreview(Params->GetPreview());
}