/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "Common.h"
#include "CameraHooks.h"
#include "Settings.h"


void __stdcall OnMoveCameraToPreset_Internal(STCamera* Camera)
{
	if (GetKeyState(GlobalSettings::RightSideCameraKey)&0x8000)
	{	//if RightSideCameraKey is down

		if (Camera->LookAtTrailer)
		{	//if we are in trailer mode then the symmetrical view angle is simply
			//minus left view angle
			if (Camera->TargetYAngle==Camera->ForwardView.YAngle)
			{
				Camera->TargetYAngle=-Camera->BackwardView.YAngle;
			}
			else
			{
				Camera->TargetYAngle=-Camera->ForwardView.YAngle;
			}
		}
		else
		{
			//if we are in trailer mode then the symmetrical view angle is
			//minus left view angle, minus about 15 degrees. Pavel what did you do?
			const float AngleOffset=DEG2RAD(-17.0f);
			if (Camera->TargetYAngle==Camera->ForwardView.YAngle ||
				Camera->TargetYAngle==Camera->ForwardView.YAngle + AngleOffset)
			{
				Camera->TargetYAngle=-AngleOffset-Camera->BackwardView.YAngle;
			}
			else
			{
				Camera->TargetYAngle=AngleOffset-Camera->ForwardView.YAngle;
			}
		}
		
		
	}

	//original code
	Camera->AutoMove=true;
}

_declspec(naked) void OnMoveCameraToPreset()
{
	__asm
	{
		pushad		//save all register
		push esi	//tell OnMoveCameraToPreset_Internal when the camera is
		call OnMoveCameraToPreset_Internal
		popad		//restore all registers
		retn
	}
}