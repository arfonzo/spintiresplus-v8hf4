/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <d3d9types.h>
#include <vector>
#include "D3DTools.h"

DWORD WINAPI MinimapMainLoop(LPVOID lpParameter);

struct IDirect3D9;
struct IDirect3DDevice9;
struct IDirect3DVertexBuffer9;
struct IDirect3DTexture9;

struct MinimapTruckMarker
{
	SolidFillVertex Vertex[3];
};

class STPMinimapManager
{
public:
	STPMinimapManager();
	~STPMinimapManager();
	static STPMinimapManager* Get();
	
	//please give the NOT hooked version of d3dcreate9
	//you must call this before using StartMinimap
	//void SetD3DCreate9Ptr(IDirect3D9* (__stdcall* D3DCreat9Ptr)(UINT));
	void SetD3D(IDirect3D9* D3D) {mD3D=D3D;}

	//Informs the minimap that it should not use this adapter
	void SetMainGameAdapter(UINT Adapter) {mMainGameAdapter=Adapter;}

	//creates the thread that will handle the minimap
	//setups directx device. This must be called from the
	//main thread
	void StartMinimap(HWND FocusWindow);

	const HWND GetWindowHandle() const {return mWindow;}

	bool Alive() const {return mAlive;}

	//adds a marker on the minimap, the input
	//coordinates are in real world space
	//angle is in radians, where 0 makes the marker point to the right.
	void AddTruckMarker(float X,float Y,float Angle);

	//call this method to tell the minimap to change
	//its...map
	void SetMapName(const std::string& NewMap);

	void MoveWindow(int DeltaX, int DeltaY);
	int GetWindowXPos() const {return mWindowX;}
	int GetWindowYPos() const {return mWindowY;}

	//loop to proces all messages sent to the minimap's window.
	//should be caled be the dedicated minimap thread.
	//does not return until the minimap is destroyed.
	void MessagePump();

	void CheckForLostDevice();

	//signals the minimap's thread to quit, and
	//deallocated the d3d device
	//MUST be run by the main thread
	void Terminate();

	void OnWMDestroy();
private:
	void UpdateBackplaneGeometry();
	void DrawLocatorsOnMap();
	void UpdateBackgroundTexture(bool ForceFullReload=false);
	void LoadIcons();

	void FlushGFXObjects();
	void ReinitGFXObjects();

	void Render();

	//Dereferences a texture and nulls out
	//the pointer.
	//passing a NULL pointer is safe
	void UnloadTexture(IDirect3DTexture9*& Texture);

	//loads a texture, returns NULL on failure.
	//filename is anything PHYSFS would accept
	//if RenderTarget is true the texture will be loaded as a render target
	IDirect3DTexture9* LoadTexture(const char* FileName,bool RenderTarget=false);
private:

	HWND mFocusWindow;	//game's main window (required for multi-D3D device setups)
	HWND mWindow;		//where to render
	WNDCLASS mWindowClass;
	HINSTANCE mProgamInstance;

	int mWindowX;
	int mWindowY;
	int mWindowWidth;
	int mWindowHeight;
	int mMapWidth;
	int mMapHeight;
	float mMapScaleFactor;
	float mMinimapTopLeftX;	//coordonates of the verticle
	float mMinimapTopLeftY;	//within the window

	HANDLE mThread;
	ULONG mThreadID;
	bool mEnableRendering;
	bool mDeviceJustReset;
	bool mAlive;
	

	bool mMarkersDirty;
	int mActiveMarkerCount; //only used inside STPMinimapManager::Render(). Do not touch.
	

	std::string mMapName;
	bool mMapNameChanged;
	int mLastKnownLocatorCount;

	D3DPRESENT_PARAMETERS mPresent;
	IDirect3DDevice9* mDevice;
	IDirect3D9* mD3D;
	UINT mMainGameAdapter;	//index of the display adapter used by the game, and that we should avoid.
	IDirect3DVertexBuffer9* mBackPlaneVertexBuffer;
	IDirect3DVertexBuffer9* mTruckMarkerBuffer;
	IDirect3DVertexBuffer9* mObjectMarkerBuffer;
	IDirect3DTexture9* mMapTexture;
	//icons
	IDirect3DTexture9* mGarageIcon;
	IDirect3DTexture9* mFuelIcon;
	IDirect3DTexture9* mObjectiveIcon;
	IDirect3DTexture9* mLumberYardIcon;
	IDirect3DTexture9* mLogKioskIcon;

	std::vector<MinimapTruckMarker> mMarkers;
};