/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "Typedefs.h"
#include "Settings.h"
#include "Vector.h"

//you'll have to find those files on you own, they are part of havok's SDK aka project anarchy.
#include "Physics\Dynamics\Entity\hkpRigidBodyCinfo.h" 
#include "Physics\Dynamics\Constraint\Bilateral\Wheel\hkpWheelConstraintData.h"

//XML reader
char* __stdcall GetXMLString(void* DOMTree,char* NodeName);

//reads a group of 3 floats in the format Name="(%f; %f; %f)"
//returns true if all 3 floats were successfully read.
bool GetXMLFloat3(void* XMLTree,char* Name,float& X,float& Y,float& Z);

//reads just one float
//returns false if the XML token didn't exist
bool GetXMLFloat(void* XMLTree,char* Name,float& Output);

//reads just one int
//returns false if the XML token didn't exist
bool GetXMLInt(void* XMLTree,char* Name,int& Output);

//returns true if a give property is true
//or false if explicitely set so, or if the property isn't defined
bool GetXMLBool(void* XMLTree,char* Name,bool DefaultValue=false);

//loads the mesh specified by MeshName or just increases its reference count
//what MeshLoaderParam is is still unknown
struct LoadedMesh;
LoadedMesh* __stdcall LoadMesh(void* MeshLoaderParam,const char* MeshName);

//Should return 1 as integer if the truck does NOT sink, 0 otherwise.
uint __stdcall TruckStaysAboveMud();

/////////////////////////////////// Wheel loading hook 
void PatchWheelLoader();

std::string* _fastcall ReadExtraWheelsTypeTokens(std::string& Input);
void RetrieveWheelSetPiggyBack();

/////////////////////////////////// Suspension loading hook 
void PatchSuspensionLoader();

/////////////////////////////////// truck addon parser
bool ParseTruckAddonHook(void* DOMTree,void*);

/////////////////////////////////// time of day change
float GetUpdatedTimeOfDay(void); //returns time in the 0-24.0 range

/////////////////////////////////// Snorkel support
struct TruckChassis;
bool _cdecl OnInstallOrUninstallAddon(TruckChassis* Base,const std::string& AddonName,void* Arg3,uint Arg4);

/////////////////////////////////// New difflock damage
float __stdcall GetDifflockStress();
int __stdcall GetDifflockDamage();

/////////////////////////////////// Tank steering
class TruckControls;
void _stdcall ApplyTorqueOnWheels(TruckControls* Truck,float AccelRatio,float FrameTime);

/////////////////////////////////// Wheelsets that require adons
void __stdcall ReadExtraWheelsSetTokens(std::string& This,const char* Input);
void __stdcall PopulateWheelsetsInGarageMenu(void* Menu,const std::wstring& SetName,uint Index);

/////////////////////////////////// Silent IsLinkedSteering
void SetSilentLinkedSteering();

//returns true if this constraint is linked to steering
//and it is engine powered (not silent).
bool ShouldRevEngineForLinkedSteerConstraint();

/////////////////////////////////// External Minimap and cloakmesh override
struct LoadedMesh;
LoadedMesh* Minimap_Hook(void* MeshLoaderParam, const char* MeshToLoad);