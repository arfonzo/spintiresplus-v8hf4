/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>
#include "piggyback.h"

class STPTruckNamePiggyBack :
	public PiggyBack
{
public:
	STPTruckNamePiggyBack();
	~STPTruckNamePiggyBack(void);

	void SaveInString(std::string& String);
	void SetSensationOverrideHash(const char* AddonName);

	bool HasSensationOverride();
	bool HasGearboxOverride();
	void SaveSensationMin(const Vector3& Sensation);
	void SaveSensationMax(const Vector3& Sensation);
	void AddTorqueMod(const std::string& AddonName,float TorqueFactor);
	void SaveGearbox(const std::string& AddonName,const std::vector<float>& OriginalGearbox);

	//Restores damage sensation overrides if AddonName is the one
	//that installed those overrides.
	//Returns true if the override was removed,
	//false otherwise.
	bool RemoveSensationOverride(const std::string& AddonName, Vector3& TruckSensationMin,Vector3& TruckSensationMax);

	//tries to remove a torque mod, returns true if it was actually removed
	//and sets TorqueFactor to the torque factor this mod applied.
	bool RemoveTorqueMod(const std::string &AddonName, float& TorqueFactor);

	//Restores a truck's original gearbox if AddonName overrode this truck's gearbox earlier
	//Returns true if the override was removed,
	//false otherwise.
	bool RemoveGearboxOverride(const std::string &AddonName, std::vector<float>& TruckGearbox);

	//called by InitFromChars and InitFromString
	//to fetch data from stream
	virtual void Init();
private:
	struct TorqueMod
	{
		uint AddonHash;	//hash of the name of the addon who applied this mod
		float TorqueFactor;
	};

//actual payload
private:
	//hash of the name of the addon that overrides SensationData
	//excludes the null terminator
	bool mSensationOverriden;
	uint mSensationOverrideHash; 
	Vector3 mOriginalSensationMin;
	Vector3 mOriginalSensationMax;

	std::vector<TorqueMod> mTorqueMods;

	bool mGearboxOverriden;
	uint mGearboxOverrideHash; 
	std::vector<float>	mGearbox;
};
