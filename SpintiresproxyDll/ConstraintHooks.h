/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>
#include <string>
#include "typedefs.h"


void ConstraintParser();
void StoreConstraintParserKeyAddresses(
		Address HavokAlloc,					//havok memory allocator
		Address DefaultCase,				//where to jump if we did not construct a constraint
		Address AddConstraintToWorld,		//where to jump if we built one
		Address Point2Plane_Constructor,	//address of hkpPointToPlaneConstraintData constructor
		Address Point2Plane_SetInBodySpace,	//address of hkpPointToPlaneConstraintData::setInBodySpace
		Address Point2Path_Constructor,		//address of hkpPointToPathConstraintData constructor
		Address Point2Path_SetInBodySpace,	//address of hkpPointToPathConstraintData::setInBodySpace
		Address LinearCurve_Constructor,	//address of hkpLinearParametricCurve's constructor
		Address LinearCurve_Addpoint,		//address of hkpLinearParametricCurve::addPoint
		Address BallSocketChainData_Constructor,		//address of hkpBallSocketChainData's constructor
		Address BallSocketChainData_addConstrain,		//address of hkpBallSocketChainData::addConstraintInfoInBodySpace
		Address ConstraintChainInstance_Constructor,	//address of hkpConstraintChainInstance's constructor
		Address ConstraintChainInstance_AddEntity,		//address of hkpConstraintChainInstance::addEntity
		Address RemoveReference,			//address of hkpReferencedObject::removeReference
		Address ConstrainFinalizer);		//function that takes the constrain data, builds the constaint and optionnally
											//wraps it in a malleable container.
											//typically starts with "Havok| Ignoring Malleable Strength because constraint is breakable. Is there sense in using both?"


byte* _fastcall HavokMalloc(uint ByteCount);

//end of hkpWorldConstraintUtil::addConstraint
//void OnConstraintAddedToWorld();

//this function should just append a string to a vector of strings
//hooked to parse extra constraint parameters from the .xml
//This function is only executed for named constraints
void __stdcall ParseExtraConstraintParams(std::vector<std::string>& Vector,const std::string& String);
