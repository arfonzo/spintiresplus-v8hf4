/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>	//for __min/__max
#include "Settings.h"

#define STP_VERSION_STRING "Spintires Plus v8"
#define STP_RESOURCE_DIRECTORY "SpintiresPlusResources"

//#define hkConvexShapeDefaultRadius 0.02
#define EPSILON 1.192092895507812E-07
#define	INSIGNIFICANT 0.0001

//please use the true FrameTime whenever you can.
#define TYPICAL_HAVOK_FRAMERATE 60

//printf to debug console if debug mode is on
#define LOG(...)					\
	if(GlobalSettings::DebugMode)	\
	{								\
		printf(__VA_ARGS__);		\
	}

//converts degrees to radians
#define DEG2RAD(x) x*M_PI/180.0f
#define RADS2RPM(x) x*60.0f/(2.0f*(float)M_PI)
#define RPM2RADS(x) x*(2.0f*(float)M_PI)/60.0f

#define Lerp(x,x1,x2,y1,y2) y1+(x-x1)*(y2-y1)/(x2-x1)
#define __clamp(min_,x,max_) __max(min_,__min(x,max_))

#define SAME_SIGN(a,b) SameSign(&a,&b,sizeof(a))

//returns true if both operand are of the same sign
//actually returns true if both MSbit are identical
bool SameSign(void* Operand1,void* Operand2,int OperandSize);

//#define DEBUG_DRIVETRAIN

#ifdef DEBUG_DRIVETRAIN
#define LOG_DT LOG
#else
	#define LOG_DT(...)
#endif

//Mounts Spintires Plus' resource
//directory into the PHYSFS system
//Duplicate calls will not clog PHYSFS
void MountSTPResources();

////////////// map related defines
#define MAPFILE_EXTENSION	".stg"
#define MAPFILE_PREFIX		"level_"
#define MAP_PROVING			"proving"
#define MAP_MENU			"menu"
#define MAP_PROVING_FILE	MAPFILE_PREFIX""MAP_PROVING""MAPFILE_EXTENSION
#define MAP_MENU_FILE		MAPFILE_PREFIX""MAP_MENU""MAPFILE_EXTENSION

#define SAVE_EXTENSION		".sts"