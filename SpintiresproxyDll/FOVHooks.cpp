/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "Common.h"

#include "FOVHooks.h"
#include "Settings.h"
#include "Typedefs.h"

struct CameraParameters
{
	byte Unknown1[0x40];
	float PerspectiveMatrix[4][4]; //actually a D3DXMATRIX
	byte Unknown2[0x80];
	//offset 0x100
	float Unknown3;
	float Unknown4;
	float FOV;	//in radians
	float AspectRatio;
	float ZNear;
	float ZFar;
};

void __fastcall ChangeFOV_Internal(CameraParameters* Camera)
{
	if (GlobalSettings::FOV>0)
	{
		Camera->FOV=GlobalSettings::FOV;
	}
	if (GlobalSettings::AspectRatio>0)
	{
		Camera->AspectRatio=GlobalSettings::AspectRatio;
	}
	LOG("Created Persepctive FOV:%.3f\tAspect:%.3f\tzn%.3f\tzf:%.3f\n",Camera->FOV,Camera->AspectRatio,Camera->ZNear,Camera->ZFar);
}
void ChangeFOV(void)
{
	__asm
	{
		fstp dword ptr [esi+104] //original code

		push ecx
		mov ecx,esi
		call ChangeFOV_Internal
		pop ecx
	}
}