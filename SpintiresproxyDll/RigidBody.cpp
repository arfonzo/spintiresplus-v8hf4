/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "RigidBody.h"
#include "Physics/Dynamics/Entity/hkpRigidBody.h"

//since my havok version doesn't match Pavels, let's reimplement the bare minimum
//of hkRigidBody
void RigidBody::Activate()
{
	//uint Destination=0x0754810;
	//__asm
	//{
	//	mov ecx, [this]
	//	mov eax, dword ptr[Destination]
	//	call eax						//call hkpEntity::activate()
	//}

}
void RigidBody::ApplyAngularImpulse(const Vector4 &Torque)
{
	Activate();
	__asm
	{
		mov ecx, [this]
		add ecx, 0xE0 //pointer to hkpMotion's specific data (skip base class stuff)

		push Torque
		mov eax,dword  ptr [ecx]		//now eax=base of vftable
		mov eax,dword  ptr [eax+0x50]	//now eax=address of the 20th virtual member
		call eax
	}
}


void RigidBody::ApplyPointImpulse(const Vector4& Force,const Vector4& Point)
{
	Activate();
	__asm
	{
		mov ecx, [this]
		add ecx, 0xE0 //pointer to hkpMotion's specific data

		push Point
		push Force
		mov eax,dword  ptr [ecx]		//now eax=base of vftable
		mov eax,dword  ptr [eax+0x4c]	//now eax=address of the 19th virtual member
		call eax						//call getRigidMotion()->applyPointImpulse(Force, Point);
	}
}

void RigidBody::SetAngularVelocity(const Vector4& newVel)
{
	Activate();
	reinterpret_cast<const hkpRigidBody*>(this)->getRigidMotion()->m_angularVelocity=*const_cast<Vector4&>(newVel).AsHkVector();
}

const Vector4& RigidBody::getLinearVelocity() const
{
	return reinterpret_cast<const Vector4&>(reinterpret_cast<const hkpRigidBody*>(this)->getLinearVelocity());
}

const Vector4& RigidBody::getAngularVelocity() const
{
	return reinterpret_cast<const Vector4&>(reinterpret_cast<const hkpRigidBody*>(this)->getAngularVelocity());
}

bool RigidBody::CollidesWithGroup(uint GroupID) const
{
	for (int i=0;i<CollisionGroups.getSize();++i)
	{
		if (CollisionGroups[i].ID==GroupID)
		{
			return true;
		}
	}

	return false;
}

void RigidBody::GetCollisionUserData(uint GroupID,hkUlong* OutPut) const
{
	for (int i=0;i<CollisionGroups.getSize();++i)
	{
		if (CollisionGroups[i].ID==GroupID)
		{
			*OutPut=CollisionGroups[i].UserData;
			return;
		}
	}
}