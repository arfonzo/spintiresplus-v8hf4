/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>
#include <vector>

#include "typedefs.h"
#include "stpdialog.h"
#include "STPListCategory.h"
#include "STP2DPlaneDrawer.h"

struct IDirect3DTexture9;
struct IDirect3DVertexBuffer9;

class STPListDialogWithPreview :
	public STPDialog
{
public:
	class EntryData
	{
	public:
		EntryData();
		~EntryData();

		void UnloadTexture();

		IDirect3DTexture9* GetPreview();
	public:
		STPListDialogWithPreview* mDialog;
		std::string mReturnValue;
		std::string mDisplayName;
		std::string mPreviewFile;
	private:
		IDirect3DTexture9* mPreview;
	};

public:
	STPListDialogWithPreview(void);
	~STPListDialogWithPreview(void);

	virtual void OnDeviceLost();
	virtual void Restore();

	//note D3D device state D3DRS_ALPHABLENDENABLE
	//is set to false during preview rendering.
	virtual void Render(IDirect3DDevice9* Device);

	
	//a lot like a mouseover callback, but also triggered when the user uses
	//the arrow keys to move over an item.
	virtual void OnHighlightChange(STPListDialogWithPreview::EntryData* Params);

	virtual void OnChangeSearchFilter(const std::string* Filter);
	const std::string& GetSearchFilter() const {return mSearchFilter;};
protected:
	//changes the preview picture
	void SetPreview(const IDirect3DTexture9* Preview);

	//Adds a var to the anttweakbar menu
	//VarName should be passed as is to TwAdd***
	//You are allowed to modify Label
	virtual void AddEntryToMenu(const char* VarName,const STPListDialogWithPreview::EntryData& Data,std::string& Label)=0;

	//override this if you want to add controls at the top of the list, before
	//it is populated. mMenu is already allocated at this point, but the menu
	//contains no controls.
	virtual void AddControlsOnTopOfList();
protected:
	std::vector<STPListDialogWithPreview::EntryData> mEntryData;
	STPListCategories* mSortingCategories;

	//a callback param with a dialog that just points to self
	//Typically useful for quit/cancel buttons
	EntryData mDefaultCallbackParam;

	STP2DPlaneDrawer mPreviewWindow;
private:
	void FilterDisplay();
	std::string mSearchFilter;
};
