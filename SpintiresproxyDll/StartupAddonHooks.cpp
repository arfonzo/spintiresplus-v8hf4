/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <vector>


#include "StartupAddonHooks.h"
#include "common.h"
#include "StartupTruck.h"
#include "STPStartupAddonManager.h"


void __stdcall ChangeStartupAddons_Internal(std::vector<StartupTruck>& StartupTrucks, MapOverrideList* Overrides)
{	
	const uint TruckCount=StartupTrucks.size();
	STPStartupAddonManager* Manager=STPStartupAddonManager::Get();
	for (uint i=0;i<TruckCount;++i)
	{
		StartupTruck& Truck=StartupTrucks[i];
		Manager->ApplyOverride(i,Truck.Addons,Truck.Trailer,Truck.nWheelsSet);
	}
	Manager->Clear();


	//we perform the truck type replacement ourself instead of letting the game do it,
	//because if the game does it, it will ignore wheelset replacement.
	const uint ReplacedTruckCount=Overrides->TruckOverrides.size();
	for (uint i=0;i<ReplacedTruckCount;++i)
	{
		StartupTruckOverride& Replacement=Overrides->TruckOverrides[i];
		StartupTrucks[ Replacement.Index ].szType = Replacement.TruckType;
	}
	Overrides->TruckOverrides.clear(); //we did the replacement for the game
}

__declspec(naked) void ChangeStartupAddons()
{
	__asm
	{
		//Overrides
		mov esi,[esp+0x60+4]	//60: original +4: call
		push esi

		//StartupTrucks
		lea esi,[esp+0xB8+4+4]	//b8: original +4: call  +4:already pushed something
		push esi

		//esi must be the pointer to the end of array
		mov esi,dword  ptr [esi+0x10]	//10: offset from start of std::vector to end of array pointer
		call ChangeStartupAddons_Internal

		ret
	}
}