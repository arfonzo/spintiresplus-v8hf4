/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <Common/Base/hkBase.h>
#include <Physics/ConstraintSolver/Constraint/Atom/hkpConstraintAtom.h>

#include "typedefs.h"

class hkpConstraintData2
{
public:
	virtual void Function0();
	virtual void Function1();
	virtual void Function2();
	virtual void Function3();
	virtual void Function4();
	virtual void Function5();
	virtual void Function6();
	virtual void Function7();
	virtual void Function8();
	virtual void Function9();
	virtual int getType() const;

public:
	hkUlong m_userData;
};

class hkpLimitedHingeConstraintData2 : public hkpConstraintData2
{
public:
	inline hkpConstraintMotor* getMotor() const {return m_motor;}
	inline hkReal getMaxAngularLimit() const {return m_angLimit.m_maxAngle;}
	inline hkReal getMinAngularLimit() const {return m_angLimit.m_minAngle;}
	inline void setMotorTargetAngle(hkReal angle) {m_targetAngle=angle;}
	inline hkReal getMotorTargetAngle()const {return m_targetAngle;}

private:
	byte Unknown[0xBC-sizeof(hkpConstraintData2)];

	//offset BC
		//looks like hkpAngMotorConstraintAtom has been rewritten with an extra
		//parameter between m_correspondingAngLimitSolverResultOffset and m_targetAngle
	hkReal m_targetAngle;
	HK_CPU_PTR(class hkpConstraintMotor*) m_motor;

	struct hkpAngFrictionConstraintAtom m_angFriction;
	hkpAngLimitConstraintAtom m_angLimit;
};