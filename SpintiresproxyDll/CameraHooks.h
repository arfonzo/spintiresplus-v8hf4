/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "typedefs.h"

void OnMoveCameraToPreset();

struct CameraPreset
{
	float YAngle;
	float Translation;
};

struct STCamera
{
	byte Unknown0[0x54];
	//offset 54
	int LookAtTrailer;
	byte Unknown05[0x3c];
	//offset 94
	float ZoomLevel;
	byte Unknown10[0x4];
	//offset 9c
	float PosX;
	float PosY;
	float PosZ;
	byte Unknown20[0x44];

	//offset EC
	float YAngle;
	//when you point the camera upward and it moves forward
	//(relative to eye) you increase TranslationRatio 0.0->1.0
	float TranslationRatio; 
	byte Unknown25[0x14];

	//offset 108
	bool AutoMove; //moves camera to target Yangle/translation ratio
	float TargetYAngle;
	float TargetTranslationRatio;
	//offset 114
	//copy this to Y/Z angle when carema should facr forwayd
	CameraPreset ForwardView;
	//offset 11C
	CameraPreset BackwardView;
	byte Unknown3[0x18];
	//offset 13c
	void* NewCameraPos;	//needs more investigation
};

