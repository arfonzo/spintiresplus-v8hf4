/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include  <limits>

#include "STPVehicleTrack.h"
#include "Common.h"
#include "typedefs.h"
#include "Vector.h"
#include "RigidBody.h"
#include "TruckControls.h"
#include "TruckChassis.h"

//forward reverse tracks will assume
//they should engage reverse mode if the truck's
//steering ratio is outside +/- SKIDSTEER_FR_DEADZONE
#define SKIDSTEER_FR_DEADZONE 0.99f

STPVehicleTrack::STPVehicleTrack(const std::vector<WheelParams*>& AllWheels,bool RightSideTrack, const TruckControls* Truck,float FrameTime)
:mTrackSpeed(0.0f)
,mAngvel(0.0f)
,mTrackLength(1.0f)
,mWheelCount(0)
,mVehicleWheelCount(0)
,mAverageRadius(0.0f)
,mResistiveTorque(0.0f)
,mSumInvKR(0.0f)
,mSumVs_K(0.0f)
{
	mRightSide=RightSideTrack;

	//preallocate
	mVehicleWheelCount=AllWheels.size();
	mWheels.reserve(mVehicleWheelCount/2);

	//positions of the wheels closer to the frot/back of the truck
	//used to calculate track length
	float ForwardmostExcentration=-std::numeric_limits<float>::max();
	float BackwardmostExcentration=std::numeric_limits<float>::max();
	for (uint i=0;i<mVehicleWheelCount;++i)
	{
		WheelParams* Wheel=AllWheels[i];

		if (Wheel->isRightSided==RightSideTrack)
		{
			mWheels.push_back(Wheel);
			mWheelCount++;

			//calculate average speeds
			mTrackSpeed+=Wheel->AngularVelocity * Wheel->fRadius;
			mAverageRadius+=Wheel->fRadius;
			
			//calculate average wheel position (to get track central position)
			mPosition+=Wheel->Wheel->Position;

			//resistive torque
			mResistiveTorque+=Wheel->GetResistiveTorque(FrameTime);

			//find the wheels closer to the front and back of the truck
			//to calculate track length
			const float TruckAxisProjection=Truck->Chassis->TruckBase->TruckBody->XVector.Dot(Wheel->Wheel->Position);
			ForwardmostExcentration=__max(TruckAxisProjection , ForwardmostExcentration);
			BackwardmostExcentration=__min(TruckAxisProjection , BackwardmostExcentration);

			//constants later used in GetmaxAchievableTrackSpeed
			mSumInvKR+=1.0f/( Wheel->GetInvInertia() * Wheel->fRadius );
			mSumVs_K+=Wheel->AngularVelocity/Wheel->GetInvInertia();
		}
	}

	mSumInvKR/=FrameTime;
	mSumVs_K/=FrameTime;

	if (!mWheelCount)	//safety
	{
		mAverageRadius=1.0f;
	}
	const float InvWheelcount=1.0f/(float)mWheelCount;
	mAverageRadius*=InvWheelcount;
	mTrackSpeed*=InvWheelcount;
	mAngvel=mTrackSpeed / mAverageRadius;
	mPosition.Scale(InvWheelcount);
	mTrackLength=ForwardmostExcentration-BackwardmostExcentration;
}

STPVehicleTrack::~STPVehicleTrack(void)
{
}

//return the number of wheels in this track
uint STPVehicleTrack::GetCount() const
{
	return mWheelCount;
}

WheelParams::STPFlags STPVehicleTrack::GetSkidSteerMode() const
{
	if (mWheelCount)
	{
		return mWheels[0]->GetSkidSteerMode();
	}
	else
	{
		return WheelParams::SkidSteer_None;
	}
}

float STPVehicleTrack::GetmaxAchievableTrackSpeed(float TorqueBudget) const
{
	//find MaxAchievableTrackVel = Radius_i*[ (Torque_i + Resistance_i) * FrameTime /Inertia_i + StartingVelocity_i ]
	//so as to use the whole torque budget
	return (TorqueBudget + mResistiveTorque + mSumVs_K) / mSumInvKR;
}

float STPVehicleTrack::AdjustTargetTrackSpeed(float TorqueBudget,float InitialTarget) const
{
	const float MaxAchievableTrackVel=GetmaxAchievableTrackSpeed(TorqueBudget);
	LOG_DT("target %f achievable %f budget %f ||\n",TargetTrackSpeed,MaxAchievableTrackVel,TorqueBudget);

	//when the AutoGearboxHint is positive for long enough the gearbox wil think it's tilme to shift up
	//but I miss the shift down considion

	if (abs(InitialTarget)<INSIGNIFICANT)
	{
		return 0.0f;
	}
	else if (MaxAchievableTrackVel/InitialTarget < 1.0f)
	{	//MaxAchievableTrackVel smaller (in abs value) than InitialTarget or of different sign
		return MaxAchievableTrackVel;
	}
	else
	{
		return InitialTarget;
	}
}
//returns how much torque was actually used
float STPVehicleTrack::ApplyTorque(float TorqueBudget, float TargetTrackSpeed, float FrameTime)
{
	if (this->mRightSide)
	{
		LOG_DT("Right ||\n")
	}
	else
	{
		LOG_DT("Left ||\n")
	}

	if (!mWheelCount)
	{
		return 0.0f; //no wheel,no used torque
	}

	TorqueBudget=(float) _copysign(TorqueBudget,TargetTrackSpeed);

	float UsedTorque=0.0f;
	TargetTrackSpeed=AdjustTargetTrackSpeed(TorqueBudget,TargetTrackSpeed); 

	//Wheel->TorqueRepartition is used to signal when to shift up
	const float InvWheelCount=1.0f/((float)mWheelCount);

	for (uint i=0;i<mWheelCount;i++)
	{
		float Torque;
		WheelParams* Wheel=mWheels[i];
		const float TargetAngvel=TargetTrackSpeed/Wheel->fRadius;

		Torque=Wheel->ServoToAngvel(TargetAngvel,FrameTime,TorqueBudget);
		
		UsedTorque+=Torque;
		LOG_DT("servo %f target %f ||\n",Torque,TargetTrackSpeed);
		Wheel->ApplyTorque(Torque,FrameTime);
		Wheel->SavePreviousAngvel();
		
		Wheel->TorqueRepartition=InvWheelCount;
	}
	LOG_DT("used torque %f ||\n",UsedTorque);
	return UsedTorque;
}

//returns how much torque it would cost to run Apply torque with the same paramters
float STPVehicleTrack::CalculateTorqueRequirement(float TorqueBudget, float TargetTrackSpeed, float FrameTime) const
{
	if (!mWheelCount)
	{
		return 0.0f; //no wheel,no used torque
	}

	TorqueBudget=(float) _copysign(TorqueBudget,TargetTrackSpeed);
	TargetTrackSpeed=AdjustTargetTrackSpeed(TorqueBudget,TargetTrackSpeed); 	
	return TargetTrackSpeed*mSumInvKR - mSumVs_K - mResistiveTorque;
}

bool STPVehicleTrack::IsReverseSteering(const float SteeringRatio, bool InLowGear) const
{
	if (InLowGear && GetSkidSteerMode()==WheelParams::SkidSteer_ForwardReverse)
	{
		if ( (mRightSide && SteeringRatio < -SKIDSTEER_FR_DEADZONE )||
			 (!mRightSide && SteeringRatio > SKIDSTEER_FR_DEADZONE ) )
		{
			return true;
		}
	}

	return false;
}

//returns the linear track speed corresponding to this half-shaft angvel
float STPVehicleTrack::AngVel2TrackSpeed(float Angvel) const
{
	return Angvel*mAverageRadius;
}