//computes the hash of the input buffer using the FNV1a algorithm
static unsigned int FNV1aHash(const void* Buffer, unsigned int Length)
{
	unsigned int Hash=2166136261;
	const char* Data=reinterpret_cast<const char*>(Buffer);
	for (size_t i = 0;i<Length;i++)
	{
		Hash*=16777619;
		Hash^=Data[i];
	}

	return Hash;
}