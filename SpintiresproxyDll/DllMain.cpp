/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include "Hooker.h"
#include "IDirect3D9_Wrapper.h"
#include "Settings.h"
#include "ExternalMinimap.h"

FARPROC p[14] = {0};
#define FARPROCSIZE 4

HINSTANCE D3D9Dll;
Hooker Hooks;

BOOL WINAPI DllMain(HINSTANCE hInst,DWORD reason,LPVOID)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		if (GlobalSettings::d3d9DllOverride==NULL || strlen(GlobalSettings::d3d9DllOverride)==0)
		{
			char Buffer[MAX_PATH];
			GetSystemDirectory(Buffer,MAX_PATH);
			strcat(Buffer,"\\d3d9.dll");
			D3D9Dll = LoadLibrary(Buffer);

			if (!D3D9Dll)
			{
				MessageBox(NULL,"Failed to load original d3d9.dll","Error",MB_OK);
				return false;
			}
		
		}
		else
		{
			D3D9Dll = LoadLibrary(GlobalSettings::d3d9DllOverride);
			if (!D3D9Dll)
			{
				MessageBox(NULL,"Failed to load the d3d9.dll you specified","Error",MB_OK);
				return false;
			}
		}


		p[0] = GetProcAddress(D3D9Dll,"D3DPERF_BeginEvent");
		p[1] = GetProcAddress(D3D9Dll,"D3DPERF_EndEvent");
		p[2] = GetProcAddress(D3D9Dll,"D3DPERF_GetStatus");
		p[3] = GetProcAddress(D3D9Dll,"D3DPERF_QueryRepeatFrame");
		p[4] = GetProcAddress(D3D9Dll,"D3DPERF_SetMarker");
		p[5] = GetProcAddress(D3D9Dll,"D3DPERF_SetOptions");
		p[6] = GetProcAddress(D3D9Dll,"D3DPERF_SetRegion");
		p[7] = GetProcAddress(D3D9Dll,"DebugSetLevel");
		p[8] = GetProcAddress(D3D9Dll,"DebugSetMute");
		p[9] = GetProcAddress(D3D9Dll,"Direct3DCreate9");
		p[10] = GetProcAddress(D3D9Dll,"Direct3DCreate9Ex");
		p[11] = GetProcAddress(D3D9Dll,"Direct3DShaderValidatorCreate9");
		p[12] = GetProcAddress(D3D9Dll,"PSGPError");
		p[13] = GetProcAddress(D3D9Dll,"PSGPSampleTexture");


	}
	else if (reason == DLL_PROCESS_DETACH)
	{
		FreeLibrary(D3D9Dll);
	}

	return 1;
}


// D3DPERF_BeginEvent
extern "C" __declspec(naked) void __stdcall __E__0__()
	{
	__asm
		{
		jmp p[0];
		}
	}

// D3DPERF_EndEvent
extern "C" __declspec(naked) void __stdcall __E__1__()
	{
	__asm
		{
		jmp p[1*FARPROCSIZE];
		}
	}

// D3DPERF_GetStatus
extern "C" __declspec(naked) void __stdcall __E__2__()
	{
	__asm
		{
		jmp p[2*FARPROCSIZE];
		}
	}

// D3DPERF_QueryRepeatFrame
extern "C" __declspec(naked) void __stdcall __E__3__()
	{
	__asm
		{
		jmp p[3*FARPROCSIZE];
		}
	}

// D3DPERF_SetMarker
extern "C" __declspec(naked) void __stdcall __E__4__()
	{
	__asm
		{
		jmp p[4*FARPROCSIZE];
		}
	}

// D3DPERF_SetOptions
extern "C" __declspec(naked) void __stdcall __E__5__()
	{
	__asm
		{
		jmp p[5*FARPROCSIZE];
		}
	}

// D3DPERF_SetRegion
extern "C" __declspec(naked) void __stdcall __E__6__()
	{
	__asm
		{
		jmp p[6*FARPROCSIZE];
		}
	}

// DebugSetLevel
extern "C" __declspec(naked) void __stdcall __E__7__()
	{
	__asm
		{
		jmp p[7*FARPROCSIZE];
		}
	}

// DebugSetMute
extern "C" __declspec(naked) void __stdcall __E__8__()
	{
	__asm
		{
		jmp p[8*FARPROCSIZE];
		}
	}

// Direct3DCreate9
extern "C" IDirect3D9* __stdcall __E__9__(UINT SDKVersion)
{
	Hooks.RunPlugins();
	Hooks.InstallHooks();


	typedef IDirect3D9* (__stdcall *D3DCreate9Ptr)(UINT);
	D3DCreate9Ptr OriginalD3DCreate9= (D3DCreate9Ptr)(p[9]);

	//we do not create the minimap's window here because if the minimap is
	//the first window of the game, the steam-in-game will display on the minimap
	//instead of the game's main window. That would cause several glitches in this overlay.
	if (OverlayEnabled()				||
		GlobalSettings::MinimapEnabled	||
		GlobalSettings::WireframeMode	||
		GlobalSettings::HandleCursorWithoutDirectX)
	{
		IDirect3D9* ReturnValue=new IDirect3D9_Wrapper(OriginalD3DCreate9(SDKVersion));
		return ReturnValue;
	}
	else
	{
		return OriginalD3DCreate9(SDKVersion);
	}
}

// Direct3DCreate9Ex
extern "C" __declspec(naked) void __stdcall __E__10__()
	{
	__asm
		{
		jmp p[10*FARPROCSIZE];
		}
	}

// Direct3DShaderValidatorCreate9
extern "C" __declspec(naked) void __stdcall __E__11__()
	{
	__asm
		{
		jmp p[11*FARPROCSIZE];
		}
	}

// PSGPError
extern "C" __declspec(naked) void __stdcall __E__12__()
	{
	__asm
		{
		jmp p[12*FARPROCSIZE];
		}
	}

// PSGPSampleTexture
extern "C" __declspec(naked) void __stdcall __E__13__()
	{
	__asm
		{
		jmp p[13*FARPROCSIZE];
		}
	}

