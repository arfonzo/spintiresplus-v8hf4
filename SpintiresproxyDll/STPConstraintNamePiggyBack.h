/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "piggyback.h"

class STPConstraintNamePiggyBack
{
public:
	STPConstraintNamePiggyBack(void);
	~STPConstraintNamePiggyBack(void);

	void InitFromChars(const char* Buffer);

	void SaveInString(std::string& String);

	void SetController(int Index) {mController=Index;}
	void SetSpeed(float Speed) {mSpeed=Speed;}
	void SetResetSpeed(float Speed) {mResetSpeed=Speed;}
	void SetResetPosition(float ResetPosition) {mResetPosition=ResetPosition;}
	void SetAutoResetMode(bool Enabled) {mAutoReset=Enabled;}

	int GetController() const {return mController;}
	float GetSpeed() const {return mSpeed;}
	float GetResetSpeed() const {return mResetSpeed;}
	float GetResetPosition() const {return mResetPosition;}
	bool AutoResets() const {return mAutoReset;}

	bool IsValid() const {return mValid;}
private:
	bool mValid;

	uint mController; //index of
	float mSpeed;
	float mResetSpeed;
	float mResetPosition;	//when resetting, go to this position
	bool mAutoReset; //does the constraint reset when the truck moves?
};
