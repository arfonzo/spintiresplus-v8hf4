/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define _USE_MATH_DEFINES
#include <math.h>

#include "TruckParserHooks.h"
#include "GameStructures.h"
#include "Hooks.h"
#include "TruckChassis.h"

extern void* (__stdcall* ShaftConstructor)(void* DestBuffer,LoadedMesh* Mesh);
extern void (__stdcall* OriginalCreateAShaft)(void* Pram1); //takes an additional parameter in edi

void* __stdcall LoadShaftModel_Internal(void* DestBuffer,LoadedMesh* Mesh, void* XMLTree, void** MeshLoaderParam)
{
	char* ShaftMesh=GetXMLString(XMLTree,"Mesh");
	if (ShaftMesh)
	{
		LoadedMesh* AltMesh=LoadMesh(*MeshLoaderParam,ShaftMesh);	//adds 2 refs to AltMesh on creation
																	//or just 1 on reload
		if (AltMesh)
		{
			void* ReturnValue=ShaftConstructor(DestBuffer,AltMesh);
			AltMesh->ReferenceCount--;	//refcount starts at 2, ShaftConstructor adds 1,unloading occurs at 0
										//LoadMesh adds 1 when the meahs has already been loaded
										//and we want to increase refcount by 1 only at every call
			return ReturnValue;
		}
	}
	return ShaftConstructor(DestBuffer,Mesh);
}


__declspec(naked) void* __stdcall LoadShaftModel(void* DestBuffer,LoadedMesh* Mesh)
{
	__asm
	{
		push dword ptr [esp+0x78 +4 +4*2]	//+78: original +4: call +4*2:2 args were pushed before

		lea eax,[esp+0x20 +4 +4*3]	//same format
		push eax					//XMLTree 

		push [esp+0x10]				//Mesh
		push [esp+0x10]				//DestBuffer
		call LoadShaftModel_Internal
		ret 8
	}
}
///////////////////////////////////// orientable shafts
//transforms BoneTransform so that the Z vector points along ShaftAxis.
//The 2 other vectors of the bone frame will be random, orthogonals and normalized
//ShaftAxis must be normalized
void BuildShaftEndpointTranform(const Vector4& ShaftAxis,D3DXMATRIX& BoneTransform)
{
	Vector4 RandomAxis(1.0f, 0.0f, 0.0f); //use X vector by defaut

	//if ShaftAxis is already along Y, use RandomAxis=Z instead
	if (ShaftAxis.Y == 0.0f && ShaftAxis.Z == 0.0f)
	{
		RandomAxis.X=0.0f;
		RandomAxis.Y=0.0f;
		RandomAxis.Z=1.0f;
	}

	Vector4 BaseVector2;
	VectorProduct(RandomAxis,ShaftAxis,BaseVector2);
	BaseVector2.Normalize3D();

	Vector4 BaseVector3;
	VectorProduct(BaseVector2,ShaftAxis,BaseVector3);

	memset(&BoneTransform,0,sizeof(BoneTransform));


	BoneTransform(0,0)=BaseVector3.X;
	BoneTransform(0,1)=BaseVector3.Y;
	BoneTransform(0,2)=BaseVector3.Z;

	BoneTransform(1,0)=BaseVector2.X;
	BoneTransform(1,1)=BaseVector2.Y;
	BoneTransform(1,2)=BaseVector2.Z;

	BoneTransform(2,0)=ShaftAxis.X;
	BoneTransform(2,1)=ShaftAxis.Y;
	BoneTransform(2,2)=ShaftAxis.Z;

	BoneTransform(3,3)=1.0f;

	//const D3DXVECTOR3 Origin(0,0,0);
	//const D3DXVECTOR3 Axis(ShaftAxis.X,ShaftAxis.Y,ShaftAxis.Z);
	//if (ShaftAxis.X==0 && ShaftAxis.Z==0)
	//{
	//	const D3DXVECTOR3 OtherVector(1,0,0);
	//	D3DXMatrixLookAtRH(&BoneTransform,&Origin,&Axis,&OtherVector);
	//}
	//else
	//{
	//	const D3DXVECTOR3 OtherVector(0,1,0);
	//	D3DXMatrixLookAtRH(&BoneTransform,&Origin,&Axis,&OtherVector);
	//}
	//for (uint i=0;i<4;++i)
	//{
	//	float Temp;
	//	Temp=BoneTransform(0,i);
	//	BoneTransform(0,i)=BoneTransform(1,i);
	//	BoneTransform(1,i)=BoneTransform(2,i);
	//	BoneTransform(2,i)=Temp;
	//}

	//D3DXMatrixTranspose(&BoneTransform,&BoneTransform);
/*	Vector4 RandomAxis(1.0f, 0.0f, 0.0f); //use X vector by defaut

	//if ShaftAxis is already along Y, use RandomAxis=Z instead
	if (ShaftAxis.Y == 0.0f && ShaftAxis.Z == 0.0f)
	{
		RandomAxis.X=0.0f;
		RandomAxis.Z=1.0f;
	}

	Vector4 BaseVector2;
	VectorProduct(RandomAxis,ShaftAxis,BaseVector2);
	BaseVector2.Normalize3D();

	Vector4 BaseVector3;
	VectorProduct(ShaftAxis,BaseVector2,BaseVector3);

	memset(&BoneTransform,0,sizeof(BoneTransform));

	//swap BoneTransform's 1st and 3rd *-1 column to perfrom a 90deg rotation around Y
	BoneTransform(0,0)=-BaseVector3.X;
	BoneTransform(1,0)=-BaseVector3.Y;
	BoneTransform(2,0)=-BaseVector3.Z;

	BoneTransform(0,1)=BaseVector2.X;
	BoneTransform(1,1)=BaseVector2.Y;
	BoneTransform(2,1)=BaseVector2.Z;

	BoneTransform(0,2)=ShaftAxis.X;
	BoneTransform(1,2)=ShaftAxis.Y;
	BoneTransform(2,2)=ShaftAxis.Z;

	BoneTransform(3,3)=1.0f;*/

/*
	//same code, but more readable and slower
	BoneTransform(0,0)=ShaftAxis.X;
	BoneTransform(1,0)=ShaftAxis.Y;
	BoneTransform(2,0)=ShaftAxis.Z;

	BoneTransform(0,1)=BaseVector2.X;
	BoneTransform(1,1)=BaseVector2.Y;
	BoneTransform(2,1)=BaseVector2.Z;

	BoneTransform(0,2)=BaseVector3.X;
	BoneTransform(1,2)=BaseVector3.Y;
	BoneTransform(2,2)=BaseVector3.Z;

	BoneTransform(3,3)=1.0f;

	D3DXMATRIX Transform;
	D3DXMatrixRotationY(&Transform,-M_PI_2);

	//BoneTransform=Transform;
	D3DXMatrixMultiply(&BoneTransform,&BoneTransform,&Transform);
	*/
	
}

//not sure this function actually creates a shaft, maybe it's anything else
//but it's a good point to set the shaft's endpoint transformation
void __stdcall CreateAShaft_Internal(void* XMLTree,ModelFrame* Bones[4],void* EDIParam,void* Param1)
{
	//Apparently the Bones[*] transforms have a slight shearing (<1e-6 on the 0,3 component and >-1e-6 on 3,0 )
	//which messes up the identity matrix detection, so we'll reset the matrices we use.
	//I don't think it has any harmful impact since those matrices do not seem to be used anyway.
	D3DXMatrixIdentity(&(Bones[1]->Transform));
	D3DXMatrixIdentity(&(Bones[2]->Transform));

	Vector4 SocketAAxis,SocketBAxis;
	if (GetXMLFloat3(XMLTree,"SocketAOrientation",SocketAAxis.X,SocketAAxis.Y,SocketAAxis.Z))
	{
		SocketAAxis.Normalize3D();
		BuildShaftEndpointTranform(SocketAAxis,Bones[1]->Transform);
	}
	if (GetXMLFloat3(XMLTree,"SocketBOrientation",SocketBAxis.X,SocketBAxis.Y,SocketBAxis.Z))
	{
		SocketBAxis.Normalize3D();
		BuildShaftEndpointTranform(SocketBAxis,Bones[2]->Transform);
	}


	//const Address OriginalFunction=0x04B14F0;
	__asm
	{
		push dword ptr [Param1]
		mov edi, dword ptr [EDIParam]
		call [OriginalCreateAShaft]
	}
}

__declspec(naked) void __stdcall CreateAShaft(void* Param)
{
	__asm
	{
		push dword ptr [esp+4] //our first argument
		push edi

		lea eax, [esp+0x154+4+4*2+4] //array of 4 pointers |154:original 4:offset 4*2:2 pushes before 4:1 arg pushed before caling us
		push eax

		lea eax,[esp+0x20+4+4*3+4] //20:original offet 4:call 4*3:pushed 3 args 4:1 arg pushed before caling us
		push eax

		call CreateAShaft_Internal

		ret 4
	}
}

//In the normal game, we should apply an Y rotation to Transforms[0] of the given Angle.
//this will make shafts endpoints face themselves.
//Coincidentally, the 2 4x4 matrices representing each ot the endpoint's transform is stored
//just after Transforms[0] on the stack.
//In ST+ we should apply a transform to those 2 matrices so that they point according to our custom orientation.
D3DXMATRIX* __stdcall RotateShaftAlongY_Internal(D3DXMATRIX Transforms[3],FLOAT Angle,ModelFrame* Bones[4])
{
	
	bool ShaftsCustomOriented=false;
	if (!D3DXMatrixIsIdentity(&(Bones[1]->Transform)) ||
		!D3DXMatrixIsIdentity(&(Bones[2]->Transform)) )
	{
		ShaftsCustomOriented=true;
	}

	if (ShaftsCustomOriented==false)
	{
		return D3DXMatrixRotationY(&(Transforms[0]),Angle); //original code
	}
	else
	{
		//if we use custom orientation, we do not automaticaly flip shaft endpoint anymore
		D3DXMatrixMultiply(&(Transforms[1]),&(Bones[1]->Transform),&(Transforms[1]));
		D3DXMatrixMultiply(&(Transforms[2]),&(Bones[2]->Transform),&(Transforms[2]));

		return D3DXMatrixIdentity(&(Transforms[0]));
	}
}
__declspec(naked) D3DXMATRIX* __stdcall RotateShaftAlongY(D3DXMATRIX *pOut,FLOAT Angle)
{
	__asm
	{
		mov eax,dword ptr [esp+0x14+4+4*2] //14:original 4:call 4*2:2 args pushed before calling us
		lea eax, [eax+8]
		push eax

		push dword ptr [esp+4+4+4]	//our 2nd argument
		push dword ptr [esp+4+4+4]	//our 1st argument
		call RotateShaftAlongY_Internal
		retn 8
	}
}

//here XMLTree point to the TruckData node
//Keep in mint that this hack overwrites 4 bytes (float Chassis->TrailerDetachingForce)
//instead of 1 in the base game (bool Chassis->TrailerCanDetach).
void __stdcall WriteTrailerDetachingForce_Internal(void* XMLTree,TruckChassis* Chassis,bool TrailerCanDetach)
{
	//Chassis->TrailerCanDetach=TrailerCanDetach;	//original code
	if (TrailerCanDetach)
	{
		if (!GetXMLFloat(XMLTree,"TrailerBreakOffThreshold",Chassis->TrailerDetachingForce))
		{	//if GetXMLFloat failed
			Chassis->TrailerDetachingForce=16000.0f; //Pavel's choice
		}

		//Ensures the least significant bit is always set so that the advanced menu still
		//thinks the boolean is true.
		//It'll corrupt user input but not much.
		Chassis->TrailerCanDetach|=1;
		//Note that this results in a "or byte ptr[Chassis->TrailerCanDetach]" and not
		//in a mov opcode. 

	}
	else
	{
		Chassis->TrailerDetachingForce=0.0f;
	}	
}

__declspec(naked) void __stdcall WriteTrailerDetachingForce()
{
	__asm
	{
		push eax
		push ebx
		lea eax, [esp+0x24+4+4*2+4*3+4*2]	//24:original +4:call +4*2:pushed 2 arguments before
											//+4*3: original code has pushed 3 things before our call
											//+4*2: 2 arguments passed to cdecl GetXMLBool that weren't popped yet
		push eax
		call WriteTrailerDetachingForce_Internal
		retn
	}
}

float __stdcall ReadTrailerDetachingForce_Internal(TruckChassis* Chassis)
{
	return Chassis->TrailerDetachingForce;
}

__declspec(naked) float __stdcall ReadTrailerDetachingForce()
{
	//I could have just put "fild dword ptr [ebx+480]", but with a ReadTrailerDetachingForce_Internal
	//my code updates if the offset of TrailerDetachingForce changes within the TruckChassis
	//structure/class.
	__asm
	{
		pushad
		push ebx
		call ReadTrailerDetachingForce_Internal
		popad
		retn
	}
}