/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>
#include <string>
#include "typedefs.h"

class STPAddonOverride
{
public:
	STPAddonOverride(void);
	~STPAddonOverride(void);

	bool IsValid() const {return mValid;}

	void SetTrailer(const std::string& Trailer);
	void SetAddons(const std::vector<std::string>& AddonList);
	void SetWheelset(int Wheelset);
	//sets both trailer and addons
	void SetAll(const std::string& Trailer, const std::vector<std::string>& AddonList,uint Wheelset);

	const std::string& GetTrailer() const {return mTrailer;}
	const std::vector<std::string>& GetAddonList() const {return mAddons;}
	uint GetWheelset() const {return mWheelset;}

private:
	bool mValid;
	std::vector<std::string> mAddons;
	std::string mTrailer;
	uint mWheelset;
};
