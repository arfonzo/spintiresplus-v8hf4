#include <D3DX9.h>
#include <d3d9.h>

#include "Settings.h"
#include "BugFixes.h"

//ID of the first entry in the "cursor groups" category
#define GAME_CURSOR_RESOURCE_ID 104

/////////////////////////////////// No cursor on high DPI settings
HCURSOR _stdcall DPIIndependentLoadCursor( HINSTANCE hInstance, LPCWSTR lpCursorName)
{
	return reinterpret_cast<HCURSOR>(LoadImageW(hInstance,lpCursorName,IMAGE_CURSOR,GlobalSettings::DPIIndependentCursorSize,GlobalSettings::DPIIndependentCursorSize,LR_DEFAULTCOLOR | LR_SHARED));
}

/////////////////////////////////// Using windows cursor
HCURSOR WINAPI SetCursorReplacement(HCURSOR hCursor)
{
	//we want to preent the game from replacing the cursor by an empty (null?) one
	static HANDLE Cursor;
	if (Cursor==NULL)
	{
		int CursorSize=32;
		if (GlobalSettings::DPIIndependentCursorSize > 0)
		{
			CursorSize=GlobalSettings::DPIIndependentCursorSize;
		}
		Cursor=LoadImage(GetModuleHandle(NULL),
			MAKEINTRESOURCE(GAME_CURSOR_RESOURCE_ID),
			IMAGE_CURSOR,
			CursorSize,
			CursorSize,LR_DEFAULTCOLOR | LR_SHARED);
	}

	return SetCursor((HCURSOR)Cursor);
}
/////////////////////////////////// Invisible/sunken mud
//this function should simply copy Source texture into dest testure
int __stdcall CopyTextureHook_Internal(const IDirect3DTexture9** Source,IDirect3DTexture9** Dest)
{

	//access texture surface
	IDirect3DSurface9* SourceSurface;
	IDirect3DSurface9* DestSurface;
	const_cast<IDirect3DTexture9*>(*Source)->GetSurfaceLevel(0,&SourceSurface);		
	(*Dest)->GetSurfaceLevel(0,&DestSurface);

	//copy surface
	D3DXLoadSurfaceFromSurface(DestSurface,NULL,NULL,SourceSurface,NULL,NULL,D3DX_FILTER_NONE,0xFF000000);

	//refcount cleaning
	SourceSurface->Release();
	DestSurface->Release();

	return -1;
}

__declspec(naked) int CopyTextureHook()
{
	__asm
	{
		push ebp
		add edi,0xe4
		push edi
		call CopyTextureHook_Internal
		ret
	}
}