/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
//gotta rename this class since it contains much more than just the controls

struct TruckChassis;

class TruckControls //game class, DO NO ADD DATA MEMBERS
{
public:
	byte Unknown0[0x18];
	TruckChassis* Chassis;
	//offset 0x1C
	float SteeringRatio; //-1.0:steering right +1 steering left
	float AccelRatio;	 //-1.0:pedal to metal +1 slamming brakes
	float ThrottleRatio; //+1:fully engaged gear 0: close to neutral
	//offset 0x28
	bool HandbrakeOn;
	bool DiffLocked;
	bool AWDEngaged;
	bool AutoGearboxEngaged;
	bool Unknown1;

	////////////SpintiresPlus Data
	byte mStallingCount;
	unsigned short mLastEngineRPM;
	///////////End SpintiresPlus Data

	float StuckTimer; //if it's >0 the truck cannot move
	//offset 0x34
	union
	{
		float MaxDeltaAngVel;//not checked
		float STPFuelConsumptionFactor;
	};
	float MaxTorque;
	//offset 0x3c
	std::vector<float> GearBox; //array base at  offset 0x48

	//offset0x54
	int SelectedGear;

	std::vector<int> AxleMapping; // AxleMapping[WheelIndex]=axle index
	//offset 0x70
	float Unknown3;	//don't who what it is, but it's a float
	//offset74:
	float GearHeadroom; //1 when you just engaged gear and engine is in low rpm
						//0 when you're at the max speed this ratio can give
						//and fuel consumption is multiplied by this.
	//offset 0x78
	float StallRatio;	//goes from 0 to 1.0 as the engine stalls
	float ShakeTimer;	//used to shake your vehicle if you are stalling
	//offset 0x80
	float AlwaysOne;
	float Unknown6;		//always 1, related to stalling, probably a discarded feature
	float EngineSilenceTimer;	//for how many seconds to silence the engine when shifting
	//offset 0x8C
	float ShiftTimer;	//time to ensure 1.5s delay between each gear shift
						//you can shift when it goes <0
	uint PreviousGear;


public:
	bool IsReversing()const;
	bool IsInNeutral() const;
	bool IsInHighGear() const;
	bool JustShifted() const; //true if we just changed gear
	bool IsAccelerating() const; //true if we are pressing the accel pedal
								 //not directly related to truck/wheel velocity
	//returns true if this average wheel angvel
	//results in low engine RPM
	bool IsRPMLow(float AverageWheelAngVel) const;

	void ShakeTruck(bool Backward);

	float GetCurrentGearRatio() const;
	float GetMaxSpeed() const;
	float GetEngineRPM(float AverageWheelAngVel) const;
	float GetEngineTorque(float EngineRPM) const;
	float GetTorqueForAngvel(float Angvel,float& MaxAngvel);
	float GetFuelConsumptionFactor() const;

	void UpdateStallRatio(bool IsLackingTorque,float AverageWheelAngvel,float FrameTime);
	void PZ_GetGearRatioData(float& MinAngvel,float& MaxAngvel) const;
};