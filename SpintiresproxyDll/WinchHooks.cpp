/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "WinchHooks.h"
#include "GameStructures.h"
#include "settings.h"
#include "Common.h"
#include "TruckChassis.h"

WinchHooks* WinchHooks::Get()
{
	static WinchHooks Instance;
	return &Instance;
}

void WinchHooks::Init(Address CursorEnabledCase, Address CursorDisabledCase, Address RemoveWinchCase)
{
	mCursorEnabledCase=CursorEnabledCase;
	mCursorDisabledCase=CursorDisabledCase;
	mRemoveWinchCase=RemoveWinchCase;
}

//This function returns the address of the next instruction that should be excetuted after the game checks
//if the cursor is visible or not.
//Normally when CursorInvisible=true the game executes mCursorDisabledCase
//or mCursorEnabledCase if CursorInvisible=false
//this function forces executing mRemoveWinchCase when ReleaseWinchKey is down
Address __stdcall WinchHooks::MustReleaseWinch_Internal(bool CursorInvisible, HUDData* HUD)
{
	if (HUD && HUD->ActiveTruck && HUD->ActiveTruck->IsWinched())
	{
		if ((GetKeyState(GlobalSettings::ReleaseWinchKey) & 0x8000))
		{
			return WinchHooks::Get()->mRemoveWinchCase;
		}
	}


	if (CursorInvisible)
	{
		return  WinchHooks::Get()->mCursorDisabledCase;
	}
	else
	{
		return  WinchHooks::Get()->mCursorEnabledCase;
	}
}

_declspec(naked) void WinchHooks::MustReleaseWinch(void)
{
	__asm
	{
		//save the 2 registers that MustReleaseWinch_Internal will not restore
		push ecx
		push edx

		push dword ptr [ebp+8]

		sete al
		push eax			//bool IsCursorInvisible
		call MustReleaseWinch_Internal
		
		//restore ecx,edx
		pop edx
		pop ecx

		jmp eax
	}
}
