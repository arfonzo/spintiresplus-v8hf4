/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "Signature.h"
#include "Typedefs.h"
#include <list>
#include <RapidXml\RapidXml.hpp>
using namespace rapidxml;
#include "STPListCategory.h"


class Hooker
{
public:
	Hooker();
	~Hooker();

	void InstallHooks();
	void RunPlugins();
private:
	enum RegisterCodes
	{
		EAX=0,
		AX=0,
		AL=0,

		ECX=1,
		CX=1,
		CL=1,

		EDX=2,
		DX=2,
		DL=2,

		EBX=3,
		BX=3,
		BL=3,

		ESP=4,
		SP=4,
		AH=4,

		EBP=5,
		BP=5,
		CH=5,

		ESI=6,
		SI=6,
		DH=6,

		EDI=7,
		DI=7,
		BH=7,
	};

	void EnableConsole();
	void ScanForSignatures();
	void InstallEarlyHooks();	//hooks that do not need signatures
								//those get installed at construction time

	void MakePageWritable(Address Where,size_t Length) const;
	void WriteJump(Address Source,Address Destination) const;
	void WriteCall(Address Source,Address Destination,uint NopCount) const;
	Address GetCallJumpDestination(Address OpcodeBegin) const;
	Address* GetImportRef(const char* ModuleName,const char* ProcName) const;
	void WriteTest(Address Where, RegisterCodes Register, char Offset, unsigned char Mask) const;

	void HandleTime64Bomb(Address& i);
	void HandleSystemTimebomb(Address& i);
	void DisarmExplosiveBundle(Address& i,const Address FoundAt);
	Address ScanForPrevious(Address From,Signature& Pattern,uint MaxDistance) const;
	Address ScanForNext(Address From,Signature& Pattern,uint MaxDistance) const;

	void ParseConfig_3rdPartyCompatibility(xml_node<>* ConfigNode);
	void ParseConfig_BugFixes(xml_node<>* ConfigNode);
	void ParseConfig_Camera(xml_node<>* ConfigNode);
	void ParseConfig_ExternalMinimap(xml_node<>* ConfigNode);
	void ParseConfig_Gameplay(xml_node<>* ConfigNode);
	void ParseConfig_Graphics(xml_node<>* ConfigNode);
	void ParseConfig_HUD(xml_node<>* ConfigNode);
	void ParseConfig_Misc(xml_node<>* ConfigNode);
	void ParseConfig_Modding(xml_node<>* ConfigNode);
	void ParseConfig_NewGame(xml_node<>* ConfigNode);
	void ParseConfig_TimeCycle(xml_node<>* ConfigNode);
	void ParseConfig_WheelmassBug(xml_node<>* ConfigNode);
	void ParseConfig_Winch(xml_node<>* ConfigNode);

	//CategoryName is the name of the xml node(s) to parse.
	//They must all be direct children of RootNode
	void ParseSTPListCategories(xml_node<>* RootNode,const char* CategoryName, STPListCategories* GroupList);

	
private:
	bool HooksInstalled;
	Address mExeBase;
	Address mImageSize;

	Signature* UniqueSignatures;
	Signature* TimebombSignatures;

	std::list<HMODULE> Plugins;

};