#include "STPStartupAddonManager.h"

STPStartupAddonManager::STPStartupAddonManager(void)
{
}

STPStartupAddonManager::~STPStartupAddonManager(void)
{
}

STPStartupAddonManager* STPStartupAddonManager::Get()
{
	static STPStartupAddonManager Instance;
	return &Instance;
}

void STPStartupAddonManager::SetOverride(uint Index, const std::vector<std::string> &Addons, const std::string &Trailer,uint Wheelset)
{
	//make sure the override vector has enough room
	if (Index+1>mOverrides.size())
	{
		mOverrides.resize(Index+1);
	}
	mOverrides[Index].SetAll(Trailer,Addons,Wheelset);
}

void STPStartupAddonManager::Clear()
{
	mOverrides.clear();
}

void STPStartupAddonManager::ApplyOverride(uint Index, std::vector<std::string>& Addons,std::string& Trailer,uint& Wheelset) const
{
	if (Index>=mOverrides.size())
	{	//out of bounds -> we don't have an override for this truck;
		return;
	}

	const STPAddonOverride& OverrideForThisTruck=mOverrides[Index];
	if (! OverrideForThisTruck.IsValid())
	{	//invalid override -> nothing to change
		return;
	}
	else
	{
		Addons=OverrideForThisTruck.GetAddonList();
		Trailer=OverrideForThisTruck.GetTrailer();
		Wheelset=OverrideForThisTruck.GetWheelset();
	}
}