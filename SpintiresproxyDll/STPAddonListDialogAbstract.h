/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "stpdialog.h"
#include <string>
#include <list>
#include "Typedefs.h"

class AddonDefinitionFile;

class STPAddonListDialogAbstract :
	public STPDialog
{
public:
	STPAddonListDialogAbstract(void);
	~STPAddonListDialogAbstract(void);
	
	void Show();
	virtual void Restore();
	virtual void OnQuit()=0;

	virtual void OnChangeSearchFilter(const std::string* Filter);
	const std::string& GetSearchFilter() const {return mSearchFilter;};

protected:
	virtual bool IsAddonInstalled(const std::string& AddonFilename) const=0;

	//should return true if the addon is to be listed in this menu
	virtual bool BelongsToList(const AddonDefinitionFile* Addon) const ;

	//sweeps the list of addons the game knows and
	//adds them to the list if this->BelongsToList()
	//returns true.
	void PopulateList();

	//Override to sort mAddonNames.
	//the default implementation sorts alphabetically
	virtual void SortList();

	//before calling InitializeCheckBoxStates
	//make sure the mAddonNames array has been filled.
	void InitializeCheckBoxStates();
protected:
	//Must be unique or AntTweakbars will see duplicate bars
	//and refuse to create the second
	std::string mUniqueIdentifier;
	std::list<StringPair> mAddonNames;
	bool* mCheckedAddons;

private:
	void FilterDisplay();
	std::string mSearchFilter;
};
