/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "STPAddonListDialogAbstract.h"
#include <vector>


class STPAddonListDialog :
	public STPAddonListDialogAbstract
{
public:
	STPAddonListDialog();
	//truck identifier is just a random number that will be added
	//to this menu's name to make it unique.
	void Init(std::vector<std::string>& AddonsOnThisTruck);

	virtual void OnQuit();

protected:
	virtual bool IsAddonInstalled(const std::string& AddonFilename) const;
	virtual bool BelongsToList(const AddonDefinitionFile* Addon) const ;
protected:
	std::vector<std::string>* mInstalledAddons;

};
