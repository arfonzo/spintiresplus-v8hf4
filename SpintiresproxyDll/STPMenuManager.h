/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <d3d9.h>
#include <AntTweakBar.h>
#include <vector>

//forward declarations
class STPDialog;

class STPMenuManager //singleton
{
public:
	static STPMenuManager* Get();
	void Init(HWND Window,IDirect3DDevice9* D3DDevice);

	//drawn the menu, you must check IsVisible()
	//before calling (or not) this.
	//Returns true if an error happened
	bool Draw();
	void OnLostFocus();
	void OnGainedFocus();
	void OnDeviceLost();
	void OnDeviceReset();
	void RenderingLoop();
	void StopRenderingLoop();
	void Destroy();

	void RegisterDialog(const STPDialog* Dialog);
	void UnregisterDialog(const STPDialog* Dialog);

	WNDPROC GetGameWindowProc() const;
	bool IsVisible() const {return mMenuVisible;}

	IDirect3DDevice9* GetD3DDevice() const {return mD3DDevice;}

	//actually return the size of the client area
	int GetScreenWidth() const;
	int GetScreenHeight() const;
private:
	STPMenuManager(void);
	~STPMenuManager(void);

private:
	HWND mWindow;
	WNDPROC mOriginalWindowProc;
	IDirect3DDevice9* mD3DDevice;

	bool mMenuVisible;
	

	//list of all dialogs to save/restore
	//when losing/regaining d3ddevice
	std::vector<STPDialog*> mDialogs;
};
