/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "AssetFile.h"
#include "GameFunctions.h"

AssetFile::AssetFile(void)
{
}

AssetFile::~AssetFile(void)
{
}


void AssetFile::GetFileName(std::string &Output) const
{
	size_t LastDot=mFilePath.find_last_of('.');
	if (LastDot==std::string::npos)
	{
		LastDot=mFilePath.length();
	}

	size_t LastSlash=mFilePath.find_last_of("\\/",LastDot);
	if (LastSlash==std::string::npos)
	{
		LastSlash=0;
	}
	Output=mFilePath.substr(LastSlash+1,LastDot-LastSlash-1);
}


struct WorkshopMod //size 0x50
{
	__int64 ItemID;
	bool UnknownBool; 
	byte Padding[3]; //actually belong to the bool

	byte Unknown0[0x8];
	std::string ModName;	//offset 14
	uint Unknown1; //wild guess: size of an array
	byte Unknown2[0x1C];
};


//Un-mangles a filename.xml and returns the corresponding human readable name from modstrings.
void AssetFile::GetFriendlyName(std::string &Output, const std::string& FileName)
{
	std::wstring Name;
	
	GetFriendlyName_Real(Name,FileName.c_str(),"assets_",0);

	Output.resize(Name.length(),'a');
	//say what you want about std::codecvt, this is simpler.
	wcstombs(const_cast<char*>(Output.c_str()),Name.c_str(),Output.capacity());
}


//returns the WorkshopMod this addon comes from
const std::string* AssetFile::GetWorkshopItem(const std::string& FileName)
{
	WorkshopMod* SourceMod=GetWorkshopItem_Real(FileName.c_str());
	//In some cases (which?) SourceMod can point to a valid WorkshopMod, but
	//the string isn't initialized.
	//2fast4steam? 
	if (SourceMod)
	{
		return &(SourceMod->ModName);
	}
	else
	{
		return NULL;
	}
}

const std::string* AssetFile::GetWorkshopItem() const
{
	std::string Filename;
	GetFileName(Filename);
	return GetWorkshopItem(Filename);
}

void AssetFile::GetFriendlyName(std::string &Output) const
{
	std::string FileName;
	GetFileName(FileName);

	GetFriendlyName(Output,FileName);
}