/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "stplistdialogwithpreview.h"

class STPMapSelectionDialog :
	public STPListDialogWithPreview
{
public:
	STPMapSelectionDialog(void);
	~STPMapSelectionDialog(void);

	//Referer should be the EntryData of the button that triggered the quit event.
	//if should hold which map to select.
	void QuitCallback(const STPListDialogWithPreview::EntryData* Referer);

	virtual void Restore();
	virtual void OnDeviceLost();
	virtual void Render(IDirect3DDevice9* Device);

	virtual void OnHighlightChange(STPListDialogWithPreview::EntryData* Entry);
protected:
	virtual void AddEntryToMenu(const char* VarName,const STPListDialogWithPreview::EntryData& Data,std::string& Label);

	virtual void AddControlsOnTopOfList();
private:
	bool IsMapFile(const char* Filename) const;

	//returns true if this map should not appear in the list of maps
	bool IsExcludedMap(const char* Filename) const;

private:
	std::string mSelectedMap;
	STP2DPlaneDrawer mFloppyIcon;
	bool mPreviewedMapHasSave;
};

int __stdcall DoMapSelection(void* Unknown, const char* OriginalMap);