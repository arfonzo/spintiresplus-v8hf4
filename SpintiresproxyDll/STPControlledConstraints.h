/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <d3dx9math.h>
#include <vector>
#include "typedefs.h"

#define STP_CONSTRAINT_NAME "STPControlledConstraint"
#define STP_CONSTRAINT_NAME_WITH_SEPARATOR STP_CONSTRAINT_NAME"|"

/////////////////////// extra controlled constraints
//...which can be controlled outised the advanced menu

//must use the prototype of D3DXMatrixInverse since that's what we are hooking
D3DXMATRIX* DriveExtraControlledConstraints(D3DXMATRIX *pOut,FLOAT *pDeterminant,const D3DXMATRIX *pM);

struct TruckChassis;
//singleton
class STPConstraintController
{
public:
	~STPConstraintController();

	static STPConstraintController* Get();

public:
	enum KeyState
	{
		NO_KEY_DOWN,
		INCREASE_DOWN,
		DECREASE_DOWN,
		RESET_DOWN,
	};
	//call this at every frame, this will update
	//key states
	void Update(const TruckChassis* Truck);

	void AddController(int IncreaseKey,int DecreaseKey,int ResetKey);
	int GetControllerCount() const {return mControllers.size();}
	KeyState GetControllerState(uint ControllerIndex) const;


	//returns the number of seconds elapsed since the last frame
	float GetFrameTime() const {return mFrameTime;}
	bool IsBackSteering() const {return mBackSteering;}
	//methods
private:
	STPConstraintController();

	//members
private:
	struct SetData
	{
		int IncreaseKey;
		int DecreaseKey;
		int ResetKey;
		KeyState KeyStates;
	};
	std::vector<SetData> mControllers;

	TruckChassis* mLastSeenTruck;
	uint mLastUpdateTime;
	float mFrameTime;
	bool mBackSteering;
};
	