/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "typedefs.h"
///////////////pointers to game functions
extern char* (__cdecl *GetXmlString_Real)(char*);

//used to get the array of all wheels ingame
struct UnknownStructure1;
extern UnknownStructure1** (*GetUnknownStructure1)();

//converts a file.xml mangled name to an human readable one
extern void (__cdecl *GetFriendlyName_Real)(std::wstring&,const char*,const char*,int);

struct TruckChassis;
extern bool (__cdecl *IsAddonInstalled)(TruckChassis* Base,const std::string& AddonName,void* Arg3,uint Arg4);

struct WorkshopMod; 
extern WorkshopMod* (__cdecl *GetWorkshopItem_Real)(const char* );

extern void (__cdecl *GameMainRenderFunc)();