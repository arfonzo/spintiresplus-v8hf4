#pragma once
#include <list>
#include "piggyback.h"

class STPWheelsSetPiggyBack :
	public PiggyBack
{
public:
	STPWheelsSetPiggyBack(void);
	~STPWheelsSetPiggyBack(void);

	void SaveInString(std::string& String);
	virtual void Init();
	
	void SetRequiredAddons(const char* RequiredAddons) {mRequiredAddons=RequiredAddons;}

	//populates AddonList with all requires addons
	//for this wheelset
	void FillAddonList(std::list<std::string>& AddonList);

private:
	std::string mRequiredAddons;
};
