/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>
#include "gamestructures.h"
#include "WheelParams.h"

class Vector4;
class TruckControls;
class STPVehicleTrack
{
public:
	STPVehicleTrack(const std::vector<WheelParams*>& AllWheels,bool RightSideTrack, const TruckControls* Truck,float FrameTime);
	~STPVehicleTrack(void);

	//return the number of wheels in this track
	uint GetCount() const;

	//tells if a track will reverse or brake when
	//steering at low speed
	WheelParams::STPFlags GetSkidSteerMode() const;

	//consumes up to TorqueBudget to move the tracks at TargetTrackSpeed
	//or the closest velocity we can achieve
	float ApplyTorque(float TorqueBudget, float TargetTrackSpeed, float FrameTime);

	//returns how much torque it would cost to run ApplyTorque with this
	//budget and target speed
	float CalculateTorqueRequirement(float TorqueBudget, float TargetTrackSpeed, float FrameTime) const;

	//returns true if this track will spin in the opposite direction
	//in order to steer the vechicle
	bool IsReverseSteering(const float SteeringRatio, bool InLowGear) const;

	float AngVel2TrackSpeed(float Angvel) const;

public: //READ ONLY stats generated for you
		//during construction
	Vector4 mPosition;
	float mTrackSpeed;	//linear speed of the track
	float mAngvel;		//angular speed normalized against radius
	float mTrackLength;
	float mResistiveTorque;
	


private:
	//returns the highest possible track speed using up all the TorqueBudget
	//giver the resistive torque on our wheels.
	//"highest" might be negative depending on the sign of the torque buget
	//return value expeseed in m/s
	float GetmaxAchievableTrackSpeed(float TorqueBudget) const;

	//Lowers target track speed if we don't have enough torque.
	float AdjustTargetTrackSpeed(float TorqueBudget,float InitialTarget) const;

private:
	bool mRightSide;
	std::vector<WheelParams*> mWheels;
	uint mWheelCount;
	uint mVehicleWheelCount;
	float mAverageRadius;

	float mSumInvKR;	//sum of 1/(k*R) of each wheel
						//withK*R=radius * frame time / inertia

	float mSumVs_K;		//sum of angular momentum/frame time of each wheel
};
