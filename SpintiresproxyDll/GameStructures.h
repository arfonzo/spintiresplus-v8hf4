/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <string>
#include <vector>
#include "typedefs.h"
#include "vector.h"

class RigidBody;
class WheelParams;
struct TruckChassis;
class TruckControls;
class Locator;

struct ContactData
{
	//those 4 are probaly a vector4
	Vector4 Position;
	Vector4 Normal;	//likely to be the contact point normal but not 100% sure
					//length is normalized
};

struct Shape //cylinder shape actually
{
	byte Unknown0[0x10];
	float ConvexShapeRadius; //defaults to ConvexShapeDefaultRadius
	float Radius;	//radius defined in the .xml - fRadiusOffset
	byte Unknown1[0x20];
	float HalfWidth;
	
};

struct UnknownStructure2
{
	byte Unknown1[0x30];
	std::vector<WheelParams*> Wheels;
};

struct UnknownStructure1
{
	byte Unknown1[0xC0];
	UnknownStructure2* UnknownS;
};

struct MountedWheel
{
	byte Unknown0[0x138];
	//offset 138
	WheelParams* Wheel;
	float angVelDamped;
	Vector3 linVelVector;
};

struct Addon
{
	byte Unknown0[0x150];
	RigidBody* TruckBody;

	byte Unknown1[0x18C];
	//offset 2e0
	std::string Name;
};

struct WheelType //size: 0x34 bytes
{
	std::string ClassName;	//the filename for this wheel, without the .xml extension.
	std::vector<uint> AppliesToWheels; //indexes of the wheels this wheeltype should apply to
};

struct WheelSet	//size: 0x34 bytes
{

	std::string Title;
	std::vector<WheelType> WheelData;
};

struct TruckState
{
	float SteeringRatio; //-1.0:steering right +1 steering left
	float AccelRatio;	 //-1.0:pedal to metal +1 slamming brakes
	float ThrottleRatio; //+1:fully engaged gear 0: close to neutral
	bool HandbrakeOn;
	bool DiffLocked;
	bool AWDMode;
};

struct DamageInfo	//size 0x80
{
	byte Unknown0[0x44];
	float CurrentHealth;	//or damage
	uint MaxHealth;			//or damage capacity
	Vector3 SensationMin;
	Vector3 SensationMax;
	byte Unknown1[0xC];
	float StallRatio;
	byte Unknown2[0x8];
	float DifflockStress; //at 1.0 the diff breaks
};

struct IKConstraint
{
	byte Unknown0[0x38];

	//offset 0x38
	float Unknown7;
	float Unknown8;
	float Unknown9;
	uint Unknown10;
	//offset 0x48
	enum STPLinkedSteering : byte
	{
		LS_None=0,
		LS_EnginePowered=1,
		LS_Silent=2,
	};
	STPLinkedSteering IsLinkedSteering; //formerly bool IsLinkedSteering;
};

class hkpConstraintInstance;
struct ConstraintBucket
{
	byte Unknown[0x120];
	//offset 120
	std::vector<hkpConstraintInstance*> Constraints;
};


//struct HUDData_UnknownStructure1_Unk1
//{
//	byte Unknown[21];
//	bool IsAdvancedMenuActive;
//};
//
//struct HUDData_UnknownStructure1
//{
//	//offset 1a4
//	HUDData_UnknownStructure1_Unk1* Menu;
//
//
//	//offset 1f8
//	std::vector<NoIdea> Unknown204;
//	byte Unknown10[4];
//	//offset 214
//	std::vector<NoIdea2> Unknown220;
//};

struct HUDData
{
	void* VfTable; //not sure
	TruckChassis* ActiveTruck;
//	byte Unknown0[0x28];
//
//	//offset 0x30
//	void* Unknown;
//	HUDData_UnknownStructure1* UnknownStructure; //UnknownStructure->0ffset 1a4->offset 21 = bool IsAdvancedMenuActive
};

struct ModelManager
{
	byte Unknown0[0x104];
	//offset 0x104
	std::vector<Locator*> Locators; //models on this map
};

struct GameManager
{
	byte Unknown0[0xC];
	ModelManager* Models;
	byte Unknown10[0x4];
	float TimeOfDay;

	//offset 0x27C
	//std::vector<DayTimeState> DayTimeStates; //a DayTimeState probably has a size of 0x64 bytes
};

struct ModMenuTruckReplacement	//exact size: 0x3C
{
	std::string OriginalTruck;	//filename
	std::string NewTruck;		//filename
	uint PointCost;		//Final cost in balance points
};

struct ModMenuData	//ModMenu = the menu where you configure your startup trucks
{
	byte Unknown[0x30];
	//offset 30
	std::vector<short> ModSlotToTruckIndexMap; //ModSlotToTruckIndexMap[Slot]=truck index
	std::vector<ModMenuTruckReplacement> Replacements;	//one entry per slot
	//offset 60
	int CurrentSlot;//slot index of the truck we are currently editting (via truck menu)
					//CurrentSlot can be -1 in some undefine circumstances.
};