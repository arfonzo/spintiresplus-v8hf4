/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <d3d9.h>
#include "STP2DPlaneDrawer.h"
#include "D3DTools.h"

STP2DPlaneDrawer::STP2DPlaneDrawer(void)
:mDevice(NULL)
,mTexture(NULL)
,mTextureOwned(false)
,mVertexBuffer(NULL)
,mX(0)
,mY(0)
,mWidth(100)
,mHeight(100)
{
}

STP2DPlaneDrawer::~STP2DPlaneDrawer(void)
{
	OnDeviceLost();
}

void STP2DPlaneDrawer::SetDevice(IDirect3DDevice9 *Device)
{
	mDevice=Device;
}

void STP2DPlaneDrawer::SetPosSize(int X,int Y,int Width, int Height,bool ApplyNow)
{
	mX=X;
	mY=Y;
	mWidth=Width;
	mHeight=Height;

	if (ApplyNow)
	{
		UpdateVertexBuffer();
	}
}

void STP2DPlaneDrawer::FreeTextureIfNeeded()
{
	if (mTexture && mTextureOwned)
	{
		mTexture->Release();
	}
	mTexture=NULL;
}

void STP2DPlaneDrawer::UpdateVertexBuffer()
{
	if (!mDevice)
	{	//no device -> can't do anything
		return;
	}

	TexturedVertex Plane[4];
	//topleft
	Plane[0].x=static_cast<float>(mX);
	Plane[0].y=static_cast<float>(mY);
	Plane[0].tu=0.0f;
	Plane[0].tv=0.0f;

	//topright
	Plane[1].x=Plane[0].x+mWidth;
	Plane[1].y=Plane[0].y;
	Plane[1].tu=1.0f;
	Plane[1].tv=Plane[0].tv;

	//bottom left
	Plane[2].x=Plane[0].x;
	Plane[2].y=Plane[0].y+mHeight;
	Plane[2].tu=Plane[0].tu;
	Plane[2].tv=1.0f;

	//bottom right
	Plane[3].x=Plane[1].x;
	Plane[3].y=Plane[2].y;
	Plane[3].tu=Plane[1].tu;
	Plane[3].tv=Plane[2].tv;

	if (!mVertexBuffer)
	{	//no vertex buffer -> create it
		//get screen size

		HRESULT hr=mDevice->CreateVertexBuffer(sizeof(Plane),	//Length
									D3DUSAGE_WRITEONLY,		//Usage
									TEXTURED_VERTEX_FORMAT,	//FVF
									D3DPOOL_DEFAULT,		//Pool
									&mVertexBuffer,			//ppVertexBuffer
									NULL);
		if (FAILED(hr))
		{
			LOG("CreateVertexBuffer failed in STP2DPlaneDrawer\n");
		}
	}


	//send updated vertices to GPU
	void* VertexBufferInRAM;
	mVertexBuffer->Lock(0,0,&VertexBufferInRAM,0);
	memcpy(VertexBufferInRAM, &Plane, sizeof(Plane));
	mVertexBuffer->Unlock();
}

void STP2DPlaneDrawer::OnDeviceLost()
{
	FreeTextureIfNeeded();
	if (mVertexBuffer)
	{
		mVertexBuffer->Release();
		mVertexBuffer=NULL;
	}
}

void STP2DPlaneDrawer::OnDeviceReset()
{
	if (mTextureOwned)
	{
		SetTextureFromFile(mTextureFileName);
	}
	UpdateVertexBuffer();
}

void STP2DPlaneDrawer::Render()
{
	if (mDevice && mVertexBuffer && mTexture)
	{
		//Bind our Vertex Buffer
		mDevice->SetStreamSource(0,	mVertexBuffer,0,sizeof(TexturedVertex));
		mDevice->SetFVF(TEXTURED_VERTEX_FORMAT);
		mDevice->SetTexture(0,mTexture);

		//Render from our Vertex Buffer
		mDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
	}
}

void STP2DPlaneDrawer::SetTextureFromFile(const std::string &Filename)
{
	if (Filename==mTextureFileName && mTexture)
	{	//if this texture is already loaded, don't reload it
		return;
	}
	FreeTextureIfNeeded();

	mTextureFileName=Filename;
	mTextureOwned=true;
	if (!Filename.empty())
	{
		mTexture=LoadTextureFromPHYSFS(Filename.c_str(),mDevice,false);
	}
}

void STP2DPlaneDrawer::SetTexture(const IDirect3DTexture9 *Texture)
{
	FreeTextureIfNeeded();
	mTextureOwned=false;
	mTextureFileName.clear();
	mTexture=const_cast<IDirect3DTexture9*>(Texture);
}