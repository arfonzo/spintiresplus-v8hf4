/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "STPTruckNamePiggyBack.h"
#include "Hashing.h"

STPTruckNamePiggyBack::STPTruckNamePiggyBack()
:mSensationOverriden(false)
,mGearboxOverriden(false)
{
}

STPTruckNamePiggyBack::~STPTruckNamePiggyBack(void)
{
}

//if we are here the stream is valid and we'll init our members from it
void STPTruckNamePiggyBack::Init()
{
	//damage sensation
	mDataStream>>mSensationOverriden;
	mDataStream>>mSensationOverrideHash;
	mDataStream>>mOriginalSensationMin;
	mDataStream>>mOriginalSensationMax;

	//torque mods
	uint TorqueModCount=0;
	mDataStream>>TorqueModCount;
	for (uint i=0;i<TorqueModCount;++i)
	{
		TorqueMod Mod;
		mDataStream>>Mod.AddonHash;
		mDataStream>>Mod.TorqueFactor;

		mTorqueMods.push_back(Mod);
	}

	//gearbox override
	mDataStream>>mGearboxOverriden;
	mDataStream>>mGearboxOverrideHash;
	uint StoredGearboxRatios;
	mDataStream>>StoredGearboxRatios;
	for (uint i=0;i<StoredGearboxRatios;++i)
	{
		float Gear;
		mDataStream>>Gear;

		mGearbox.push_back(Gear);
	}
}

void STPTruckNamePiggyBack::SaveInString(std::string& String)
{
	ClearStream();

	//damage sensation
	mDataStream<<mSensationOverriden;
	mDataStream<<mSensationOverrideHash;
	mDataStream<<mOriginalSensationMin;
	mDataStream<<mOriginalSensationMax;

	//torque modifiers
	const uint TorqueModCount=mTorqueMods.size();
	mDataStream<<TorqueModCount ;
	for (uint i=0;i<TorqueModCount;++i)
	{
		TorqueMod& Mod=mTorqueMods[i];
		mDataStream<<Mod.AddonHash;
		mDataStream<<Mod.TorqueFactor;
	}

	//gearbox override
	mDataStream<<mGearboxOverriden;
	mDataStream<<mGearboxOverrideHash;
	const uint StoredGearboxRatios=mGearbox.size();
	mDataStream<<StoredGearboxRatios;
	for (uint i=0;i<StoredGearboxRatios;++i)
	{
		mDataStream<<mGearbox[i];
	}

	PiggyBack::SaveInString(String);
}

bool STPTruckNamePiggyBack::HasSensationOverride()
{
	if (!IsValid())
	{
		return false;
	}

	return mSensationOverriden;
}

bool STPTruckNamePiggyBack::HasGearboxOverride()
{
	if (!IsValid())
	{
		return false;
	}

	return mGearboxOverriden;
}

//saves the original sensation min in the piggyback
//don't forget to call SetSensationOverrideHash or you won't be able to remove
//this piggy
void STPTruckNamePiggyBack::SaveSensationMin(const Vector3& Sensation)
{
	mSensationOverriden=true;
	mOriginalSensationMin=Sensation;
}

void STPTruckNamePiggyBack::SaveSensationMax(const Vector3& Sensation)
{
	mSensationOverriden=true;
	mOriginalSensationMax=Sensation;
}

//save the has of the addon that overwrote sensation min max
void STPTruckNamePiggyBack::SetSensationOverrideHash(const char* AddonName)
{
	mSensationOverriden=true;
	mSensationOverrideHash=FNV1aHash(AddonName,strlen(AddonName));
}

bool STPTruckNamePiggyBack::RemoveSensationOverride(const std::string& AddonName,Vector3& TruckSensationMin,Vector3& TruckSensationMax)
{
	if (HasSensationOverride() && FNV1aHash(AddonName.data(),AddonName.length())==mSensationOverrideHash)
	{
		mSensationOverriden=false;
		TruckSensationMin=mOriginalSensationMin;
		TruckSensationMax=mOriginalSensationMax;
		return true;
	}
	return false;
}

void STPTruckNamePiggyBack::AddTorqueMod(const std::string &AddonName, float TorqueFactor)
{
	TorqueMod Mod;
	Mod.AddonHash=FNV1aHash(AddonName.data(),AddonName.length());
	Mod.TorqueFactor=TorqueFactor;
	mTorqueMods.push_back(Mod);
}

void STPTruckNamePiggyBack::SaveGearbox(const std::string& AddonName,const std::vector<float>& OriginalGearbox)
{
	if (!HasGearboxOverride())
	{
		mGearboxOverrideHash=FNV1aHash(AddonName.data(),AddonName.length());
		mGearbox=OriginalGearbox;
		mGearboxOverriden=true;
	}
}

bool STPTruckNamePiggyBack::RemoveTorqueMod(const std::string &AddonName, float& TruckTorque)
{
	uint RemovedAddonHash=FNV1aHash(AddonName.data(),AddonName.length());

	std::vector<TorqueMod>::iterator End=mTorqueMods.end();
	for (std::vector<TorqueMod>::iterator i=mTorqueMods.begin() ; i!=End ; ++i)
	{	//loop over all elements of mTorqueMods
		TorqueMod& Mod=(*i);
		if (Mod.AddonHash==RemovedAddonHash)
		{
			TruckTorque=Mod.TorqueFactor;
			mTorqueMods.erase(i);
			return true;
		}

	}
	return false;
}

bool STPTruckNamePiggyBack::RemoveGearboxOverride(const std::string &AddonName, std::vector<float>& TruckGearbox)
{
	if (HasGearboxOverride() && FNV1aHash(AddonName.data(),AddonName.length())==mGearboxOverrideHash)
	{
		mGearboxOverriden=false;
		TruckGearbox=mGearbox;
		return true;
	}
	return false;
}