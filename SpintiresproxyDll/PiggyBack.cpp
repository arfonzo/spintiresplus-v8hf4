/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "PiggyBack.h"


//00 1D C0DE in memory
//the least significant byte must always be a 0 because it also serves as null terminator
#define ID_CODE 0xDEC01D00

PiggyBack::PiggyBack()
:mValid(false)
{
}

PiggyBack::~PiggyBack(void)
{
}

//Checks if there is a piggyback after this string
//whenever possible prefer using InitFromStdString
//this check can reliably tell that there is no piggyback
//but cannot tell if it is leftovers in memory or really data attached to this buffer.
//You should give a pointer to the beginning of the string (ie: the text), the function
//wil automatically jump over it.
void PiggyBack::InitFromChars(const char* Buffer)
{
	const uint StringLength=strlen(Buffer);
	const PiggyHeader* Header=reinterpret_cast<const PiggyHeader*>(Buffer+StringLength);
	if (Header->Magic==ID_CODE)
	{
		mValid=true;
		mDataStream.write(Buffer+StringLength+sizeof(PiggyHeader),Header->Size);

		//rewind to beginning in preparation for the next operations (read)
		mDataStream.seekp(0, std::ios_base::beg);
		mDataStream.seekg(0, std::ios_base::beg);

		this->Init();
	}
	else
	{
		mValid=false;
	}
}

bool PiggyBack::InitFromStdString(const std::string& Buffer)
{
	const char* RawBuffer=Buffer.data();
	const uint StringLength=strlen(RawBuffer);
	if (Buffer.length()>=StringLength+sizeof(PiggyHeader))
	{	//see if there is enough room to hold a basic piggy

		const PiggyHeader* Header=reinterpret_cast<const PiggyHeader*>(RawBuffer+StringLength);
		if (Header->Magic==ID_CODE)
		{	//check if ID code is valid

			uint TotalSize=Header->Size+sizeof(PiggyHeader);
			if (Buffer.length()>=StringLength+TotalSize)
			{	//the string is large enough to hold the piggy it pretends to contain

				mValid=true;
				mDataStream.write(Buffer.data()+StringLength+sizeof(PiggyHeader),Header->Size);

				//rewind to beginning in preparation for the next operations (read)
				mDataStream.seekp(0, std::ios_base::beg);
				mDataStream.seekg(0, std::ios_base::beg);
				this->Init();
				return true;
			}
		}
	}

	//too short to hold a piggy or invalid id code
	mValid=false;
	return false;
}

void PiggyBack::SaveInString(std::string& String)
{
	const uint StringLength=strlen(String.data());
	String.resize(StringLength);	//remove any existing piggy

	uint BufferSize=mDataStream.GetSize();

	//reserve room for the payload at the end of the string
	String.append(sizeof(PiggyHeader)+BufferSize,'\0');

	//add the piggy header to the string
	//the first bytes we added at the end of the string are for the header
	char* DataPointer=const_cast<char*>(String.data()+StringLength);
	PiggyHeader* Header=reinterpret_cast<PiggyHeader*>(DataPointer);
	Header->Magic=ID_CODE;
	Header->Size=BufferSize;

	//add the payload, yes it's dirty, but at least it's safe.
	mDataStream.read(DataPointer+sizeof(PiggyHeader),BufferSize);
}

void PiggyBack::ClearStream()
{
	//clear stream
	mDataStream.Empty();
}
	