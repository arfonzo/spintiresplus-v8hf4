/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Common.h"
#include "PHYSFS_minimal.h"

bool SameSign(void* Operand1,void* Operand2,int OperandSize)
{
	char* Op1=static_cast<char*>(Operand1);
	char* Op2=static_cast<char*>(Operand2);
	return (Op1[OperandSize-1]&0x80)==(Op2[OperandSize-1]&0x80);
}

void MountSTPResources()
{
	//append to search path since our resources are not expected (yet) to be used often
	PHYSFS_mount(STP_RESOURCE_DIRECTORY,STP_RESOURCE_DIRECTORY,1);
}