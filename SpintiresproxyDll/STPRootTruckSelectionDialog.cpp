#include "STPRootTruckSelectionDialog.h"
#include "AntTweakBar.h"
#include "STPMenuManager.h"
#include "GameStructures.h" //for ModMenuData
#include "STPStartupAddonManager.h"

//defined in hooker.cpp
extern ModMenuData* (* GetModMenu)(void);
extern void (__stdcall* SaveTruckOverride_Real)(const char*);

//prototype
void __stdcall SaveTruckOverride(ModMenuData* ModMenu,const char* TruckName);
//////////////////////////////
//////////callbacks
//////////////////////////////
void TW_CALL STPRootTruckSelectionDialog_OnOK(void* Params)
{
	static_cast<STPRootTruckSelectionDialog*>(Params)->OnQuit();
}

void TW_CALL STPRootTruckSelectionDialog_OnCancel(void* Params)
{
	static_cast<STPRootTruckSelectionDialog*>(Params)->OnCancel();
}
//////////////////////////////
//////////class definition
//////////////////////////////
STPRootTruckSelectionDialog::STPRootTruckSelectionDialog(void)
{
	mWheelset=0;

	//place self
	uint WindowWidth=STPMenuManager::Get()->GetScreenWidth();
	uint WindowHeight=STPMenuManager::Get()->GetScreenHeight();
	const uint MenuWidth=WindowWidth/3;
	SetPosition(2*MenuWidth, WindowHeight-100);
	SetSize(MenuWidth, 100);
	
	//place sub menus
	mTruckList.SetPosition(0,0);
	mTruckList.SetSize(MenuWidth, WindowHeight/2);

	mAddonDialog.SetPosition(MenuWidth,0);
	mAddonDialog.SetSize(MenuWidth, WindowHeight);

	mTrailerDialog.SetPosition(0,WindowHeight/2);
	mTrailerDialog.SetSize(MenuWidth, WindowHeight/2);

	mAddonDialog.Init(mAddons);
	mTrailerDialog.Init(mTrailer);
}

STPRootTruckSelectionDialog::~STPRootTruckSelectionDialog(void)
{
	if (mMenu)
	{
		TwDeleteBar(mMenu);
		mMenu=NULL;
	}
}

void STPRootTruckSelectionDialog::OnQuit()
{
	//close the addon dialogs, in doing so they will update our
	//mAddons and mTrailer members
	mAddonDialog.OnQuit();
	mTrailerDialog.OnQuit();

	const std::string* SelectedTruck=mTruckList.GetSelectedTruckFileName();
	if (SelectedTruck)
	{	//only save the override if a truck was selected
		//ie: we don't allow modifying only the addons since I don't know how
		//the game would react.
		ModMenuData* ModMenu=GetModMenu();

		if (ModMenu->CurrentSlot!=-1)
		{
			uint TruckIndex=ModMenu->ModSlotToTruckIndexMap[ModMenu->CurrentSlot];
			STPStartupAddonManager::Get()->SetOverride(TruckIndex,mAddons,mTrailer,mWheelset);
		}

		SaveTruckOverride(ModMenu,SelectedTruck->c_str());
	}

	STPMenuManager::Get()->StopRenderingLoop();
}

void STPRootTruckSelectionDialog::OnCancel()
{
	STPMenuManager::Get()->StopRenderingLoop();
}

void STPRootTruckSelectionDialog::Restore()
{
	if (mMenu)
	{
		TwDeleteBar(mMenu);
	}
	mMenu=TwNewBar("Main",NULL);

	STPDialog::Restore(); //restore from parent class

	TwAddVarRW(mMenu,"Wheelset",TW_TYPE_UINT32,&mWheelset,NULL);
	TwAddButton(mMenu,"OK", STPRootTruckSelectionDialog_OnOK, this, NULL);
	TwAddButton(mMenu,"Cancel", STPRootTruckSelectionDialog_OnCancel, this, NULL);

	mAddonDialog.SetVisible(IsVisible());
	mTrailerDialog.SetVisible(IsVisible());
	mTruckList.SetVisible(IsVisible());
}

void STPRootTruckSelectionDialog::DoModal()
{
	mAddonDialog.Restore();
	mTrailerDialog.Restore();
	mTruckList.Restore();
	STPDialog::DoModal();
}


__declspec(naked) void __stdcall SaveTruckOverride(ModMenuData* ModMenu,const char* TruckName)
{
	__asm
	{
		//save esi
		push esi

		//load ModMenu
		//mov esi,dword ptr [ModMenu]
		mov esi, dword ptr [esp+4+4]	//+4:1st argument +4:saved esi

		//push TruckName
		//push [TruckName]
		push dword ptr [esp+8+4]	//+8:2nd argument +4:saved esi
		call [SaveTruckOverride_Real]

		//restore esi
		pop esi
		ret 8
	}
}

bool __stdcall DoTruckSelection(void* Unknown, const char* OriginaTruck)
{
	//__int64 tic,tac,freq;
	//QueryPerformanceCounter ((LARGE_INTEGER*)&tic);

	
	STPRootTruckSelectionDialog Dialog;

	//QueryPerformanceCounter ((LARGE_INTEGER*)&tac);
	//QueryPerformanceFrequency ((LARGE_INTEGER*)&freq);
	//LOG("+++creating the dialog took: %I64dms \n",(tac - tic)*1000/freq);

	Dialog.SetVisible(true);
	Dialog.DoModal();

	delete Unknown; //not sure when this thing should be deallocated actually
	return true;
}