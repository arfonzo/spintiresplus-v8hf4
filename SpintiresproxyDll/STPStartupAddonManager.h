/*
Copyright (c) 2016 Localhost

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include "STPAddonOverride.h"
#include "typedefs.h"

class STPStartupAddonManager
{
public:
	~STPStartupAddonManager(void);
	static STPStartupAddonManager* Get(); //singleton access

	//index is the index of the truck's slot for this map
	//strings are internal filenames of the addons/trailer.
	//NOT friendly names.
	void SetOverride(uint Index,const std::vector<std::string>& Addons,const std::string& Trailer,uint Wheelset);

	//get addon/trainer override data
	void ApplyOverride(uint Index, std::vector<std::string>& Addons,std::string& Trailer,uint& Wheelset) const;

	//deallocates all known overrides
	void Clear();
private:
	STPStartupAddonManager(void);
	std::vector<STPAddonOverride> mOverrides;
};
